<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container">
                <div class="page-404">
                    <!-- End images -->
                    <div class="text center">
                        <div class="icon-box box">
                            <img alt="icon" src="{{asset('assets/site/images/icon-page404.png')}}">
                        </div>
                        <h3>THIS IS NOT THE WEB PAGE YOU ARE LOOKING FOR</h3>
                        <p>Please try one of the following pages <a href="{{route('home')}}" title="link">home page <i
                                    class="fa fa-angle-double-right"></i></a></p>
                        <form action="{{ route('search') }}" method="GET" id="form_signup">
                            <input class="input-text required-entry" type="text" name="s" style="color: #2b2b2b; !important;">
                            <button class="button" title="Search" type="submit">Search</button>
                        </form>
                    </div>
                    <!-- End text -->
                </div>
                <!-- End page404 -->
            </div>
        </div>
    </div>

    <!-- End container -->
    @include('partials.footer')
</div>

</body>
</html>

