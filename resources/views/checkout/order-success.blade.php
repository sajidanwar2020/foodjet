<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    @include('partials.headers-style')
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,300' rel='stylesheet' type='text/css'>
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
         <div class="main-content">
        <!-- End container -->
        <div class="container container-ver2--">
            <div class="box float-left order-complete center space-50">
                <div class="icon space-20">
                    <img src="{{asset('assets/site/images/icon-order-complete.png')}}" alt="icon" style="width: 100px;">
                </div>
                <p class="box center space-50">Thank you for shopping with us, your order is complete!</p>
                <div class="box">
                    <a class="link-v1 lh-50 margin-right-20 space-20 color-brand" href="{{route('home')}}" title="HOME PAGE">HOME PAGE</a>
                    <a class="link-v1 lh-50 rt space-20" href="{{route('customer_orders')}}" title="VIEW ORDER">VIEW ORDER</a>
                </div>
            </div>
        </div>
        <!-- End container -->
    </div>
    </div>
    @include('partials.footer')
</div>
<!-- End wrappage -->
@include('partials.footer-js')
</body>
</html>











