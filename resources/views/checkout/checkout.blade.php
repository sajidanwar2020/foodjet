<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
    <style>
        .payment-order ul.tabs li {padding: 0 !important;position: relative;display: flex;}
        .payment-order ul.tabs li input {width: 22px;margin-bottom: 9px;}
    </style>
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')

    <div class="container content-wrapper">
        <div class="main-content">
            <div class="cart-box-container check-out">
                <div class="container">
                    <div class="row">
                        <form action="{{ route('proceed_check_out') }}" method="post" id="proceed-check-out" class="form-horizontal">
                             <div class="col-md-6">
                                <h3 class="title-brand">BILLING ADDRESS</h3>
                                       {{ csrf_field() }}
                                        <input type="hidden" id="shiping_charges" name="shiping_charges" value="">

                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class=" control-label" for="first_name">First name<span class="color">*</span></label>
                                            <input class="form-control" id="first_name" name="first_name"
                                                   value="{{isset($user_detail['first_name']) ? $user_detail['first_name']: ''}}" readonly type="text">
                                        </div>
                                        <div class="col-md-6">
                                            <label class=" control-label" for="last_name">Last name<span class="color">*</span></label>
                                            <input class="form-control" id="last_name" name="last_name"
                                                   value="{{isset($user_detail['last_name']) ? $user_detail['last_name']: ''}}"
                                                   readonly>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-md-6">
                                            <label class=" control-label" for="email">Email<span class="color">*</span></label>
                                            <input class="form-control" id="email" name="email"
                                                    value="{{isset($user_detail['email']) ? $user_detail['email']: ''}}" readonly type="text">
                                        </div>
                                        <div class="col-md-6">
                                            <label class=" control-label" for="phone">Phone<span class="color">*</span></label>
                                            <input class="form-control" id="phone" name="phone_number"
                                                   value="{{isset($user_detail['phone_number']) ? $user_detail['phone_number']: ''}}"
                                                   readonly>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class=" control-label" for="inputstreet">Adress<span
                                                class="color">*</span></label>
                                        <input type="text" class="form-control" name="location" id="location"
                                               placeholder="Location*"
                                               value="{{isset($user_detail['location']) ? $user_detail['location']: ''}}">
                                        <input type="hidden" name="gmap_latitude" id="gmap_latitude"
                                               value="{{isset($user_detail['latitude']) ? $user_detail['latitude']: ''}}">
                                        <input type="hidden" name="gmap_longitude" id="gmap_longitude"
                                               value="{{isset($user_detail['longitude']) ? $user_detail['longitude']: ''}}">
                                        <input type="hidden" name="distance_km" id="distance_km"
                                               value="{{ old('distance_km') }}">
                                    </div>

                                        <div class="form-group">
                                            <label class=" control-label" for="additional_notes"> Additional notes</label>
                                            <textarea class="form-control" rows="5" id="additional_notes" name="additional_notes"></textarea>
                                        </div>

                                         <?php /* <span class="form-check space-50">Create an account?</span> */ ?>
                                </div>
                             <div class="col-md-6 space-30">
                                <div class="box">
                                    <h3 class="title-brand">YOUR ORDER</h3>
                                    <div class="info-order">
                                        <div class="product-name">
                                            <ul>
                                                <li class="head">
                                                    <span class="name">PRODUCTS NAME</span>
                                                    <span class="qty"><b>QTY</b></span>
                                                    <span class="total"><b>SUB TOTAL</b></span>
                                                </li>
                                                <?php foreach(Cart::content() as $row) : ?>
                                                    <li>
                                                        <span class="name">{{$row->name}} </span>
                                                        <span class="qty">{{$row->qty}} </span>
                                                        <span class="total">€<?php echo $row->total; ?></span>
                                                    </li>
                                                <?php endforeach;?>
                                            </ul>
                                        </div>
                                        <!-- End product-name -->
                                        <ul class="product-order">
                                            <li>
                                                <span class="left">CART SUBTOTAL</span>
                                                <span class="right">€<?php echo Cart::subtotal(); ?></span>
                                            </li>
                                            <li>
                                                <span class="left">SHIPPING & HANDLING</span>
                                                <span class="right shiping_charges_span"></span>
                                            </li>
                                            <li>
                                                <span class="left">ORDER TOTAL</span>
                                                <span class="right brand c_product_price total_price"><b>€<?php echo Cart::total(); ?></b></span>
                                            </li>
                                        </ul>
                                        <p id="order_range_error"></p>
                                    </div>
                                    <!-- End info-order -->
                                    <div class="payment-order box float-left">
                                        <h3 class="title-brand">PAYMENT MENTHOD</h3>
                                        <ul class="tabs">
                                            <?php /*
                                            <li>
                                                <i class="icon"></i>
                                                <h4>Direct Bank Transfer</h4>
                                                <p>Make your payment directly info our bank account. Please use your order
                                                    ID as the
                                                    payment reference. You product won't be shipped untill payment
                                                    confiimation. </p>
                                            </li> */ ?>
                                            <li>
                                                <input type="radio" id="payment_method" name="payment_type" value="1"  checked>
                                                <label for="payment_method">Cash on delivery</label>
                                            </li>
                                            <li>
                                                <input type="radio" id="paypal" name="payment_type" value="2">
                                                <label for="paypal">PayPal</label>
                                            </li>

                                        </ul>
                                    </div>
                                    <input type="submit" value="PLACE ORDER" title="PLACE ORDER" class="link-v1 box lh-50 rt full" id="checkout_btn">
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End row -->
                </div>
                <!-- End container -->
            </div>
            <!-- End cat-box-container -->
        </div>
    </div>
    @include('partials.footer')
</div>
<!-- End wrappage -->
@include('partials.footer-js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCB59pcScbhhVj5SD6-m1is6v0Qq3cGwT4&libraries=geometry,places" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#location").keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });
        $( "#dob" ).datepicker({ dateFormat: 'yy-mm-dd' });
    });


    function initialize() {
        //alert();
        currentLocation();
        calculateDistance();
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();
            var address = place.address_components;
            var address_details = address[address.length - 1];
            document.getElementById('location').value = place.name;
            document.getElementById('gmap_latitude').value = place.geometry.location.lat();
            document.getElementById('gmap_longitude').value = place.geometry.location.lng();
            calculateDistance();

        });
    }

    function currentLocation() {


    }
    function calculateDistance() {
        var gmap_latitude = document.getElementById('gmap_latitude').value;
        var gmap_longitude = document.getElementById('gmap_longitude').value;

        var destination = new google.maps.LatLng(gmap_latitude, gmap_longitude);
        var  src_loc = new google.maps.LatLng('{{DEFAULT_MAP_LAT}}', '{{DEFAULT_MAP_LON}}');
        //  console.log('destination :'+gmap_latitude+'-------'+gmap_longitude);
        //  console.log('src_loc :'+'{{DEFAULT_MAP_LAT}}'+'-------'+'{{DEFAULT_MAP_LON}}');
        var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
            {
                origins: [src_loc],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                avoidHighways: false,
                avoidTolls: false
            }, callback);

        function callback(response, status) {
            if(status == "OK"){
                // console.log(response);
                distance = response.rows[0].elements[0].distance.value;
                // console.log(distance);
                duration = response.rows[0].elements[0].duration.text;
                distance2 = distance/1000;
               // console.log(distance2);
                $('#checkout_btn').show();
                if(distance2 <= 50)
                    charges = 100;
                else if(distance2 >= 50 && distance2 <= 100)
                    charges = 200;
                else if(distance2 >= 100 && distance2 <= 150)
                    charges = 400;
                else if(distance2 >= 150 && distance2 <= 200)
                    charges = 800;
                else if(distance2 > 200){
                    Swal.fire({
                        title: 'Sorry your current address is out of range',
                        showClass: {
                            popup: 'animated fadeInDown faster'
                        },
                        hideClass: {
                            popup: 'animated fadeOutUp faster'
                        }
                    });
                    $('#checkout_btn').hide();
                    charges = '---';
                }


                $('.shiping_charges_span').text('€'+charges);
                $('#shiping_charges').val(charges);
                $('.total_price b').text('€'+(charges+{{Cart::total()}}));

                if(charges > 0) {
                    $.ajax({
                        method: "GET",
                        url: "{{ route('distance-detail') }}",
                        data: {charges: charges,distance_km:distance2}
                    }).done(function (data) {
                       // console.log(parseFloat(data));
                       // console.log(parseFloat( {{Cart::total()}}));
                        if(parseFloat(data) > parseFloat( {{Cart::total()}}))
                        {
                           // alert(distance2);
                            if(distance2 <= 200) {
                                $('#checkout_btn').hide();
                                $('#order_range_error').html('According your distance minimum order will be €' + data);
                                Swal.fire({
                                    title: 'According your distance minimum order will be €' + data,
                                    showClass: {
                                        popup: 'animated fadeInDown faster'
                                    },
                                    hideClass: {
                                        popup: 'animated fadeOutUp faster'
                                    }
                                });
                            }
                        }
                    });
                }

            } else {
                alert("Error: " + status);
            }
        }
    }
    function showPosition(position) {
        //console.log(position);
        $('#gmap_latitude').val(position.coords.latitude);
        $('#gmap_longitude').val(position.coords.longitude);
        let geocoder = new google.maps.Geocoder();
        let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                var address = results[results.length - 1].address_components[0];
                $('#location').attr("placeholder", results[0].formatted_address);
                $('#location').val(results[0].formatted_address);
                $('#country_iso').val(address['short_name']);
            } else {
                alert('Geocode was not successful due to following reason: ' + status);
            }
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

</body>

</html>

