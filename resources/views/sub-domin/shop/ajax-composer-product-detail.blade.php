@if($composeProducts)
	@php
		$restaurantDetail = Session::get('restaurantDetail');
		$currency = isset($restaurantDetail->currency)?$restaurantDetail->currency:'CHF';
	@endphp
	<div class="modal-header border-bottom-0">
		<h5 class="modal-title" id="exampleModalLabel">
			{{$composeProducts->name}}
		</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body modelbody">
		<input type="hidden" id="menu_composer_id" value="{{$composeProducts->id}}" />
		<input type="hidden" id="menu_composer_type" value="{{$composeProducts->type}}" />
		<form action="">
			<div class="bannerimg">
			 @if(isset($composeProducts->image))
				<img class="img-responsive"
					 src="{{ asset('uploads/products/'.$composeProducts->image ) }}"
					 alt=""/>
				@endif
			</div>

			<div class="title-pop"> <p> {{$composeProducts->description}}</p> </div>

			<section class="">
				<div class="greybg">
					<div class="title-pop2">
						<h4> {{$composeProducts->name}}</h4>
					</div>
				</div>
				<p id="validate_error" style="color: red;margin: 4px 2px 1px 24px;"></p>
				<div class="title-pop3 margin-set">
					<?php 
						$counter_quantity = 0;
					?>
					@foreach($composeProducts['composeQuantites'] as $composeQuantite)
						<?php if($counter_quantity == 0) { 
								$ingredients = $composeQuantite->ingredients;
							} 
						?>		
						<label class="radio-inline" style="display:{{$composeQuantite->price <= 0 ? 'none' : ''}}">
							<input type="radio" name="quantity" class="quantity_type" value="{{$composeQuantite->id}}" <?php if($counter_quantity == 0) echo "checked"; ?> data-price="{{$composeQuantite->price}}" data-max="{{$composeQuantite->ingredients}}"> {{$composeQuantite->quantity}} ({{$currency}} {{$composeQuantite->price}})
						</label>
						<?php  $counter_quantity++; ?>
					@endforeach	 
					<p class="item_can_select" data-max="<?php echo $ingredients ?>">Vous pouvez sélectionner au maximum <?php echo $ingredients ?> articles</p>				
				</div>
			<div class="row">
				@foreach($composeProducts['composeProductItem'] as $composeProductItem1)
				<div class="col-md-6">
					<div class="title-pop3 only-set">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input p_attribute composeProductItem _items_simple" id="{{$composeProductItem1->id}}" value="{{$composeProductItem1->id}}" data-item-price="{{$composeProductItem1->price}}">
							<label class="custom-control-label" for="{{$composeProductItem1->id}}">
								<div class="row" style="align-items: center;">
									@if(isset($composeProductItem1->image))
										<div class="col-xs-3 ml-3">
											<img src="{{ asset('uploads/products/'.$composeProductItem1->image ) }}" alt=""/>
										</div>
									@endif
									<div class="col-xs-9 ml-3">
										<span class="newh1" >
										{{$composeProductItem1->name}}
										@if(isset($composeProductItem1->price))
											<p class="price-set1" >
												({{$currency}} {{$composeProductItem1->price}})
											</p>
										@endif
										</span>
									</div>

								</div>
							</label>
						</div>
					</div>
					</div>
				@endforeach
				<div>
			</section>
			
				<div class="greybg">
				<div class="title-pop2">
					<h4>Demande spéciale</h4>
				</div>
			</div>

			<div class="title-pop3">

				<div class="form-group">

				<textarea class="form-control area12" id="special_note" rows="3"></textarea>
				</div>
			</div> 
			
		</form>

		
		<div class="d-flex justify-content-end title-pop3">
			<h4>Total: <span class="price text-success" id="p_price">
							@if(isset($composeProducts->composeQuantites[0]->price))
							{{$currency}} {{$composeProducts->composeQuantites[0]->price}}
							@endif
						</span>
			</h4>
		</div> 
	</div>

<div class="modal-footer border-top-0 d-flex justify-content-between title-pop3">
	<button type="button" class="btn btn-lg btn-outline-info text-light text-capitalize" data-dismiss="modal">retour</button>
	@if(isset($restaurantDetail->accessibility) && ($restaurantDetail->accessibility == 2 || $restaurantDetail->accessibility == 3))
		<div class="myModal">
			<button type="button" class="btn btn-lg btn-outline-info text-light text-capitalize add-cart-menu-composer"> Ajouter au chariot </button>
		</div>
	@endif
</div>

@endif
