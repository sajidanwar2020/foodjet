	@if($products_detail)
	@php
		$restaurantDetail = Session::get('restaurantDetail');
		$currency = isset($restaurantDetail->currency)?$restaurantDetail->currency:'CHF';
		$nutrients_ids = isset($products_detail->nutrients_ids)? json_decode($products_detail->nutrients_ids, true) : '';
	
		$logo=asset('assets/restraurant-site/img/logo.png');
		if(isset($restaurantDetail->logo) AND !empty($restaurantDetail->logo)){
			$logo =url('uploads/vendor/logo/'.$restaurantDetail->logo);
		}
	@endphp
	<div class="modal-header border-bottom-0">
		<h5 class="modal-title" id="exampleModalLabel">
			{{$products_detail->product_name}}
		</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	<div class="modal-body modelbody">
		
		<div class="bannerimg">
				@if(isset($products_detail->product_images[0]->image_name))
				<img class="img-responsive image-fluid"
					 src="{{ asset('uploads/products/'.$products_detail->product_images[0]->image_name ) }}"
					 alt=""/>
					  @else
						   <img class="card-img-top" style=""
									 src="{{  $logo }}"
									 alt=""/>
				@endif	 
		</div>
		
		
		@if($products_detail->product_description)
			<div class="title-pop">
				<p>{{$products_detail->product_description}}</p>
			</div>
		@endif
		
		@if($products_detail->label_images|| $products_detail->nutrients_arr)
		<div class="labal-icon">
			@if($products_detail->label_images)
				@foreach($products_detail->label_images as $label_images)
					<img class="img-responsive"
					 src="{{ asset('uploads/labelimages/'.$label_images->image_path ) }}"
					 alt=""/>
				@endforeach
			@endif
			<table class="nut-values" >
				<thead>
				<tr>
					@if($products_detail->nutrients_arr)
						@foreach($products_detail->nutrients_arr as $nutrients)
							<th scope="col">{{$nutrients->label}}</th>
						@endforeach
					@endif
				</tr>
				</thead>
				<tbody>
				<tr>
					@if($products_detail->nutrients_arr)
						@foreach($products_detail->nutrients_arr as $nutrients)
							<td>{{$nutrients_ids[$nutrients->id]}}</td>
						@endforeach
					@endif
				</tr>
				</tbody>
			</table>
		</div>
		@endif
	

		<form action="">
			<input type="hidden" id="popup_product_id" value="{{$products_detail->id}}">
			<p id="validate_error" style="color: red;margin: 4px 2px 1px 24px;"></p>
			
			@if(count($products_detail['product_price']) > 0)
				@php
					$total_product_price = count($products_detail['product_price']);
				@endphp
				<div class="row ">
				<div class="col-md-7 col-12">
					@foreach($products_detail['product_price'] as $product_price)
						<?php // When an item has only one option we don't need to show the check button.  therefore visibility:hidden is used ?>
						
					<div class="title-pop3 attrbuite_outer">
						<div class="custom-control custom-checkbox">
							<div class="row align-items-center">
								<?php 
								 if($product_price->attribute_name == "Standard" || $product_price->attribute_name == "Prix") 
									$product_price->attribute_name = ''; 
								?>
								<div class="col-md-8 col-8" <?php if($total_product_price == 1) echo 'style="display:none"' ?>>
									<input type="checkbox" class="custom-control-input p_attribute attrbuite_detail " id="{{$product_price->attribute_id}}" data-attribute_id="{{$product_price->attribute_id}}" data-price="{{$product_price->price}}" name="attribute_id[{{$product_price->attribute_id}}]" <?php if($total_product_price == 1) echo 'checked' ?>>
									<label class="custom-control-label ml-2" for="{{$product_price->attribute_id}}">{{ $product_price->attribute_name}} ({{$currency}} {{ $product_price->price}})</label>
								</div>
								<?php if($total_product_price == 1) { ?>
									<span class="col-md-8 col-8"> {{$product_price->attribute_name}} ({{$currency}} {{ $product_price->price}})</span>
								<?php } ?>
								<div class="col-md-4 col-4" >
									<span class="product_selection qty" >
										<select class="qunatity form-control" name="qunatity">
											<option value="1" selected="">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10</option>
										</select>
									</span>
								</div>
								<?php 
									$display = 'none'; 
									if($total_product_price == 1)  
										$display = 'block'; 
								?>
								<div class=" col-md-12 col-12" id="special_note_{{$product_price->attribute_id}}" style="display:{{$display}}">

									<div class="">
										<div class="form-group">
											<textarea class="form-control area12" class="special_note" data-notes="{{$product_price->attribute_id}}"  rows="4" placeholder="{{$product_price->attribute_name}} Demande spéciale"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				
			
					<?php /* if($total_product_price == 1) { ?>
						
						<div class="title-pop3">
							<span>{{ $product_price->attribute_name}} ({{$currency}} {{ $product_price->price}})</span>
							<span class="product_selection" style="display: inline-block;">
								<select class="qunatity form-control" name="qunatity">
									<option value="1" selected="">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
								</select>
							</span>
						</div>
					<?php } */?>
					@endforeach
				</div>
				
				<?php
				// show only if wine slider
					$json_array_wine = json_decode($products_detail->json_array,true);
					if(!empty($json_array_wine) AND count($json_array_wine['wine']) > 0) {
						echo '<div class="col-md-5 mt-1">';
						foreach($json_array_wine['wine'] as $wine) {
				?>
							<div class="slidecontainer row  ml-2 mr-2" style="display: flex; align-items: baseline;">
								<div class="col-4">
									<p>{{$wine['initial']}}</p>
								</div>

								<div class="form-group col-4">
									<input type="range" class="form-control-range slider-range" id="formControlRange" disabled value="{{$wine['percentage']}}">
								</div>
								<div class="col-4">
								<p>{{$wine['final']}}</p>
								</div>
							</div>
					<?php
						}
						echo '</div>';
					}
					?>
				</div>
			@endif
			
		</form>

		<?php /*	
		<div class="d-flex justify-content-end title-pop3">
			<h4>Total: <span class="price text-success">89$</span></h4>
		</div> */ ?>
	</div>

	<div class="modal-footer border-top-0 d-flex justify-content-between title-pop3">
		<button type="button" class="btn btn-lg btn-outline-info text-light text-capitalize" data-dismiss="modal">retour</button>
		
		@if(isset($restaurantDetail->accessibility) && ($restaurantDetail->accessibility == 2 || $restaurantDetail->accessibility == 3))
			<div class="myModal">
				<button type="button" class="btn btn-lg btn-outline-info text-light text-capitalize add-cart" data-id="{{$products_detail->id}}">
					Ajouter au chariot
				</button>
			</div>
		@endif
	</div>

@endif