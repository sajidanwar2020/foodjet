@if($composeProducts)
	@php
		$restaurantDetail = Session::get('restaurantDetail');
		$currency = isset($restaurantDetail->currency)?$restaurantDetail->currency:'CHF';
	@endphp
	
	<?php /*<style>.steps.clearfix { display: none;} </style> */ ?>
	
	<div class="modal-header border-bottom-0">
		<h5 class="modal-title" id="exampleModalLabel">
			{{$composeProducts->name}}
		</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
	</div>
	
	<div class="modal-body modelbody">
		<input type="hidden" id="menu_composer_id" value="{{$composeProducts->id}}" />
		<input type="hidden" id="menu_composer_type" value="{{$composeProducts->type}}" />
		
		<div class="bannerimg" style="display:none">
			 @if(isset($composeProducts->image))
				<img class="img-responsive"
					 src="{{ asset('uploads/products/'.$composeProducts->image ) }}"
					 alt=""/>
			@endif
		</div> 
          <div class="row">
			  <div class="col-md-12">
				  <table class="table compose-table">
						<thead>
							<tr>
							  <th scope="col"></th>
								<?php  $counter_quantity = 0; ?>
							  @foreach($composeProducts['composeQuantites'] as $composeQuantite)
								<th scope="col">
									<label class="radio-inline">
										<input type="radio" class="quantity_type" name="quantity" value="{{$composeQuantite->id}}" <?php if($counter_quantity == 0) echo "checked"; ?> data-price="{{$composeQuantite->price}}"> {{$composeQuantite->quantity}} ({{$currency}} {{$composeQuantite->price}})
									</label>
								</th>
								<?php  $counter_quantity++; ?>
							  @endforeach	
							</tr>
						</thead>
							<tbody>
							<?php 
								$total_item  = count($composeProducts['composeProductItem']);
							?>
							@if (count($composeProducts['composeProductItem']) > 0)
								<?php $counter_item = 0; ?>
								@foreach($composeProducts['composeProductItem'] as $composeProductItem1)
								<?php 
									$ingredients = json_decode($composeProductItem1->ingredients,true);
									$ingredients_arr = $ingredients[$composeProductItem1->id];
								?>
								  <tr>
									<th scope="row"><?php /*<input type="radio" class="item_id" name="item_id" value="{{$composeProductItem1->id}}" <?php if($counter_item == 0) echo "checked"; ?>>*/ ?>{{$composeProductItem1->name}}</th>
									 @if (count($composeProducts['composeQuantites']) > 0)
														@foreach($composeProducts['composeQuantites'] as $composeQuantite)
									    <td>
										  <table>
											  <tr class="table-settings" style="display: none" id="quantity_{{$composeQuantite->id}}_{{$composeProductItem1->id}}">
													<?php for($i=1; $i<=$total_item; $i++) { 
														$ingredients = $bg = '';
														if($i == 1) {
															$ingredients = $ingredients_arr[$composeQuantite->id].'X';
															$ingredients_for_color = $ingredients_arr[$composeQuantite->id];
														}
														if( $i <= $ingredients_for_color)
															$bg = $composeProductItem1->color;
														//if($ingredients_for_color == 2)
															//echo "hereeee";
													?>
														<td style="background-color: {{$bg}}">{{$ingredients}}</td>
													<?php } ?>	
											  </tr>
										  </table>
										</td>
										<?php $counter_item++; ?>
										@endforeach
									@endif
								  </tr>
							@endforeach
						@endif
					  </tbody>
				  </table>

			  </div>


			  </div>


		  </div>
		<div class="title-pop">
			<p> {{$composeProducts->description}}</p>
		</div>
			<section class="">
				
				<p id="validate_error222"></p>
				
					
			</section>

		<div class="wrapper">
			<form action="" id="wizard">
			<?php 
				$item_arr = array();
				$counter_item = 0;
				$composeProductItem_total = count($composeProducts['composeProductItem']);
				
			?>
			@foreach($composeProducts['composeProductItem'] as $composeProductItem1)
			<?php 
				
				$item_arr[$composeProductItem1->id] =  array('item_name' => $composeProductItem1->name , 'item_img' => $composeProductItem1->image,'ingredients' => $composeProductItem1->ingredients );
			?>
				<h4></h4>
				<section>
					<p class="validate_error"></p>
					<p class="item_can_select">Vous pouvez sélectionner au maximum <span class="max_ingredients"></span> 1 articles</p>
					
					@foreach($composeProductItem1['composeGroups'] as $composeGroup)
						@if($composeGroup->title)
							<div class="greybg">
								<div class="title-pop2">
									<h4> {{$composeGroup->title}}</h4>
								</div>
							</div>
						@endif
						@foreach($composeGroup['sub_item'] as $sub_item2)
						
						<div class="title-pop3">
							
							<div class="custom-control custom-checkbox">
								<input type="checkbox" class="custom-control-input p_attribute composeProductItem _items_mixture" id="{{$sub_item2->id}}" value="{{$sub_item2->id}}" name="example" >
								<label class="custom-control-label" for="{{$sub_item2->id}}">
									<div class="row">
										<div class="col-xs-3 ml-3 ">
											@if(isset($sub_item2->image))
												<img src="{{ asset('uploads/products/'.$sub_item2->image ) }}" alt=""/>
											@endif
										</div>
										
										<div class="col-xs-9 ml-3">
											<span class="newh1">{{$sub_item2->name}}</span>
										</div>
									</div>
								</label>
							</div>
						</div>
						@endforeach	
					@endforeach	
					
					@if( $counter_item == $composeProductItem_total - 1) 
						<div class="greybg">
							<div class="title-pop2">
								<h4>Demande spéciale</h4>
							</div>
						</div>
						<div class="title-pop3">
							<div class="form-group">
								<textarea class="form-control area12" id="special_note" rows="3"></textarea>
							</div>
						</div>
					@endif	
					
				</section>
				<?php $counter_item++ ?>
			@endforeach	
				
			</form>
			<div class="d-flex justify-content-end title-pop3 __total_price">
				<h4>Total: <span class="price text-success" id="p_price">
								@if(isset($composeProducts->composeQuantites[0]->price))
									{{$currency}} {{$composeProducts->composeQuantites[0]->price}}
								@endif
							</span>
				</h4>
			</div>
		</div>
	</div>
	<script src="{{asset('assets/restraurant-site/js/jsform/jquery.steps.js')}}"></script>	
	
	<?php
		$button_text = 'Terminer';
		$return  = true;
		if(isset($restaurantDetail->accessibility) && ($restaurantDetail->accessibility == 2 || $restaurantDetail->accessibility == 3))	{
			$button_text = 'Ajouter au chariot';
			$return  = false;
		}
		//echo "<pre>";
		//print_r($item_arr);exit;
	?>
	
	<script>
	$(function(){
	$("#wizard").steps({
        headerTag: "h4",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        transitionEffectSpeed: 300,
        labels: {
            next: "Continuer",
            previous: "Retour",
            finish: '{{$button_text}}',
        },
        onStepChanging: function (event, currentIndex, newIndex) { 
			// start valdation check for valdation means at least select one checkbox
			var attrbuite_arr1 = [];
			$('#product_detail_popup .current input.p_attribute:checkbox:checked').each(function () {
				attrbuite_arr1.push($(this).val());
			}); 
			
			$('.current .validate_error').html('');
			if (attrbuite_arr1.length == 0 && newIndex > currentIndex) {
				$('.current .validate_error').html('Veuillez sélectionner au moins une quantité');
				return false;
			}
			
			// start how many item can select message
			if (attrbuite_arr1.length > 0) {
				item_selection();
			}

			function item_selection() {
				newIndex2 = newIndex + 1;
				var quantity_type_val = $('#product_detail_popup input.quantity_type:checked').val();
				var item_id = parseInt($('.steps ul li:nth-child('+newIndex2+') a .step-order').data('item-id'));
				var params = $('.steps ul li:nth-child('+newIndex2+') a .step-order').data('params');

				if(params) {
					$('#quantity_'+quantity_type_val+'_'+item_id).css('display','block');
					data = JSON.parse(JSON.stringify(params)); 
					limit_selected = data[item_id][quantity_type_val];
					$('.item_can_select').html('Vous pouvez sélectionner au maximum '+limit_selected+' articles');
				}
			}
			// hide/show quantity_type
            if ( newIndex > 0 ) {
				$("#product_detail_popup input.quantity_type:not(:checked)").parent().css('display','none');
            } else {
				$("#product_detail_popup input.quantity_type").parent().css('display','inline-block');
			}
			
            return true; 
        },
		onFinished: function (event, currentIndex) {
			select_min = $('.current a .step-order').attr('data-max');
			<?php if($return) { ?>
					return false;
			<?php } ?>
			var attrbuite_arr1 = [];
			$('#product_detail_popup .current input.p_attribute:checkbox:checked').each(function () {
				attrbuite_arr1.push($(this).val());
			}); 
			
			if (attrbuite_arr1.length == 0) {
				$('.current .validate_error').html('Veuillez sélectionner au moins une quantité');
				return false;
			}
			
			add_cart_menu_composer_fun();
		}
    });
    // Custom Button Jquery Stepss
    $('.forward').click(function(){
    	$("#wizard").steps('next');
    })
    $('.backward').click(function(){
        $("#wizard").steps('previous');
    })
	$("a[href$='previous']").click(function(){
    	$('#cartModal2').modal('hide');
    })
	
	$(document).on("click", ".item_id", function(e){
		var item_id = $(this).val();
		//$('.item_'+item_id).trigger('click');
		//alert($('.current ._items_mixture:checked').length);
		if ($('.current ._items_mixture:checked').length < 1) {	
			e.target.checked = false;
			e.preventDefault();
			//return false;
		}
		$('.item_'+item_id).trigger('click');
		//$('.item_'+item_id).trigger('click');
	})
	
    // Create Steps Image
	<?php
		$counter = 0;
		$total_items = count($item_arr);		
			foreach($item_arr as $key => $item) {
				//print_r($item);exit;
			
				if(isset($item["item_img"]))
					$item_img = asset('uploads/products/'.$item["item_img"]);
				else
					$item_img = asset('assets/site/images/products/featured/2.jpg');
			
				
					if($counter == 0) {  ?>
						$('.steps ul li:first-child').append('<img src="<?php echo asset("assets/restraurant-site/js/jsform/img/step-arrow.png") ?>" alt="" class="step-arrow">').find('a').append('<img src="<?php echo $item_img ?>" alt="" class="composer_selector_image"> ').append('<span class="step-order item_<?php echo $key ?>"  data-params="<?php echo htmlspecialchars(($item["ingredients"]), ENT_QUOTES, "UTF-8"); ?>"  data-item-id="<?php echo $key ?>" ><?php echo $item["item_name"] ?></span>');
					<?php 
							$counter++; 
							continue; 
						} 
						$counter++; 
					?>
					
					<?php if( $total_items != $counter AND $total_items > 2) { ?>
						$('.steps ul li:nth-child(<?php echo $counter ?>').append('<img src="<?php echo asset("assets/restraurant-site/js/jsform/img/step-arrow.png") ?>" alt="" class="step-arrow">').find('a').append('<img src="<?php echo $item_img ?>" alt="" class="composer_selector_image">').append('<span class="step-order item_<?php echo $key ?>" data-params="<?php echo htmlspecialchars(($item["ingredients"]), ENT_QUOTES, "UTF-8"); ?>"  data-item-id="<?php echo $key ?>"><?php echo $item["item_name"] ?></span>');
					<?php  }  ?>
					
					<?php if($total_items == $counter) { ?>	
						$('.steps ul li:last-child a').append('<img src="<?php echo $item_img ?>" alt="" class="composer_selector_image">').append('<span class="step-order item_<?php echo $key ?>" data-params="<?php echo htmlspecialchars(($item["ingredients"]), ENT_QUOTES, "UTF-8"); ?>"  data-item-id="<?php echo $key ?>" ><?php echo $item["item_name"] ?></span>');
					<?php  }  ?>
				
		<?php } ?>
	})

	
	</script>
@endif