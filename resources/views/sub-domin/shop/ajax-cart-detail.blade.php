<?php 
//echo "<pre>";
//print_r(Cart::content());exit;
?>
@if(count(Cart::content()) > 0)
	@php
		$restaurantDetail = Session::get('restaurantDetail');
		$currency = isset($restaurantDetail->currency)?$restaurantDetail->currency:'CHF';
	@endphp

	<div class="modal-body">
		<div class="cart-scroll-set">
		<table class="table table-image" id="example">
			<thead>
			<tr>

				<th scope="col">Produit</th>
				<th scope="col">Comment</th>
				<th scope="col">Prix</th>
				<th scope="col">Quantité</th>
				<th scope="col">Total</th>
				<th scope="col">Actions</th>
			</tr>
			</thead>
			<tbody>
			 <?php
				foreach(Cart::content() as $row) :  
			
					$attirbute_str = '';
					if($row->options->composer == 'yes') {
						foreach($row->options->item_detail as $options) {
							$attirbute_str .= $options->name.' ,';
						}
						$attirbute_str = rtrim($attirbute_str, ',');
					}
					if($row->options->composer == 'no') {
					
						if($row->options->attribute_detail->attribute_name == "Standard" || $row->options->attribute_detail->attribute_name == "Prix") 
							$row->options->attribute_detail->attribute_name = ''; 
								
						$attirbute_str = !empty($row->options->attribute_detail->attribute_name) ? '('.$row->options->attribute_detail->attribute_name.')' : '';
					}
					//echo "<pre>";
					//print_r($row);exit;
			?>
				<tr class="item_cart" id="{{$row->rowId}}">
					<td class="font-set"><?php echo $row->name ?> 
						<?php echo $attirbute_str;  ?>
					</td>
					@if($row->options->composer == 'yes') 
						<td><?php echo isset($row->options->raw_data['special_note']) ? $row->options->raw_data['special_note'] : '---' ?></td>
					 @else
						<td style="word-break: break-all;"><?php echo $row->options->special_note ? $row->options->special_note : '---' ?></td>
					@endif
					<td>{{$currency}} <?php echo $row->price; ?></td>
					<td class="qty">
						<select class="qunatity form-control" name="qunatity">
							@for ($i = 1; $i <= 10; $i++)
								<option value="{{ $i }}" {{ $row->qty == $i ? 'selected' : ''}}>{{ $i }}</option>
							@endfor
						</select>
					</td>
					<td class="total-price">{{$currency}} <?php echo $row->total; ?></td>

					<td class="text-c">
						<a href="javascript:void(0)" class="btn btn-danger btn-sm remove_cart">
							<i class="fa fa-times"></i>
						</a>
					</td>
				</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		</div>
		<div class="d-flex justify-content-end pt-4">
			<h5>Total: <span class="price text-success cart_total ">{{$currency}} <span><?php echo Cart::subtotal(); ?></p></span></h5>
		</div>
	</div>

	<div class="modal-footer border-top-0 d-flex justify-content-between">
		<button type="button" class="btn btn-lg btn-outline-info text-light text-capitaliz" data-dismiss="modal"> Ajouter plus d'articles</button>
		<a href="<?php echo route('check_out', ['id' => $restaurantDetail->restaurant_code]); ?>"  class="btn btn-lg btn-outline-info text-light text-capitalize">Check-out</a>
	</div>
  @else
		<div class="cart_empty_wrapper">
			<p>Votre carte est actuellement vide.</p>
			<p class="return-to-shop">
				<a class="button wc-backward" href="home"> Retour à la boutique</a>
			</p>
		</div>
 @endif
 
 
 
 
 <script type="text/javascript">
    $(document).ready(function () {
		
		$(document).on("change",".qunatity",function() {	
            var closest_tr = $(this).closest('tr');
            var quantity = closest_tr.find('.qunatity').val();
            var pid = closest_tr.attr('id');
            var _this  = $(this);

            $.ajax('{{ route('update_cart') }}', {
                method: 'POST',
                dataType: "json",
                data: {'_token': '{{ csrf_token() }}', pid: pid,quantity: quantity},
                success: function (data) {
                    closest_tr.find('.total-price').text('{{$currency}}' + ' '+data.current_product_total.toFixed(2) );
                    $('.cart-count').html(data.cart_count);
                    $('.cart_total span').text(data.total_cart_amount);
                }
            });
        });

        $(document).on("click",".remove_cart",function() {
            var closest_tr = $(this).closest('tr');
            var pid = closest_tr.attr('id');
            $.ajax('{{ route('remove_cart') }}', {
                method: 'POST',
                dataType: "json",
                data: {'_token': '{{ csrf_token() }}', pid: pid},
                success: function (data) {
                    closest_tr.remove();
                   $('.cart-count').html(data.cart_count);
                    $('.cart_total span').text(data.total_cart_amount);
					if(data.cart_count < 1) {
                       $('#cartModal').modal('hide');
                    }
                }
            });
        });

    });

</script>

 
 
 
 