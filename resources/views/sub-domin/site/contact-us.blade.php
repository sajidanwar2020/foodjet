	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		 @include('partials.restaurant-site.headers-style')
	</head>
		<body>
			@include('partials.restaurant-site.top-navbar') 
			@include('partials.restaurant-site.breadcrums') 
			<?php
				$restaurantDetail = Session::get('restaurantDetail');
				//echo "<pre>";
				//print_r($restaurantDetail);
				//exit;
			?>

			<!-- Start contact-page Area -->
			<section class="contact-page-area section-gap">
				<div class="container">
					<div class="row">
						<?php /*<div class="map-wrap" style="width:100%; height: 445px;" id="map"></div> */ ?>
						
								<div class="col-lg-4 d-flex flex-column address-wrap">
									<div class="single-contact-address d-flex flex-row">
										<div class="icon">
											<span class="lnr lnr-home"></span>
										</div>
										<div class="contact-details">
											
											<h5>Adresse : {{$restaurantDetail['location']}}</h5>
											
										</div>
									</div>
									<div class="single-contact-address d-flex flex-row">
										<div class="icon">
											<span class="lnr lnr-phone-handset"></span>
										</div>
										<div class="contact-details">
											<h5>Téléphone : {{$restaurantDetail['phone_number']}}</h5>
										</div>
									</div>
									<div class="single-contact-address d-flex flex-row">
										<div class="icon">
											<span class="lnr lnr-envelope"></span>
										</div>
										<div class="contact-details">
											<h5>Email : {{$restaurantDetail['contact_email']}}</h5>
											<p>Send us your query anytime!</p>
										</div>
									</div>														
								</div>
							
						<div class="col-lg-8">
							 @include('partials.flash-message')
						   <form method="post" action="{{ route('contact_submitted') }}" id="" autocomplete="off" class="form-area contact-form text-right">
								  {{ csrf_field() }} 
								<div class="row">	
									<div class="col-lg-6 form-group">
										<input name="name" placeholder="Entrez votre nom" class="common-input mb-20 form-control"  type="text">
									
										<input name="email" placeholder="Entrer l'adresse e-mail"  class="common-input mb-20 form-control" type="text">

										<input name="subject" placeholder="Entrez le sujet" class="common-input mb-20 form-control" type="text">
									</div>
									<div class="col-lg-6 form-group">
										<textarea class="common-textarea form-control" name="message" placeholder="Entrez un message"  ></textarea>
									</div>
									<div class="col-lg-12">
										<div class="alert-msg" style="text-align: left;"></div>
										<button class="btn btn-lg btn-outline-info text-light text-capitalize" type="submit" style="float: right;">Envoyer le message</button>											
									</div>
								</div>
							</form>	
						</div>
					</div>
				</div>
			</section>
			<!-- End contact-page Area -->

			@include('partials.restaurant-site.footer')
			 @include('partials.restaurant-site.footer-js')	
			 @include('partials.restaurant-site.common_js')	
		</body>
	</html>