	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		 @include('partials.restaurant-site.headers-style')
	</head>
		<body>
			@include('partials.restaurant-site.top-navbar') 
			@include('partials.restaurant-site.breadcrums') 
			<?php
				$restaurantDetail = Session::get('restaurantDetail');
				//echo "<pre>";
				//print_r($restaurantDetail);
				//exit;
			?>

			<!-- Start contact-page Area -->
			<section class="contact-page-area section-gap">
				<div class="container">
					<div class="row">
						@if(count($labelimages) > 0)
							<div class="col-md-12 mb-3"><h2>Label images</h2></div>
							@foreach($labelimages as $image)
								<div class="form-group col-lg-3 col-md-3 labal-icon1">
									<img src="{{ asset('uploads/labelimages/'.$image->image_path ) }}" alt="" width="30px" height="30px">
									<label>{{ $image->label_name }}</label>
								</div>
							@endforeach
						@endif												
					</div>
					
					<div class="row">
						@if(count($nutrients) > 0)
								<div class="col-md-12 mb-3"><h2>Nutrients</h2></div>
								@foreach($nutrients as $nutrient)
								<div class="form-group col-md-4 label-icon2">
								<label>{{ $nutrient->label}}</label> 
									<p>{{ $nutrient->description}}</p> 
									<?php /* <label>{{ $nutrient->label}}</label> */?>
								</div>
							@endforeach
						@endif												
					</div>
				</div>	
			</section>
			<!-- End contact-page Area -->

			@include('partials.restaurant-site.footer')
			 @include('partials.restaurant-site.footer-js')	
			 @include('partials.restaurant-site.common_js')	
		</body>
	</html>