	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		 @include('partials.restaurant-site.headers-style')
	</head>
		<body>
			@include('partials.restaurant-site.top-navbar') 
			@include('partials.restaurant-site.breadcrums') 
			<?php
				$restaurantDetail = Session::get('restaurantDetail');
				//echo "<pre>";
				//print_r($restaurantDetail);
				//exit;
			?>

			<!-- Start contact-page Area -->
			<section class="contact-page-area section-gap">
				<div class="container">
					<div class="row">
						<?php /*<div class="map-wrap" style="width:100%; height: 445px;" id="map"></div> */ ?>
						
								<div class="col-lg-12 d-flex flex-column address-wrap">
								
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th scope="col">#</th>
									  <th scope="col">Nom</th>
									  <th scope="col">La description</th>
									</tr>
								  </thead>
								  @if (count($ingredients_for_webs) > 0)
									  <?php $counter = 1; ?>
										@foreach($ingredients_for_webs as $ingredients_for_web)
								  <tbody>
									<tr>
									  <th scope="row">{{$counter}}</th>
									  <td>{{$ingredients_for_web->name}}</td>
										<td>{{$ingredients_for_web->description}}</td>
									</tr>
								  </tbody>
									<?php $counter++; ?>
								    @endforeach
								@else
									<tr>
										<td colspan="8">
											<p>Aucun tableau ajouté</p>
										</td>
									</tr>
								@endif 
								</table>													
								</div>
							
					
					</div>
				</div>	
			</section>
			<!-- End contact-page Area -->

			@include('partials.restaurant-site.footer')
			 @include('partials.restaurant-site.footer-js')	
			 @include('partials.restaurant-site.common_js')	
		</body>
	</html>