<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	@include('partials.restaurant-site.headers-style')
	<style>.center.slider:not(.active){display:none} </style>
</head>
<body>
@php
	$logo=asset('assets/restraurant-site/img/logo.png');
	if(isset($restaurant->logo) AND !empty($restaurant->logo)){
		$logo = url('uploads/vendor/logo/'.$restaurant->logo);
	}

	$header=asset('assets/restraurant-site/img/hero-bg.jpg');
	if(isset($restaurant->header_image) AND !empty($restaurant->header_image)){
		$header= url('uploads/vendor/header_banner/'.$restaurant->header_image);
	}
@endphp

@php
	$currency=isset($restaurant->currency)?$restaurant->currency:'CHF';
    $restaurantDetail = Session::get('restaurantDetail');
@endphp

<header id="header">
	<div class="container main-menu">
		<div class="row align-items-center justify-content-between height-set" >
			<a class="navbar-brand" href="home"><img src="{{$logo}}"></a>
			<p class="text-white">
				{{isset($restaurant->logo_text)?$restaurant->logo_text:''}}
			</p>
			<div class="mar-set1">
				<a href="<?php echo route('booking', ['id' => $restaurantDetail->restaurant_code]); ?>">
					<img src="{{ asset('assets/fonts/reservation25.png') }}" style="width: 25px; height: auto; padding-bottom: 5px;" data-toggle="tooltip" title="Reservation">
				</a>
				<a href="">
					<img src="{{ asset('assets/fonts/takeaway.png') }}" style="width: 18px; height: auto; padding-bottom: 6px;" data-toggle="tooltip" title="Takeaway">
				</a>
			</div>
			<div class="mar-set1">
				<a id="bellIcon" >
					<i class="fas fa-bell  bell" data-toggle="tooltip" title="Waiter call"></i></a>
				@if(isset($restaurantDetail->accessibility) && ($restaurantDetail->accessibility == 2 || $restaurantDetail->accessibility == 3))
					<a href="javascript:void(0)" id="cart_popup">
						<i class="fas fa-shopping-cart cart-set" data-toggle="tooltip" title="Cart"></i>
						<span class="badge style-set cart-count" > {{Cart::count()}} </span>
					</a>
				@endif
			</div>
			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li><a href="<?php echo route('home-sub', ['id' => $restaurantDetail->restaurant_code]); ?>">Accueil</a></li>
					<li><a href="<?php echo route('ingredients-for-web', ['id' => $restaurantDetail->restaurant_code]); ?>">Origines</a></li>
					<li><a href="<?php echo route('glossary', ['id' => $restaurantDetail->restaurant_code]); ?>">Glossaire</a></li>
					<li><a href="<?php echo route('contact-us', ['id' => $restaurantDetail->restaurant_code]); ?>">Contact</a></li>

					@if((Session::has('is_logged_in') && !empty(Session::get('is_logged_in'))) || Session::has("order_id"))
						<li class="level1"><a href="{{ route('customer_orders', ['id' => $restaurantDetail->restaurant_code]) }}">My orders</a></li>
					@endif

					@if(Session::has('is_logged_in') && !empty(Session::get('is_logged_in')))
						<li class=""><a href="{{ route('logout') }}">Logout</a></li>
					@endif

					@if(!Session::has('is_logged_in') && empty(Session::get('is_logged_in')))
						<li class=""><a href="signin">Sign in</a></li>
					@endif

				</ul>
			</nav><!-- #nav-menu-container -->
		</div>
	</div>
</header><!-- #header -->

<section class=" container-fluid tab-back" >
	@if(count($categoryTypes) > 0)
		<?php $counter = 1; ?>
		<ul class="top_category_outer list-inline text-center row">
			@foreach($categoryTypes as $categoryType)
				<?php
				$active_class = '';
				if($counter == 1)
					$active_class ="active";
				?>
				<li class="list-inline-item col liactive {{$active_class}}">

					<a class="top_category colo-set" href="javascrip:void(0)" data-top_category="{{$categoryType->id}}">
						@if(isset($categoryType->icon))
							<img class="card-img-top1"
								 src="{{ url('uploads/categories/icons/'.$categoryType->icon) }}"
								 alt=""/>
						@else
							<img class="card-img-top1"
								 src="{{  $logo }}"
								 alt=""/>

						@endif
						<span>{{$categoryType->name}}</span>
					</a>
				</li>
				<?php $counter++; ?>
			@endforeach
		</ul>
	@endif
</section>

<?php $counter = 1; ?>
@if(count($categoryTypes) > 0)
	@foreach($categoryTypes as $categoryType)
		@if(count($categoryType['parent_categories']) > 0)
			<?php
			$active_class = '';
			if($counter == 1)
				$active_class ="active";
			$counter9 = 0;
			?>
			<section class="center slider container-fluid {{$active_class}}" style="margin-top:0px;" id="categories_type_{{$categoryType->id}}">
				@foreach($categoryType['parent_categories'] as $category)
					@if(sizeof($category['products_arr'])>0 || count($category['child_categories']) > 0 || count($category['composeProduct']) > 0)
						<div>
							<a id="main_menu" class="btn btn-outline-info btn-rounded waves-effect" href="#menu_{{$category->id}}" data-parent="{{$category->id}}"  data-slick="{{$counter9}}"> {{$category->name}}</a>
						</div>
						<?php $counter9++; ?>
					@endif
				@endforeach
			</section>
			<?php $counter++; ?>
		@endif
	@endforeach
@endif

<?php /*
	@if(count($categories) > 0)
			<?php
				//echo "<pre>";
				//print_r($categories);exit;
			?>
			<section class="center2 slider container-fluid"  style="margin-top:28px;height: 31px; display:block; border-top: 1px solid rgb(190, 190, 190);"></section>
			@foreach($categories as $category)
				@if(sizeof($category['products_arr'])>0 || count($category['child_categories']) > 0 || count($category['composeProduct']) > 0)
					<section class="center2 slider container-fluid" id="sub_menu_{{$category->id}}" data-parent="{{$category->id}}"  style="margin-top:28px;    height: 31px; display:none; border-top: 1px solid rgb(190, 190, 190);">

					@if(count($category['child_categories']) > 0)
						<?php $counter9 = 0; //$close_section = false; ?>
						@foreach($category['child_categories'] as $child_category)
							@if(count($child_category['products_arr']) > 0)
									<div class="child-categories-slider" id="">
										<a  class="btn btn-outline-info btn-rounded waves-effect" href="#sub_menu_{{$child_category->id}}" data-parent="{{$category->id}}" data-child="{{$child_category->id}}"  data-slick2="{{$counter9}}"> {{$child_category->name}}</a>
									</div>
								<?php $counter9++; ?>
							@endif


						@endforeach
				@endif
					</section>
			@endif
		@endforeach
	@endif

<?php */

//echo "<pre>";
//print_r(Cart::content());
//exit;

?>

<!-- start banner Area -->
<div style=" padding-top: 137px; overflow-x: hidden;">
	<section class="banner-area relative" style="background:url({{$header}}) top center/cover; height: 520px;">
		<div class="overlay overlay-bg"></div>
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-between hp">
				<div class="col-lg-12 banner-content">
					<h6 class="text-white">{{isset($restaurant->above_heading_text)?$restaurant->above_heading_text:''}}</h6>
					<h1 class="text-white">{{isset($restaurant->main_heading)?$restaurant->main_heading:''}}</h1>
					<p class="text-white">
						{{isset($restaurant->header_image_text)?$restaurant->header_image_text:''}}
					</p>
					<p class="text-white">
						{{isset($restaurant->description)?$restaurant->description:''}}
					</p>
					@if(isset($restaurant->button_text))
						<a style="display:none" href="#main_menu" class="btn btn-outline-primary text-uppercase" style="font-size: 12px">{{ $restaurant->button_text }}</a>
					@endif
				</div>
			</div>
		</div>
	</section>
</div>
<!-- End banner Area -->

<!-- <..................slider............> -->
@if(count($suggested_products) > 0)
	<div class="container-fulid slider58 ">

		<div class="title text-center pb-70">
			<h1 class="mb-10">Suggestions du jour</h1>

		</div>
		<div class="owl-carousel owl-theme ">
			@foreach($suggested_products as $suggested_product)
				<div class="item menu_item_outer" data-id="{{$suggested_product->id}}">
					<a style="display:none;" href="javascript:void(0);" data-toggle="tooltip" title="test description" class="style-info"><i class="fas fa-info-circle"></i></a>
					<div class="card">
						<div class="card-header">
							@if(isset($suggested_product->product_image))
								<img class="card-img-top"
									 src="{{ asset('uploads/products/'.$suggested_product->product_image ) }}"
									 alt=""/>
							@else
								<img class="card-img-top" style="width: 80px;"
									 src="{{  $logo }}"
									 alt=""/>

							@endif
						</div>
						<div class="card-body">
							<div class="title-wrap justify-content-between card-title">
								<h4>{{$suggested_product->product_name}}</h4>
								<h4 class="price" id="#cartModal">
									@if(isset($product->product_price[0]->price))
										{{$currency}}
										{{$product->product_price[0]->price}}
									@endif
								</h4>
							</div>
							<p>
								{{$suggested_product->product_description}}
							</p>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endif


<?php $counter = 1; ?>
@if($categoryTypes)
	@foreach($categoryTypes as $categoryType)
		@if($categoryType['parent_categories'])
			@foreach($categoryType['parent_categories'] as $category)
				@if(sizeof($category['products_arr'])>0 || count($category['child_categories']) > 0 || count($category['composeProduct']) > 0)
					<div class="container-fluid top-set product_container top_category_id_{{$categoryType->id}}" style="<?php if($counter == 1) echo "display:block"; else echo "display:none"; ?>"  id="menu_{{$category->id}}">
						<div class="row cab">
							<div class="col-md-12 text-center">
								<h1> {{$category->name}} </h1>
							</div>
						</div>
						<div class="row newsec">
							@foreach($category['products_arr'] as $product)
								<div class="col-md-4">
									<a  style="display:none;" href="javascript:void(0);" data-toggle="tooltip" title="test description" class="style-info"><i class="fas fa-info-circle"></i></a>
									<div class="single-menu row menu_item_outer" data-id="{{$product->id}}">
										<div class="col-xs-3"  align="center">
											@if(isset($product->product_image))
												<img class="otg"
													 src="{{ asset('uploads/products/'.$product->product_image ) }}"
													 alt=""/>
											@else
												<img class="card-img-top" style="width: 80px;"
													 src="{{  $logo }}"
													 alt=""/>
											@endif
										</div>
										<div class="col-xs-9">
											<div class="title-wrap">
												<h4>{{$product->product_name}}</h4>
												<h4 class="price">
													@if(isset($product->price) AND $product->price > 0)
														{{$currency}}
														{{$product->price}}
													@endif
												</h4>
												<p>
													{{$product->product_description}}
												</p>
											</div>
										</div>
									</div>
								</div>
							@endforeach
							@if(count($category['child_categories']) > 0)
								@foreach($category['child_categories'] as $child_category)
									@if(count($child_category['products_arr']) > 0)
										<div class="col-md-12 mar-set" id="sub_menu_{{$child_category->id}}">
											<h1 class="back-set"> {{$child_category->name}} </h1>
										</div>

										@foreach($child_category['products_arr'] as $product1)
											<div class="col-md-4">
												<a  style="display:none;" href="javascript:void(0);" data-toggle="tooltip" title="test description" class="style-info"><i class="fas fa-info-circle"></i></a>
												<div class="single-menu row menu_item_outer" data-id="{{$product1->id}}">
													<div class="col-xs-3"  align="center">

														@if(isset($product1->product_image))
															<img class="otg"
																 src="{{ asset('uploads/products/'.$product1->product_image ) }}"
																 alt=""/>
														@else
															<img class="card-img-top" style="width: 80px;"
																 src="{{  $logo }}"
																 alt=""/>
														@endif

													</div>
													<div class="col-xs-9">
														<div class="title-wrap">
															<h4>{{$product1->product_name}}</h4>
															<h4 class="price">
																@if(isset($product1->price) AND $product1->price > 0)
																	{{$currency}}
																	{{$product1->price}}
																@endif
															</h4>
															<p>
																{{$product1->product_description}}
															</p>
														</div>
													</div>
												</div>
											</div>
										@endforeach
									@endif
								@endforeach
							@endif

							@foreach($category['composeProduct'] as $composeProduct)
								<div class="col-md-4">
									<a  style="display:none;" href="javascript:void(0);" data-toggle="tooltip" title="test description" class="style-info"><i class="fas fa-info-circle"></i></a>
									<div class="single-menu row composer_item_outer" data-id="{{$composeProduct['id']}}"  data-type="{{$composeProduct['type']}}">
										<div class="col-xs-3"  align="center">

											@if(isset($composeProduct['image']))
												<img class="otg"
													 src="{{ asset('uploads/products/'.$composeProduct['image'] ) }}"
													 alt=""/>
											@else
												<img class="card-img-top" style="width: 80px;"
													 src="{{  $logo }}"
													 alt=""/>
											@endif

										</div>
										<div class="col-xs-9">
											<div class="title-wrap">
												<h4>{{$composeProduct['name']}}</h4>
												<h4 class="price">
													@if(isset($composeProduct->composeQuantites[0]->price) AND $composeProduct->composeQuantites[0]->price > 0)
														{{$currency}}
														{{$composeProduct->composeQuantites[0]->price}}
													@endif
												</h4>
												<p>
													{{$composeProduct->description}}
												</p>
											</div>
										</div>
									</div>
								</div>

							@endforeach
						</div>
					</div>
				@endif

			@endforeach
			<?php $counter++; ?>
		@endif
	@endforeach
@endif



<div class="modal fade" id="cartModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content modalwidth">
		</div>
	</div>
</div>

<div class="modal fade" id="cartModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content modalwidth" id="product_detail_popup">
		</div>
	</div>
</div>
<?php /*
<div class="modal fade " id="cookieModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body  set-check row">
				<div class="col-md-9">
					<h4 class="">En poursuivant votre navigation sur ce site, vous acceptez l’utilisation :</h4>

					<div class="label-setter">

				<label>
					<input type="checkbox" class="mr-2">d’un cookie qui vous permettra e d’obtenir la statut de vos commandes
					<span class="checkmark"></span>
				</label>

				<label>
					<input type="checkbox" class="mr-2">de la géolocalisation temporaire de votre appareil pour vous indiquer les établissements E-menew de proximité et permettre la livraison
					<span class="checkmark "></span>
				</label>
					</div>
				</div>
				<div class="col-md-3 text-center">
				    <a href="#" class="btn btn-lg btn-outline-info text-light btn-lg-sm  text-capitalize mr-2" data-dismiss="modal">ACCEPTER</a>
				<a href="#" class="btn btn-lg btn-outline-danger btn-lg-sm   text-capitalize" data-dismiss="modal">REFUSER</a>
				</div>
		</div>
	</div>
</div>
</div> */ ?>
@include('partials.restaurant-site.footer')
@include('partials.restaurant-site.footer-js')
@include('partials.restaurant-site.common_js')
<script type="text/javascript">
	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		items: 1,
		responsiveClass:true,
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		responsive:{
			0:{
				items:1,
				autoplayTimeout:2000,
				nav:false,
				dots: true,
			},
			600:{
				items:3,
				autoplayTimeout:2000,
				nav:false,
				slideBy:1,

			},
			1000:{
				items:5,
				autoplayTimeout:2000,
				nav:true,
				loop:true
			}
		}
	})


	//--------------------------------------------------------------------
	///$(document).ready(function() {
		//$('#cookieModal').modal('show');
	//});
	//--------------------------------------------------------------------
	$(document).ready(function () {
		$(document).on("scroll", onScroll);
	});

	function onScroll(event){
		var scrollPos = $(document).scrollTop() + 150;
		$('.center .slick-track a').each(function () {
			var currLink = $(this);
			var refElement = $(currLink.attr("href"));
			var height = refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos;
			//console.log(height);
			if (height) {
				parent_id = $(this).data('parent');
				if($('#menu_'+parent_id).css('display') == 'block')
				{
					$('.center .slick-track a').removeClass("active");
					console.log($(this).attr("data-parent"));
					currLink.addClass("active");
					var slideIndex = $('.center .slick-track a.active').attr('data-slick')-1;
					console.dir(currLink);
					$('.center').slick('slickGoTo', parseInt(slideIndex), false);

					//$('.center2.slider').css('display','none');
					//var parent = $(this).attr('data-parent');
					//console.log(parent);
					/*
                    if(parent) {
                        $('#sub_menu_'+parent).css('display','block');
                    } else {
                    } */
				}
			}
			else{
				//console.log('elseeeeeee');
				currLink.removeClass("active");
				//$( ".slick-prev.slick-arrow" ).trigger( "click" );
			}
		});
	}

</script>


</body>
</html>
