<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
	 @include('partials.restaurant-site.headers-style')
</head>
	<body>
		@include('partials.restaurant-site.top-navbar') 
		<?php /*@include('partials.restaurant-site.breadcrums')  */ ?>
		<?php
			$restaurantDetail = Session::get('restaurantDetail');
			//echo "<pre>";
			//print_r($restaurantDetail);
			//exit;
		?>
		<main class="card-space">
			<div class="container wow fadeIn">
		<!-- Start contact-page Area -->
		<section class="">

				<div class="row justify-content-center">

						<?php /*<div class="map-wrap" style="width:100%; height: 445px;" id="map"></div> */ ?>
						

							
						<div class="col-md-8 space-set">
							<div class="card booking-set">
							@include('partials.flash-message')
							<form action="<?php echo route('booking-submitted', ['id' => $restaurantDetail->restaurant_code]); ?>" class="card-body" method="post" id="login-form"> 
								 {{ csrf_field() }}
								<h3 class="h3 mb-4">Réservation</h3>
								 <div class="form-row">
									<div class="form-group col-md-6 md-form">
									  <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Prénom" value="{{old('first_name')}}">
									</div>
									<div class="form-group col-md-6 md-form">
									  <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Nom de famille" value="{{old('last_name')}}">
									</div>
								  </div> 
								  
								   <div class="form-row">
									<div class="form-group col-md-6 md-form">
										<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{old('email')}}">
									</div>
									<div class="form-group col-md-6 md-form">
										<input type="tel" class="form-control" id="phone_number" name="phone_number" placeholder="Téléphone" value="{{old('phone_number')}}">
									</div>
								  </div>
								  
								   <div class="form-row">
										<div class="form-group col-md-6 md-form">
									<?php
//											$start = '12:00AM';
//											$end = '11:59PM';
//											$interval = '+1 hour';

											// $interval = '+30 minutes';
//											 $interval = '+15 minutes';

//											$start_str = strtotime($start);
//											$end_str = strtotime($end);
//											$now_str = $start_str;

//											echo '<select class="form-control" name="_time">';
//											echo '<option value="">Please select time</option>';
//											while($now_str <= $end_str){
//											echo '<option value="' . date('h:i A', $now_str) . '">' . date('h:i A', $now_str) . '</option>';
//												$now_str = strtotime($interval, $now_str);
//											}
//											echo '</select>';
//										?>
										<input  class="form-control" id="datetimepicker" name="datetimepicker" type="text" placeholder="Date/Time">
										</div>

									<div class="form-group col-md-6 md-form">
									  <input type="text" class="form-control" placeholder="Nombre de personnes" name="no_of_people" id="no_of_people" value="{{old('no_of_people')}}" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
									</div>
								  </div>
								   <div class="form-group md-form">
										<textarea class="form-control" id="comments" name="comments" placeholder="Commentaires" style="height: 120px;">{{old('comments')}}</textarea>
								  </div>
								  <button type="submit" class="link-v1 rt btn btn-lg btn-outline-info text-light text-capitalize btn-block mb-2">Réservation</button>
							</div>
							</form>
						</div>
					</div>

		</section>
		<!-- End contact-page Area -->
			</div>
		</main>
		@include('partials.restaurant-site.footer')
		@include('partials.restaurant-site.footer-js')	
		@include('partials.restaurant-site.common_js') 
		<script type="text/javascript">
			$(document).ready(function () {
				  //elem = document.getElementById("date_time")
				  // var today = new Date().toISOString().split('T')[0];
				//document.getElementsByName("date_time")[0].setAttribute('min', today);
			});


		</script>
	</body>
</html>