	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		 @include('partials.restaurant-site.headers-style')
	</head>
		<body>
        <header id="header">
            <div class="container main-menu">
                <div class="row align-items-center  d-flex">
                    <a class="navbar-brand" href="#"><img src="{{asset('assets/restraurant-site/img/logo.png')}}"></a>
                    <i class="fas fa-bell ml-auto bell" style=""></i>
                    <a>
                        <i class="fas fa-shopping-cart cart-set"></i>
                        <span class="badge style-set" > 6 </span>
                    </a>
                    <nav id="nav-menu-container" class="ml-auto">
                        <ul class="nav-menu">
                            <li><a href="home">Accueil</a></li>
                            <li><a href="about">À propos de</a></li>
                            <li><a href="contact-us">Contact</a></li>
                        </ul>
                    </nav><!-- #nav-menu-container -->
                </div>
            </div>
        </header><!-- #header -->
			<!-- start banner Area -->
			<section class="relative about-banner">	
				<div class="overlay overlay-bg"></div>
				<div class="container">				
					<div class="row d-flex align-items-center justify-content-center">
						<div class="about-content col-lg-12">
							<h1 class="text-white">
								Check Out				
							</h1>	
							<p class="text-white link-nav"><a href="home">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="check-out"> check out</a></p>
						</div>	
					</div>
				</div>
			</section>
			<!-- End banner Area -->				  

			
<section>
	

  <!--Main layout-->
  <main class="">
    <div class="container wow fadeIn">

      <!-- Heading -->
      <h1 class="h1 check-h">Formulaire de commande</h1>
        <div class="row mb-3">
            <button class="btn btn-lg btn-info  text-capitalize ml-3" type="submit">
            S'inscrire maintenant
           </button>
            <button class="btn btn-lg btn-outline-info text-light text-capitalize ml-3 " type="submit">
                Se connecter
            </button>
        </div>
      <!--Grid row-->
      <div class="row">

        <!--Grid column-->
        <div class="col-md-8 mb-4">

          <!--Card-->
          <div class="card">

            <!--Card content-->
            <form class="card-body">

              <!--Grid row-->
              <div class="row">

                <!--Grid column-->
                <div class="col-md-6 ">

                  <!--firstName-->
                  <div class="md-form ">
                    <input type="text" id="firstName" class="form-control with1" placeholder=" Prénom">
 
                  </div>

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-md-6">

                  <!--lastName-->
                  <div class="md-form ">
                    <input type="text" id="lastName" class="form-control" placeholder="Nom de famille">
                  </div>
                </div>
                <!--Grid column-->

              </div>
              <!--Grid row-->

              <!--email-->
              <div class="md-form">
                <input type="text" id="email" class="form-control" placeholder="Email ">
              </div>

              <!--address-->
              <div class="md-form">
                <input type="date" id="address" class="form-control" placeholder="Date de naissance">
              </div>

              <div class="custom-control custom-checkbox md-form">
                <input type="checkbox" class="custom-control-input" id="save-info">
                <label class="custom-control-label" for="save-info">Enregistrez ces informations pour la prochaine fois</label>
              </div>

              <hr>

              <div class="d-block my-3">
                <div class="custom-control custom-radio">
                  <input id="credit" name="paymentMethod" type="radio" class="custom-control-input" checked required>
                  <label class="custom-control-label" for="credit">Credit card</label>
                </div>
                <div class="custom-control custom-radio">
                  <input id="debit" name="paymentMethod" type="radio" class="custom-control-input" required>
                  <label class="custom-control-label" for="debit">Debit card</label>
                </div>
                <div class="custom-control custom-radio">
                  <input id="paypal" name="paymentMethod" type="radio" class="custom-control-input" required>
                  <label class="custom-control-label" for="paypal">Paypal</label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6  md-form">
                  <label for="cc-name">Name on card</label>
                  <input type="text" class="form-control" id="cc-name" placeholder="Name" required>
                  <small class="text-muted">Full name as displayed on card</small>
                  <div class="invalid-feedback">
                    Name on card is required
                  </div>
                </div>
                <div class="col-md-6  md-form">
                  <label for="cc-number">Credit card number</label>
                  <input type="text" class="form-control" id="cc-number" placeholder="**** **** ***** ***" required>
                  <div class="invalid-feedback">
                    Credit card number is required
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-3  md-form">
                  <label for="cc-expiration">Expiration</label>
                  <input type="text" class="form-control" id="cc-expiration" placeholder="8/20" required>
                  <div class="invalid-feedback">
                    Expiration date required
                  </div>
                </div>
                <div class="col-md-3  md-form">
                  <label for="cc-expiration">CVV</label>
                  <input type="text" class="form-control" id="cc-cvv" placeholder="XXXX" required>
                  <div class="invalid-feedback">
                    Security code required
                  </div>
                </div>
              </div>
              <hr class="mb-4">
              <button class="btn btn-lg btn-outline-info text-light text-capitalize btn-block" type="submit">Continue to checkout</button>

            </form>

          </div>
          <!--/.Card-->

        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-4 mb-4">

          <!-- Heading -->
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Votre panier</span>
            <span class="badge badge-secondary badge-pill">{{ Cart::count() }}</span>
          </h4>

          <!-- Cart -->
          <ul class="list-group mb-3 z-depth-1">
            <?php foreach(Cart::content() as $row) : ?>
				<li class="list-group-item d-flex justify-content-between lh-condensed">
				  <div>
					<h6 class="my-0">{{$row->name}}</h6>
					<small class="text-muted">Brief description</small>
				  </div>
				  <span class="text-muted">$<?php echo $row->total; ?></span>
				</li>
			 <?php endforeach;?>
			 
            <li class="list-group-item d-flex justify-content-between">
              <span>Total (USD)</span>
              <strong>$<?php echo Cart::total(); ?></strong>
            </li>
          </ul>
          <!-- Cart -->

          <!-- Promo code -->
          <form class="card p-2">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Promo code" aria-label="Recipient's username" aria-describedby="basic-addon2">
              <div class="input-group-append">
                <button class="btn btn-secondary btn-md waves-effect m-0" type="button">Redeem</button>
              </div>
            </div>
          </form>
          <!-- Promo code -->

        </div>
        <!--Grid column-->

      </div>
      <!--Grid row-->

    </div>
  </main>
  <!--Main layout-->

  

</section>
			
			
			<!-- End contact-page Area -->

			@include('partials.restaurant-site.footer')
			 @include('partials.restaurant-site.footer-js')	
		</body>
	</html>