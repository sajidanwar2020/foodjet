@extends('layouts.mainlayout')
@section('content')
    <div class="container margin_60_35">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="box_account">
                    @include('partials.flash-message')
                    <div class="form_container">
                        <h3 class="client">Reset your account password</h3>
                        <form action="{{ route('reset_update_password') }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="token" value="{{ $token }}">
                            </div>
                            <div class="form-group">
                                <div class="hideShowPassword-wrapper" style="position: relative; display: block; vertical-align: baseline; margin: 0px;">
                                    <input type="password" class="form-control" name="password" id="password"  placeholder="Password*" style="margin: 0px; padding-right: 51.1111px;">
                                    <button type="button" role="button" aria-label="Show Password" title="Show Password" tabindex="0" class="my-toggle hideShowPassword-toggle-show" aria-pressed="false"
                                            style="position: absolute; right: 0px; top: 50%; margin-top: -15px; display: none;">
                                        Show
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="hideShowPassword-wrapper" style="position: relative; display: block; vertical-align: baseline; margin: 0px;">
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm password*" style="margin: 0px; padding-right: 51.1111px;">
                                    <button type="button" role="button" aria-label="Show Password" title="Show Password" tabindex="0" class="my-toggle hideShowPassword-toggle-show" aria-pressed="false"
                                            style="position: absolute; right: 0px; top: 50%; margin-top: -15px; display: none;">
                                        Show
                                    </button>
                                </div>
                            </div>
                            <div class="text-center">
                                <input type="submit" value="Reset password" class="btn_1 full-width">
                            </div>
                        </form>
                        <div class="divider"><span>Or</span></div>
                        <div class="text-center">
                            Already have an account?? <a href="{{ route('signin') }}">Sign in</a>
                        </div>
                    </div>
                    <!-- /form_container -->
                </div>
                <!-- /box_account -->
                <!-- /row -->
            </div>
            <div class="col-xl-5 col-lg-6 text-right d-none d-lg-block">
                <div class="about-tryngo">
                    <img src="{{ asset('/assets/img/graphic_home_2.svg') }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>


@endsection
