<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container">
        <div class="title-text-v2">
            <h3>Profile</h3>
        </div>
        <div class="featured-products home_2 new-arrivals lastest">

            <div class="tab-container space-10">
                <div id="tab_0" class="tab-contenta">
                    <div class="products hover-shadow ver2 border-space-product">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="card-body">
                                    @include('partials.flash-message')

                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link" id="b2c-tab" href="{{ route('customer_signup') }}">Private client</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" id="b2b-tab" href="#">Business customer</a>
                                        </li>
                                    </ul>
                                    <form action="{{ route('vendor_submitted') }}" method="post" id="form_signup">
                                        @if($errors->any())
                                            @foreach($errors->all() as $error)
                                                <span style="color: red">{{ @"*".$error }}</span><br>
                                            @endforeach
                                        @endif
                                        {{ csrf_field() }}
                                        <input type="hidden"  name="user_type" value="3">
                                        <div class="form-header">
                                            <h2>Sign Up business customer</h2>
                                        </div>
                                        <div class="form-group">
                                            <label>Salutation*</label>
                                            <select name="salutation" class="custom-select form-control"  aria-invalid="false">
                                                <option value="" selected="">Please select ...</option>
                                                <option value="MRS">Mrs</option>
                                                <option value="MR">Mr</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>First name*</label>
                                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name*" value="{{ old('first_name') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Last name*</label>
                                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name*" value="{{ old('last_name') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Email Address*</label>
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Email*" value="{{ old('email') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Company*</label>
                                            <input type="text" class="form-control" name="company" required="required" value="{{ old('company') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Tax id*</label>
                                            <input type="text" class="form-control" name="tax_id" required="required" value="{{ old('tax_id') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Street*</label>
                                            <input type="text" class="form-control" name="street" required="required" value="{{ old('street') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>  House number*</label>
                                            <input type="text" class="form-control" name="house_number" required="required" value="{{ old('house_number') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Post code*</label>
                                            <input type="text" class="form-control" name="post_code" required="required" value="{{ old('post_code') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Location*</label>
                                            <input type="text" class="form-control" name="location" id="location" placeholder="Location*" value="{{ old('location') }}">
                                            <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ old('gmap_latitude') }}">
                                            <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ old('gmap_longitude') }}">
                                            <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ old('country_iso') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Country*</label>
                                            <select name="country" class="custom-select form-control"  aria-invalid="false">
                                                <option value="1">Germany</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Date of birth*</label>
                                            <input type="text" class="form-control" name="dob" id="dob" required="required" readonly value="{{ old('dob') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Mobile number*</label>
                                            <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ old('phone_number') }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Password*</label>
                                            <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" placeholder="Password*">
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm Password*</label>
                                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password') }}" placeholder="Confirmation password*">
                                        </div>


                                        <div class="form-group">
                                            <label class="checkbox-inline"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block btn-lg">Sign Up</button>
                                        </div>
                                    </form>
                                    <div class="text-center small">Already have an account? <a href="#">Login here</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <?php /*
                <div id="tab_2" class="tab-content">
                    <div class="products hover-shadow ver2 border-space-product">
                        <div class="product hover-shadow">
                            <div class="product-images">
                                <a href="#" title="product-images">
                                    <img class="primary_image"
                                         src="{{asset('assets/site/images/products/featured/1.jpg')}}" alt=""/>
                                </a>
                                <div class="action">
                                    <a class="add-cart" href="#" title="Add to cart"></a>
                                    <a class="wish" href="#" title="Wishlist"></a>
                                    <a class="zoom" href="#" title="Quick view"></a>
                                </div>
                                <!-- End action -->
                            </div>
                            <a href="#" title="Union Bed"><p class="product-title">Union Bed</p></a>
                            <p class="product-price-old">$700.00</p>
                            <p class="product-price">$350.00</p>

                        </div>
                        <!-- End item -->

                        <!-- End item -->
                    </div>
                    <!-- End product-tab-content products -->
                </div>
                <!-- End tab-content -->
                <div id="tab_3" class="tab-content">
                    <div class="products ver2 hover-shadow border-space-product">
                        <div class="product">
                            <div class="product-images">
                                <a href="#" title="product-images">
                                    <img class="primary_image"
                                         src="{{asset('assets/site/images/products/featured/4.jpg')}}" alt=""/>
                                </a>
                                <div class="action">
                                    <a class="add-cart" href="#" title="Add to cart"></a>
                                    <a class="wish" href="#" title="Wishlist"></a>
                                    <a class="zoom" href="#" title="Quick view"></a>
                                </div>
                                <!-- End action -->
                            </div>
                            <a href="#" title="Union Bed"><p class="product-title">Union Bed</p></a>
                            <p class="product-price-old">$700.00</p>
                            <p class="product-price">$350.00</p>
                        </div>


                        <!-- End item -->
                    </div>
                    <!-- End product-tab-content products -->
                </div>
                <!-- End tab-content --> */ ?>

            </div>
        </div>
        <div class="box center space-padding-tb-30 space-30" style="display: none">
            <a class="link-v1 color-brand font-300" href="#" title="View All">View All</a>
        </div>
    </div>

    <div id="back-to-top" style="display: none">
        <i class="fa fa-long-arrow-up"></i>
    </div>
    @include('partials.footer')
</div>

@include('partials.footer-js')
@include('partials.common_js')
</body>
</html>












