<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container">
                <div class="page-login box space-50">
                    <div class="row">
                        <div class="col-md-10 sign-in space-30">
                            <h3>Profile</h3>
                                    @include('partials.flash-message')
                                    <form action="{{ route('profile_update') }}" method="post" id="form_signup">
                                        {{ csrf_field() }}

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="group box space-20">
                                                    <label class="control-label">Salutation*</label>
                                                    <select name="salutation" class="custom-select form-control"  aria-invalid="false">
                                                        <option value="" selected="">Please select ...</option>
                                                        <option value="MRS" {{ $info->salutation == "MRS" ? 'selected':'' }}>Mrs</option>
                                                        <option value="MR" {{ $info->salutation == "MR" ? 'selected':'' }}>Mr</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="group box">
                                                  <label class="control-label">First name*</label>
                                                  <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name*" value="{{ isset($info->first_name) ? $info->first_name : '' }}">
                                                 </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="group box space-20">
                                                     <label class="control-label">Last name*</label>
                                                     <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name*" value="{{ isset($info->last_name) ? $info->last_name : '' }}">
                                                 </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="group box">
                                                      <label class="control-label">Email Address*</label>
                                                      <input type="email" readonly class="form-control" name="email" id="email" placeholder="Email*" value="{{ isset($info->email) ? $info->email : '' }}">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="group box space-20">
                                                     <label class="control-label">Street*</label>
                                                     <input type="text" class="form-control" name="street" required="required" value="{{ isset($info->street) ? $info->street : '' }}">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="group box">
                                                     <label class="control-label">  House number*</label>
                                                    <input type="text" class="form-control" name="house_number" required="required" value="{{ isset($info->house_number) ? $info->house_number : '' }}">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="group box space-20">
                                                     <label class="control-label">Post code*</label>
                                                     <input type="text" class="form-control" name="post_code" required="required" value="{{ isset($info->post_code) ? $info->post_code : '' }}">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="group box">
                                                    <label class="control-label">Location*</label>
                                                    <input type="text" class="form-control" name="location" id="location" placeholder="Location*" value="{{ isset($info->location) ? $info->location : '' }}">
                                                    <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ isset($info->latitude) ? $info->latitude : '' }}">
                                                    <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ isset($info->longitude) ? $info->longitude : '' }}">
                                                    <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ isset($info->country) ? $info->country : '' }}">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="group box space-20">
                                                    <label class="control-label">Country*</label>
                                                    <select name="country" class="custom-select form-control"  aria-invalid="false">
                                                        <option value="1">Germany</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="col-sm-6">
                                                <div class="group box space-20">
                                                    <label class="control-label">Mobile number*</label>
                                                    <input type="text" class="form-control"  readonly name="mobile_number" id="mobile_number" placeholder="Mobile number" value="{{ isset($info->phone_number) ? $info->phone_number : '' }}">
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="group box">
                                                    <button type="submit" class="btn btn-primary btn-block btn-lg">Update profile</button>
                                                </div>
                                            </div>

                                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.footer')
</div>

@include('partials.footer-js')
@include('partials.common_js')
</body>
</html>












