<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <link href="{{asset('assets/site/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/css/style.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/vendor/settings.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/images/favicon.png')}}" rel="shortcut icon"/>
    <script type="text/javascript" src="{{asset('assets/site/js/jquery-3.2.0.min.js')}}"></script>
    <title>Foodjet</title>
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
@include('partials.top-navbar')
    <!-- End banner -->
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container">
                <div class="page-login box space-50">
                    <div class="row">
                        <div class="col-md-10 sign-in space-30">
                            <h3>Sign Up</h3>
                            <!-- End social -->
                                <form action="{{ route('customer_submitted') }}" method="post" id="form_signup" class="form-horizontal">
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            <span style="color: red">{{ @"*".$error }}</span><br>
                                        @endforeach
                                    @endif
                                    {{ csrf_field() }}
                                    <input type="hidden" name="user_type" value="2">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="salutation">Salutation*</label>
                                            <select name="salutation" id="salutation" class="custom-select form-control is_required"
                                                    aria-invalid="false">
                                                <option value="" selected="">Please select ...</option>
                                                <option value="MRS">Mrs</option>
                                                <option value="MR">Mr</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="email">Email Address*</label>
                                            <input class="form-control is_required email" name="email" id="email" placeholder="Email Address" type="text" value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="first_name">First name*</label>
                                            <input class="form-control is_required" name="first_name" id="first_name" placeholder="First name" type="text" value="{{ old('first_name') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="last_name">Last name*</label>
                                            <input class="form-control is_required" name="last_name" id="last_name" placeholder="Last name" type="text" value="{{ old('last_name') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="street">Street*</label>
                                            <input class="form-control is_required" name="street" id="street" placeholder="Street" type="text" value="{{ old('street') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="house_number">House number*</label>
                                            <input type="text" class="form-control is_required" name="house_number" id="house_number" placeholder="House number"  value="{{ old('house_number') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="street">Post code*</label>
                                            <input type="text" class="form-control is_required" name="post_code" id="post_code"  placeholder="Post code" value="{{ old('post_code') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="location">Location*</label>
                                            <input type="text" class="form-control is_required" name="location" id="location"  placeholder="Location*" value="{{ old('location') }}">
                                            <input type="hidden" class="form-control" name="gmap_latitude"  id="gmap_latitude" value="{{ old('gmap_latitude') }}">
                                            <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ old('gmap_longitude') }}">
                                            <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ old('country_iso') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="country">Country*</label>
                                            <select name="country" class="custom-select form-control"
                                                    aria-invalid="false">
                                                <option value="1">Germany</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="phone_number">Mobile number*</label>
                                            <input type="text" class="form-control is_required" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ old('phone_number') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="password">Password*</label>
                                            <input type="password" class="form-control is_required password" name="password" id="password"  required="required" value="{{ old('password') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <label class="control-label" for="confirm_password">Confirm Password*</label>
                                            <input type="password" class="form-control is_required confirm_password"  id="confirm_password" name="confirm_password" value="{{ old('confirm_password') }}">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="group box space-20">
                                            <div class="remember">
                                                <input id="remeber" name="check" type="checkbox" value="remeber" class="is_required-- chkbox--">
                                                <label class="label-check" for="remeber">I agree to the terms and conditions and data protection provisions.</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>



                                <button class="link-v1 rt" type="submit">Create an account</button>
                                <a href="{{ route('signin') }}" class="btn btn-link btn-have-acc ">Already have an account?</a>
                            </form>
                            <!-- End form -->
                        </div>


                        <!-- End col-md-6 -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')
</div>

@include('partials.footer-js')
@include('partials.common_js')
<script>

$(document).ready(function(e) {
    $(document).on('click', 'form button[type=submit]', function(e) {
        var input_field = $('form').find('.is_required');
        var error = false;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var number = /^[0-9]+$/;

        $.each(input_field, function (){
            //alert($(this).val());
            $(this).next('span.msg_error').remove();

            if ($(this).val() == '')
            {
                $(this).after( "<span class='msg_error'>This field is required!</span>" );
                $(this).css({border: '1px solid red'});
                error = true;

            }else if ($(this).hasClass('chkbox')){

                if (!$(this).is(':checked')){
                    $(this).after( "<span class='msg_error'>This field is required!</span>" );
                    error = true;
                }

            }else if( $(this).hasClass('email') )
            {
                if (!emailReg.test($(this).val()))
                {
                    $(this).after( "<span class='msg_error'>Email format is wrong!</span>" );
                    $(this).css({border: '1px solid red'});
                    error = true;
                } else {
                    $(this).css({border: '1px solid #d5d5d5'});
                    $(this).next('span.msg_error').remove();
                }

            } else{
                $(this).css({border: '1px solid #d5d5d5'});
                $(this).next('span.msg_error').remove();

            }
        });

        if( ($('#password').val() != "" ) &&  ($('#confirm_password').val() != "" ) )
        {
            if (($('#password').val() != $('#confirm_password').val() ) )
            {
                $('#confirm_password').after( "<span class='msg_error'>Password not matching!</span>" );
                $('#confirm_password').css({border: '1px solid red'});
                error = true;
            } else {
                $('#confirm_password').css({border: '1px solid #d5d5d5'});
                $('#confirm_password').next('span.msg_error').remove();
            }
        }

        if (error)
        {
            return false;
        }else{
            return true;
        }


     });

});

</script>
</body>
</html>

