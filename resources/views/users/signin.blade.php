<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
@include('partials.top-navbar')
    <!-- End banner -->
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container container-ver2--">
                <div class="page-login box space-50">
                    <div class="row">
                        <div class="col-md-6 sign-in space-30">
                            <h3>Sign in</h3>
                            <p>Hello, welcome to Foodjet.</p>

                            <!-- End social -->
                                <form action="{{ route('signin_submitted',['prev'=>url()->previous()]) }}" class="form-horizontal" method="post" id="login-form">
                                @include('partials.flash-message')
                                    {{ csrf_field() }}
                                    <input type="hidden"  name="user_type" value="3">
                                <div class="group box space-20">
                                    <label class="control-label" for="email">EMAIL *</label>
                                    <input class="form-control" name="email" id="email"  placeholder="Your email" type="text" value="{{ old('email') }}">
                                </div>
                                <div class="group box">
                                    <label class="control-label" for="password">PASSWORD *</label>
                                    <input class="form-control" name="password" id="password" required="required" placeholder="Password" type="password" value="{{ old('password') }}">
                                </div>
                                <div class="remember">
                                    <input id="remeber" name="check" type="checkbox" value="remeber">
                                    <label class="label-check" for="remeber">Remember me!</label>
                                    <a class="help"  href="#" id="forgot_password" title="help ?">Fogot your password?</a>
                                </div>
                                <button class="link-v1 rt" type="submit">LOGIN NOW</button>
                                    <div class="show-hider"><strong>or</strong></div>
                                <a class="link-v1 rt pull-right" href="{{ route('customer_signup') }}">Sign UP >></a>
                            </form>
                            <!-- End form -->
                        </div>
                        <div class="col-md-6 login-img">
                            <img src=" {{ asset('/assets/site/images/home1-images-banner1-2.png') }}" alt="">
                        </div>

                        <!-- End col-md-6 -->

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.footer')
</div>

<script src="{{ asset('/assets/site/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/site/js/jquery.mousewheel.min.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $("#forgot_password").click(function (event) {
            Swal.fire({
                title: 'Enter your email',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Search',
                showLoaderOnConfirm: true,
                inputValidator: (value) => {
                    if (value === '' || !isEmail(value)) {
                        return 'Please enter your valid email'
                    }
                }, preConfirm: (login) => {
                    $.ajax({
                        method: "GET",
                        url: "{{ route('forgot_password') }}",
                        data: {email: login}
                    }).done(function (data) {
                        if (data === '1') {
                            Swal.fire(
                                'Sent!',
                                'Password reset link is sent on your registered email!',
                                'success'
                            )
                        } else {
                            alert(data);
                        }
                    });
                    return false;
                }
            })
        });

    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>
</body>
</html>



























