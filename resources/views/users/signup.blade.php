<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		 @include('partials.restaurant-site.headers-style')
	</head>
		<body>	
			@include('partials.restaurant-site.top-navbar') 
			@include('partials.restaurant-site.breadcrums') 
			@include('partials.restaurant-site.common_js') 
			<section style="background-image: url('img/hero-bg.png');">


				<!--Main layout-->
				<main class="card-space">
					<div class="container wow fadeIn">

						<!-- Heading -->

						<!--Grid row-->
						<div class="row justify-content-center">

							<!--Grid column-->
							<div class="col-md-6 space-set">

								<!--Card-->
								<div class="card" >

									@include('partials.flash-message') 
									<!--Card content-->
									<form class="card-body" action="{{ route('customer_submitted') }}" method="post">
										{{ csrf_field() }}
										<input type="text" name="gmap_latitude" id="gmap_latitude" hidden>
										<input type="text" name="gmap_longitude" id="gmap_longitude" hidden>
										<input type="text" name="location" id="location" hidden>
										<h3 class="h3 mb-4">FORMULAIRE D'INSCRIPTION</h3>
										<!--Grid row-->
										<div class="row">

											<!--Grid column-->
											<div class="col-md-6 ">

												<!--firstName-->
												<div class="md-form ">
													<input type="text" id="firstName" class="form-control with1" placeholder="First Name" name="first_name" value="{{ old('first_name') }}">
												</div>

											</div>
											<!--Grid column-->

											<!--Grid column-->
											<div class="col-md-6">

												<!--lastName-->
												<div class="md-form">
													<input type="text" id="lastName" class="form-control" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}">
												</div>
											</div>
											<!--Grid column-->

										</div>
										<!--Grid row-->

										<!--email-->
										<div class="md-form">
											<input type="email" id="email" class="form-control" placeholder="Email " name="email" value="{{ old('email') }}">
										</div>

										<div class="md-form">
											<input type="text" id="phone" class="form-control" placeholder="Phone Number" name="phone_number" value="{{ old('phone_number') }}">
										</div>


										<!--address-->
										<!-- <div class="md-form">
											<input type="date" id="address" class="form-control" placeholder="Date of Birth">
										</div> -->

										<div class="md-form">
											<input type="password" id="password" class="form-control" placeholder="Password" name="password" value="{{ old('password') }}">
										</div>
										<div class="md-form">
											<input type="password" id="confirm_password" class="form-control" placeholder="Confirm Password" name="confirm_password" value="{{ old('confirm_password') }}">
										</div>
										<?php /*
										<div class="custom-control custom-checkbox md-form">
											<input type="checkbox" class="custom-control-input" id="save-info">
											<label class="custom-control-label" for="save-info">Save this information for next time</label>
										</div> */ ?>
										@php
											$restaurantDetail = Session::get('restaurantDetail');
										@endphp
										<button class="btn btn-lg btn-outline-info text-light text-capitalize btn-block" type="submit">Register</button>
										<div class="md-form" align="center"> Already register click here to
											<a class="link-v1 rt " href="<?php echo route('signinu', ['id' => $restaurantDetail->restaurant_code]); ?>" >login</a>
										</div>

									</form>

								</div>
								<!--/.Card-->

							</div>
							<!--Grid column-->


						</div>
						<!--Grid row-->

					</div>
				</main>
				<!--Main layout-->



			</section>


			@include('partials.restaurant-site.footer')
@include('partials.restaurant-site.footer-js')	

<script>
    $(document).ready(function () {
    	currentLocation();
        $("#forgot_password").click(function (event) {
            Swal.fire({
                title: 'Enter your email',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Search',
                showLoaderOnConfirm: true,
                inputValidator: (value) => {
                    if (value === '' || !isEmail(value)) {
                        return 'Please enter your valid email'
                    }
                }, preConfirm: (login) => {
                    $.ajax({
                        method: "GET",
                        url: "{{ route('forgot_password') }}",
                        data: {email: login}
                    }).done(function (data) {
                        if (data === '1') {
                            Swal.fire(
                                'Sent!',
                                'Password reset link is sent on your registered email!',
                                'success'
                            )
                        } else {
                            alert(data);
                        }
                    });
                    return false;
                }
            })
        });

    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>
</body>
</html>



























