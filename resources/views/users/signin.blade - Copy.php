<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Sign in</title>
<link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/sweetalert2.min.css') }}" rel="stylesheet">


<style type="text/css">
	.login-form {
		width: 340px;
    	margin: 50px auto;
	}
    .login-form form {
    	margin-bottom: 15px;
        background: #f7f7f7;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
    .login-form h2 {
        margin: 0 0 15px;
    }
    .form-control, .btn {
        min-height: 38px;
        border-radius: 2px;
    }
    .btn {
        font-size: 15px;
        font-weight: bold;
    }
</style>
</head>
<body>
<div class="login-form">
       <form action="{{ route('signin_submitted',['prev'=>url()->previous()]) }}" method="post">

	      @include('partials.flash-message')

	    <input type="hidden"  name="user_type" value="3">
         {{ csrf_field() }}
        <h2 class="text-center">Log in</h2>
        <div class="form-group">
			 <input type="email" class="form-control" name="email" id="email" placeholder="Email" required="required" autocomplete="off">
        </div>
        <div class="form-group">
			<input type="password" class="form-control" name="password" id="password" required="required" placeholder="Password*" autocomplete="off">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Log in</button>
        </div>
        <div class="clearfix">
            <label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
            <a href="#" id="forgot_password" class="pull-right">Lost Password?</a>
        </div>
    </form>
    <p class="text-center"><a href="{{ route('customer_signup') }}"> Sign Up as customer</p>
</div>

<script src="{{ asset('/assets/js//jquery.min.js') }}"></script>
<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/sweetalert2.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $("#forgot_password").click(function (event) {
            Swal.fire({
                title: 'Enter your email',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                showCancelButton: true,
                confirmButtonText: 'Search',
                showLoaderOnConfirm: true,
                inputValidator: (value) => {
                    if (value === '' || !isEmail(value)) {
                        return 'Please enter your valid email'
                    }
                }, preConfirm: (login) => {
                    $.ajax({
                        method: "GET",
                        url: "{{ route('forgot_password') }}",
                        data: {email: login}
                    }).done(function (data) {
                        if (data === '1') {
                            Swal.fire(
                                'Sent!',
                                'Password reset link is sent on your registered email!',
                                'success'
                            )
                        } else {
                            alert(data);
                        }
                    });
                    return false;
                }
            })
        });

    });

    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>


</body>
</html>
