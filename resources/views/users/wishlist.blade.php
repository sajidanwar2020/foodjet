<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    @include('partials.headers-style')
    <link href="{{ asset('/assets/css/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>


<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="cart-box-container">
                <div class="container">
                    <div class="box cart-container">
                        <table class="table cart-table space-80">
                            <thead>
                            <tr>
                                <th class="product-photo"></th>
                                <th class="produc-name">Product Name</th>
                                <th class="produc-price">Price</th>
                                <th class="add-cart">Add to cart</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                             // echo "<pre>";
                             //  print_r($wish_list);
                             // exit;


                            if(count($wish_list) > 0) {
                                foreach($wish_list as $row) :  ?>
                                    <tr class="item_cart" id="{{$row->product_id}}">
                                        <td class="product-photo"><img alt="Futurelife" src="{{ asset('uploads/products/'.findProductImage($row->product_id)  ) }}"></td>
                                        <td class="produc-name"><a href="{{ route('product_detail',['id'=>$row->product_id]) }}" title="">{{$row->product_name}}</a></td>
                                        <td class="produc-price">{{$row->regular_price}} {{$row->weight}}</td>
                                        <td class="add-cart">
                                            <a class="link-v1 lh-50 rt add-cart-btn" href="javascript:void(0)" title="Add to cart" data-id="{{$row->product_id}}"><i class="fa fa-shopping-cart"></i>Shop now</a>
                                            <a class="link-v1 lh-50 rt add-cart-btn remove-wish-btn" href="javascript:void(0)" title="Remove from wishlist" data-id="{{$row->product_id}}"><i class="fa fa-trash-o"></i>Remove</a>
                                        </td>
                                    </tr>
                            <?php
                                endforeach;
                            } else { ?>
                            <tr class="item_cart">
                                <td colspan="4">Wishlist is empty</td>
                            </tr>
                           <?php } ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- End container -->
                </div>
                <!-- End cat-box-container -->
            </div>
        </div>
    </div>
    @include('partials.footer')
</div>
@include('partials.footer-js')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".add-cart-btn", function () {
            _this = $(this);
            var pid = $(this).attr('data-id');
            var remove = 'no';
            if(_this.hasClass('remove-wish-btn')) {
               var remove = 'yes';
           }
            $.ajax('{{ route('add_to_cart_remove_wish') }}', {
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', product_id: pid,remove : remove},
                success: function (data) {
                    //------------------------------------------------------
                    $('tr#'+data).remove();
                    //-------------------------------------------------------
                    var count = $('.wish-list-count').text();
                    $('.wish-list-count').html(count - 1);
                    var count = $('.wish-list-count').text();
                    if (count == 0) {
                        $('.wish-list-count').hide();
                    }
                    //-------------------------------------------------------
                    if(!_this.hasClass('remove-wish-btn')) {
                        var count = $('.cart-count').html();
                        if (count == '')
                            count = 0;
                        $('.cart-count').html(parseFloat(count) + 1);
                        $('.cart-count').show();
                    }
                    //-------------------------------------------------------
                }
            });

        });
        //-------------------------------------------------------------------
    });
</script>
</body>
</html>

