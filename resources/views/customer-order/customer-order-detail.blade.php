<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    @include('partials.headers-style')
    <link href="{{ asset('/assets/css/sweetalert2.min.css') }}" rel="stylesheet">
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
         <div class="main-content">
        <div class="cart-box-container">
            <!-- End container -->
            <div class="container">
                <div class="box cart-container">
                    @if (count($order_detail) > 0)

                        <div class="order_detail_wrapper">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3><b> Location of delivery : </b> {{$order_detail->order_detail['address']}}</h3>
                                </div>
                                <div class="col-sm-3">
                                    <h3><b>Phone :</b> {{$order_detail->order_detail['phone_number']}}</h3>
                                </div>
                                <div class="col-sm-3">
                                    <h3><b>Shiping Cost :</b> {{$order_detail->order_detail['shiping_cost']}}</h3>
                                </div>
                                <div class="col-sm-6">
                                    <?php /*<b>Total Cost :</b> {{$order_detail->order_detail['shiping_cost']}} */ ?>
                                </div>
                            </div>
                        </div>

                        <table class="table cart-table space-30">
                        <thead>
                        <tr>
                            <th>Order#</th>
                            <th>Product name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                            @foreach($order_detail as $order_det)
                                <tr>
                                    <td>{{$order_det->o_id}}</td>
                                    <td> {{$order_det->product_name}}</td>
                                    <td> {{$order_det->quantity}}</td>
                                    <td> {{$order_det->price}}</td>
                                    <td>{{getOrderItemStatus($order_det->item_status)}}</td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else
                        <p>Not found any Orders</p>
                        <p class="return-to-shop">
                            <a class="button wc-backward" href="{{ route('home') }}"> Return to Home</a>
                        </p>
                    @endif
                        <div class="box space-80"></div>
                </div>
                <!-- End container -->
            </div>
            <!-- End cat-box-container -->
        </div>
    </div>
    </div>
</div>
<!-- End wrappage -->
@include('partials.footer')
@include('partials.footer-js')
</body>
</html>

