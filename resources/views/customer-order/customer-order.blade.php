<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    @include('partials.headers-style')
    <link href="{{ asset('/assets/css/sweetalert2.min.css') }}" rel="stylesheet">
    <style>.btn {width: auto !important;border-radius: 4px !important;height: auto !important;}</style>
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
        @include('partials.top-navbar')
      <div class="container content-wrapper">
        <div class="main-content">
            <div class="cart-box-container">
                <!-- End container -->
                <div class="container cont-set">
                    <div class="box cart-container">
                        @if (count($my_orders) > 0)
                            <table class="table cart-table space-30 myorder">
                                <thead>
                                <tr>
                                    <th>Order#</th>
                                    <th>Payment Method</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    <th>Date</th>
                                    <th>Detail</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($my_orders as $my_order)
                                        <tr class="item_cart">
                                            <td>{{$my_order->o_id}}</td>
                                            <td>{{getPaymentMethod($my_order->payment_type)}}</td>
                                            @if($my_order->o_status == 5)
                                                <td>---</td>
                                            @else
                                                <td>€{{ ($my_order->total_price + $my_order->distance_charges ) - $my_order->cancelled_amount }}</td>
                                            @endif
                                            <td>{{getOrderStatus($my_order->o_status)}} </td>
                                            <td>{{$my_order->o_created_at}} </td>
                                            <td>
                                                <?php
                                                    $status_arr = array(4, 5);
                                                ?>
                                                @if($my_order->o_status == 3 AND $my_order->payment_type == 2 AND !in_array($my_order->o_status, $status_arr))
                                                    <a href="{{ route('credits',['order_id'=>$my_order->o_id]) }}">
                                                        Pay Via Paypal |
                                                    </a>
                                                @endif

                                                <a href="{{route('customer_orders_detail',['order_id'=>$my_order->o_id])}}"><i class="fas fa-eye"></i></a>
                                            </td>
                                            <td>

                                            @if($my_order->count_item == $my_order->is_order_processed)
                                                <a href="{{ route('change_main_order_status_by_customer',['order_id'=>$my_order->o_id,'status'=>5]) }}" class=" c_order_cancelled"
                                                   type="warning" msg="Are you sure order to Cancelled?">
                                                    <i class="far fa-times-circle" style="color: red"></i>
                                                </a>
                                            @endif
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        @else
                            <p>Not found any Orders</p>
                            <p class="return-to-shop">
                                <a class="button wc-backward" href="{{ route('home') }}"> Return to Home</a>
                            </p>
                    @endif

                        <div class="box space-80"></div>
                    </div>
                    <!-- End container -->
                </div>
                <!-- End cat-box-container -->
            </div>

        </div>
    </div>
</div>
<!-- End wrappage -->
@include('partials.footer')
@include('partials.footer-js')

<script type="text/javascript">
    $(document).ready(function () {
        $(".c_order_cancelled").click(function (event) {
            event.preventDefault();
            var msg = ($(this).attr('msg'));
            var type = ($(this).attr('type'));
            var url = ($(this).attr('href'));
            Swal.fire({
                title: msg,
                type: type,
                showCancelButton: true,
                cancelButtonText: 'Cancel',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    window.location.href = url;
                }
            })
        });
    });

</script>

</body>
</html>

