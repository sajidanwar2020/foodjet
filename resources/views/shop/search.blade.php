<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container">
                <div class="col-xs-12 col-md-12" id="primary---">
                    <div class="products ver2 grid_full grid_sidebar hover-shadow furniture" id="product_outer">
                        @if (count($products) > 0)
                            @foreach($products as $product)
                                <div class="item-inner">
                                    <div class="product">
                                        <div class="product-images" style="min-height: 275px;">
                                            <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="{{$product->product_name}}">
                                                @if(isset($product->product_images[0]->image_name))
                                                    <img class="primary_image"
                                                         src="{{ asset('uploads/products/'.$product->product_images[0]->image_name ) }}"
                                                         alt="aaa"/>
                                                @else
                                                    <img class="primary_image"
                                                         src="{{asset('assets/site/images/products/featured/2.jpg')}}"
                                                         alt=""/>
                                                @endif

                                            </a>
                                            <div class="action">
                                                <a class="add-cart {{$product->is_cart=="no"?'':'active'}}" data-id="{{$product->id}}" data-is_cart="{{$product->is_cart}}" data-cart_id="{{$product->cart_id}}"
                                                   title="{{$product->is_cart=="no"?'Add to cart':'Remove to cart'}}"></a>
                                                <a class="wish_list wish @if(in_array($product->id,wish_list())) {{'remove_wish'}} @endif" href="javascript:void(0)" title="@if(in_array($product->id,wish_list())) {{'Remove from wishlist'}} @else {{'Add to wishlist'}} @endif" data-id="{{$product->id}}"></a>

                                            </div>
                                            <!-- End action -->
                                        </div>
                                        <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="{{$product->product_name}}">
                                            <p class="product-title">{{trim_words($product->product_name,15)}}</p>
                                        </a>
                                        <p class="product-price-old">
                                            @if(!empty($product->sale_price) AND $product->sale_price > 0)
                                                €{{$product->regular_price}}
                                            @endif
                                        </p>
                                        <p class="product-price">
                                            @if(!empty($product->sale_price) AND $product->sale_price > 0)
                                                €{{$product->sale_price}} {{$product->weight}}
                                            @else
                                                €{{$product->regular_price}} {{$product->weight}}
                                            @endif
                                        </p>

                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p>Not found any Products <a href="{{ route('home') }}">Back to Home</a></p>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('partials.footer')
</div>

@include('partials.footer-js')

</body>
</html>
