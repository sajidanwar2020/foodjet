<!DOCTYPE html>
<html lang="en">
    <head>
        @include('partials.headers-style')
    </head>
<body>

   <!-- End pushmenu -->
        <div class="wrappage">
            @include('partials.top-navbar')
            <div class="container content-wrapper">
                <div class="main-content">
                <div class="container">
                    <div class="row product-details-content">
                        <div class="col-sm-4">
                              <div class="slider-for">
                                <div>
                                  <span class="zoom">
                                       @if(\Storage::disk('uploads')->exists('/products/' . $products_detail->product_images[0]->image_name))
                                          <img class="zoom-images"
                                               src="{{ asset('uploads/products/'.$products_detail->product_images[0]->image_name ) }}"
                                               alt=""/>
                                      @else
                                          <img class="zoom-images"
                                               src="{{ asset('uploads/image-not-avilable.png' ) }}"
                                               alt=""/>
                                      @endif
                                  </span>
                                </div>

                              </div>
                              <!-- End slider-for -->
                              <div class="slider-nav">
                                <div>
                                    @if(\Storage::disk('uploads')->exists('/products/' . $products_detail->product_images[0]->image_name))
                                        <img class="zoom-images"
                                             src="{{ asset('uploads/products/'.$products_detail->product_images[0]->image_name ) }}"
                                             alt=""/>
                                    @else
                                        <img class="zoom-images"
                                             src="{{ asset('uploads/image-not-avilable.png' ) }}"
                                             alt=""/>
                                    @endif
                                </div>
                              </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="box-details-info">
                                <div class="product-name">
                                    <h2>{{$products_detail->product_name}}</h2>
									<p>Product code: {{$products_detail->product_code}}</p>
									<p>Units per box: {{$products_detail->units_per_box}}</p>
                                </div>
                                <!-- End product-name -->
                                <?php /*
                                <div class="rating">
                                    <div class="overflow-h">
                                        <div class="icon-rating">
                                            <input type="radio" id="star-horizontal-rating-1" name="star-horizontal-rating" checked="">
                                            <label for="star-horizontal-rating-1"><i class="fa fa-star-half-o"></i></label>
                                            <input type="radio" id="star-horizontal-rating-2" name="star-horizontal-rating" checked="">
                                            <label for="star-horizontal-rating-2"><i class="fa fa-star"></i></label>
                                            <input type="radio" id="star-horizontal-rating-3" name="star-horizontal-rating" checked="">
                                            <label for="star-horizontal-rating-3"><i class="fa fa-star"></i></label>
                                            <input type="radio" id="star-horizontal-rating-4" name="star-horizontal-rating">
                                            <label for="star-horizontal-rating-4"><i class="fa fa-star"></i></label>
                                            <input type="radio" id="star-horizontal-rating-5" name="star-horizontal-rating">
                                            <label for="star-horizontal-rating-5"><i class="fa fa-star"></i></label>
                                        </div>
                                    </div>
                                </div> */ ?>
                                <!-- End Rating -->
                                <div class="wrap-price">
                                  <?php /*  <del class="price-old">$700.00</del> */ ?>
                                    <p class="price">€{{$products_detail->regular_price}} {{$products_detail->weight}}</p>
                                </div>
                                <!-- End Price -->
                            </div>
                            <!-- End box details info -->
                            <div class="options">
                                <p>
                                    {{$products_detail->product_description}}
                                </p>
                                <!-- End action -->

                                <!--End Description-->
                                <div class="box space-30">
                                    <div class="row">

                                    <!-- End col-md-6 -->
                                    <div class="col-md-5" style="display: none">
                                        <div class="title">
                                            <h3>Quantity</h3>
                                        </div>
                                         <form enctype="multipart/form-data">
                                            <input data-step="1" id="quantity" value="{{isset($products_detail->cart_detail->qty) ? $products_detail->cart_detail->qty : 1}}" title="Qty" min="1" size="4" type="number">
                                        </form>
                                    </div>
                                    <!-- End col-md-5 -->
                                    </div>
                                    <!-- End row -->
                                </div>
                                <!-- End row -->
                                <div class="action">
                                    <a class="link-v1 add-cart bg-brand"  href="javascript:void(0)"  data-id="{{$products_detail->id}}" data-is_cart="{{$products_detail->is_cart}}" data-cart_id="{{isset($products_detail->cart_detail->rowId) ? $products_detail->cart_detail->rowId : ''}}">
                                        <span>{{$products_detail->is_cart == 'no' ? 'Add to cart ' : 'Remove from cart '}}</span>
                                    </a>
                                   <?php /* <a class="link-v1 wish" title="Wishlist" href="#"><i class="icon icon-heart"></i></a> */ ?>
                                    <?php /*<a class="link-v1 chart" title="Compare" href="#"><i class="icon icon-magnifier"></i></a> */ ?>
                                </div>
                                <?php /*
                                <div class="social box">
                                    <h3>Share this :</h3>
                                    <a class="twitter" href="#" title="social"><i class="fa fa-twitter-square"></i></a>
                                    <a class="dribbble" href="#" title="social"><i class="fa fa-dribbble"></i></a>
                                    <a class="skype" href="#" title="social"><i class="fa fa-skype"></i></a>
                                    <a class="pinterest" href="#" title="social"><i class="fa fa-pinterest"></i></a>
                                    <a class="facebook" href="#" title="social"><i class="fa fa-facebook-square"></i></a>
                                </div> */ ?>
                                <!-- End share -->
                            </div>
                            <!-- End Options -->
                        </div>
                    </div>
                    <!-- End product-details-content -->
                <?php /*
                    <div class="hoz-tab-container space-padding-tb-30">
                        <ul class="tabs">
                            <li class="item" rel="description">Description</li>

                            <li class="item" rel="product-tags">Additional infomation</li>
                            <li class="item" rel="customer">Customer Reviews (15)</li>
                        </ul>
                        <div class="tab-container">
                            <div id="description" class="tab-content">
                                <div class="text">
                                    <h3>{{$products_detail->product_name}}</h3>
                                    <p>
                                        {{$products_detail->product_description}}
                                    </p>
                                </div>
                            </div>

                            <div id="product-tags" class="tab-content">
                                <p>
                                    <span>Brand</span>
                                    <span>Hong Quat Packging</span>
                                </p>
                                <p>
                                    <span>Kg</span>
                                    <span>54g</span>
                                </p>
                                <p>
                                    <span>Calo</span>
                                    <span>36 Calo</span>
                                </p>
                            </div>
                            <div id="customer" class="tab-content">
                                <div class="box border">
                                    <h3>Reviews (0)</h3>
                                    <p>There are no reviews yet.</p>
                                </div>
                                <form class="form-horizontal">
                                    <h3>Add a Review</h3>
                                    <div class="box">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class=" control-label" for="inputName">Name *</label>
                                                <input type="text" class="form-control" id="inputName" placeholder="Name">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class=" control-label" for="inputsumary">Email <span class="color">*</span></label>
                                                <input type="text" class="form-control" id="inputsumary" placeholder="Email">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box rating">
                                        <p>Your Rating <span class="color">*</span></p>
                                        <ul>
                                            <li>
                                                <a href="#" title="rating">
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="active" href="#" title="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" title="rating">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-group">
                                        <label class=" control-label" for="inputReview">Review <span class="color">*</span></label>
                                        <textarea class="form-control" id="inputReview"></textarea>
                                    </div>
                                    <a class="button-v1" href="#" title="add tags">Send review</a>
                                </form>
                            </div>
                        </div>
                    </div> */ ?>
                    <!-- tab-container -->
                    @if (count($related_products) > 0)
                        <div class="title-text-v2 space-60">
                            <h3>Related Products</h3>
                        </div>

                            <div class="upsell-product owl-carousel products furniture hover-shadow ver2">
                                @foreach($related_products as $product)
                            <div class="item-inner">
                                <div class="product">
                                    <div class="product-images">
                                        <a href="#" title="product-images">
                                            @if(\Storage::disk('uploads')->exists('/products/' . $product->product_images[0]->image_name))
                                                <img class="primary_image"
                                                     src="{{ asset('uploads/products/'.$product->product_images[0]->image_name ) }}"
                                                     alt=""/>
                                            @else
                                                <img class="primary_image"
                                                     src="{{asset('assets/site/images/products/featured/2.jpg')}}"
                                                     alt=""/>
                                            @endif

                                                @if(isset($product->product_images[0]->image_name))
                                                    <img class="secondary_image"
                                                         src="{{ asset('uploads/products/'.$product->product_images[0]->image_name ) }}"
                                                         alt=""/>
                                                @else
                                                    <img class="secondary_image"
                                                         src="{{ asset('uploads/image-not-avilable.png' ) }}"
                                                         alt=""/>
                                                @endif
                                        </a>
                                        <div class="action">
                                            <div class="action">
                                                <a class="add-cart {{$product->is_cart=="no"?'':'active'}}" data-id="{{$product->id}}" data-is_cart="{{$product->is_cart}}" data-cart_id="{{$product->cart_id}}"
                                                   title="{{$product->is_cart=="no"?'Add to cart':'Remove to cart'}}"></a>
                                                <a class="wish_list wish @if(in_array($product->id,wish_list())) {{'remove_wish'}} @endif" href="javascript:void(0)" title="@if(in_array($product->id,wish_list())) {{'Remove from wishlist'}} @else {{'Add to wishlist'}} @endif" data-id="{{$product->id}}"></a>

                                            </div>
                                        </div>
                                        <!-- End action -->
                                    </div>
                                    <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="{{$product->product_name}}">
                                        <p class="product-title">{{$product->product_name}}</p>
                                    </a>
                                    <p class="product-price-old"></p>
                                    <p class="product-price">€{{$product->regular_price}} {{$product->weight}}</p>
                                </div>
                                <!-- End product -->
                            </div>

                                @endforeach
                    </div>

                    @endif
                </div>
                <!-- End container -->
          </div>
            </div>
          <!-- End MainContent -->
            @include('partials.footer')
        </div>
        <!-- End wrappage -->
    <script type="text/javascript" src="{{asset('assets/site/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/jquery.themepunch.revolution.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/jquery.themepunch.plugins.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/engo-plugins.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/jquery.zoom.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/site/js/store.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on("click", ".add-cart", function () {
                //alert($(this).data('id'));
                _this = $(this);
                var pid = $(this).attr('data-id');
                var cart_id = $(this).attr('data-cart_id');
                var id_added = $(this).attr('data-is_cart');
                var quantity = $(this).closest('tr').find('.qty').val();
                // alert(id_added);
                //return false;
                if (id_added == 'no') {
                    $.ajax('{{ route('add_to_cart') }}', {
                        method: 'POST',
                        data: {'_token': '{{ csrf_token() }}', pid: pid, product_id: pid,quantity: quantity},
                        success: function (data) {
                            _this.attr('data-cart_id', data);
                            _this.attr('data-is_cart', 'yes');
                            _this.addClass('active');
                            _this.attr('title','Remove from cart');
                            _this.attr('title','Remove from cart');
                            $( _this ).find( 'span' ).text('Remove from cart');
                        }
                    });
                    var count = $('.cart-count').html();
                    if (count == '')
                        count = 0;
                    $('.cart-count').html(parseFloat(count) + 1);
                    $('.cart-count').show();
                } else if (id_added == 'yes') {
                    $.ajax('{{ route('remove_cart') }}', {
                        method: 'POST',
                        data: {'_token': '{{ csrf_token() }}', pid: cart_id},
                        success: function (data) {
                            //_this.attr('data-cart_id', data);
                            _this.attr('data-is_cart','no');
                            _this.removeClass('active');
                            _this.attr('title','Add to cart');
                            $( _this ).find( 'span' ).text('Add to cart');
                        }
                    });
                    var count = $('.cart-count').html();
                    //alert(count);
                    if (count != '' && count != 0)
                        $('.cart-count').html(parseFloat(count) - 1);
                    hide_cart_count();
                }
            });

            //==========================================================================================================

            $(document).on("click", ".wish_list", function () {
                var product_id = $(this).attr('data-id');
                var _this = $(this);
                var url;
                if (!$(this).hasClass("remove_wish")) {
                    url = '{{route('update_wish_list')}}';
                    $.get(url, {
                        product_id: product_id
                    }, function(data) {
                        if(data == '11') {
                            window.location.href = '{{route('signin')}}'
                        }
                        else {
                            var count = $('.wish-list-count').text();
                            if (count == '')
                                count = 0;
                            $('.wish-list-count').html(parseInt(count) + 1);

                            $('.wish-list-count').show();
                            _this.addClass('remove_wish');
                            _this.attr('title','Remove from wishlist');
                        }

                    });
                } else {
                    url =  '{{route('update_wish_list')}}';
                    $.get(url, {
                        product_id: product_id
                    }, function(data) {
                        var count = $('.wish-list-count').text();
                        $('.wish-list-count').html(count - 1);
                        var count = $('.wish-list-count').text();
                        if (count == 0) {
                            $('.wish-list-count').hide();
                        }
                        _this.attr('title','Add to wishlist');
                        _this.removeClass('remove_wish');
                    });
                }
            });

            //==========================================================================================================


        });

        function hide_cart_count() {
            var count = $('.cart-count').html();
            if (count == 0)
                $('.cart-count').hide();
        }
    </script>


</body>
</html>

