<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a  href="{{ route('add-page') }}" id="ac" class="btn btn-success">
                                Add Page
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Slug</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                               <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($pages) > 0)
                                                @foreach($pages as $page)
                                                    <tr>
                                                        <td>{{$page->title}}</td>
                                                        <td>{{$page->slug}}</td>
                                                        <td>{{$page->description}}</td>
                                                        <td>{{getProductStatus($page->status)}}</td>

                                                        <td>

                                                            <a href="{{ route('edit-page',['page_id'=>base64_encode($page->id.":".$page->id)]) }}" class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="Edit Offer">
                                                                Edit
                                                            </a>
                                                            |
                                                            <a href="{{ route('change-page-status',['page_id'=>base64_encode($page->id.":".$page->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded product_status" data-toggle="tooltip"
                                                               data-original-title="Delete product" type="warning" msg="Delete this Product">
                                                                Delete
                                                            </a>
                                                            |
                                                            @if($page->status == 0 || $page->status == 2)
                                                                <a href="{{ route('change-page-status',['page_id'=>base64_encode($page->id.":".$page->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                   data-original-title="Publish Page" type="warning" msg="Activate this Page">
                                                                    Activate
                                                                </a>
                                                            @elseif($page->status == 1)
                                                                <a href="{{ route('change-page-status',['page_id'=>base64_encode($page->id.":".$page->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                   data-original-title="DeActivate Page" type="warning" msg="DeActivate this Page">
                                                                    DeActivate
                                                                </a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any Page</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $pages->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
