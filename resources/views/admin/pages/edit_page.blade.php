<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                    <form action="{{ route('edit-page-submitted') }}" method="post"  enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                        <input type="hidden" id="page_id" name="page_id" value="{{ $page->id }}"/>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Title</label>
                                <input type="text" name="title" id="title" class="form-control required" placeholder="Title*" value="{{ $page->title }}">

                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="pd" class="col-form-label">Description</label>
                                <textarea class="form-control required" col="4" rows="5"  id="description" name="description">{{ $page->description }}</textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Update Page</button>
                    </form>
                 </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')

</body>
</html>




