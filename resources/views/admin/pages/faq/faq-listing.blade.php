<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a  href="{{ route('add-faq') }}" id="ac" class="btn btn-success">
                                Add Faq
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($faqs) > 0)
                                                @foreach($faqs as $faq)
                                                    <tr>
                                                        <td>{{$faq->title}}</td>
                                                        <td>{{$faq->question}}</td>
                                                        <td>{{$faq->answer}}</td>
                                                        <td>{{getProductStatus($faq->status)}}</td>

                                                        <td>

                                                            <a href="{{ route('edit-faq',['faq_id'=>base64_encode($faq->id.":".$faq->id)]) }}" class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="Edit Faq">
                                                                Edit
                                                            </a>
                                                            |
                                                            <a href="{{ route('change-faq-status',['faq_id'=>base64_encode($faq->id.":".$faq->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded product_status" data-toggle="tooltip"
                                                               data-original-title="Delete Faq" type="warning" msg="Delete this Faq">
                                                                Delete
                                                            </a>
                                                            |
                                                            @if($faq->status == 0 || $faq->status == 2)
                                                                    <a href="{{ route('change-faq-status',['faq_id'=>base64_encode($faq->id.":".$faq->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                       data-original-title="Publish Faq" type="warning" msg="Activate this Faq">
                                                                        Activate
                                                                    </a>
                                                            @elseif($faq->status == 1)
                                                                    <a href="{{ route('change-faq-status',['faq_id'=>base64_encode($faq->id.":".$faq->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                       data-original-title="DeActivate Faq" type="warning" msg="DeActivate this Faq">
                                                                        DeActivate
                                                                    </a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any Faqs</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $faqs->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
