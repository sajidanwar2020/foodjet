<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <form action="{{ route('add-faq-submitted') }}" method="post"  enctype="multipart/form-data">
                                @include('partials.flash-message')
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="pn" class="col-form-label">Faq type</label>
                                        <select name="type_id" id="type_id" class="form-control required">
                                            <option value="">Please select Faq</option>
                                            @if(!$faqTypes->isEmpty())
                                                @foreach($faqTypes as $faqType)
                                                    <option value="{{ $faqType->id }}">{{ $faqType->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="pn" class="col-form-label">Question</label>
                                        <input type="text" name="question" id="question" class="form-control required" placeholder="Question*" value="{{ old('question')}}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="pd" class="col-form-label">Answer</label>
                                        <textarea class="form-control required" col="4" rows="5"  id="answer" name="answer">{{ old('answer') }}</textarea>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Add Faq</button>
                            </form>
                             </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')

</body>
</html>




