<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <form action="{{ route('edit-faq-submitted') }}" method="post"  enctype="multipart/form-data">
                                <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                                <input type="hidden" id="faq_id" name="faq_id" value="{{ $faq->id }}"/>
                                @include('partials.flash-message')
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="pn" class="col-form-label">Faq type</label>
                                        <select name="type_id" id="type_id" class="form-control required">
                                            <option value="">Please select Faq</option>
                                            @if(!$faqTypes->isEmpty())
                                                @foreach($faqTypes as $faqType)
                                                    <option value="{{ $faqType->id }}" @if($faqType->id == $faq->type_id) {{'selected'}} @endif>{{ $faqType->title }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="pn" class="col-form-label">Question</label>
                                        <input type="text" name="question" id="question" class="form-control required" placeholder="Question*" value="{{ $faq->question}}">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="pd" class="col-form-label">Answer</label>
                                        <textarea class="form-control required" col="4" rows="5"  id="answer" name="answer">{{ $faq->answer }}</textarea>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Update Faq</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')

</body>
</html>




