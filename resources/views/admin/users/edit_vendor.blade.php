<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>



<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="card-box">
                                <form action="{{ route('edit-vendor-submitted') }}" method="post" id="form_signup">
                                    <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                                    <input type="hidden" id="vendor_id" name="vendor_id" value="{{ $vendor->id }}"/>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            <span style="color: red">{{ @"*".$error }}</span><br>
                                        @endforeach
                                    @endif
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label>Salutation*</label>
                                        <select name="salutation" class="custom-select form-control"  aria-invalid="false">
                                            <option value="" selected="">Please select ...</option>
                                            <option value="MRS" {{ $vendor_detail->salutation == 'MRS' ? 'selected' : ''}}>Mrs</option>
                                            <option value="MR" {{ $vendor_detail->salutation == 'MR' ? 'selected' : ''}}>Mr</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>First name*</label>
                                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name*" value="{{ $vendor_detail->first_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Last name*</label>
                                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name*" value="{{ $vendor_detail->last_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Email Address*</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email*" value="{{ $vendor->email }}">
                                    </div>


                                    <div class="form-group">
                                        <label>Tax id*</label>
                                        <input type="text" class="form-control" name="tax_id"  value="{{  $vendor_detail->tax_id }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Street*</label>
                                        <input type="text" class="form-control" name="street"  value="{{ $vendor_detail->street }}">
                                    </div>

                                    <div class="form-group">
                                        <label>  House number*</label>
                                        <input type="text" class="form-control" name="house_number"  value="{{  $vendor_detail->house_number }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Post code*</label>
                                        <input type="text" class="form-control" name="post_code"  value="{{  $vendor_detail->post_code }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Location*</label>
                                        <input type="text" class="form-control" name="location" id="location" placeholder="Location*" value="{{ $vendor_detail->location }}">
                                        <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ $vendor_detail->location }}">
                                        <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ $vendor_detail->location }}">
                                        <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ $vendor_detail->country }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Country*</label>
                                        <select name="country" class="custom-select form-control"  aria-invalid="false">
                                            <option value="1">Germany</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label>Date of birth*</label>
                                        <input type="text" class="form-control" name="dob" id="dob"  readonly value="{{ $vendor_detail->date_of_birth }}">
                                    </div>

                                    <div class="form-group">
                                        <label>Mobile number*</label>
                                        <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ $vendor->phone_number }}">
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-block btn-lg">Edit vendor</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')
@include('partials.common_js')
<script src="{{ asset('/assets/js/jquery-ui.js') }}"></script>
</body>
</html>




