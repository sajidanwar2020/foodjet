<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <form action="{{ route('add-vendor-submitted') }}" method="post" id="form_signup">
                                @if($errors->any())
                                    @foreach($errors->all() as $error)
                                        <span style="color: red">{{ @"*".$error }}</span><br>
                                    @endforeach
                                @endif
                                {{ csrf_field() }}
                                <input type="hidden"  name="user_type" value="3">
                                <div class="form-group">
                                    <label>Salutation*</label>
                                    <select name="salutation" class="custom-select form-control"  aria-invalid="false">
                                        <option value="" selected="">Please select ...</option>
                                        <option value="MRS">Mrs</option>
                                        <option value="MR">Mr</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>First name*</label>
                                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name*" value="{{ old('first_name') }}">
                                </div>

                                <div class="form-group">
                                    <label>Last name*</label>
                                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name*" value="{{ old('last_name') }}">
                                </div>

                                <div class="form-group">
                                    <label>Email Address*</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email*" value="{{ old('email') }}">
                                </div>


                                <div class="form-group">
                                    <label>Tax id*</label>
                                    <input type="text" class="form-control" name="tax_id"  value="{{ old('tax_id') }}">
                                </div>

                                <div class="form-group">
                                    <label>Street*</label>
                                    <input type="text" class="form-control" name="street"  value="{{ old('street') }}">
                                </div>

                                <div class="form-group">
                                    <label>  House number*</label>
                                    <input type="text" class="form-control" name="house_number"  value="{{ old('house_number') }}">
                                </div>

                                <div class="form-group">
                                    <label>Post code*</label>
                                    <input type="text" class="form-control" name="post_code"  value="{{ old('post_code') }}">
                                </div>

                                <div class="form-group">
                                    <label>Location*</label>
                                    <input type="text" class="form-control" name="location" id="location" placeholder="Location*" value="{{ old('location') }}">
                                    <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ old('gmap_latitude') }}">
                                    <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ old('gmap_longitude') }}">
                                    <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ old('country_iso') }}">
                                </div>

                                <div class="form-group">
                                    <label>Country*</label>
                                    <select name="country" class="custom-select form-control"  aria-invalid="false">
                                        <option value="1">Germany</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Date of birth*</label>
                                    <input type="text" class="form-control" name="dob" id="dob"  readonly value="{{ old('dob') }}">
                                </div>

                                <div class="form-group">
                                    <label>Mobile number*</label>
                                    <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ old('phone_number') }}">
                                </div>

                                <div class="form-group">
                                    <label>Password*</label>
                                    <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" placeholder="Password*">
                                </div>

                                <div class="form-group">
                                    <label>Confirm Password*</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password') }}" placeholder="Confirmation password*">
                                </div>



                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg">Add vendor</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')
@include('partials.common_js')
<script src="{{ asset('/assets/js/jquery-ui.js') }}"></script>
</body>
</html>

