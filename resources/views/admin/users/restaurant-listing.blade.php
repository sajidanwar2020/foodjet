<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a href="{{ route('add-restaurant') }}" class="btn btn-sm btn-primary float-right octf-btn">Add Restaurant</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Location</th>
												<th>Restaurant Code</th>
												<th>Site Url</th>
                                                <th>Email</th>
                                                <th>Phone#</th>
                                                <th>Status</th>
                                                <th style="width: 200px">Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($vendors) > 0)
                                                @foreach($vendors as $vendor)
                                                    <?php
                                                    //print_r($vendor);exit;
                                                    ?>
                                                    <tr>
                                                        <td>{{$vendor->first_name}} {{$vendor->last_name}}</td>
                                                        <td>{{$vendor->location}}</td>
														<td>{{$vendor->restaurant_code}}</td>
                                                        <td> <a href="{{ route('home-sub',['id'=>$vendor->restaurant_code]) }}" target="_blank" class="btn btn-success octf-btn octf-btn1"> Website</a></td>
														<td>{{$vendor->email}}</td>
                                                        <td>{{$vendor->phone_number}}</td>
                                                        <td>{{getStatus($vendor->status)}}</td>
                                                        <td>

                                                            <a href="{{ route('edit-restaurant',['id'=>base64_encode($vendor->user_id.":".$vendor->user_id)]) }}" class="btn octf-btn btn-set" data-toggle="tooltip" data-original-title="Edit">
                                                                <i class="fas fa-edit" aria-hidden="true"></i>
                                                            </a>
                                                            |
                                                            <a href="{{ route('change-restaurant-status',['vendor_id'=>base64_encode($vendor->user_id.":".$vendor->user_id.":2")]) }}" class="btn octf1-btn btn-set" data-toggle="tooltip" data-original-title="Delete">
                                                                <i class="fas fa-trash"></i>
                                                            </a>
                                                            |
															 <a href="{{ route('documents-detail',['vendor_id'=>base64_encode($vendor->user_id.":".$vendor->id)]) }}" class="btn btn-info btn-set" data-toggle="tooltip" data-original-title="Document detail">
                                                                Doc
                                                            </a>
                                                            |
                                                            @if($vendor->status == 0 || $vendor->status == 2)
                                                                <a href="{{ route('change-restaurant-status',['vendor_id'=>base64_encode($vendor->user_id.":".$vendor->user_id.":1")]) }}" class="btn btn-set octf-btn octf2-btn " data-toggle="tooltip" data-original-title="Activate">
                                                                    <i class="fas fa-eye"></i>
                                                                </a>

                                                            @elseif($vendor->status == 1)
                                                                <a href="{{ route('change-restaurant-status',['vendor_id'=>base64_encode($vendor->user_id.":".$vendor->user_id.":0")]) }}"class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="DeActivate">
                                                                    <i class="fas fa-eye-slash"></i>
                                                                </a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any restaurant</p>
                                                    </td>

                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $vendors->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
