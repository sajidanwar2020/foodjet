    <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">
                <div class="row">
					<div class="form-group col-md-12">
						<label for="qu" class="col-form-label">ID</label>
						@if(isset($info->_ID) AND !empty($info->_ID))
							<div class="cat_image_preview"> <a href="{{ url('uploads/vendor/doc_proof/'.$info->_ID) }}" target="_blank"><img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->_ID) }}" alt="avatar" style="width: 150px;height: 150px"> </a></div>
						@endif
					</div>
					
					<div class="form-group col-md-12">
						<label for="qu" class="col-form-label">Proof of Address</label>
						@if(isset($info->proof_of_address) AND !empty($info->proof_of_address))
							<div class="cat_image_preview"> <a href="{{ url('uploads/vendor/doc_proof/'.$info->proof_of_address) }}" target="_blank"> <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->proof_of_address) }}" alt="avatar" style="width: 150px;height: 150px">  </a></div>
						@endif
					
					</div>
					
					<div class="form-group col-md-12">
						<label for="qu" class="col-form-label">Driver’s License</label>
						@if(isset($info->driver_License) AND !empty($info->driver_License))
							<div class="cat_image_preview"> <a href="{{ url('uploads/vendor/doc_proof/'.$info->driver_License) }}" target="_blank"> <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->driver_License) }}" alt="avatar" style="width: 150px;height: 150px">  </a></div>
						@endif
					
					</div>	

					
					<div class="form-group col-md-12">
						<label for="qu" class="col-form-label">Articles of Association</label>
						@if(isset($info->articles_of_association) AND !empty($info->articles_of_association))
							<div class="cat_image_preview"> <a href="{{ url('uploads/vendor/doc_proof/'.$info->articles_of_association) }}" target="_blank"> <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->articles_of_association) }}" alt="avatar" style="width: 150px;height: 150px">  </a></div>
						@endif
						
					</div>	
					
					<div class="form-group col-md-12">
						<label for="qu" class="col-form-label">EU Passport</label>
						@if(isset($info->eu_passport) AND !empty($info->eu_passport))
							<div class="cat_image_preview"> <a href="{{ url('uploads/vendor/doc_proof/'.$info->eu_passport) }}" target="_blank"> <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->eu_passport) }}" alt="avatar" style="width: 150px;height: 150px"> </a> </div>
						@endif
						
					</div>	
                </div>
            </div>
        </div>
         @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')
@include('partials.common_js')
</body>
</html>

