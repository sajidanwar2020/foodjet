<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>



<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <div class="card-box">
                                <form action="{{ route('edit-restaurant-submitted') }}" method="post" id="form_signup">
                                    <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                                    <input type="hidden" id="vendor_id" name="vendor_id" value="{{ $vendor->id }}"/>
                                    @if($errors->any())
                                        @foreach($errors->all() as $error)
                                            <span style="color: red">{{ @"*".$error }}</span><br>
                                        @endforeach
                                    @endif
                                    {{ csrf_field() }}
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
										<label>Name*</label>
										<input type="text" name="name" id="name" class="form-control" placeholder="Name*" value="{{  $restaurant_other_detail->name }}">
									</div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-12">
										<label>Description*</label>
										<textarea id="description" rows="4" cols="50"  name="description" class="form-control">{{  $restaurant_other_detail->description }}</textarea>
									</div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                        <label>Email Address*</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email*" value="{{ $vendor->email }}">
                                    </div>
                                        <div class="form-group col-md-6">
                                            <label>Mobile number*</label>
                                            <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ $vendor->phone_number }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Location*</label>
                                        <input type="text" class="form-control" name="location" id="location" placeholder="Location*" value="{{ $vendor_detail->location }}">
                                        <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ $vendor_detail->location }}">
                                        <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ $vendor_detail->location }}">
                                        <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ $vendor_detail->country }}">
                                    </div>

									<div class="form-check">
										<input type="checkbox" class="form-check-input" name="serving" id="serving" value="1" <?php echo ($restaurant_other_detail->serving == '1') ?  "checked" : "" ;  ?>>
										<label class="form-check-label" for="serving">Serving</label>
									</div>
									
									<div class="form-group mt-2">
										<input type="radio" name="accessibility" value="1" <?php echo ($restaurant_other_detail->accessibility == '1') ?  "checked" : "" ;  ?>>
										<label class="mr-2">Menu</label>
                                        <input type="radio" name="accessibility" value="2" <?php echo ($restaurant_other_detail->accessibility == '2') ?  "checked" : "" ;  ?>>
										<label class="mr-2">Menu/Order </label>
                                        <input type="radio" name="accessibility" value="3" <?php echo ($restaurant_other_detail->accessibility == '3') ?  "checked" : "" ;  ?>>
                                        <label class="mr-2">Menu/Order/Payment</label>
                                    </div>
									
									<div class="form-check">
										<input type="checkbox" class="form-check-input" name="online_payment" id="online_payment" value="1" <?php echo ($restaurant_other_detail->online_payment == '1') ?  "checked" : "" ;  ?>>
										<label class="form-check-label" for="online_payment">Online payment</label>
									</div>
									
									<div class="form-row">
										<div class="form-group col-md-6">
											<label class="form-check-label" for="commission">Commission</label>
											<input type="text" class="form-control" name="commission" id="commission" placeholder="Commission" value="{{ $restaurant_other_detail->commission }}">
										</div>
									</div>
                                    
                                    <div class="form-group">
                                        <button type="submit" class="btn octf-btn">Edit vendor</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')
@include('partials.common_js')
</body>
</html>




