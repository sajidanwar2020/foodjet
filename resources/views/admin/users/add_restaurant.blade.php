    <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <form action="{{ route('add-restaurant-submitted') }}" method="post" id="form_signup">
                                @if($errors->any())
                                    @foreach($errors->all() as $error)
                                        <span style="color: red">{{ @"*".$error }}</span><br>
                                    @endforeach
                                @endif
                                {{ csrf_field() }}
								<div class="form-row">
                                <div class="form-group col-md-12">
                                    <label>Name*</label>
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name*" value="{{ old('name') }}">
                                </div>
                                </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                    <label>Description*</label>
									<textarea id="description" rows="4" cols="50"  name="description" class="form-control">{{ old('description') }}</textarea>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                    <label>Email Address*</label>
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Email*" value="{{ old('email') }}">
                                </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone number*</label>
                                            <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ old('phone_number') }}">
                                        </div>
                                        </div>
                                <div class="form-group">
                                    <label>Location*</label>
                                    <input type="text" class="form-control" name="location" id="location" placeholder="Location*" value="{{ old('location') }}">
                                    <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ old('gmap_latitude') }}">
                                    <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ old('gmap_longitude') }}">
                                    <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ old('country_iso') }}">
                                </div>



                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                    <label>Password*</label>
                                    <input type="password" class="form-control" name="password" id="password" value="{{ old('password') }}" placeholder="Password*">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Confirm Password*</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="{{ old('password') }}" placeholder="Confirmation password*">
                                </div>
                                    </div>
								<div class="form-check">
									<input type="checkbox" class="form-check-input" name="serving" id="serving" value="1">
									<label class="form-check-label" for="serving">Serving</label>
								</div>
								
								<div class="form-group mt-2">

									<input type="radio" id="role" name="accessibility" value="1">
                                    <label class="mr-2">Menu</label>

									<input type="radio" id="role" name="accessibility" value="2">
                                    <label class="mr-2">Menu/Order </label>

									<input type="radio" id="role" name="accessibility" value="3">
                                    <label class="mr-2">Menu/Order/Payment</label>
                                </div>
								
								<div class="form-check">
									<input type="checkbox" class="form-check-input" name="online_payment" id="online_payment" value="1">
									<label class="form-check-label" for="online_payment">Online payment</label>
								</div>
								
								<div class="form-row">
									<div class="form-group col-md-6">
										<label class="form-check-label" for="commission">Commission</label>
										<input type="text" class="form-control" name="commission" id="commission" placeholder="Commission" value="{{ old('commission') }}">
									</div>
								</div>
								
                                <div class="form-group">
                                    <button type="submit" class="btn octf-btn ">Add Restaurant</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')
@include('partials.common_js')
</body>
</html>

