<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
            <?php /*
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a href="{{ route('admin-add-product') }}" class="btn btn-sm btn-primary float-right">Add product</a>
                        </div>
                    </div>
                </div> */ ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Location</th>
                                                <th>Email</th>
                                                <th>Phone#</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($customers) > 0)
                                                @foreach($customers as $customer)
                                                    <tr>
                                                        <td>{{$customer->first_name}} {{$customer->last_name}}</td>
                                                        <td>{{$customer->location}}</td>
                                                        <td>{{$customer->email}}</td>
                                                        <td>{{$customer->phone_number}}</td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any Customer</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $customers->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
