<h4 class="header-title mt-0 mb-3">{{$language->name}} ({{$language->currency}})</h4>
<div class="row p-t-10">
	<input type="hidden"  name="price_plan[1][id]"  value="{{isset($actvation['id']) ? $actvation['id'] : ''}}">
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label">Activation</label>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[1][economic_status]" value="1" <?php if(isset($actvation['economic_status']) AND $actvation['economic_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Economic</label>
			<input type="text"  name="price_plan[1][economic]" class="form-control" value="{{isset($actvation['economic']) ? $actvation['economic'] : ''}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[1][standered_status]" value="1" <?php if(isset($actvation['standered_status']) AND $actvation['standered_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Standered</label>
			<input type="text"  name="price_plan[1][standered]" class="form-control" value="{{isset($actvation['standered']) ? $actvation['standered'] : ''}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[1][premium_status]" value="1" <?php if(isset($actvation['premium_status']) AND $actvation['premium_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Premium</label>
			<input type="text"  name="price_plan[1][premium]" class="form-control" value="{{isset($actvation['premium']) ? $actvation['premium'] : ''}}">
		</div>
	</div>
</div>


<div class="row p-t-10">
	<input type="hidden"  name="price_plan[2][id]"  value="{{isset($subscription['id']) ? $subscription['id'] : ''}}">
		<div class="col-md-3">
		<div class="form-group">
			<label class="control-label">Subscription</label>
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[2][economic_status]" value="1" <?php if(isset($subscription['economic_status']) AND $subscription['economic_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Economic({{$language->currency}})</label>
			<input type="text"  name="price_plan[2][economic]" class="form-control" value="{{isset($subscription['economic']) ? $subscription['economic'] : ''}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[2][standered_status]" value="1" <?php if(isset($subscription['standered_status']) AND $subscription['standered_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Standered({{$language->currency}})</label>
			<input type="text"  name="price_plan[2][standered]" class="form-control" value="{{isset($subscription['standered']) ? $subscription['standered'] : ''}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[2][premium_status]" value="1" <?php if(isset($subscription['premium_status']) AND $subscription['premium_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Premium({{$language->currency}})</label>
			<input type="text"  name="price_plan[2][premium]" class="form-control" value="{{isset($subscription['premium']) ? $subscription['premium'] : ''}}">
		</div>
	</div>
</div>


<div class="row p-t-10">
	<input type="hidden"  name="price_plan[3][id]"  value="{{isset($transaction['id']) ? $transaction['id'] : ''}}">
	<div class="col-md-3">
		<div class="form-group">
			<label class="control-label">Transaction</label>
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[3][economic_status]" value="1" <?php if(isset($transaction['economic_status']) AND $transaction['economic_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Economic({{$language->currency}})</label>
			<input type="text"  name="price_plan[3][economic]" class="form-control" value="{{isset($transaction['economic']) ? $transaction['economic'] : ''}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[3][standered_status]" value="1" <?php if(isset($transaction['standered_status']) AND $transaction['standered_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Standered({{$language->currency}})</label>
			<input type="text"  name="price_plan[3][standered]" class="form-control" value="{{isset($transaction['standered']) ? $transaction['standered'] : ''}}">
		</div>
	</div>
	<div class="col-md-3">
		<div class="form-group">
			<input type="checkbox"  name="price_plan[3][premium_status]" value="1" <?php if(isset($transaction['premium_status']) AND $transaction['premium_status'] == 1) echo "checked"; ?>>
			<label class="control-label">Price Plan Premium({{$language->currency}})</label>
			<input type="text"  name="price_plan[3][premium]" class="form-control" value="{{isset($transaction['premium']) ? $transaction['premium'] : ''}}">
		</div>
	</div>
</div>

<div class="col-md-12">
	<button type="submit" class="btn octf-btn">Submit</button>
</div>