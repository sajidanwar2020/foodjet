<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
							<?php 
							//echo "<pre>";
							//print_r($pricePlan);exit;
							?>
                            <h4 class="header-title mt-0 mb-3">Update  Price plan</h4>
							
                            <form action="{{ route('price-plan-submitted') }}" method="post" class="tab-wizard wizard-circle" id="price_plan">
							
									<div class="row p-t-10">
								<div class="col-md-8">
								   @if($languages)
										<div class="form-group" style="display: flex;">
											<div class="col-md-3 p-0">
												<label class="control-label">Select Country</label>
											</div>
											<div class="col-md-4">
												<select class="form-control" name="cid" onchange="get_price_plan_data(this.value)">
													<option value="">Select Country</option>
													@foreach($languages as $key=>$language)
														<option value="{{ $language->id }}" >{{ $language->name }}</option>
													@endforeach
												</select>
											</div>
										</div>
									@endif
								</div>
							</div>
							
								<!-- Step 1 -->
								{{ csrf_field() }}
								<div id="form_data"></div>
								
							</form>
							
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
@include('partials.admin.footer-js')
<script>
function get_price_plan_data(cid)
    {
        $.ajax({
            method: "GET",
            url: "{{route('ajax-price-data')}}",
            data: {cid: cid}
        }).done(function (data) {
            $('#form_data').html(data);
        });
    }
</script>
</body>
</html>

















