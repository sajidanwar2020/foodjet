<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <form action="{{ route('add-minimum-order-submitted') }}" method="post" id="form_signup" enctype="multipart/form-data">
                                @include('partials.flash-message')
                                {{ csrf_field() }}

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="km_from" class="col-form-label">Kilometer From </label>
                                        <input type="text" name="km_from" id="km_from" class="form-control required" placeholder="Kilometer From*" value="{{ old('km_from') }}">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="km_to" class="col-form-label">Kilometer To </label>
                                        <input type="text" name="km_to" id="km_to" class="form-control required" placeholder="Kilometer to*" value="{{ old('km_to') }}">
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="order_value" class="col-form-label">Order value </label>
                                        <input type="text" name="order_value" id="order_value" class="form-control required" placeholder="Order value*" value="{{ old('order_value') }}">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Add order range</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')

</body>
</html>

