<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a  href="{{ route('add-minimum-order') }}" id="ac" class="btn btn-success">
                                Add order range
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>KM FROM</th>
                                                <th>KM TO</th>
                                                <th>Order Value</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($minimum_orders) > 0)
                                                @foreach($minimum_orders as $minimum_order)
                                                    <tr>
                                                        <td>{{$minimum_order->km_from}}</td>
                                                        <td>{{$minimum_order->km_to}}</td>
                                                        <td>{{$minimum_order->order_value}}</td>
                                                        <td>{{getProductStatus($minimum_order->status)}}</td>
                                                        <td>
                                                            <a href="{{ route('edit-minimum-order',['id'=>base64_encode($minimum_order->id.":".$minimum_order->id)]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                Edit
                                                            </a>
                                                            |
                                                            <a href="{{ route('change-minimum-order-status',['id'=>base64_encode($minimum_order->id.":".$minimum_order->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded">
                                                                Delete
                                                            </a>
                                                            |
                                                            @if($minimum_order->status == 0 || $minimum_order->status == 2)
                                                                <a href="{{ route('change-minimum-order-status',['id'=>base64_encode($minimum_order->id.":".$minimum_order->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                    Activate
                                                                </a>
                                                            @elseif($minimum_order->status == 1)
                                                                <a href="{{ route('change-minimum-order-status',['id'=>base64_encode($minimum_order->id.":".$minimum_order->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                    DeActivate
                                                                </a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any order range</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $minimum_orders->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
