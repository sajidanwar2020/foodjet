<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <form action="{{ route('edit-slide-submitted') }}" method="post" enctype="multipart/form-data">
                                <input type="hidden" id="" name="slide_id" value="{{ $homeSlider->id }}">
                                @include('partials.flash-message')
                                {{ csrf_field() }}
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="pd" class="col-form-label">Image text</label>
                                        <textarea class="form-control required"  cols="4" rows="5" id="text" name="text">{{ $homeSlider->text }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="qu" class="col-form-label">Image*</label>
                                    @if(isset($homeSlider->image))
                                        <input type="hidden" id="old_id_0" name="old_image_id" class="old_image_id" value="{{ $homeSlider->id }}"/>
                                        <img class="rounded avatars" id="avatar2" src="{{ url('uploads/home_slider/'.$homeSlider->image) }}" alt="avatar" style="width: 150px;height: 150px">
                                    @endif
                                    <input type="file" class="form-control-file" id="slide_image0" name="slide_image0">
                                </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Add Slide</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.admin.footer')
</div>
</div>
@include('partials.admin.footer-js')

</body>
</html>

