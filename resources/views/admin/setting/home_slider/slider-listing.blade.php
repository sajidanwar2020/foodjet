<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a  href="{{ route('add-slide') }}" id="ac" class="btn btn-success">
                                Add Slide
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Text</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($sliders) > 0)
                                                @foreach($sliders as $slide)
                                                    <tr>
                                                        <td>
                                                            @if(isset($slide->image))
                                                                <img class="primary_image"
                                                                     src="{{ asset('uploads/home_slider/'.$slide->image ) }}"
                                                                     alt="" width="50" height="50"/>
                                                            @else
                                                                <img class="primary_image"
                                                                     src="{{asset('assets/site/images/products/featured/2.jpg')}}"
                                                                     alt="" width="50" height="50"/>
                                                            @endif
                                                        </td>
                                                        <td>{{$slide->text}}</td>
                                                        <td>{{getProductStatus($slide->status)}}</td>

                                                        <td>

                                                            <a href="{{ route('edit-slide',['id'=>base64_encode($slide->id.":".$slide->id)]) }}" class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="Edit slide">
                                                                Edit
                                                            </a>
                                                            |
                                                            <a href="{{ route('change-slide-status',['id'=>base64_encode($slide->id.":".$slide->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded product_status" data-toggle="tooltip"
                                                               data-original-title="Delete slide" type="warning" msg="Delete this slide">
                                                                Delete
                                                            </a>
                                                            |
                                                            @if($slide->status == 0 || $slide->status == 2)
                                                                <a href="{{ route('change-slide-status',['id'=>base64_encode($slide->id.":".$slide->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                   data-original-title="Publish slide" type="warning" msg="Activate this slide">
                                                                    Activate
                                                                </a>
                                                            @elseif($slide->status == 1)
                                                                <a href="{{ route('change-slide-status',['id'=>base64_encode($slide->id.":".$slide->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                   data-original-title="DeActivate slide" type="warning" msg="DeActivate this slide">
                                                                    DeActivate
                                                                </a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any slide</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $sliders->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
