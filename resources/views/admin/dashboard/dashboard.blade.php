<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')

    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <div class="container-fluid">

                <div class="row">

                    <div class="col-xl-3 col-md-6">
                        <a href="{{ route('products_listing') }}">
                            <div class="card-box">
                                <h4 class="header-title mt-0 mb-4">Total Order</h4>
                                <div class="widget-chart-1">

                                    <div class="widget-detail-1 text-right">
                                        <h2 class="font-weight-normal pt-2 mb-1"> {{$total_orders}} </h2>
                                    </div>
                                </div>
                            </div>
                        </a>

                    </div>

                    <div class="col-xl-3 col-md-6">
                        <a href="{{ route('order-listing') }}">
                            <div class="card-box">
                                <h4 class="header-title mt-0 mb-4"> Total Products</h4>
                                <div class="widget-chart-1">

                                    <div class="widget-detail-1 text-right">
                                        <h2 class="font-weight-normal pt-2 mb-1">  {{$total_products}}  </h2>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

            </div>

        </div>

        @include('partials.admin.footer')

    </div>


</div>

@include('partials.admin.footer-js')

</body>
</html>
