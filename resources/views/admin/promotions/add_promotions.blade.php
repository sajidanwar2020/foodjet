<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <form action="{{ route('add-promotions-submitted') }}" method="post">
                                @include('partials.flash-message')
                                {{ csrf_field() }}

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="qu" class="col-form-label">Parent Category</label>
                                        <select name="parent_category" id="parent_category" class="form-control required" onchange="get_child_category(this.value)">
                                            <option value="">Please select Parent Category</option>
                                            @if(!$categories->isEmpty())
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" {{ old('parent_category') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                                                @endforeach
                                            @else
                                                <option value="">No option</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="qu" class="col-form-label">Child Category</label>
                                        <select name="child_category" id="child_category" class="form-control required">
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="km_from" class="col-form-label">Start datetime </label>
                                        <input type="text" name="start_datetime" id="start_datetime" class="form-control required" placeholder="Start datetime*" value="{{ old('start_datetime') }}" readonly>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="km_to" class="col-form-label">End datetime</label>
                                        <input type="text" name="end_datetime" id="end_datetime" class="form-control required" placeholder="End datetime*" value="{{ old('end_datetime') }}" readonly>
                                    </div>

                                    <div class="form-group col-md-4">
                                        <label for="discount_percent" class="col-form-label">Discount percent </label>
                                        <input type="text" name="discount_percent" id="discount_percent" class="form-control required" placeholder="Discount Percent*" value="{{ old('discount_percent') }}">
                                    </div>


                                </div>

                                <button type="submit" class="btn btn-primary">Add promotion</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')
<script src="{{ asset('/assets/js/jquery-ui.js') }}"></script>
</body>
</html>

