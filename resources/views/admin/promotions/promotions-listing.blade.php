<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a  href="{{ route('add-promotions') }}" id="ac" class="btn btn-success">
                                Add promotions
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Category</th>
                                                <th>Start datetime</th>
                                                <th>End datetime</th>
                                                <th>Discount percent</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($promotions) > 0)
                                                @foreach($promotions as $promotion)
                                                    <?php
                                                        $category_detail = getParentCategoryByChildId($promotion->category_idd);
                                                    ?>
                                                    <tr>
                                                        <td>{{$category_detail->name}}</td>
                                                        <td>{{$promotion->start_datetime}}</td>
                                                        <td>{{$promotion->end_datetime}}</td>
                                                        <td>{{$promotion->discount_percent}}</td>
                                                        <td>{{getProductStatus($promotion->status)}}</td>
                                                        <td>
                                                            <a href="{{ route('edit-promotions',['id'=>base64_encode($promotion->id.":".$promotion->id)]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                Edit
                                                            </a>
                                                            |
                                                            <a href="{{ route('change-promotions-status',['id'=>base64_encode($promotion->id.":".$promotion->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded">
                                                                Delete
                                                            </a>
                                                            |
                                                            @if($promotion->status == 0 || $promotion->status == 2)
                                                                <a href="{{ route('change-promotions-status',['id'=>base64_encode($promotion->id.":".$promotion->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                    Activate
                                                                </a>
                                                            @elseif($promotion->status == 1)
                                                                <a href="{{ route('change-promotions-status',['id'=>base64_encode($promotion->id.":".$promotion->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                    DeActivate
                                                                </a>
                                                            @endif

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any promotions</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $promotions->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
