<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <h4 class="header-title mt-0 mb-3">Edit  Category aa</h4>

                            <form action="{{ route('editCategorySubmitted') }}" method="post" enctype="multipart/form-data">
                                @include('partials.flash-message')
                                {{ csrf_field() }}
                                <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                                <input type="hidden" id="cat_id" name="cat_id" value="{{ $category->id }}"/>
                                @if(isset($_GET['parent_id']) AND $_GET['parent_id'] > 0)
                                    <div class="form-group">
                                        <label class="control-label">Select Parent Category</label>
                                        <select class="form-control" id="parent_id" name="parent_id">
                                            <option value="">Select Parent Category</option>
                                            @foreach($parent_category as $category1)
                                                <option value="{{ $category1->id }}" @if($category1->id == $category->parent_id) {{'selected'}} @endif >{{ $category1->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                 <div class="form-group">
                                    <label for="name">Name*</label>
                                    <input type="text" name="name"  class="form-control" id="name" value="{{$category->name}}">
                                </div>

                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea rows="4" cols="50" name="description" id="description" class="form-control">{{$category->description}}</textarea>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="status">Status*</label>
                                    <select id="cat_status" name="cat_status" class="form-control">
                                        <option value="1" {{$category->status == '1' ? 'selected' : ''}}>Activate</option>
                                        <option value="2" {{$category->status == '2' ? 'selected' : ''}}>DeActivate</option>
                                        <option value="3" {{$category->status == '3' ? 'selected' : ''}}>Deleted</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="qu" class="col-form-label">Image*</label>
                                    @if(isset($category->image) AND !empty($category->image))
                                        <input type="hidden" id="old_image" name="old_image" value="{{ $category->image }}"/>
                                        <div class="cat_image_preview"> <img class="rounded avatars" id="avatar2" src="{{ url('uploads/categories/'.$category->image) }}" alt="avatar" style="width: 150px;height: 150px"> </div>
                                    @endif
                                    <input type="file" class="form-control-file file_preview" id="category" name="category" accept="image/*">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="qu" class="col-form-label">Icon*</label>
                                    @if(isset($category->icon) AND !empty($category->icon))
                                        <div class="cat_image_preview">  <img class="rounded avatars" id="avatar2" src="{{ url('uploads/categories/icons/'.$category->icon) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
                                    @endif
                                    <input type="file" class="form-control-file file_preview" id="icon" name="icon" accept="image/*">
                                </div>

                                <div class="form-group text-right mb-0">
                                    <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect waves-light">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
@include('partials.admin.footer-js')

</body>
</html>
