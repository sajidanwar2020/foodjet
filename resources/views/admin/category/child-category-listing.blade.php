<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a  href="{{ route('add-category',['parent_id'=>$_GET['parent_id']]) }}" id="ac" class="btn btn-success">
                                Add Category
                            </a>
                        </div>

                    <?php /*
                        <div class="col-md-4">
                             <select class="form-control _status" id="_status">
                             </select>
                        </div> */ ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Description</th>
                                                <th>Category Name</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($categories as $category)
                                                <tr>
                                                    <td>
                                                        @if(isset($category->image))
                                                            <img class="primary_image"
                                                                 src="{{ asset('uploads/categories/'.$category->image ) }}"
                                                                 alt="" width="50" height="50"/>
                                                        @else
                                                            <img class="primary_image"
                                                                 src="{{asset('assets/site/images/products/featured/2.jpg')}}"
                                                                 alt="" width="50" height="50"/>
                                                        @endif

                                                    </td>
                                                    <td>{{ $category->description }}</td>
                                                    <td>{{ $category->name }}</td>
                                                   <?php
                                                         $categoryStatusArr = array('1' =>'Activate','2' => 'DeActivate' ,'3' => 'Trash');
                                                    ?>
                                                    <td>{{ $categoryStatusArr[$category->status] }}</td>
                                                    <td>

                                                            <a href="{{ route('edit_category',['cat_id'=>$category->id,'parent_id'=>$_GET['parent_id']]) }}" class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="Edit Offer">
                                                                Edit
                                                            </a>
                                                            |
                                                            <a href="{{ route('change_category_status',['cat_id'=>base64_encode($category->id.":".$category->id.":3")]) }}" class="btn btn-sm btn-danger btn-rounded product_status" data-toggle="tooltip"
                                                               data-original-title="Delete product" type="warning" msg="Delete this Product">
                                                                Delete
                                                            </a>
                                                            |
                                                            <a href="{{ route('change_category_status',['cat_id'=>base64_encode($category->id.":".$category->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                               data-original-title="Publish Offer" type="warning" msg="Activate this Product">
                                                                Activate
                                                            </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    {{ $categories->links('vendor.pagination.custom') }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
