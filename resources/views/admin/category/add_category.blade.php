<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <h4 class="header-title mt-0 mb-3">Add  Category</h4>


                            <form action="{{ route('categorySubmitted') }}" method="post"  enctype="multipart/form-data">
                                @include('partials.flash-message')
                                {{ csrf_field() }}
                                @if(isset($_GET['parent_id']) AND $_GET['parent_id'] > 0)
                                    <div class="form-group">
                                        <label class="control-label">Select Parent Category</label>
                                        <select class="form-control" id="parent_id" name="parent_id">
                                            <option value="">Select Parent Category</option>
                                            @foreach($parent_category as $category)
                                                <option value="{{ $category->id }}" @if($category->id == $_GET['parent_id']) {{'selected'}} @endif >{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="userName">Name*</label>
                                    <input type="text" name="name"  class="form-control" id="name" value="">
                                </div>

                                <div class="form-group">
                                    <label for="emailAddress">Description</label>
                                    <textarea rows="4" cols="50" name="description" id="description" class="form-control"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="pass1">Status*</label>
                                    <select id="cat_status" name="cat_status" class="form-control">
                                        <option value="1">Activate</option>
                                        <option value="2">DeActivate</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="pass1">Image*</label>
                                    <input id="category" type="file" name="category" class="form-control file_preview">
                                    <div class="cat_image_preview"></div>
                                </div>

                                <div class="form-group">
                                    <label for="pass1">Icon*</label>
                                    <input id="category_icon" type="file" name="icon" class="form-control file_preview">
                                    <div class="cat_image_preview"></div>
                                </div>

                                <div class="form-group text-right mb-0">
                                    <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect waves-light">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
@include('partials.admin.footer-js')

</body>
</html>
