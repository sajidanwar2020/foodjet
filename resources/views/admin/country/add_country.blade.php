<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <h4 class="header-title mt-0 mb-3">Add  Country</h4>


                            <form action="{{ route('add-country-submitted') }}" method="post">
                                @include('partials.flash-message')
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="userName">name*</label>
                                    <input type="text" name="name"  class="form-control" id="name">
                                </div>

                                <div class="form-group">
                                    <label for="userName">Iso Code*</label>
                                    <input type="text" name="iso_code"  class="form-control" id="iso_code">
                                </div>
								
								<div class="form-group">
                                    <label for="userName">Currency*</label>
                                    <input type="text" name="currency"  class="form-control" id="currency">
                                </div>
								
                                <div class="form-group text-right mb-0">
                                    <button class="btn mr-1 octf-btn" type="submit">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn octf1-btn">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
@include('partials.admin.footer-js')

</body>
</html>
