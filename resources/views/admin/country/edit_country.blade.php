<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <h4 class="header-title mt-0 mb-3">Edit  Country</h4>

                            <form action="{{ route('edit-country-submitted') }}" method="post">
                                @include('partials.flash-message')
                                {{ csrf_field() }}
                                <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                                <input type="hidden" id="cid" name="cid" value="{{ $country->id }}"/>
                                    <div class="form-group">
                                    <label for="userName">Name*</label>
                                    <input type="text" name="name"  class="form-control" id="name" value="{{$country->name}}">
                                </div>

                                <div class="form-group">
                                    <label for="userName">Iso Code*</label>
                                    <input type="text" name="iso_code"  class="form-control" id="iso_code" value="{{$country->iso_code}}">
                                </div>
								
								<div class="form-group">
                                    <label for="userName">Currency*</label>
                                    <input type="text" name="currency"  class="form-control" id="currency" value="{{$country->currency}}">
                                </div>
                              
								
                                <div class="form-group text-right mb-0">
                                    <button class="btn octf-btn mr-1" type="submit">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn octf1-btn">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->

</div>
@include('partials.admin.footer-js')
</body>
</html>
