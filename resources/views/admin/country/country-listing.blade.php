<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a  href="{{ route('add-country') }}" id="ac" class="btn btn-success octf-btn">
                                Add Country
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Iso code</th>
                                                <th>Currency</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($countries as $country)
                                                <tr>
                                                    <td>{{ $country->name }}</td>
                                                    <td>{{ $country->iso_code }}</td>
                                                    <td>{{ $country->currency }}</td>
                                                    <?php
                                                    $countryStatusArr = array('1' =>'Activate','2' => 'DeActivate' ,'3' => 'Trash');
                                                    ?>
                                                    <td>{{ $countryStatusArr[$country->status] }}</td>
                                                    <td>
                                                        <a href="{{ route('edit-country',['cid'=>$country->id]) }}"class="btn octf-btn btn-set" data-toggle="tooltip" data-original-title="Edit">
                                                            <i class="fas fa-edit" aria-hidden="true"></i>
                                                        </a>
                                                            |
                                                            <a href="{{ route('change-country-status',['cid'=>base64_encode($country->id.":".$country->id.":3")]) }}" class="btn octf1-btn btn-set" data-toggle="tooltip" data-original-title="Delete">
                                                                <i class="fas fa-trash"></i>
                                                            </a>

                                                            @if($country->status == 2 || $country->status == 3)
                                                                <a href="{{ route('change-country-status',['cid'=>base64_encode($country->id.":".$country->id.":1")]) }}" class="btn btn-set octf-btn octf2-btn " data-toggle="tooltip" data-original-title="Activate">
                                                                    <i class="fas fa-eye"></i>
                                                                </a>
                                                            @elseif($country->status == 1)
                                                                <a href="{{ route('change-country-status',['cid'=>base64_encode($country->id.":".$country->id.":2")]) }}" class="btn btn-set octf-btn octf3-btn" data-toggle="tooltip" data-original-title="DeActivate">
                                                                    <i class="fas fa-eye-slash"></i>
                                                                </a>
                                                            @endif

                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    {{ $countries->links('vendor.pagination.custom') }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
