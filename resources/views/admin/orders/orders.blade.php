<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <?php /*
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a href="{{ route('admin-add-product') }}" class="btn btn-sm btn-primary float-right">Add product</a>
                        </div>
                    </div>
                </div> */ ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">

                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Order#</th>
                                                <th>Payment Method</th>
                                                <th>Is paid</th>
                                                <th>Customer name</th>
                                                <th>Address</th>
                                                <th>Phone#</th>
                                                <th>Order Total</th>
                                                <th>Additional notes</th>
                                                <th>Status</th>
                                              <th>Detail</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($orders) > 0)
                                                @foreach($orders as $my_order)
                                                    <tr>
                                                        <td>{{$my_order->o_id}}</td>
                                                        <td>{{getPaymentMethod($my_order->payment_type)}}</td>
                                                        <td class="{{($my_order->online_transaction['status'] == 1 ? 'paid': 'not_paid')}}">
                                                            @if($my_order->payment_type == 1)
                                                                -
                                                            @else
                                                                {{($my_order->online_transaction['status'] == 1 ? 'Yes': 'No')}}
                                                            @endif
                                                        </td>
                                                        <td>{{$my_order->first_name}} {{$my_order->last_name}}</td>
                                                        <td>{{$my_order->o_address}}</td>
                                                        <td>{{$my_order->phone_number}}</td>
                                                        <td>€{{ ($my_order->total_price + $my_order->distance_charges ) - $my_order->cancelled_amount }}</td>
                                                        <td>{{$my_order->additional_notes}}</td>
                                                        <td>{{getOrderStatus($my_order->o_status)}} </td>

                                                        <td> <?php /*
                                                            <?php
                                                            $status_arr = array(4, 5);
                                                            ?>
                                                            @if($my_order->o_status == 3 AND !in_array($my_order->o_status, $status_arr))
                                                                <a href="{{ route('change_main_order_status',['order_id'=>$my_order->o_id,'status'=>4]) }}" class="btn btn-success btn-rounded"
                                                                   type="warning" msg="Are you sure order to complete?">
                                                                    Complete
                                                                </a>
                                                            @endif

                                                            @if($my_order->is_order_processed == 0 AND $my_order->o_status != 3 AND !in_array($my_order->o_status, $status_arr))
                                                                <a href="{{ route('change_main_order_status',['order_id'=>$my_order->o_id,'status'=>3]) }}" class="btn btn-secondary btn-sm btn-rounded product_status"
                                                                   type="warning" msg="Are you sure order to complete?" style="padding-left:4px;padding-right: 4px; ">
                                                                    Ready for Delivery
                                                                </a>

                                                            @endif

                                                            @if(!in_array($my_order->o_status, $status_arr))
                                                                <a href="{{ route('change_main_order_status',['order_id'=>$my_order->o_id,'status'=>5]) }}" class="btn btn-danger btn-rounded"
                                                                   type="warning" msg="Are you sure order to Cancelled?">
                                                                    Cancel
                                                                </a>
                                                            @endif */ ?>



                                                            <a href="{{ route('order-detail',['order_id'=>$my_order->o_id]) }}" class="btn btn-sm btn-info btn-rounded" >
                                                                Detail
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any Orders</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>


                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $orders->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
