<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <?php /*
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a href="{{ route('admin-add-product') }}" class="btn btn-sm btn-primary float-right">Add product</a>
                        </div>
                    </div>
                </div> */ ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">

                                        @if (count($order_detail) > 0)
                                            <div class="order_detail_wrapper">
                                                <div class="rows">
                                                    <div class="col-md-4">
                                                        <b>Address :</b> {{$order_detail->order_detail['address']}}
                                                    </div>
                                                    <div class="col-md-4">
                                                        <b> Phone :</b> {{$order_detail->order_detail['phone_number']}}
                                                    </div>
                                                </div>
                                            </div>

                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th>Order#</th>
                                                    <th>Customer name</th>
                                                    <th>Product name</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <th>Status</th>
                                                <?php /*<th>Action</th> */ ?>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($order_detail as $order_det)
                                                    <tr>
                                                        <td>{{$order_det->o_id}}</td>
                                                        <td>{{$order_det->first_name}} {{$order_det->last_name}}</td>
                                                        <td> {{$order_det->product_name}}</td>
                                                        <td> {{$order_det->quantity}}</td>
                                                        <td> {{$order_det->price}}</td>
                                                        <td>{{getOrderItemStatus($order_det->item_status)}}</td>
                                                        <?php /*
                                                        <td>
                                                            @if($order_det->item_status == 1)
                                                                <a href="{{ route('change_order_status',['item_id'=>$order_det->items_id,'order_id'=>$order_det->o_id,'status'=>2]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                   data-original-title="Process" type="warning" msg="Are you sure order to Process?">
                                                                    Process
                                                                </a>
                                                            @endif

                                                            @if($order_det->item_status == 1)
                                                                <a href="{{ route('change_order_status',['item_id'=>$order_det->items_id,'order_id'=>$order_det->o_id,'status'=>3]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                                   data-original-title="Reject" type="warning" msg="Are you sure order to Reject?">
                                                                    Reject
                                                                </a>
                                                            @endif

                                                        </td> */ ?>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        @else
                                            <tr>
                                                <td colspan="8">
                                                    <p>Not found any Orders</p>
                                                </td>
                                            </tr>
                                        @endif

                                </div>



                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
