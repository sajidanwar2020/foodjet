<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')

</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')

    <div class="content-page">
        <div class="content">

            <!-- Start Content-->
            <div class="container-fluid">
                <?php /*
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto">
                            <a href="{{ route('admin-add-product') }}" class="btn btn-sm btn-primary float-right">Add product</a>
                        </div>
                    </div>
                </div> */ ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card-box">
                            <div class="responsive-table-plugin">
                                <div class="table-rep-plugin">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Product name</th>
                                                <th>Descrption</th>
                                                <th>Price</th>
                                                <th>Unit</th>
                                                <th>Status</th>
                                               <?php /* <th>Action</th> */ ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if (count($products) > 0)
                                                @foreach($products as $product)
                                                    <tr>
                                                        <td>{{$product->product_name}}</td>
                                                        <td>{{$product->product_description}}</td>
                                                        <td>{{$product->regular_price}}</td>
                                                        <td>{{$product->weight}}</td>
                                                        <td>{{getProductStatus($product->status)}}</td>
                                                        <?php /*
                                                        <td>

                                                            <a href="{{ route('admin_edit_product',['product_id'=>base64_encode($product->id.":".$product->id)]) }}" class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="Edit Offer">
                                                                Edit
                                                            </a>
                                                            |
                                                            <a href="{{ route('admin_change_product_status',['product_id'=>base64_encode($product->id.":".$product->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded product_status" data-toggle="tooltip"
                                                               data-original-title="Delete product" type="warning" msg="Delete this Product">
                                                                Delete
                                                            </a>
                                                            |
                                                            <a href="{{ route('admin_change_product_status',['product_id'=>base64_encode($product->id.":".$product->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                               data-original-title="Publish Offer" type="warning" msg="Activate this Product">
                                                                Activate
                                                            </a>

                                                        </td> */ ?>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="8">
                                                        <p>Not found any product</p>
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <div style="float: right;margin-top: 32px;">
                                        <div class="card-footer---">
                                            {{ $products->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end row -->

            </div> <!-- container-fluid -->

        </div> <!-- content -->

        @include('partials.admin.footer')

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
@include('partials.admin.footer-js')

</body>
</html>
