<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.admin.headers-style')
</head>

<body>

<!-- Begin page -->
<div id="wrapper">

    @include('partials.admin.top-navbar')
    @include('partials.admin.left-sidebar')
    <div class="content-page">
        <div class="content">
            <!-- Start Content-->
            <div class="container-fluid">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                             <form action="{{ route('adminProductSubmitted') }}" method="post" id="form_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <input type="hidden"  name="user_type" value="2">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Product Name</label>
                                <input type="text" name="product_name" id="product_name" class="form-control required" placeholder="Product Name*" value="{{ old('product_name') }}">

                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="pd" class="col-form-label">Product Description</label>
                                <textarea class="form-control required"  cols="4" rows="5" id="product_description" name="product_description">{{ old('product_description') }}</textarea>
                             </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Unit Regular Price</label>
                                <input type="text" class="form-control" name="sale_price" id="sale_price"  value="{{ old('sale_price') }}">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Unit Sale Price</label>
                                <input type="text" class="form-control required" name="regular_price" id="regular_price"  value="{{ old('regular_price') }}">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Quantity Unit</label>
                                <select name="quantity_unit" id="quantity_unit" class="form-control required">
                                    <option value="">Please select quantity unit</option>
                                    <option value="1" {{ old('quantity_unit') == '1' ? 'selected' : ''}}>KG</option>
                                    <option value="2" {{ old('quantity_unit') == '2' ? 'selected' : ''}}>Dozen</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Category</label>
                                <select name="category" id="category" class="form-control required">
                                    <option value="">Please select Category</option>
                                    @if(!$categories->isEmpty())
                                        @foreach($categories as $category)
                                            <optgroup label="{{ $category->name }}">
                                                @foreach($category->child_categories as $child_category)
                                                    <option value="{{ $child_category->id }}">{{ $child_category->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    @else
                                        <option value="">No option</option>
                                    @endif
                                </select>

                            </div>
                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Image*</label>
                                <input type="file" class="form-control-file" id="product_image0" name="product_image0">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Add Product</button>
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         @include('partials.admin.footer')
    </div>
</div>
@include('partials.admin.footer-js')

</body>
</html>

