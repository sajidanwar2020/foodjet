<form action="{{ $paypal_url }}" method="post" id="paypal_form">

    @foreach($elements as $key=>$element)
        <input type="hidden"  name="{{ $key }}" id="{{ $key }}" value="{{ $element }}">
    @endforeach
</form>

<script>
    document.getElementById('paypal_form').submit();
</script>


