<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <?php
    /*
    //echo "<pre>";
   // print_r($categories);
    //exit;
    foreach($categories as $category) {
        foreach($category['products_arr'] as $product) {

        }
    } */
    ?>
    <div class="container">
        <div class="title-text-v2">
            <h3>Order Success</h3>
        </div>
        <div class="featured-products home_2 new-arrivals lastest space-30" style="margin-bottom: 39px !important;">

            <div class="tab-container space-10">
                <div id="tab_0" class="tab-contenta">
                    <div class="products hover-shadow ver2 border-space-product">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="card-body">
                                    @include('partials.flash-message')
                                    <p> <a href="{{route('home')}}"> << Back to Home </a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <?php /*
                <div id="tab_2" class="tab-content">
                    <div class="products hover-shadow ver2 border-space-product">
                        <div class="product hover-shadow">
                            <div class="product-images">
                                <a href="#" title="product-images">
                                    <img class="primary_image"
                                         src="{{asset('assets/site/images/products/featured/1.jpg')}}" alt=""/>
                                </a>
                                <div class="action">
                                    <a class="add-cart" href="#" title="Add to cart"></a>
                                    <a class="wish" href="#" title="Wishlist"></a>
                                    <a class="zoom" href="#" title="Quick view"></a>
                                </div>
                                <!-- End action -->
                            </div>
                            <a href="#" title="Union Bed"><p class="product-title">Union Bed</p></a>
                            <p class="product-price-old">$700.00</p>
                            <p class="product-price">$350.00</p>

                        </div>
                        <!-- End item -->

                        <!-- End item -->
                    </div>
                    <!-- End product-tab-content products -->
                </div>
                <!-- End tab-content -->
                <div id="tab_3" class="tab-content">
                    <div class="products ver2 hover-shadow border-space-product">
                        <div class="product">
                            <div class="product-images">
                                <a href="#" title="product-images">
                                    <img class="primary_image"
                                         src="{{asset('assets/site/images/products/featured/4.jpg')}}" alt=""/>
                                </a>
                                <div class="action">
                                    <a class="add-cart" href="#" title="Add to cart"></a>
                                    <a class="wish" href="#" title="Wishlist"></a>
                                    <a class="zoom" href="#" title="Quick view"></a>
                                </div>
                                <!-- End action -->
                            </div>
                            <a href="#" title="Union Bed"><p class="product-title">Union Bed</p></a>
                            <p class="product-price-old">$700.00</p>
                            <p class="product-price">$350.00</p>
                        </div>


                        <!-- End item -->
                    </div>
                    <!-- End product-tab-content products -->
                </div>
                <!-- End tab-content --> */ ?>

            </div>
        </div>
        <div class="box center space-padding-tb-30 space-30" style="display: none">
            <a class="link-v1 color-brand font-300" href="#" title="View All">View All</a>
        </div>
    </div>

    <!--End Shipping Version 2-->


    <div class="special bg-images special-v2 box"
         style="background-image:url('{{asset('assets/site/images/home1-banner1.jpg')}}');background-repeat: no-repeat; display: none">
        <div class="col-md-7 float-left align-right">
            <img class="images-logo" src="{{asset('assets/site/images/home1-images-banner1-2.png')}}" alt="images">
        </div>
        <!-- End col-md-7 -->
        <div class="col-md-5 float-right">
            <div class="special-content align-center">
                <p>Angebot</p>
                <h3>Sie erhalten 30% Rabatt</h3>
                <h5>Ihre Bestellung von $100 oder mehr</h5>
                <div class="time" data-countdown="countdown" data-date="04-20-2017-10-20-30"></div>
                <a class="link-v1 color-brand" href="#" title="shopnow">Jetzt Shoppen</a>
            </div>
        </div>
        <!-- End col-md-5 -->
    </div>
    <!-- End container -->

    <div id="back-to-top" style="display: none">
        <i class="fa fa-long-arrow-up"></i>
    </div>
    @include('partials.footer')
</div>


</body>
</html>













