<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.header')
</head>
<body>

@include('partials.top_navbar')

@yield('content')

<?php /*@include('partials.footer') */ ?>

@include('partials.js')

@yield('custom_scripts')
 </body>
</html>
