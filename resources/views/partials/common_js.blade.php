<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCB59pcScbhhVj5SD6-m1is6v0Qq3cGwT4&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#location").keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });


       //-----------------------------------------------------------------

    });

    function initialize() {
        //alert();
        currentLocation();
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();
            var address = place.address_components;
            var address_details = address[address.length - 1];
            document.getElementById('location').value = place.name;
            document.getElementById('gmap_latitude').value = place.geometry.location.lat();
            document.getElementById('gmap_longitude').value = place.geometry.location.lng();
           // document.getElementById('country_iso').value = address_details['short_name'];

        });
    }

    function currentLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $('#gmap_latitude').val('{{  isset($latitude) ? $latitude : '0' }}');
            $('#gmap_longitude').val('{{ isset($longitude) ? $longitude : '0' }}');
        }
    }

    function showPosition(position) {
        //console.log(position);
        $('#gmap_latitude').val(position.coords.latitude);
        $('#gmap_longitude').val(position.coords.longitude);
        let geocoder = new google.maps.Geocoder();
        let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                var address = results[results.length - 1].address_components[0];
                $('#location').attr("placeholder", results[0].formatted_address);
                $('#location').val(results[0].formatted_address);
                $('#country_iso').val(address['short_name']);
            } else {
                alert('Geocode was not successful due to following reason: ' + status);
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
