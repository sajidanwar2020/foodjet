<div class="left-side-menu">

    <div class="slimscroll-menu">

        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="{{route('admin-dashboard')}}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Dashboard </span>
                    </a>
                </li>


                <li>
                    <a href="javascript: void(0);">
                        <i class="mdi mdi-format-list-bulleted"></i>
                        <span> Product Categories </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul aria-expanded="false" class="nav-second-level">
                        <li><a href="{{ route('add-category') }}">Add New Category</a></li>
                        <li><a href="{{ route('categoryListing') }}">All Categories</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('products_listing') }}">
                        <i class="mdi mdi-format-list-bulleted"></i>
                        <span> Products</span>
                    </a>
					 
                    
                     
                    <?php /*
                        <ul aria-expanded="false" class="nav-second-level">
                             <li><a href="{{ route('admin-add-product') }}">Add New Product</a></li>
                           <li><a href="{{ route('products_listing') }}">All Products</a></li>
                        </ul>
                    */ ?>
                </li>

                <li>
                    <a href="{{ route('order-listing') }}">
                        <i class="mdi mdi-cart-outline"></i>
                        <span> Orders</span>
                    </a>
                </li>



                <li>
                    <a href="{{ route('customer-listing') }}">
                        <i class="mdi mdi-face"></i>
                        <span> Customers</span>
                    </a>
                    <?php /*
                    <ul aria-expanded="false" class="nav-second-level">
                        <li><a href="{{ route('add-customer') }}">Add New Customers</a></li>
                        <li><a href="{{ route('customer-listing') }}">All Customers</a></li>
                    </ul> */ ?>
                </li>

                <li>
                    <a href="{{ route('vendor-listing') }}">
                        <i class="mdi mdi-face"></i>
                        <span> Vendor/Shop</span>
                    </a>
                </li>
                <?php /*
                <li>
                    <a href="{{ route('promotions-listing') }}">
                        <i class="mdi mdi-web"></i>
                        <span>Promotions</span>
                    </a>
                </li> */ ?>

                <li>
                    <a href="javascript: void(0);">
                        <i class="mdi mdi-format-list-bulleted"></i>
                        <span> Setting </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul aria-expanded="false" class="nav-second-level">
                        <li><a href="{{ route('slide-listing') }}">Homepage slider</a></li>
                        <li><a href="{{ route('pages-listing') }}">Pages</a></li>
                        <li><a href="{{ route('faq-listing') }}">Faqs</a></li>
                        <li><a href="{{ route('minimum-order-listing') }}">Order range</a></li>
                    </ul>
                </li>


            </ul>
        </div>
        <div class="clearfix"></div>

    </div>

</div>
