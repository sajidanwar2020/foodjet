<style>
    li.page-item.active span {
        position: relative;
        display: block;
        padding: 0.5rem 0.75rem;
        margin-left: -1px;
        line-height: 1.25;
        color: #1c53a6;
        margin: 1px;
        background-color: #e9edf0;
        border: 1px solid #7c94ac;
    }
</style>
<div class="navbar-custom">
    <ul class="list-unstyled topnav-menu float-right mb-0">
        <?php /*
        <li class="dropdown notification-list">
            <a aria-expanded="false" aria-haspopup="false" class="nav-link dropdown-toggle  waves-effect"
               data-toggle="dropdown"
               href="#" role="button">
                <i class="fe-bell noti-icon"></i>
                <span class="badge badge-danger rounded-circle noti-icon-badge">9</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                <!-- item-->
                <div class="dropdown-item noti-title">
                    <h5 class="m-0">
                        <span class="float-right">
                            <a class="text-dark" href="">
                                <small>Clear All</small>
                            </a>
                        </span>Notification
                    </h5>
                </div>
                <!-- All-->
                <a class="dropdown-item text-center text-primary notify-item notify-all" href="javascript:void(0);">
                    View all
                    <i class="fi-arrow-right"></i>
                </a>

            </div>
        </li> */ ?>

        <li class="dropdown notification-list">
            <a aria-expanded="false" aria-haspopup="false"
               class="nav-link dropdown-toggle nav-user mr-0 waves-effect"
               data-toggle="dropdown" href="#" role="button">
               <?php /* <img alt="user-image" class="rounded-circle" src="{{ asset('/assets/site/admin/images/users/user-1.jpg') }}"> */ ?>
                <span class="pro-user-name ml-1">
                     @if(Session::has('is_admin') && !empty(Session::get('is_admin')))
                        {{Session::get('is_admin')['first_name']}} {{Session::get('is_admin')['last_name']}}

                    @endif
                    <i class="mdi mdi-chevron-down"></i>
               </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                <!-- item-->
                <div class="dropdown-header noti-title">
                    <h6 class="text-overflow m-0">Welcome !</h6>
                </div>
                <?php /*

                <!-- item-->
                <a class="dropdown-item notify-item" href="javascript:void(0);">
                    <i class="fe-user"></i>
                    <span>My Account</span>
                </a>

                <!-- item-->
                <a class="dropdown-item notify-item" href="javascript:void(0);">
                    <i class="fe-settings"></i>
                    <span>Settings</span>
                </a> */ ?>

                <div class="dropdown-divider"></div>

                <!-- item-->
                <a class="dropdown-item notify-item" href="{{ route('admin_logout') }}">
                    <i class="fe-log-out"></i>
                    <span>Logout</span>
                </a>

            </div>
        </li>
    </ul>

    <!-- LOGO -->
    <div class="logo-box">
        <a class="logo text-center" href="{{route('admin-dashboard')}}">
                        <span class="logo-lg">
                            <img alt="" height="36" src="{{ asset('/assets/site/images/logo-v2.svg') }}">
                            <!-- <span class="logo-lg-text-light">Xeria</span> -->
                        </span>
            <span class="logo-sm">
                            <!-- <span class="logo-sm-text-dark">X</span> -->
                            <img alt="" height="24" src="{{ asset('/assets/site/images/logo-v2.svg') }}">
                        </span>
        </a>
    </div>

    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <button class="button-menu-mobile disable-btn waves-effect">
                <i class="fe-menu"></i>
            </button>
        </li>

        <li>
            <h4 class="page-title-main">{{$title}} </h4>
        </li>

    </ul>
</div>
