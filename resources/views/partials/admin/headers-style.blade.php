<meta charset="utf-8"/>
<title>Admin Dashboard</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="description"/>
<meta content="" name=""/>
<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
<!-- App favicon -->
<link href="{{ asset('/assets/site/admin/images/favicon.ico') }}" rel="shortcut icon">

<!-- App css -->
<link href="{{ asset('/assets/site/admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/assets/site/admin/css/icons.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('/assets/site/admin/css/admin-app.css') }}" rel="stylesheet" type="text/css"/>
