<script src="{{ asset('/assets/site/admin/js/vendor.min.js') }}"></script>
<script src="{{ asset('/assets/site/admin/libs/jquery-knob/jquery.knob.min.js') }}"></script>
<?php /*<script src="{{ asset('/assets/site/admin/js/pages/dashboard.init.js') }}"></script> */ ?>
<script src="{{ asset('/assets/site/admin/js/app.min.js') }}"></script>

<script type="text/javascript">
    //alert();
    $(document).ready(function () {
        $(".file_preview").change(function () {
            image_preview = $(this).closest('.form-group').find('.cat_image_preview');
            filePreview(this,image_preview);
        });
        if($("#start_datetime,#end_datetime").length) {
            $("#start_datetime,#end_datetime").datepicker({dateFormat: 'yy-mm-dd'});
        }

        //alert();
        //----------------------------------------------------------------------

    });

    function get_child_category(parent_id)
    {
        $.ajax({
            method: "GET",
            url: "{{ route('child_category') }}",
            data: {parent_id: parent_id}
        }).done(function (data) {
            $('#child_category').html('');
            $('#child_category').html(data);
        });
    }
    function filePreview(input,image_preview) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                image_preview.html('<img src="'+e.target.result+'" width="250" height="150"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
