
<footer id="footer" class="footer-v3 align-left">
    <div class="container">
        <div class="footer-inner">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <h3 class="title-footer">Über Uns</h3>
                    <p>Mit mehr als 15 Jahren Erfahrung können wir stolz sagen, dass wir einer der besten im
                        Geschäft sind, ein vertrauenswürdiger Lieferant für mehr als 1000 Unternehmen...</p>
                    <a class="link-footer" href="{{ URL('page/about-us') }}" title="footer">Lesen Sie mehr <i
                            class="fa fa-long-arrow-right"></i></a>
                </div>
                <div class="col-md-2 col-sm-6">
                    <h3 class="title-footer">Informationen</h3>
                    <ul class="list-footer">
                        <li><a href="#" title="title">Lieferung</a></li>
                        <li><a href="#" title="title">Rechtliche Hinweise</a></li>
                        <li><a href="#" title="title">Allgemeine Geschäftsbedingungen</a></li>
                        <li><a href="{{ URL('page/about-us') }}" title="title">Über Uns</a></li>
                        <li><a href="#" title="title">Sichere Zahlung</a></li>
                        <li><a href="{{route('faq')}}" title="title">FAQs</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h3 class="title-footer">Holen Sie Es Berühren</h3>
                    <div class="social space-30">
                        <a href="#" title="t"><i class="fa fa-twitter"></i></a>
                        <a href="#" title="f"><i class="fa fa-facebook"></i></a>
                        <a href="#" title="y"><i class="fa fa-youtube-play"></i></a>
                        <a href="#" title="g"><i class="fa fa-google"></i></a>
                    </div>
                    <h3 class="title-footer">Zahlung Akzeptieren</h3>
                    <a href="#" title="paypal"><img src="{{asset('assets/site/images/paypal-footer.png')}}"
                                                    alt="images"></a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <h3 class="title-footer">Erhalten Sie Newsletter</h3>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</p>
                    <div class="subscribe">
                        <form action="#" method="POST" accept-charset="utf-8">
                            <input type="text" placeholder="Enter Your Email Address" class="input-text required-entry email validate-email"
                                   title="Sign up for our newsletter" id="newsletter" name="email">
                                      <button class="button button1 hover-white" title="Subscribe" type="button" id="subscriber_btn">Abonnieren<i
                                    class="fa fa-long-arrow-right"></i></button>
                        </form>
                    </div>
                    <!-- End subscribe -->
                </div>
            </div>
            <!-- End row -->
        </div>
        <!-- End footer-inner -->
    </div>
    <!-- End container -->
    <div class="footer-bottom box">
        <div class="container">
            <div class="box bottom">
                <p class="float-left">Copyright &copy;2020 FoodJet- All Rights Reserved.</p>
                <div class="float-right">
                    <ul class="menu-bottom-footer">
                        <li><a href="#" title="Contact Us">Kontaktieren Sie Uns</a></li>
                        <li><a href="#" title="Term of Use">Nutzungsbedingungen</a></li>
                        <li><a href="#" title="Privacy Policy">Datenschutzerklärung</a></li>
                        <li><a href="#" title="Site Map">Siteübersicht</a></li>
                    </ul>
                    <div class="language">
                        <span>Language:</span>
                        <div class="hover-menu">
                            <p>EN <i class="fa fa-angle-down"></i></p>
                            <div class="list-menu">
                                <ul>
                                    <li><a href="#" title="EN">EN</a></li>
                                    <li><a href="#" title="VN">VN</a></li>
                                </ul>
                            </div>
                        </div>
                        <span>Price:</span>
                        <div class="hover-menu">
                            <p>USD <i class="fa fa-angle-down"></i></p>
                            <div class="list-menu">
                                <ul>
                                    <li><a href="#" title="USD">USD</a></li>
                                    <li><a href="#" title="VND">VND</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End language -->
                </div>
            </div>
        </div>
        <!-- End container -->
    </div>
</footer>

<script type="text/javascript">
    $(document).ready(function () {
        //-----------------------------------------------------------------
        $(document).on('click', '#subscriber_btn', function(e) {
            $('#newsletter').next('span.msg_error').remove();
            error = false;
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
          //  alert($('#newsletter').val());
            if( ($('#newsletter').val() == "" ))
            {
                $('#newsletter').after( "<span class='msg_error'>Email is required!</span>" );
                $('#newsletter').css({border: '1px solid red'});
                error = true;
            } else if( $('#newsletter').hasClass('email') )
            {
                if (!emailReg.test($('#newsletter').val()))
                {
                    $('#newsletter').after( "<span class='msg_error'>Email format is wrong!</span>" );
                    $('#newsletter').css({border: '1px solid red'});
                    error = true;
                } else {
                    $('#newsletter').css({border: '1px solid #d5d5d5'});
                    $('#newsletter').next('span.msg_error').remove();
                }

            }
            else {
                $('#newsletter').css({border: '1px solid #d5d5d5'});
                $('#newsletter').next('span.msg_error').remove();
            }


            if (error)
            {
                return false;
            }else{
                $.ajax('{{ route('subscribe') }}', {
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', email: $('#newsletter').val()},
                    success: function (data) {
                        if(data == 0) {
                            $('#newsletter').after( "<span class='msg_success'>You have successfully subscribed.</span>" );
                        } else {
                            $('#newsletter').after( "<span class='msg_error'>This email is already subscribe.</span>" );
                            $('#newsletter').css({border: '1px solid red'});
                        }
                        setTimeout(function() {
                            $('#newsletter').next('span.msg_error,span.msg_success').remove();
                            $('#newsletter').css({border: '1px solid #d5d5d5'});
                        }, 5000);
                    }
                });
            }
        });
        //-----------------------------------------------------------------
    });
</script>
