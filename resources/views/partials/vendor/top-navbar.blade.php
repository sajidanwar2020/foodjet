<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">

            <li class="dropdown notification-list">
                <!-- Mobile menu toggle-->
                <a class="navbar-toggle nav-link">
                    <div class="lines">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </a>
                <!-- End mobile menu toggle-->
            </li>

            <?php /*
                <li class="dropdown notification-list">

                    <a class="nav-link dropdown-toggle waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <i class="fe-bell noti-icon"></i>
                        <span class="badge badge-danger rounded-circle noti-icon-badge">2</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-lg">

                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="m-0">
                                        <span class="float-right">
                                            <a href="" class="text-dark">
                                                <small>Clear All</small>
                                            </a>
                                        </span>Notification
                            </h5>
                        </div>
                    </div>
                </li>*/ ?>

            <li class="dropdown notification-list">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{asset('assets/site/admin/images/users/user-1.jpg')}}" alt="user-image" class="rounded-circle">
                    <span class="pro-user-name ml-1">
                            @if(Session::has('is_seller') && !empty(Session::get('is_seller')))
                            {{Session::get('is_seller')['first_name']}} {{Session::get('is_seller')['last_name']}}

                        @endif
                            <i class="mdi mdi-chevron-down"></i>
                        </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <!-- item-->
                    <div class="dropdown-header noti-title">
                        <h6 class="text-overflow m-0">Welcome !</h6>
                    </div>

                    <div class="dropdown-divider"></div>

                    @if(Session::has('is_seller') && !empty(Session::get('is_seller')))
                        <a href="{{ route('logout') }}" class="dropdown-item notify-item">
                            <i class="fe-log-out"></i>
                            <span>Logout</span>
                        </a>
                    @endif

                </div>
            </li>

        </ul>

        <!-- LOGO -->
        <div class="logo-box">
            <a href="{{ route('vendor_dashboard') }}" class="logo text-center">
                            <span class="logo-lg">
                                <!-- <img src="../assets/admin/images/logo-light.png" alt="" height="16"> -->
                                <span class="logo-lg-text-light">FoodJet</span>
                            </span>
                <span class="logo-sm">
                                <span class="logo-sm-text-dark">FJ</span>
                    <!-- <img src="../assets/admin/images/logo-sm.png" alt="" height="24"> -->
                            </span>
            </a>
        </div>

    </div> <!-- end container-fluid-->
</div>
<div class="topbar-menu">
    <div class="container-fluid">
        <div id="navigation">
            <!-- Navigation Menu-->
            <ul class="navigation-menu">
                @if(Session::has('is_seller') && Session::get('is_seller')['role_id'] =='3')
                    <li class="has-submenu">
                        <a href="{{ route('vendor_dashboard') }}"><i class="mdi mdi-view-dashboard"></i>Dashboard</a>
                    </li>
                    <li class="has-submenu">
                        <a href="{{ route('my-products') }}"> <i class="mdi mdi-invert-colors"></i>Products  <div class="arrow-down"></div></a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li>
                                        <a href="{{ route('add-product') }}">Add Products</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('my-products') }}">My products</a>
                                    </li>
									  <li>
                                        <a href="{{ route('vendor-import-product') }}">Import products</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="has-submenu">
                        <a href="{{ route('driver_listing') }}"> <i class="mdi mdi-invert-colors"></i>Drivers  <div class="arrow-down"></div></a>
                        <ul class="submenu megamenu">
                            <li>
                                <ul>
                                    <li>
                                        <a href="{{ route('add_driver') }}">Add Driver</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('driver_listing') }}">Driver listing</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="has-submenu">
                        <a href="{{ route('my_orders') }}"><i class="mdi mdi-view-dashboard"></i>My Orders</a>
                    </li>
                @endif

                @if(Session::has('is_seller') && Session::get('is_seller')['role_id'] =='4')

                    <li class="has-submenu">
                        <a href="{{ route('driver_orders') }}"><i class="mdi mdi-view-dashboard"></i>Orders</a>
                    </li>
                @endif

            <?php /*
                <li class="has-submenu">
                    <a href="{{ route('profile') }}"><i class="mdi mdi-view-dashboard"></i>Profile</a>
                </li>
                <li class="has-submenu">
                    <a href="{{ route('change_password') }}"><i class="mdi mdi-view-dashboard"></i>Change Password</a>
                </li> */ ?>

            </ul>
            <!-- End navigation menu -->

            <div class="clearfix"></div>
        </div>
        <!-- end #navigation -->
    </div>
    <!-- end container -->
</div>
