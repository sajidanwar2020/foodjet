<script src="{{asset('assets/site/admin/js/vendor.min.js')}}"></script>

<!-- knob plugin -->
<script src="{{asset('assets/site/admin/libs/jquery-knob/jquery.knob.min.js')}}"></script>

<!--Morris Chart-->
<script src="{{asset('assets/site/admin/libs/morris-js/morris.min.js')}}"></script>
<script src="{{asset('assets/site/admin/libs/raphael/raphael.min.js')}}"></script>

<!-- Dashboard init js-->
<?php /* <script src="{{asset('assets/site/admin/js/pages/dashboard.init.js')}}"></script> */ ?>

<!-- App js-->
<script src="{{asset('assets/site/admin/js/app.min.js')}}"></script>

<script>
    $(document).ready(function() {
        //-------------------------------------------------------------------------------------------------------

        $('#sale_price').keyup(function(){
            if (parseFloat($(this).val()) > parseFloat($('#regular_price').val()) || $('#regular_price').val() == ""){
                console.log('Regular price : '+$('#regular_price').val() +'--------- sale_price: '+$(this).val());
                alert("Sale value must be less then regular price");
                $(this).val('');
            }
        });

        //-------------------------------------------------------------------------------------------------------

        $(".product_img").change(function () {
            filePreview(this);
        });

        //-------------------------------------------------------------------------------------------------------
    });

    //-------------------------------------------------------------------------------------------------------

    function get_child_category(parent_id)
    {
        $.ajax({
            method: "GET",
            url: "{{ route('child_category') }}",
            data: {parent_id: parent_id}
        }).done(function (data) {
            $('#child_category').html('');
            $('#child_category').html(data);
        });
    }
    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.image_preview  img').remove();
                $('.image_preview').html('<img src="'+e.target.result+'" width="250" height="150"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
