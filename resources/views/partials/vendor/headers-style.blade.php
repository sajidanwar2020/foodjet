<meta charset="utf-8" />
<?php
$info = Session::get('is_seller');
if($info['role_id'] == 3)
    $redirect =  'Vendor Dashboard';
else if($info['role_id'] == 4)
    $redirect =  'Driver Dashboard';
?>
<title>{{$redirect}}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta content="" name="description" />
<meta content="" name="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!-- App favicon -->
<link rel="shortcut icon" href="{{asset('assets/site/admin/images/favicon.ico')}}">
<!--Morris Chart-->
<link rel="stylesheet" href="{{asset('assets/site/admin/libs/morris-js/morris.css')}}" />
<!-- App css -->
<link href="{{asset('assets/site/admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/site/admin/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/site/admin/css/vendor-app.css')}}" rel="stylesheet" type="text/css" />
