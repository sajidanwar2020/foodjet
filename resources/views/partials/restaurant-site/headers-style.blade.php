	@php
	$logo=asset('assets/restraurant-site/img/logo.png');
		if(isset($restaurant->logo) AND !empty($restaurant->logo)){
			$logo=url('uploads/vendor/logo/'.$restaurant->logo);
		}
	
	$color="#17a2b8";
	if(isset($restaurant->theme_color) AND !empty($restaurant->theme_color)){
		$color=$restaurant->theme_color;
	}
	@endphp
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Author Meta -->
	<meta name="author" content="tryngo">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<link rel="icon" href="{{$logo}}" type="image/png">
	<title>{{ (isset($restaurant->first_name) AND !empty($restaurant->first_name)) ? $restaurant->first_name : '' }}</title>

	<?php /*<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> */ ?>
	<!--
    CSS
    ============================================= -->
	
	<?php /*<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">  */ ?>
	<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/linearicons.css')}}"> 
	<?php /*<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/font-awesome.min.css')}}"> */ ?>
	<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/bootstrap.css')}}">
	<?php /*<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/magnific-popup.css')}}"> */ ?>
	<?php /*<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/jquery-ui.css')}}">	 */ ?>			
	<?php /*<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/nice-select.css')}}">	 */ ?>						
	<?php /*<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/animate.min.css')}}"> */ ?>
	<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/owl.carousel.css')}}">		
	<link rel="stylesheet" href="{{asset('assets/restraurant-site/css/main.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/restraurant-site/css/style.css?v=1')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/restraurant-site/css/main1.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/restraurant-site/css/fontawesome/css/all.css')}}">
		<?php /*<link rel="stylesheet" href="{{asset('assets/restraurant-site/dist/assets/owl.carousel.min.css')}}" /> */ ?>		
	<?php /* <link rel="stylesheet" href="{{asset('assets/restraurant-site/dist/assets/owl.theme.default.min.css')}}" /> */ ?>
	<script src="{{asset('assets/js/vendor/jquery-2.2.4.min.js')}}"></script>
	<link rel="stylesheet"  href="{{asset('assets/restraurant-site/css/slick/slick.css')}}">
	<link rel="stylesheet"  href="{{asset('assets/restraurant-site/css/slick/slick-theme.css')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.min.css" />
	<style>
	:root
	{
	--main-theme-color: {{$color}};
	}

	</style>
	
	

