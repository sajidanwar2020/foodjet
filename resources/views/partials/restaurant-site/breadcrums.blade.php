<section class="relative about-banner" style="background-image: url('{{asset('assets/restraurant-site/img/hero-bg.png')}}'); padding-top: 60px;">
	<div class="overlay overlay-bg"></div>
	<div class="container">
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
					{{$title}}
				</h1>
				<?php /*<p class="text-white link-nav"><a href="home">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="check-out"> {{$title}}	</a></p> */ ?>
			</div>
		</div>
	</div>
</section>