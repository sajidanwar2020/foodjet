	@php
		$currency=isset($restaurant->currency)?$restaurant->currency:'CHF';
	@endphp
<script type="text/javascript">
   	$( document ).ready(function() {
			//---------------- show popup when click on normal product-------------------------
			$( ".menu_item_outer" ).click(function() {	
				var p_id = $(this).attr('data-id');
				$.ajax({
					method: "GET",
					url: "{{ route('home_product_detail') }}",
					data: {p_id: p_id}
				}).done(function (data) {
					$('#product_detail_popup').html('');
					$('#product_detail_popup').html(data);
					$('#cartModal2').modal('show'); 
				});
			});
			
			//-------------------show popup when click on compsed product based on type ----------------------
			$( ".composer_item_outer" ).click(function() {	
				var p_id = $(this).attr('data-id');
				var p_type = $(this).attr('data-type');
				$.ajax({
					method: "GET",
					url: "{{ route('home_product_detail') }}",
					data: {p_id: p_id,'menu_composer':'yes','p_type':p_type}
				}).done(function (data) {
					$('#product_detail_popup').html('');
					$('#product_detail_popup').html(data);
					$('#cartModal2').modal('show'); 
				});
			});
			$( "#cartModal2" ).on('shown.bs.modal', function(){
				$( ".quantity_type:first" ).trigger('click');
			});
			//--------------------- Show message that how many item can select ------------------------------------------------
			
			$(document).on("click", ".quantity_type", function(){
				if($(this).attr('data-price')) 
					$('#p_price').html('{{$currency}}'+$(this).attr('data-price'));
					var quantity_type_val = $(this).val();
					$('.table-settings').css('display','none');
					
					// start show message in case of mixture product	
					var params = $('.current a .step-order').data('params');
					if(params) {
						//alert();
						var item_id = $('.current a .step-order').data('item-id');
						//alert('#quantity_'+quantity_type_val+'_'+item_id)
						$('#quantity_'+quantity_type_val+'_'+item_id).css('display','block');
						data = JSON.parse(JSON.stringify(params));
						limit_selected = data[item_id][quantity_type_val];
						//alert(limit_selected);
						$('.item_can_select').html('Vous pouvez sélectionner au maximum '+limit_selected+' articles');
					}
					// end of mixture product				
					
				if($(this).attr('data-max') && params == "") {
					var _max_item = $(this).attr('data-max');
					$('.item_can_select').html('Vous pouvez sélectionner au maximum '+_max_item+' articles');
					$('.item_can_select').attr('data-max',_max_item);
				}
				$("#product_detail_popup input.p_attribute").prop('checked', false); 
			})	
			
			//-------------------------------- Item checkbox show on change event . show/hide comment box --------------
			
			$(document).on("change", ".attrbuite_outer .p_attribute", function(){
				var attribute_id = $(this).data('attribute_id');
				if ($(this).is(':checked')) {
					$('#special_note_'+attribute_id).css('display','block');
				} else {
					$('#special_note_'+attribute_id).css('display','none');
				}
			})
			//--------------------------------How many item can select on simple compsed product. Disbale checkboxs --------------
			
			$(document).on("change", "._items_simple", function(){
			//$('._items').change(function(e){
			    var limit_sel = $(this).closest('section').find(".item_can_select");
			    var limit_val = limit_sel.attr('data-max');
				var item_price_sum = quantity_type_price = 0;
				$('#product_detail_popup input.p_attribute:checkbox:checked').each(function () {
					if($(this).data('item-price') > 0)
						item_price_sum = item_price_sum + $(this).data('item-price');
				}); 
				
				if(item_price_sum > 0) {
					quantity_type_price = parseFloat( $('#product_detail_popup input.quantity_type:checked').attr('data-price'));
					var final_price =  quantity_type_price + parseFloat(item_price_sum);
					$('#p_price').html('CHF'+final_price);
				}
	
			   if ($('._items_simple:checked').length > limit_val) {	
					limit_sel.addClass('_error1');
					$(this).prop('checked', false)
			   }
			})
			
			//-----------------------------How many item can select on mixture compsed product Disbale checkboxs--------------------------------
			
			$(document).on("change", ".current ._items_mixture", function(){
			    limit_selected = $('.current a .step-order').attr('data-max');
			    var quantity_type_val = $('#product_detail_popup input.quantity_type:checked').val();
				//alert(quantity_type_val);
			    var params = $('.current a .step-order').data('params');
				if(params) {
					var item_id = $('.current a .step-order').data('item-id');
					data = JSON.parse(JSON.stringify(params));
					limit_selected = data[item_id][quantity_type_val];
					//$('.item_can_select').html('Vous pouvez sélectionner au maximum '+limit_selected+' articles');
				}	
				
			    if ($('.current ._items_mixture:checked').length > limit_selected) {	
					//limit_sel.addClass('_error1');
					$(this).prop('checked', false)
			    }
			})
			//----------------------------- When new order placed then popup will show--------------------------------
			
			@if ($message = Session::get('order'))
				$('#order-status').modal('show'); 
			@endif
			//--------------------------------------------------------------
		
			$('#bellIcon').click(function(){
				callWaiter();
			});
				
			//--------------------------------------------------------------
			
		});
		
		function callWaiter(){
		    $.ajax({
		           type: "POST",
		           url: "{{ route('callwaiter') }}",
		           data: {
		           		'restaurant_id':8,
		           		'table_id':1,
		           		'_token': "{{ csrf_token() }}" 
		           	},
		           success: function(data) {
		              //console.log("success");
		             // console.log(data.response);
		              $('#successTextModal').modal();
		           },
		           error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
		              /*setTimeout(onevehiclecall,2000,vehicle_plate_number);*/
		             // console.log(JSON.stringify(jqXHR));
		            //console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
		        }
		    });
		}
</script>
