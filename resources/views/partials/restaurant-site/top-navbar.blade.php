@php
	$logo=asset('assets/restraurant-site/img/logo.png');
	if(isset($restaurant->logo) AND !empty($restaurant->logo)){
		$logo=url('uploads/vendor/logo/'.$restaurant->logo);
	}
	$restaurantDetail = Session::get('restaurantDetail');
@endphp
<header id="header">
	<div class="container main-menu">
		<div class="row align-items-center justify-content-between height-set">
			<a class="navbar-brand" href="home"><img src="{{$logo}}"></a>
			<p class="text-white">
				{{isset($restaurant->logo_text)?$restaurant->logo_text:''}}
			</p>
			<div class="mar-set1">
				<a href="<?php echo route('booking', ['id' => $restaurantDetail->restaurant_code]); ?>">
					<img src="{{ asset('assets/fonts/reservation25.png') }}" style="width: 25px; height: auto; padding-bottom: 5px;" data-toggle="tooltip" title="Reservation">
				</a>
				<a href="">
					<img src="{{ asset('assets/fonts/takeaway.png') }}" style="width: 18px; height: auto; padding-bottom: 6px;" data-toggle="tooltip" title="Takeaway">
				</a>
			</div>
			<div class="mar-set1">
				<a id="bellIcon" >
					<i class="fas fa-bell  bell" data-toggle="tooltip" title="Waiter call"></i></a>
				@if(isset($restaurantDetail->accessibility) && ($restaurantDetail->accessibility == 2 || $restaurantDetail->accessibility == 3))
					<a href="javascript:void(0)" id="cart_popup">
						<i class="fas fa-shopping-cart cart-set" data-toggle="tooltip" title="Cart"></i>
						<span class="badge style-set cart-count" > {{Cart::count()}} </span>
					</a>
				@endif
			</div>
			<nav id="nav-menu-container">
				<ul class="nav-menu">
					<li><a href="<?php echo route('home-sub', ['id' => $restaurantDetail->restaurant_code]); ?>">Accueil</a></li>
					<li><a href="<?php echo route('ingredients-for-web', ['id' => $restaurantDetail->restaurant_code]); ?>">Origines</a></li> 
					<li><a href="<?php echo route('glossary', ['id' => $restaurantDetail->restaurant_code]); ?>">Glossaire</a></li>
					<li><a href="<?php echo route('contact-us', ['id' => $restaurantDetail->restaurant_code]); ?>">Contact</a></li>

					@if((Session::has('is_logged_in') && !empty(Session::get('is_logged_in'))) || Cookie::has("order_id"))
						<li class="level1"><a href="{{ route('customer_orders', ['id' => $restaurantDetail->restaurant_code]) }}">My orders</a></li>
					@endif

					@if(Session::has('is_logged_in') && !empty(Session::get('is_logged_in')))
						<li class=""><a href="{{ route('logout') }}">Logout</a></li>
					@endif

					@if(!Session::has('is_logged_in') && empty(Session::get('is_logged_in')))
						<li class=""><a href="signin">Sign in</a></li>
					@endif

				</ul>
			</nav><!-- #nav-menu-container -->
		</div>
	</div>
</header><!-- #header -->