<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/vendor/bootstrap.min.js')}}"></script>
<?php /*				
<script src="{{asset('assets/js/jquery-ui.js')}}"></script>					
<script src="{{asset('assets/js/easing.min.js')}}"></script>			
<script src="{{asset('assets/js/hoverIntent.js')}}"></script>
<script src="{{asset('assets/js/superfish.min.js')}}"></script>	
<script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>						
<script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script> */ ?>					
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>	
<?php /*		
<script src="{{asset('assets/js/isotope.pkgd.min.js')}}"></script>								
<script src="{{asset('assets/js/mail-script.js')}}"></script>	
*/ ?>	
<script src="{{asset('assets/js/main.js')}}"></script>
<script src="{{asset('assets/restraurant-site/css/slick/slick.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.min.js"></script>
<script src="http://cdn.craig.is/js/rainbow-custom.min.js"></script>

<script type="text/javascript">
	$(document).on('show.bs.tooltip', function (e) {
		setTimeout(function() {   //calls click event after a certain time
			$('[data-toggle="tooltip"]').tooltip('hide');
		}, 1500);
	});

    $(document).ready(function () {
		
        $(document).on("click", ".add-cart", function () {
			var attrbuite_arr = [];
			$('#product_detail_popup input.attrbuite_detail:checkbox:checked').each(function () {

				// create object assoctive array of product quantity and attribute id 
				obj = {};
			   // obj[$(this).closest('.attrbuite_outer').find('.product_selection .qunatity').val()] = $(this).attr('data-attribute_id');
				//obj[counter444] = 'test comment';
			    obj['quantity'] = $(this).closest('.attrbuite_outer').find('.product_selection .qunatity').val();
			    obj['attribute_id'] = $(this).attr('data-attribute_id');
				obj['special_note'] = $('#special_note_'+$(this).attr('data-attribute_id')+' textarea').val();
				attrbuite_arr.push(obj);	
			}); 
			//console.log(attrbuite_arr);
			//return false;
			if (attrbuite_arr.length == 0) {
				$('#validate_error').html('Veuillez sélectionner au moins une quantité');
				return false;
			} 	
			
            _this = $(this);
            var pid = $(this).attr('data-id');
			var special_note  = $('#special_note').val();
            var cart_id = $(this).attr('data-cart_id');
			$.ajax('{{ route('add_to_cart') }}', {
				method: 'POST',
				data: {'_token': '{{ csrf_token() }}',  special_note:special_note,pid: pid, product_id: pid,attrbuite_arr:attrbuite_arr},
				success: function (data) {
					//alert(data);
					increment_cart_box(data);
					$('#cartModal2').modal('hide');
					//ajax_cart_html();
				}
			});
        });

        //-------------------------------------Menu composer add to cart ------------------------------------------------ 
		
		$(document).on("click", ".add-cart-menu-composer", function () {
			add_cart_menu_composer_fun();
        });
		
        //-------------------------------------------------------------------------------------------------------------
		$(document).on("click", "#cart_popup", function () {
			ajax_cart_html();
        });
		//-------------------------------------------------------------------------------------------------------------
		$('[data-toggle="tooltip"]').tooltip();
		//-------------------------------------------------------------------------------------------------------------
		//    $(".center").slick({
        //     slidesToShow: 3,
        //     slidesToScroll: 1,
        //     dots: false,
        //     infinite: false,
        //     cssEase: 'linear',
        //     variableWidth: true,
        //     variableHeight: true
        // });
		$('.center').slick({
			// centerMode: true,
			slidesToShow: 3,
			swipeToSlide: true,
			dots: false,
			infinite: false,
			cssEase: 'linear',
			speed: 250,
			variableWidth: true,
			// variableHeight: true
			mobileFirst: true, //optional, to be used only if your page is mobile first
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 9,
					swipeToSlide: true,
					// centerMode: true,
					variableWidth: true,
				}
			}]
		});
		
		$('.center2').slick({
			// centerMode: true,
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			infinite: false,
			cssEase: 'linear',
			speed: 250,
			variableWidth: true,
			// variableHeight: true
			mobileFirst: true, //optional, to be used only if your page is mobile first
			responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 9,
					slidesToScroll: 2,
					infinite: false,
					// centerMode: true,
					variableWidth: true,
					infinite: false,
				}
			}]
		});

		//-----------------------------------------------------------------------------------------------------------
		/*
		$(document).on("click", ".center .slick-slide a", function () {
			$('.center2.slider').css('display','none');
			var parent = $(this).attr('data-parent');
			if(parent) {
				//$('#sub_menu_'+parent).css('display','block');
			}
		}); */
		
		$(document).on("click", ".top_category_outer .top_category", function () {
			// remove and add class to food and drinks
			$('.top_category_outer li').removeClass('active');
			$(this).parent().addClass('active');
			//---------------------------------------
			var top_category_id = $(this).attr('data-top_category');
			$('.center.slider').css('display','none');
			$('#categories_type_'+top_category_id).css('display','block');
			// Hide all product first
			$('.product_container').css('display','none');
			// Display product block based on top category
			$('.top_category_id_'+top_category_id).css('display','block');
			//-------- Animate to first category
			parent_id = $('#categories_type_'+top_category_id+'  a').attr('data-parent');
			 $('html, body').animate({
				scrollTop: $("#menu_"+parent_id).offset().top - 138
			}, 500);
		});  
		
    });
	
	//-------------------------------------------------End of document body -------------------------------------------

    function hide_cart_count() {
        var count = $('.cart-count').html();
        if (count == 0)
            $('.cart-count').hide();
    }
	
	//----------------------------------------------------------------------------------------------------------------- 



	function increment_cart_box(count) {
		$('.cart-count').html(count);
		$('.cart-count').show(); 
    }
	
	//--------------------------------------------------------------------------------------------------------------------  
	
	function ajax_cart_html() {
	   $.ajax('{{ route('get_cart_html') }}', {
			method: 'GET',
			data: {'_token': '{{ csrf_token() }}', html: 'yes'},
			success: function (data) {
				$('#cart_html').html(data);
				$('#cartModal2').modal('hide');
				$('#cartModal').modal('show');
			}
		});
	}
	
	//----------------------------------------------------------------------------------------------------------------- 
	
	function add_cart_menu_composer_fun() {
		var attrbuite_arr = [];
		var item_price_sum = 0;
		$('#product_detail_popup input.p_attribute:checkbox:checked').each(function () {
			attrbuite_arr.push($(this).val());
			//console.log($(this).data('item-price'));
			if($(this).data('item-price') > 0)
				item_price_sum = item_price_sum + $(this).data('item-price');
		}); 
		//console.log(item_price_sum); 
		if (attrbuite_arr.length == 0) {
			$('#validate_error').html('Veuillez sélectionner au moins une quantité');
			return false;
		}
			
		var quantity_type = $('#product_detail_popup input.quantity_type:checked').val();
		var price = parseFloat( $('#product_detail_popup input.quantity_type:checked').attr('data-price')) + parseFloat(item_price_sum);
		var pid = $('#menu_composer_id').val();
		var type = $('#menu_composer_type').val();
		var special_note  = $('#special_note').val();
		
		$.ajax('{{ route('add_to_cart') }}', {
			method: 'POST',
			data: {'_token': '{{ csrf_token() }}', special_note:special_note,pid: pid,attrbuite_arr:attrbuite_arr,quantity_type: quantity_type,price: price,type:type,'composer':'yes'},
			success: function (data) {
				increment_cart_box(data);
				$('#cartModal2').modal('hide');
				//ajax_cart_html();
			}
		}); 
	}
	//-------------------------vibrate when click on top menu category slider----------------------------- 
	function play_sound() {
		if (window.navigator && window.navigator.vibrate) {
		  navigator.vibrate(50); 
		} else {
		   console.log('not shake');
		}
	}
	//--------------------------------------------------------------------------------------------------------
	// var slider = document.getElementById("myRange");
	// var output = document.getElementById("demo");
	// output.innerHTML = slider.value; // Display the default slider value
	//
	// // Update the current slider value (each time you drag the slider handle)
	// slider.oninput = function() {
	// 	output.innerHTML = this.value;
	// }

	$(document).ready(function() {
		$.datetimepicker.setLocale('pt-BR');
		$('#datetimepicker').datetimepicker();
	});
	
</script>


