<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			
			<div class="modal-header border-bottom-0">
				<h5 class="modal-title" id="exampleModalLabel"> Voir votre commande</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div id="cart_html"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="order-status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content hset">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div class="modal-body pos-set">
				<img src="{{asset('assets/img/healthy-food.svg')}}" style="width: 120px; height: 120px; margin-bottom: 20px;">
				<h3 class="mb-2">Nous vous remercions de votre commande</h3>
				<p class="mb-2">
					@if ($message = Session::get('order')) 
						{!!$message !!}
					@endif
				</p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="successTextModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content hset">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div class="modal-body pos-set">
				<img src="{{asset('assets/img/waiter.svg')}}" style="width: 120px; height: 120px; margin-bottom: 20px;" align="center">
				<h3 align="center" class="mb-2">Le serveur arrive</h3>
				<button type="button" class="btn btn-lg btn-outline-info text-light text-capitalize" data-dismiss="modal">Return</button>
			</div>
		</div>
	</div>
</div> 
	
<footer class="footer-area mt-4">
	<div class="footer-widget-wrap">
		<div class="container">
			<div class="row section-gap">
				
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h4>Liens rapides</h4>
						<ul class="hr-list">
							<li><a href="javascript:void(0)" data-toggle="modal" data-target="#conditions_generales_de_vente">Conditions générales E-menew</a></li>
							<li><a href="#!" data-toggle="modal" data-target="#exampleModal2">Mentions légales</a></li>
							<li><a href="https://www.lemonway.com/conditions-generales-dutilisation/" target="_blank">Conditions générales services de paiement</a></li>
						</ul>
					</div>
				</div> 
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h4>Nous contacter</h4>
						<p>
							<?php echo isset($restaurant->location) ? $restaurant->location : ''; ?>
						</p>
						<p class="number">
							<?php echo isset($restaurant->phone_number) ? $restaurant->phone_number : ''; ?>
						</p>
					</div>
				</div>
				<div class="col-lg-4  col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h4>Bulletin</h4>
						<p>Vous pouvez nous faire confiance. nous n'envoyons que des offres promotionnelles, pas un seul spam.</p>
						<div class="d-flex flex-row" id="mc_embed_signup">


							<form class="navbar-form" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get">
								<div class="input-group add-on align-items-center d-flex">
									<input class="form-control" name="EMAIL" placeholder="Your Email address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email address'" required="" type="email">
									<div style="position: absolute; left: -5000px;">
										<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
									</div>
									<div class="input-group-btn">
										<button class="genric-btn"><span class="lnr lnr-arrow-right"></span></button>
									</div>
								</div>
								<div class="info mt-20"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-wrap">
		<div class="container">
			<div class="row footer-bottom d-flex justify-content-between align-items-center">
				<p class="col-lg-8 col-mdcol-sm-6 -6 footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> Tous droits réservés <a href="https://tryngo-services.com/tryngo-services-francais/" target="_blank">Tryngo Services</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				<ul class="col-lg-4 col-mdcol-sm-6 -6 social-icons text-right" style="display:none">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
					<li><a href="#"><i class="fa fa-behance"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>



<div class="modal fade conditions-pupup" id="conditions_generales_de_vente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Conditions Générales de Vente du service E-menew de Tryngo Services Sàrl</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-4 bg-light">
       	
 <h6>1- CHAMP D’APPLICATION</h6>
 <p>
Les conditions générales de vente (ci-après désignées « CGV ») s’appliquent à toutes les commandes passées et contrats conclus (ci-après désignés par « Commande ») via le site internet ou la place de marché en ligne www.e-menew.com de Tryngo Services Sàrl (ci-après désigné « E-menew »), mise à disposition des établissements d’hôtellerie et de restauration.

E-menew se réserve le droit de modifier ses CGV. La version des CGV en vigueur et celle du moment auquel la Commande est confirmée. Elle ne peut pas être modifiée de manière unilatérale pour la Commande en question.
</p>
<h6>2- OFFRE</h6>
<p>
L’offre s'adresse aux établissements d’hôtellerie ou de restauration qui ont leur domicile ou leur siège social en Suisse, dans l’Union Européenne ou au Royaume-Uni (ci-après « Utilisateurs Professionnels ») et qui souhaitent proposer leurs services aux consommateurs et clients finaux (ci-après les « Consommateurs »).

Les services offerts sont constitués par une plateforme technologique et d’hébergement de données (ci-après désignés « Services E-menew » ou « Services ») qui permettent aux Utilisateurs Professionnels de proposer des services de restauration, de service en chambre ou d’hébergement à leurs Consommateurs (ci-après « Services de Tiers »).

Selon la formule d’abonnement choisie, les Services E-Menew permettent aux Utilisateurs Professionnels de proposer et d’actualiser leur menu ou proposition de services en temps réel, de prendre les commandes de consommateurs en ligne (ci-après les Commandes-Consommateurs »), de percevoir le règlement de ces Commandes-Consommateurs et de bénéficier de statistiques et d’informations marketing et commerciales.

Les offres et les différentes formules d’abonnement ou souscription demeurent valables tant qu’elles apparaissent sur le site E-menew. Des modifications de prix et de formule d’abonnement ou de souscription sont possibles en tout temps. Les images présentées dans les publicités, les prospectus, le site internet, servent d’illustration à titre purement indicatif et ne sont pas contractuelles. Les images, illustrations, descriptions et offres liée aux menus demeurent de la responsabilité exclusive des Utilisateurs Professionnels.
</p>
<h6>3- PRIX</h6>
<p>
Les tarifs des services proposés et facturés aux Utilisateurs Professionnels s'entendent en francs suisses pour la Suisse, en euros pour les pays de la Communauté Européenne, en livres sterling pour le Royaume-Uni. Dans tous les cas, ces tarifs sont exprimés taxe sur la valeur ajoutée (TVA) exclue.

Le tarif indiqué sur le site internet E-menew et sélectionné par l’Utilisateur Professionnel au moment de la Commande fait foi.

Les prestations supplémentaires payantes telles que saisies complémentaires, assistance, location et mise à disposition de matériel, etc. apparaissent séparément dans le panier ou sur les factures complémentaires.
</p>
<h6>4- COMMANDE</h6>
<p>
Les différentes formules d’abonnement ou de souscription aux Services sont présentées sur le site E-menew. E-menew se réserve le droit de restreindre les quantités de Commandes acceptées pour certaines formules ou dans le cadre de certaines promotions et de ne pas accepter la Commande si le quota de promotion est atteint ou momentanément si des contraintes techniques l’impose.

Tout Utilisateur Professionnel potentiel des Services peut transmettre une demande d’ouverture de compte via le site internet E-menew. Cette demande est considérée comme demande de conclusion d’un contrat auprès de E-menew. Cette demande comprend une demande d’ouverture de compte de paiement qui fait l'objet d'un contrôle dit "KYC" (Know Your Customer) de la part des fournisseurs de solution de paiement de E-menew. Chaque demande doit donc être accompagnée des justificatifs demandés et est sujette à acceptation de E-menew et de ses fournisseurs de solutions de paiement.

E-menew et ses fournisseurs de solution de paiement peuvent à tout moment et suivant leur libre appréciation refuser d’ouvrir un compte de paiement à un utilisateur potentiel sans que cette décision ne puisse donner lieu à des dommages et intérêts en faveur de l'utilisateur potentiel.

Après transmission de la Commande, l’Utilisateur Professionnel reçoit automatiquement un accusé de réception, qui confirme que la Commande est bien parvenue à E-menew. L’Utilisateur Professionnel ne peut alors plus compléter ou annuler sa Commande, sauf à ce que E-menew lui demande de fournir des documents complémentaires.
</p>
<h6>5- CONCLUSION, DURÉE DU CONTRAT</h6>
<p>
Le contrat est conclu pour la durée prévue après traitement de la Commande par l’administration E-Menew et son acceptation par E-menew et ses fournisseurs de solution de paiement en ligne.

L’Utilisateur Professionnel dont la Commande est acceptée par E-menew devient client (ci-après le « Client »). Dès acceptation de sa Commande, le Client reçoit automatiquement par e-mail une confirmation et ses identifiants lui permettant d’accéder aux Services.
</p>
<h6>6- POSSIBILITÉS DE PAIEMENT</h6>
<p>
Les paiements sont effectués en francs suisses pour la Suisse, en euros pour les pays de l’Union Européenne, en livres sterling pour le Royaume-Uni.

Les moyens de paiement acceptés sont les suivants :
Cartes Visa et Mastercard

En cas de paiement par carte de crédit ou de débit, le prélèvement a lieu après traitement et acceptation de la Commande-Consommateur par E-menew.

Avant acceptation de la Commande-Consommateur, E-menew et ses fournisseurs de solution de paiement effectuent une série de vérifications préalables.

Si le Client a droit à des remboursements ou des avoirs, ils seront crédités sur son compte client.
</p>
<h6>7- SÉCURITÉ DES PAIEMENTS</h6>
<p>
Les fournisseurs de solution de paiement de E-menew sont soumis aux orientations de l'Autorité Bancaire Européenne, de la Directive (UE) 2015/2366 du Parlement européen et du Conseil du 25 novembre 2015 concernant les services de paiement dans le marché intérieur ainsi qu'au Règlement délégué (UE) 2018/389 de la Commission du 27 novembre 2017 complétant la directive (UE) 2015/2366 du Parlement européen et du Conseil par des normes techniques de réglementation relatives à l'authentification forte du client et à des normes ouvertes communes et sécurisées de communication.
Ainsi, une authentification forte (3D Secure ou toute autre norme technique en vigueur) est appliquée pour toutes les opérations de paiement effectuées par carte bancaire à destination du Compte de paiement bénéficiaire. Des dérogations peuvent être applicables en fonction de la nature de l'opération, du niveau de risque de l’opération, du montant, du caractère récurrent et du moyen utilisé pour exécuter l’opération de paiement. En cas de tokenisation d’une carte bancaire, l’enregistrement de la carte bancaire ou de paiement pourra être refusé ou annulée par mesure de sécurité ou sur demande de son prestataire de services de paiement.
</p>
<h6>8- CHARGEBACKS</h6>
<p>
Le Client prend à sa charge (i) le montant de tous les Chargebacks et (ii) les frais de Chargebacks. Il est précisé que le Client doit se conformer pendant toute la durée du Contrat au taux de Chargeback trimestriel suivant : 0,1 % maximum. Si le taux de Chargeback trimestriel détecté pour les opérations en cartes bancaires dépasse 0,3 % en volume, E-Menew peut modifier immédiatement les tarifs par simple notification ou procéder à la résiliation du contrat pour une faute réputée grave. 
</p>
<h6>9- RETARD DE PAIEMENT</h6>
<p>
Si le Client accuse un retard de paiement complet ou partiel, tous les montants impayés sous quelque forme que ce soit sont dus immédiatement et E-menew peut en exiger le paiement sans délai. En cas de retard de paiement, E-menew se réserve le droit d’interrompre la délivrance des Services.

En cas de retard de paiement, E-menew se réserve le droit de facturer des frais de rappel et intérêts moratoires au Client. Tous les frais liés à l’obtention du règlement de créances en souffrance sont à la charge du Client. En cas de sommations infructueuses, les montants facturés peuvent être cédés à une entreprise de recouvrement. L’entreprise de recouvrement fera valoir les sommes impayées en son propre nom et pour son propre compte. Elle pourra percevoir des frais de dossier supplémentaires et des intérêts moratoires à partir de la date d’échéance.

Jusqu’à paiement complet de tous les montants dus, le Client ne peut réclamer aucun dédommagement/crédit pour la période de validité inutilisable de son abonnement.
</p>
<h6>10- GARANTIE</h6>
<p>
E-menew fournit ses prestations dans le cadre de ses ressources entrepreneuriales et des exigences prévisibles de façon soigneuse et conformément aux règles de l'art, dans la mesure où E-menew ne se trouve pas dans l'incapacité de fournir les prestations pour des motifs dont elle n'est pas responsable.

Le Client sait que E-menew fournit ses prestations via Internet, notamment en se servant de réseaux de communication. Il peut y avoir des perturbations provisoires ou des interruptions de l'exécution des prestations de E-menew, notamment en raison de dysfonctionnements techniques, de dérangements et de perturbations ou d'interruptions de réseaux de communication et en raison d'une panne d'infrastructures IT, ou d'autres parties de l'infrastructure nécessaire pour exécuter la prestation. E-menew n'accorde donc aucune garantie pour la disponibilité permanente et l'absence d'erreurs pour ses prestations.

Dans des conditions normales d’utilisation, E-menew garantit la disponibilité des Services 24 heures sur 24 et 7 jours sur 7, avec un taux de disponibilité de 99,5 % et une garantie de taux de rétablissement de l’Environnement de quatre (4) heures.
</p>
<h6>11- LIMITATION DE RESPONSABILITÉS</h6>
<p>
En cas de violations de ses propres obligations contractuelles résultant des présentes CGV, E-menew est responsable vis-à-vis de son Client à hauteur du montant de l’abonnement pour la période considérée pour les dommages directs et prouvés, occasionnés par E-menew par intention illégale ou négligence grossière.

Toute responsabilité est expressément exclue pour des dommages indirects ou des dommages consécutifs. Les dommages consécutifs sont en particulier les manques à gagner, les dommages occasionnés à la réputation et la perte de données suite à des perturbations préalables ou à des interruptions de la disponibilité des prestations de E-menew et suite à la défaillance des canaux de commercialisation, les erreurs de transmission, de prestations erronées et d'erreurs dans les confirmations de Commande-Consommateur. Toute responsabilité de E-menew est exclue pour les contenus des sites de tierces parties qui renvoient aux site E-menew ou auxquels le site E-menew renvoie.
</p>
<h6>12- RESPONSABILITÉS DES CLIENTS</h6>
<p>
E-menew ne peut pas contrôler l'exhaustivité, l'exactitude et la licéité des informations publiées par ses Clients.

E-menew ne peut pas non plus contrôler si son Client remplit correctement et intégralement ses obligations à prestations liées ou résultant du contrat conclu entre ce dernier et le Consommateur. Seuls les Clients, Utilisateurs Professionnels des Services E-menew, et en aucun cas E-menew, sont responsables des violations contractuelles ou d’une qualité insuffisante des prestations vis à vis du Consommateur.

Les Utilisateurs Professionnels utilisant les Services E-menew reconnaissent qu'ils demeurent seuls responsables de la prestation qu’ils proposent à leurs propres clients. Il est ainsi porté à la connaissance des consommateurs que E-menew ne fournit ni n’est responsable des Services de Tiers, notamment de la qualité des produits de restauration servis ni de la bonne exécution des Services de Tiers, et que l'ensemble des dits Services de Tiers sont fournis par des prestataires tiers qui ne sont pas employés par E-menew.

Les illustrations et les informations mises à disposition des Consommateurs concernant les produits (liste des ingrédients, valeurs nutritives, indications relatives aux allergènes, etc.) servent d’illustration à titre purement indicatif et relèvent de la responsabilité exclusive des Utilisateurs Professionnels.
</p>
<h6>13- CONFIDENTIALITÉ</h6>
<p>
E-Menew et ses Clients (les « Parties ») s'engagent à traiter comme confidentiels, les informations et documents échangés entre eux et auxquels ils auraient pu avoir accès au cours de l'exécution du contrat et de la période précontractuelle. Chaque Partie s'engage en particulier à garder strictement confidentielles toutes les informations recueillies du fait de sa présence dans les locaux de l’autre Partie et à observer la plus grande discrétion quant aux techniques, moyens et procédés de l’autre Partie, dont elle aurait été amenée à partager la connaissance du fait de l'exécution du Contrat.
Chacune des Parties s'oblige à garder strictement confidentielles les Informations Confidentielles, à s'abstenir de les communiquer à quiconque, sauf aux fins strictement nécessaires à la bonne exécution du Contrat, et à s'abstenir de les exploiter, directement ou indirectement, à toute fin autre que la bonne exécution du Contrat.
</p>
<h6>14- PROTECTION DES DONNÉES</h6>
<p>
En application de l’article 13 de la Constitution fédérale suisse, de la loi fédérale suisse sur la protection des données (LPD) du 19 juin 1992, de l’Ordonnance relative à la loi fédérale suisse sur la protection des données (OLPD) du 14 juin 1993, de la loi française 78-17 du 6 janvier 1978 et du Règlement UE 2016/679 (RGPD), en application le 25 mai 2018, il est rappelé que les données à caractère personnel qui sont demandées aux Utilisateurs Professionnels sont nécessaires à la création de leur compte sur le site internet leur permettant l'accès au Services. La collecte et le traitement de ces données ont comme finalité la satisfaction des exigences de KYC fixées par nos fournisseurs de solution de paiement, le traitement des abonnements pour les Utilisateurs Professionnels et le traitement des Commandes-Consommateurs.
A cet effet, l'Utilisateur Professionnel se verra offrir la possibilité de cocher une case en ligne pour confirmer son consentement au moment de son enregistrement sur le site, l'informant de ses droits au titre des règlementations ci-dessus et lui permettant de s'assurer de la protection et de la sécurisation de ses données à caractère personnel, et d'avoir le nom d'un interlocuteur, responsable de traitement, au sein de E-menew. Ces données peuvent être communiquées aux partenaires de E-menew chargés de l'exécution, du traitement, de la gestion et des transactions de paiement, ces derniers étant tenus par une obligation de confidentialité. L'Utilisateur Professionel dispose, conformément aux réglementations nationales et européennes en vigueur d'un droit d'accès permanent, de modification, de rectification et d'opposition s'agissant des informations le concernant.
Chaque Utilisateur Professionnel qui effectue la collecte de données nominatives d'Utilisateurs consommateurs, dans le cadre de la commercialisation de ses produits et/ou services par l'intermédiaire des Services du site internet pour en effectuer une exploitation de quelque nature que ce soit, devra respecter les dispositions légales énoncées ci-dessus et devra procéder aux formalités requises en matière d'information et de consentement, cette obligation relève de sa seule responsabilité. E-menew n'est tenue à aucune vérification et n'aura aucune responsabilité en la matière.
Concernant les Consommateurs, la bonne exécution du traitement des Commandes-Consommateurs passent par (i) l’enregistrement d’un cookie sur le terminal du Consommateur afin lui permettre de suivre l’avancement du traitement de sa commande et (ii) la géolocalisation du terminal du Consommateur afin de lui indiquer les établissements de proximité proposant les Services de Tiers, faciliter le livraisons à domicile et prémunir l’Utilisateur Professionnel contre des Commandes-Consommateurs indues.
</p>
<h6>15- PARTENAIRES</h6>
<p>
En matière de paiement, E-menew est partenaire de <img src="{{asset('assets/main-site/images/Lemon-Way-Logo-Inline-200px-black.png')}}" style="width: 116px;"/> (http://www.lemonway.com), établissement de paiement agréé par l’ACPR en France le 24 décembre 2012 sous le numéro 16568 ».
Les Conditions Générales d’Utilisation de Services de paiement Lemon Way sont consultables par le biais de ce lien : <a href="https://www.lemonway.com/conditions-generales-dutilisation/" target="_blank">https://www.lemonway.com/conditions-generales-dutilisation/</a>
<br> 
En matière de terminaux de type « Point of Sales » (POS), E-menew est partenaire de Swiss Authentis (<a href="https://www.authentis.swiss" target="_blank">https://www.authentis.swiss</a>).
</p>
<h6>16- RÉCLAMATION</h6>
<p>
Tout incident de fonctionnement ou réclamation lié aux Services devra être adressé par e-mail à l’adresse contact(at)e-menew.com, et confirmé, en cas de non réponse sous 48h, par un courrier suivi ou lettre avec accusé de réception adressé à
Tryngo Services Sàrl
Réclamation E-menew
Bureau 606
Quai de l’Ile 13
CH-1204 Genève, Suisse
E-menew se réserve le droit d’exiger une preuve de l’incident (copie d’écran, etc.).

Tout incident plus particulièrement lié au processus de paiement pourra faire l’objet d’une réclamation sur le site du fournisseur de solution de paiement :
https://www.lemonway.com/reclamation.

Les demandes de médiation peuvent être faite auprès de l’AFEPAME :
https://mediateur-consommation-afepame.fr/
</p>

<h6>17- DROIT APPLICABLE ET FOR JURIDIQUE</h6>
<p>
Le droit matériel suisse s’applique à la relation juridique entre E-menew et le Client, à l’exclusion des dispositions sur les conflits juridiques et de la Convention de Vienne.

Le for juridique est Genève, Suisse.
</p>
<h6>18- CONTACT ET SERVICE CLIENTÈLE</h6>
<p>
Vous avez une question? Consultez nos FAQ. Vous y trouverez les questions et réponses les plus fréquentes. A défaut, veuillez vous adresser au service clientèle de E-menew par e-mail à contact(at)e-menew.com.
</p>

      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Mentions légales</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body p-4 bg-light">
       	
		<p>Le service E-menew vous est proposé par <strong style="font-size: 17px;font-weight: bold;">Tryngo Services Sàrl</strong>, groupe Swiss Authentis, Quai de l’Ile 13, CH-1204 Genève, Suisse.<br />Notre société assume pleinement ses responsabilités et notamment celles liées à la rédaction et publication de ce site.<br />Vous pouvez nous joindre à toute heure par e-mail (contact(at)tryngo-services.com) et aux heures ouvrables par téléphone +41 22 596 2230. Nous serons ravis de nous entretenir avec vous.<br />Tryngo Services est inscrite au Registre du Commerce de Genève sous le numéro CHE-370.654.438 et son numéro d’identification TVA est le CHE-370.654.438 TVA.</p>
		  <strong style="font-size: 17px;font-weight: bold;">Politique en matière de Cookies et Traceurs</strong>
		  <p>Un cookie est un petit fichier informatique, un traceur. Il permet d'analyser le comportement des usagers lors de la visite d'un site internet, de la lecture d'un courrier électronique, de l'installation ou de l'utilisation d'un logiciel ou d'une application mobile. RASSUREZ-VOUS, CE SITE N’UTILISE PAS DE TELS COOKIES. Les seuls cookies utilisés par notre site sont strictement nécessaires pour la délivrance de services expressément demandés par le consommateur (suivi du statut des commandes passées).</p>
		  
      </div>
      
    </div>
  </div>
</div>
