<header id="header">
	<div class="container main-menu">
		<div class="row align-items-center  d-flex">
			<a class="navbar-brand" href="#"><img src="{{asset('assets/restraurant-site/img/logo.png')}}"></a>
			<a id="bellIcon" class="ml-auto">
				<i class="fas fa-bell  bell" style=""></i></a>
			<a>
				<i class="fas fa-shopping-cart cart-set"></i>
				<span class="badge style-set" > 6 </span>
			</a>
			<nav id="nav-menu-container" class="ml-auto">
				<ul class="nav-menu">
					<li><a href="home">Accueil</a></li>
					<li><a href="about">À propos de</a></li>
					<li><a href="contact-us">Contact</a></li>
					@if(!Session::has('is_logged_in') && empty(Session::get('is_logged_in')))
						<li class=""><a href="signin">Sign in</a></li>
					@endif
					
				    @if(Session::has('is_logged_in') && !empty(Session::get('is_logged_in')))
						<li class=""><a href="{{ route('logout') }}">Logout</a></li>
					@endif
				</ul>
			</nav><!-- #nav-menu-container -->
		</div>
	</div>
</header><!-- #header -->
<!-- start banner Area -->
<section class="relative about-banner">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
					Contact Us				
				</h1>	
				<?php /*<p class="text-white link-nav"><a href="index.html">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="contact.html"> Contact Us</a></p> */ ?>
			</div>	
		</div>
	</div>
</section>