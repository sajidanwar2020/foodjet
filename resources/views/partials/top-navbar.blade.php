<header id="header" class="header-v3 header-v2">
    <div class="header-top">
        <div class="container">
            <div class="box">
                <p class="icon-menu-mobile"><i class="fa fa-bars"></i></p>
                <div class="logo"><a href="{{ route('home') }}" title="Uno">
                        <img src="{{asset('assets/site/images/logo-v2.svg')}}" alt="images">
                    </a></div>
                <div class="logo-mobile"><a href="{{ route('home') }}" title="Xanadu"><img
                            src="{{asset('assets/site/images/logo-v2.svg')}}" alt="Xanadu-Logo"></a></div>

                <div class="box-right">
                    <div class="cart hover-menu">
                        <a href="{{ route('show_cart') }}">
                            <p class="icon-cart" title="Cart">
                                <i class="icon"></i>
{{--                                <i class="fa fa-shopping-basket" aria-hidden="true"></i>--}}
                                <span class="cart-count" @if(Cart::count()==0) style="display: none" @endif>{{Cart::count()}}</span>
                            </p>
                        </a>
                        <?php /* <div class="cart-list list-menu">
                                <ul class="list">
                                    <li>
                                        <a href="#" title="" class="cart-product-image"><img
                                                    src="{{asset('assets/site/images/products/1.jpg')}}" alt="Product"></a>
                                        <div class="text">
                                            <p class="product-name">Black Berry</p>
                                            <p class="product-price"><span class="price-old">$700.00</span><span
                                                        class="price">$350.00</span></p>
                                            <p class="qty">QTY:01</p>
                                        </div>
                                        <a class="close" href="#" title="close"><i class="fa fa-times-circle"></i></a>
                                    </li>
                                </ul>
                                <p class="total"><span class="left">Total:</span> <span class="right">$350.00</span></p>
                                <div class="bottom">
                                    <a class="link-v1" href="#" title="viewcart">View Cart</a>
                                    <a class="link-v1 rt" href="#" title="checkout">Check out</a>
                                </div>
                            </div> */ ?>
                    </div>
                    <div class="cart wish-list-outer hover-menu">
                        <a href="{{ route('wish_list') }}">
                            <p class="icon-wish" title="Wishlist">
                                <i class="icon"></i>
{{--                                <i class="far fa-heart"></i>--}}
                                <span class="wish-list-count" @if(count(wish_list())==0) style="display: none" @endif>{{count(wish_list())}}</span>
                            </p>
                        </a>
                    </div>
                    <div class="search dropdown" data-toggle="modal" data-target=".bs-example-modal-lg">
                        <i class="icon"></i>
{{--                        <i class="fas fa-search"></i>--}}
                    </div>
                </div>
                <nav class="mega-menu">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <ul class="nav navbar-nav" id="navbar">
                        <li class="level1 active"><a href="{{ route('home') }}" title="Home">Startseite</a></li>
                        <li class="level1"><a href="{{ URL('page/about-us') }}" title="About us">Über Uns</a></li>
                        <?php /*<li class="level1"><a href="{{ route('shop') }}" title="Shop">Shop</a></li> */ ?>
                        <li class="level1"><a href="{{ route('contact_us') }}" title="Contact us">Kontakt</a></li>

                        @if(!Session::has('is_logged_in') && empty(Session::get('is_logged_in')))
                            <li class="level1"><a href="{{ route('signin') }}">Sign in</a></li>
                        @endif

                        @if(Session::has('is_logged_in') && !empty(Session::get('is_logged_in')))
                            <li class="level1"><a href="{{ route('customer_orders') }}">My orders</a></li>
                            <li class="level1"><a href="{{ route('logout') }}">Logout</a></li>
                        @endif
                    </ul>
                </nav>

            </div>
        </div>
        <!-- End container -->
    </div>
    <!-- End header-top -->
</header><!-- /header -->


<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content popup-search">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i>
            </button>
            <div class="modal-body">
                <form action="{{ route('search') }}" method="GET" id="form_signup">
                    <div class="input-group">
                        <input type="text" class="form-control control-search" name="s"
                               placeholder="Suchen und drücken Sie die EINGABETASTE..." />
                        <button class="button_search" type="submit">Suche</button>
                    </div><!-- /input-group -->
                </form>
            </div>
        </div>
    </div>
</div>
