<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/css/style.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/vendor/settings.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/vendor/owl-slider.css')}}"/>
<link href="{{ asset('/assets/css/sweetalert2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/jquery-ui.css') }}" rel="stylesheet">
<link rel="shortcut icon" href="{{asset('assets/site/images/favicon.png')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/site/fontawesome/css/all.css')}}"/>
<script type="text/javascript" src="{{asset('assets/site/js/jquery-3.2.0.min.js')}}"></script>
<title>FoodJet</title>
