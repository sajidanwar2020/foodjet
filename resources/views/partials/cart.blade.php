<script type="text/javascript">
    $(document).ready(function () {
        $(".update_cart").click(function () {
            var pid = $(this).attr('data-pid');
            var quantity = $(this).closest('tr').find('.quantity').val();
           // alert(quantity);
            $.ajax('{{ route('update_cart') }}', {
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', pid: pid,quantity: quantity},
                success: function (data) {
                   location.reload();
                }
            });
        });

        $(".remove_cart").click(function () {
            var pid = $(this).attr('data-pid');
            $.ajax('{{ route('remove_cart') }}', {
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', pid: pid},
                success: function (data) {
                    location.reload();
                }
            });
        });

    });
    function hide_cart_count() {
        var count = $('.cart-count').html();
        if (count == 0)
            $('.cart-count').hide();
    }
</script>
