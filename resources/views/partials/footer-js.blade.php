<script type="text/javascript" src="{{asset('assets/site/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/jquery.themepunch.revolution.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/jquery.themepunch.plugins.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/engo-plugins.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/site/js/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/site/js/store.js')}}"></script>
<script src="{{ asset('/assets/js/jquery-ui.js') }}"></script>
<script src="{{ asset('/assets/js/sweetalert2.min.js') }}"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".add-cart", function () {
            //alert($(this).data('id'));
            _this = $(this);
            var pid = $(this).attr('data-id');
            var cart_id = $(this).attr('data-cart_id');
            var id_added = $(this).attr('data-is_cart');
            // alert(id_added);
            //return false;
            if (id_added == 'no') {
                $.ajax('{{ route('add_to_cart') }}', {
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', pid: pid, product_id: pid},
                    success: function (data) {
                        _this.attr('data-cart_id', data);
                        _this.attr('data-is_cart', 'yes');
                        _this.addClass('active');
                        _this.attr('title','Remove from cart');
                    }
                });
                var count = $('.cart-count').html();
                if (count == '')
                    count = 0;
                $('.cart-count').html(parseFloat(count) + 1);
                $('.cart-count').show();
            } else if (id_added == 'yes') {
                $.ajax('{{ route('remove_cart') }}', {
                    method: 'POST',
                    data: {'_token': '{{ csrf_token() }}', pid: cart_id},
                    success: function (data) {
                        //_this.attr('data-cart_id', data);
                        _this.attr('data-is_cart','no');
                        _this.removeClass('active');
                        _this.attr('title','Add to cart');
                    }
                });
                var count = $('.cart-count').html();
                //alert(count);
                if (count != '' && count != 0)
                    $('.cart-count').html(parseFloat(count) - 1);
                hide_cart_count();
            }
        });

        //---------------------------------------------------------------------------------------------------- Wish list
        $(document).on("click", ".wish_list", function () {
            var product_id = $(this).attr('data-id');
            var _this = $(this);
            var url;
            if (!$(this).hasClass("remove_wish")) {
                url = '{{route('update_wish_list')}}';
                $.get(url, {
                    product_id: product_id
                }, function(data) {
                    if(data == '11') {
                        window.location.href = '{{route('signin')}}'
                    }
                    else {
                        var count = $('.wish-list-count').text();
                        if (count == '')
                            count = 0;
                        $('.wish-list-count').html(parseInt(count) + 1);

                        $('.wish-list-count').show();
                        _this.addClass('remove_wish');
                        _this.attr('title','Remove from wishlist');
                    }

                });
            } else {
                url =  '{{route('update_wish_list')}}';
                $.get(url, {
                    product_id: product_id
                }, function(data) {
                    var count = $('.wish-list-count').text();
                    $('.wish-list-count').html(count - 1);
                    var count = $('.wish-list-count').text();
                    if (count == 0) {
                        $('.wish-list-count').hide();
                    }
                    _this.attr('title','Add to wishlist');
                    _this.removeClass('remove_wish');
                });
            }
        });
        //-------------------------------------------------------------------------------------------------------------

    });

    function hide_cart_count() {
        var count = $('.cart-count').html();
        if (count == 0)
            $('.cart-count').hide();
    }
</script>
