@foreach($products as $product)
    <div class="item-inner">
        <div class="product">
            <div class="product-images" style="min-height: 275px;">
                <a href="#" title="{{$product->product_name}}">
                    @if(isset($product->product_images[0]->image_name))
                        <img class="primary_image"
                             src="{{ asset('uploads/products/'.$product->product_images[0]->image_name ) }}"
                             alt=""/>
                    @else
                        <img class="primary_image"
                             src="{{asset('assets/site/images/products/featured/2.jpg')}}"
                             alt=""/>
                    @endif

                </a>
                <div class="action">
                    <a class="add-cart {{$product->is_cart=="no"?'':'active'}}" data-id="{{$product->id}}" data-is_cart="{{$product->is_cart}}" data-cart_id="{{$product->cart_id}}"
                       title="{{$product->is_cart=="no"?'Add to cart':'Remove to cart'}}"></a>

                </div>
                <!-- End action -->
            </div>
            <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="{{$product->product_name}}">
                <p class="product-title">{{$product->product_name}}</p>
            </a>
           <?php /* <p class="card-text">{{$product->product_description}}</p> */ ?>
            <p class="product-price">€{{$product->regular_price}}  {{$product->weight}}</p>

        </div>
    </div>
@endforeach
