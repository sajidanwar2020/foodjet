<div class="subcatList home-categories">
    <div class="home-categories-wrapper">
        <div class="row">
		<?php 
			//echo "<pre>";
			//print_r($parent_child_category);exit;
		?>
            @if (count($categories) > 0)
                @foreach($categories as $category)
			
                    <div class="col-6 col-lg-3 subcat-wrapper">
                        <div class="card">
                            <div class="card-body">
                                <a class="subcat-box" href="{{ route('category',['cat_id'=>$category->id]) }}" title="{{$category->name}}">
                                    <div class="subcat-name"><font style="vertical-align: inherit;"><font
                                                style="vertical-align: inherit;">
                                                {{$category->name}}
                                            </font></font></div>
                                    <img alt="special offers"
                                         class="subcat-image" src="{{ asset('uploads/categories/icons/'.$category->icon ) }}">
                                </a>
								<?php /*
                                <ul class="list-inline">
                                    <li class="list-inline-item">
                                        <a class="item-link"
                                           href="#"><font
                                                style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">chocolate</font></font></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="item-link"
                                           href="#"><font
                                                style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">Candy &amp; chewing
                                                    gum</font></font></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="item-link"
                                           href="#"><font
                                                style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">Chips &amp;
                                                    snacks</font></font></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="item-link"
                                           href="#"><font
                                                style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">Cookies, waffles &amp;
                                                    pastries</font></font></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="item-link"
                                           href="#"><font
                                                style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">Fruit gums &amp;
                                                    liquorice</font></font></a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a class="item-link"
                                           href="#"><font
                                                style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">meat
                                                    snacks</font></font></a>
                                    </li>
                                </ul> */ ?>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

