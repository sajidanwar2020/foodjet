<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="page-faq">
                <div class="container">
                    @if (count($faqs) > 0)
                        @foreach($faqs as $faq)

                             <div class="content-text space-50">
                                <h2>{{$faq->title}}</h2>
                                <div class="row">
                                    @foreach($faq['faqs'] as $faq_question_answer)
                                        <div class="col-md-6 space-50">
                                            <h3>{{$faq_question_answer->question}}</h3>
                                            <p>{{$faq_question_answer->answer}}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <!-- End container -->
            </div>
            <!-- End page faq -->
        </div>
    </div>
    @include('partials.footer')
{{--</div>--}}
<!-- End wrappage -->

</body>

</html>

