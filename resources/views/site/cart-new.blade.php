<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    @include('partials.headers-style')
    <link href="{{ asset('/assets/css/sweetalert2.min.css') }}" rel="stylesheet">

</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">

            <div class="cart-box-container">
                <!-- End container -->
                <div class="container">
                    <div class="box cart-container">
                        @if(count(Cart::content()) > 0)
                        <table class="table cart-table space-30">
                            <thead>
                            <tr>
                                <th class="product-photo">Products</th>
                                <th class="produc-name"></th>
                                <th class="produc-price">PRICE</th>
                                <th class="product-quantity">QUANTITY</th>
                                <th class="total-price">TOTAL</th>
                                <th class="product-remove"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach(Cart::content() as $row) :  ?>
                            <tr class="item_cart">
                                <td class="product-photo"><img src="{{ asset('uploads/products/'.findProductImage($row->id)  ) }}" alt="Futurelife"></td>
                                <td class="produc-name"><a href="{{ route('product_detail',['id'=>$row->id]) }}" title="">{{$row->name}}</a></td>
                                <td class="produc-price">€<?php echo $row->price; ?></td>
                                <td class="product-quantity">
                                    <form enctype="multipart/form-data">
                                        <div class="product-signle-options product_15 clearfix">
                                            <div class="selector-wrapper size">
                                                <div class="quantity">

                                                    <div class="quantity"><span class="minus"><i class="fa fa-minus"></i></span>
                                                        <input data-step="1" value="<?php echo $row->qty; ?>" title="Qty" class="qty" size="4" type="text">
                                                        <span class="plus"><i class="fa fa-plus"></i></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                                <td class="total-price">€<?php echo $row->total; ?></td>
                                <td class="col-sm-1 col-md-2">
                                    <a href="javascript:void(0)" class="btna btn-dangera remove_cart" data-pid="{{$row->rowId}}">
                                        <span class="glyphicon glyphicon-remove"></span> Remove
                                    </a>
                                    |
                                    <a href="javascript:void(0)" class="btna btn-dangera update_cart" data-pid="{{$row->rowId}}">
                                        <span class="glyphicon glyphicon-remove"></span> Update
                                    </a>
                                </td>
                            </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                        <div class="row-total">
                            <div class="float-left">
                                <h3>Sub Total</h3>
                            </div>
                            <!--End align-left-->
                            <div class="float-right">
                                <p>€<?php echo Cart::subtotal(); ?></p>
                            </div>
                            <!--End align-right-->
                        </div>
                        <div class="box space-80">
                            <div class="float-left">
                                <a class="link-v1 lh-50 margin-right-20 space-20" id="clear_shoping_cart" href="{{ route('clear_shoping_cart') }}" title="CLEAR SHOPPING CART">CLEAR SHOPPING CART</a>
                               <?php /* <a class="link-v1 lh-50 space-20" href="#" title="UPDATE SHOPPING CART">UPDATE SHOPPING CART</a> */ ?>
                            </div>
                            <!-- End float left -->
                            <div class="float-right">
                                <a class="link-v1 lh-50 bg-brand" href="{{ route('check_out') }}" title="CONTINUS SHOPPING">CONTINUS SHOPPING</a>
                            </div>
                            <!-- End float-right -->
                        </div>
                        @else
                            <p>Your cart is currently empty.</p>
                            <p class="return-to-shop">
                                <a class="button wc-backward" href="{{ route('home') }}"> Return to shop</a>
                            </p>
                    @endif
                        <!-- End box -->

                    </div>
                    <!-- End container -->
                </div>
                <!-- End cat-box-container -->
            </div>

        </div>
    </div>
    @include('partials.footer')
</div>
<!-- End wrappage -->
@include('partials.footer-js')
<script src="{{ asset('/assets/js/sweetalert2.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".update_cart").click(function () {
            //alert('update');
           // return false;
            var pid = $(this).attr('data-pid');
            var quantity = $(this).closest('tr').find('.qty').val();
            // alert(quantity);
            $.ajax('{{ route('update_cart') }}', {
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', pid: pid,quantity: quantity},
                success: function (data) {
                    location.reload();
                }
            });
        });

        $(".remove_cart").click(function () {
            //alert('remove');
            //return false;
            var pid = $(this).attr('data-pid');
            $.ajax('{{ route('remove_cart') }}', {
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', pid: pid},
                success: function (data) {
                    location.reload();
                }
            });
        });
    });

</script>
</body>
</html>

