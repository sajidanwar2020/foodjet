<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>E-MEnEW</title>
<!--    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />-->
   	<link rel="stylesheet" href="{{asset('assets/main-site/css/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/font-awesome.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/flaticon.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/slick.css')}}">
	<link rel="stylesheet" href="{{asset('assets/main-site/css/slick-theme.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/magnific-popup.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css//style.css')}}" />
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"/>
	
	
</head>

<body class="">
    <div id="page" class="site">
        <header id="site-header" class="site-header header-style-2 header-fullwidth">
            <!-- Main Header start -->
            <div class="octf-main-header">
                <div class="octf-area-wrap">
                    <div class="container-fluid octf-mainbar-container">
                        <div class="octf-mainbar">
                            <div class="octf-mainbar-row octf-row">
                                <!-- logo start -->
                                <div class="octf-col">
                                    <div id="site-logo" class="site-logo">
                                        <a href="{{ route('home') }}">
                                            <img class="logo-static" src="{{asset('assets/main-site/images/logo-dark.png')}}" alt="E-MEnEW">
                                            <img class="logo-scroll" src="{{asset('assets/main-site/images/logo-dark.png')}}" alt="E-MEnEW">
                                        </a>
                                    </div>
                                </div>
                                <!-- logo start -->

                                <!-- nav start -->
                                <div class="octf-col">
                                    <nav id="site-navigation" class="main-navigation">          
                                        <ul id="primary-menu" class="menu">
											 <li class="current-menu-item "><a href="{{ route('about') }}" >Le E-menew, c’est simple</a></li>
                                            <li class=""><a href="{{ route('reasons') }}">10 raisons de l’adopter</a></li>
											<li class=""><a href="{{ route('home') }}#section1">Tarification à la carte</a></li>
                                           
                                        </ul>                               
                                    </nav><!-- #site-navigation -->
                                </div>

                                <div class="octf-col text-right">
                                <!-- Call To Action -->
                                    <div class="octf-btn-cta">                                                                      
                                        <div class="octf-header-module">
                                            <div class="btn-cta-group btn-cta-header">
                                                <a class="octf-btn octf-btn-third" data-toggle="modal" data-target="#exampleModal">Je m'inscris</a>
                                            </div>
                                        </div>
                                        
                                    </div>                              
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Header mobile -->
            <div class="header_mobile">
                <div class="container">
                    <div class="mlogo_wrapper clearfix">

                        <!-- logo mobile start -->
                        <div class="mobile_logo">
                            <a href="{{ route('home') }}"><img src="{{asset('assets/main-site/images/logo-dark.png')}}" alt="E-MEnEW"></a>
                        </div>
                        <!-- logo mobile end -->

                       
                        <div id="mmenu_toggle">
                            <button></button>
                        </div>

                    </div>

                    <!-- nav mobile start -->
                    <div class="mmenu_wrapper">
                        <div class="mobile_nav">
                            <ul id="menu-main-menu" class="mobile_mainmenu">
								<li class=""><a href="{{ route('about') }}" >Le E-menew, c’est simple</a></li>
                                <li class=""><a href="{{ route('reasons') }}">10 raisons de l’adopter</a></li>
								<li class=""><a href="{{ route('home') }}#section1">Tarification à la carte</a></li>
                                <li class=""><a href="#" data-toggle="modal" data-target="#exampleModal">Je m’inscris</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- nav mobile end -->
                </div>
            </div>
        </header>

        <div id="content" class="site-content">
            <div class="page-header dtable text-center" style="background-image: url({{asset('assets/main-site/images/bg-page-header.jpg')}});">
                <div class="dcell">
                    <div class="container">
                        <h1 class="page-title">Le E-menew, c’est simple</h1>
                    </div>
                </div>
            </div>


			
			<div class="ot-tabs p-t70">
				 <div class="container">
				
                                <ul class="tabs-heading unstyle">
                                    <li class="tab-link octf-btn current" data-tab="tab-1454">Simple pour le Consommateur</li>
                                    <li class="tab-link octf-btn" data-tab="tab-2454">Simple pour le Restaurateur</li>
                                </ul>
				
				</div>
				
				<div id="tab-1454" class="tab-content current">
                                    
          <section class="tab-slider-content p-tb70 bg-white" id="">
                <div class="container">
					
                    <div class="flex-row section-content" id="content-mkt">
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>Avec son smartphone, sans installer d’application, le client scanne le QR code affiché sur sa table.</h3>
                               
                               
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/qr-code.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="tab-slider-content p-tb70" id="">
                <div class="container">
                    <div class="flex-row section-content" id="">
                        
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/choose-meal.jpg')}}" alt="">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>Le menu sans contact s’affiche sur le smartphone du client. En balayant l'écran, le client fait son choix.</h3>
                                
                                
                            </div>
                        </div>
						
                    </div>
                </div>
            </section>
			
					<section class="tab-slider-content p-tb70 bg-white" id="">
                <div class="container">
                    <div class="flex-row section-content" id="">
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>En quelques clics, le client passe sa commande et procède au règlement.</h3>
                                
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/proceed-pay.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
					
			
            
          
			
			<section class="tab-slider-content p-tb70" id="">
                <div class="container">
                    <div class="flex-row section-content" id="">
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>A tout moment, en un seul clic, le client peut appeler un serveur.</h3>
                                
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/order.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
						
					<section class="tab-slider-content p-tb70 bg-white" id="">
					<div class="container">
						<div class="flex-row section-content" id="">

							<div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
								<div class="tab-slider-img">
									<img src="{{asset('assets/main-site/images/order-deliver.jpg')}}" alt="">
								</div>
							</div>

							<div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
								<div class="tab-slider-block">
									<h3>Le voisinage peut utiliser à distance le même système pour ses commandes à livrer ou à emporter selon les options proposées par l'établissement.</h3>


								</div>
							</div>

						</div>
					</div>
				</section>
									
                                </div>
                                <div id="tab-2454" class="tab-content">
									
									
									                            
          <section class="tab-slider-content p-tb70 bg-white" id="">
                <div class="container">
					
                    <div class="flex-row section-content" id="content-mkt">
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>Le restaurateur décide librement des paramètres : validation automatique ou manuelle des commandes, paiement à la commande ou en fin de repas, monnaies acceptées, acceptation ou non des commandes à emporter ou à livrer, etc.</h3>
                               
                               
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/owner-order.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="tab-slider-content p-tb70" id="">
                <div class="container">
                    <div class="flex-row section-content" id="">
                        
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/choose-meal.jpg')}}" alt="">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>Le restaurateur modifie à sa convenance et en temps réel les suggestions mises en avant et le détail du menu.</h3>
                                
                                
                            </div>
                        </div>
						
                    </div>
                </div>
            </section>
				
				<section class="tab-slider-content p-tb70 bg-white" id="">
                <div class="container">
					
                    <div class="flex-row section-content" id="content-mkt">
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>Les commandes sont directement transmises en cuisine et enregistrées en facturation.</h3>
                               
                               
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/order-kitchen.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="tab-slider-content p-tb70" id="">
                <div class="container">
                    <div class="flex-row section-content" id="">
                        
                        <div class="col-md-6 col-sm-12 col-xs-12 align-self-center">
                            <div class="tab-slider-img">
                                <img src="{{asset('assets/main-site/images/waiter.png')}}" alt="">
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12 align-self-center sm-m-b40">
                            <div class="tab-slider-block">
                                <h3>Le serveur est averti de la disponibilité des plats (bientôt disponible).</h3>
                                
                                
                            </div>
                        </div>
						
                    </div>
                </div>
            </section>
                                    
									
                            </div>
                                
									
                                </div>

        </div>

    
 
   @include('partials.footer') 
</div><!-- #page -->
	<a id="back-to-top" href="#" class="show"><i class="flaticon-arrow-pointing-to-up"></i></a>
@include('partials.footer-js')
@include('partials.common_js')	
</body>
</html>
<!--
<script type="text/javascript">
    window.jQuery = window.$ = jQuery;  
    (function($) { "use strict";
        //Preloader
        Royal_Preloader.config({
            mode           : 'logo',
            logo           : 'images/logo-dark.svg',
            logo_size      : [160, 75],
            showProgress   : true,
            showPercentage : true,
            text_colour: '#000000',
            background:  '#ffffff'
        });
    })(jQuery);
</script> -->
