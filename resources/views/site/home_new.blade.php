<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <link href="{{asset('assets/site/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/css/style-home.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/css/style.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/vendor/owl-slider.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/vendor/slick.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/images/favicon.png')}}" rel="shortcut icon"/>
    <link href="{{asset('assets/site/vendor/settings.css')}}" rel="stylesheet" type="text/css"/>
    <script src="{{asset('assets/site/js/jquery-3.2.0.min.js')}}" type="text/javascript"></script>
    <title>FoodJet</title>
</head>
<body>


<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content-home">
            <!--Slider section-->
            <div class="carousel slide" data-ride="carousel" id="myCarousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    @if (count($homeSlider) > 0)
                        <?php
                        $counter = 0;
                        ?>
                        @foreach($homeSlider as $homeSlide)
                            <?php
                            $active_class = '';
                            if($counter == 0)
                                $active_class = 'active';
                            $counter++;
                            ?>
                            <div class="item {{$active_class}}">
                                <img alt="" src="{{ asset('uploads/home_slider/'.$homeSlide->image ) }}" style="width:100%;">
                            </div>

                        @endforeach
                    @endif
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" data-slide="prev" href="#myCarousel">
                    <span class="fa fa-angle-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" data-slide="next" href="#myCarousel">
                    <span class="fa fa-angle-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!-- End Slider section-->

            <div class="container-fluid has-hero">
                @include('site.pages_parts.categories')
            </div>

            <div class="container-fluid container-communication">
                <div class="row">
                    <div class="col-sm-12 col-xs-12" style="padding: 0 5px;">
                        <div class="communication">

                            <div class="row">
                                <div class="col-lg-4">

                                    <div class="communication-teaser communication-teaser-image communication-teaser-color4"
                                         style="background-image: url(https://www.getnow.com/out/pictures/ddmedia/communication_460x280.jpg)">
                                        <a href="#" class="communication-teaser-link">
                                <span class="communication-teaser-text-wrapper">
                                    <span class="communication-teaser-text"><font style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">For business customers </font></font></span>
                                    <span class="communication-teaser-cta"><font style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">Discover our B2B world</font></font></span>
                                </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="communication-teaser communication-teaser-image communication-teaser-color6"
                                         style="background-image: url(https://www.getnow.com/out/pictures/ddmedia/com_banner_460x280_sonderangebote_d_2.png)">
                                        <a href="#" class="communication-teaser-link">
                                 <span class="communication-teaser-text-wrapper">
                                    <span class="communication-teaser-text"><font style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">New offers every week </font></font></span>
                                    <span class="communication-teaser-cta"><font style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;">Click here for our price blockbuster</font></font></span>
                                </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="row">
                                    <div class="communication-teaser communication-teaser-color2">
                                        <a href="#" class="communication-teaser-link">
                                <span class="communication-teaser-text-wrapper">
                                    <span class="communication-teaser-text"><strong><font
                                                    style="vertical-align: inherit;"><font
                                                        style="vertical-align: inherit;">For new customers</font></font></strong></span>
                                    <span class="communication-teaser-text"><font style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;"> Here you will find many favorite products</font></font></span>
                                </span>
                                        </a>
                                    </div>
                                    </div>
                                    <div class="row">
                                            <div class="communication-teaser communication-teaser-color0">
                                                <a href="#" class="communication-teaser-link">
                               <span class="communication-teaser-text-wrapper">
                                    <span class="communication-teaser-text"><strong><font
                                                    style="vertical-align: inherit;"><font
                                                        style="vertical-align: inherit;">Organic</font></font></strong></span>
                                    <span class="communication-teaser-text"><font style="vertical-align: inherit;"><font
                                                    style="vertical-align: inherit;"> over 250 products</font></font></span>
                                </span>
                                                </a>
                                            </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--End Banner Home 2 -->
     <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-md-3 col-xs-6" style="">
                        <div class="card" style="text-align: center; margin: 10px 0px; -webkit-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.15); -moz-box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.15); box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.15);">
                            <img src="{{asset('assets/site/images/cat/p2.png')}}" style="  padding-top: 10px; width: auto; height: 160px;">
                            <!-- Badge overlay DIV -->
                            <div class="badge-overlay">
                                <!-- Change Badge Position, Color, Text here-->
                                <span class="top-left badge orange">Sale 50% Off</span>
                            <div class="card-body" style="padding: 10px 5px 10px;">
                                <p class="card-text" style="text-align: left; word-break: break-all; width: 95%">HIMBEEREN(90kil SCHALEN)</p>
                                <p class="card-text" style="text-align: right;">2533</p>
                                <div class="row">

                                    <div class="col-lg-6 col-md-6 col-xs-6" style="padding-right: 5px;"> <span style="display: block; text-align: left;">10.80 KOL</span>  <span style="display: block; text-align: left;">0.90 St</span> </div>
                                    <div class="col-lg-6 col-md-6 col-xs-6 text-right" style="padding-left: 5px;">
                                        <a class="btn" style="width: 100%; height: 39px; padding: 2px 0px; border-radius: 4px; background-color: #65bc4e;"><i class="fa fa-cart-plus" style="font-size: 35px;"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @if (count($categories) > 0)
                @foreach($categories as $category)
                    <div class="container-fluid" style="margin-bottom: 10px;">
                        <div class="row">
                        <?php  /* <div class="col-sm-2">
                                <a href="{{route('category',['cat_id'=>$category->id])}}" title="{{$category->name}}">  <img alt="" src="{{asset('uploads/categories/'.$category->image)}}"> </a>
                            </div> */?>
                            <div class="col-sm-12">
								<?php /*  <div class="upsell-product owl-carousel products furniture hover-shadow ver2"> */ ?>
                                <a href="{{route('category',['cat_id'=>$category->id])}}" title="{{$category->name}}"> <h2 class="cate-name">{{$category->name}}</h2></a>
                                <div class="upsell-product-new products ver2 grid_full grid_sidebar hover-shadow furniture ver2">
                                    @foreach($category['products_arr'] as $product)
                                        <div class="item-inner">
                                            <div class="product">
                                                <div class="product-images 22222">
                                                    <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="product-images">
														@if(\Storage::disk('uploads')->exists('/products/' . $product->product_images[0]->image_name))
                                                            <img class="primary_image"
                                                                 src="{{ asset('uploads/products/'.$product->product_images[0]->image_name ) }}"
                                                                 alt=""/>
                                                        @else
                                                            <img class="primary_image"
                                                                 src="{{ asset('uploads/image-not-avilable.png' ) }}"
                                                                 alt=""/>
                                                        @endif

                                                    </a>
                                                    <div class="action">
                                                        <a class="add-cart" data-id="{{$product->id}}"
                                                           data-is_cart="{{$product->is_cart}}"
                                                           data-cart_id="{{$product->cart_id}}"
                                                           title="{{$product->is_cart == 'yes' ? 'Remove to cart' : 'Add to cart'}}"></a>
                                                        <a class="wish_list wish @if(in_array($product->id,wish_list())) {{'remove_wish'}} @endif"
                                                           href="javascript:void(0)"
                                                           title="@if(in_array($product->id,wish_list())) {{'Remove from wishlist'}} @else {{'Add to wishlist'}} @endif"
                                                           data-id="{{$product->id}}"></a>
                                                    </div>
                                                    <!-- End action -->
                                                </div>
                                                <a href="{{ route('product_detail',['id'=>$product->id]) }}"
                                                   title="{{$product->product_name}}">
                                                    <p class="product-title">{{$product->product_name}}</p>
                                                </a>
                                                <p class="product-price-old"></p>
                                                <p class="product-price">€{{$product->regular_price}} {{$product->weight}}</p>
                                            </div>
                                            <!-- End product -->
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
        @endif


        <!-- Business model -->

            <div class="container-fluid ">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h2>How does our service work?</h2>
                    </div>
                    <div class="col-sm-12">
                    <?php /*
                        <a class="resp-image-link" href="#">
                            <picture class="resp-image-picture">
                                <source media="(min-width: 1200px)"
                                        srcset="{{asset('assets/site/images/cat/process-buying.png')}}">
                                <source media="(min-width: 400px)"
                                        srcset="{{asset('assets/site/images/cat/process-buying.png')}}">
                                <img alt="" class="resp-image-img"
                                     src="{{asset('assets/site/images/cat/process-buying.png')}}">
                            </picture>
                        </a> */ ?>
                        <div class="row text-center deli-set">
                            <div class="col-md-3 col-lg-3 col-12"  align="center">
                                <span>
                                    1
                                </span>
                                <img src="{{asset('assets/site/images/cat/p1.png')}}">
                                <h3>Shop</h3>
                                <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            </div>
                            <div class="col-md-3 col-lg-3 col-12"  align="center">
                                <span>
                                    2
                                </span>
                                <img src="{{asset('assets/site/images/cat/p3.png')}}">
                                <h3>Wait</h3>
                                <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            </div>
                            <div class="col-md-3 col-lg-3 col-12"  align="center">
                                <span>
                                    3
                                </span>
                                <img src="{{asset('assets/site/images/cat/p4.png')}}">
                                <h3>Pick up</h3>
                                <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            </div>
                            <div class="col-md-3 col-lg-3 col-12"  align="center">
                                <span>
                                    4
                                </span>
                                <img src="{{asset('assets/site/images/cat/p2.png')}}">
                                <h3>Pay</h3>
                                <p> It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Business model -->

            <div id="back-to-top">
                <i class="fa fa-long-arrow-up"></i>
            </div>
        </div>
    </div>
    @include('partials.footer');
</div>
@include('partials.footer-js')
<?php /*
<script src="{{asset('assets/site/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/site/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/site/js/engo-plugins.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/site/js/jquery.mousewheel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/site/js/slick.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/site/js/jquery.zoom.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/site/js/store.js')}}" type="text/javascript"></script> */ ?>
</body>
</html>
