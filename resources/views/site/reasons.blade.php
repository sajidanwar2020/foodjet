<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>E-MEnEW</title>
<!--    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />-->
    <link rel="stylesheet" href="{{asset('assets/main-site/css/bootstrap.min.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/font-awesome.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/flaticon.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/slick.css')}}">
	<link rel="stylesheet" href="{{asset('assets/main-site/css/slick-theme.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css/magnific-popup.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/main-site/css//style.css')}}" />
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"/>
</head>

<body class="">
    <div id="page" class="site">
        <header id="site-header" class="site-header header-style-2 header-fullwidth">
            <!-- Main Header start -->
            <div class="octf-main-header">
                <div class="octf-area-wrap">
                    <div class="container-fluid octf-mainbar-container">
                        <div class="octf-mainbar">
                            <div class="octf-mainbar-row octf-row">
                                <!-- logo start -->
                                <div class="octf-col">
                                    <div id="site-logo" class="site-logo">
                                        <a href="{{ route('home') }}">
                                             <img class="logo-static" src="{{asset('assets/main-site/images/logo-dark.png')}}" alt="E-MEnEW">
                                            <img class="logo-scroll" src="{{asset('assets/main-site/images/logo.png')}}" alt="E-MEnEW">
                                        </a>
                                    </div>
                                </div>
                                <!-- logo start -->

                                <!-- nav start -->
                                <div class="octf-col">
                                    <nav id="site-navigation" class="main-navigation">          
                                        <ul id="primary-menu" class="menu">
											<li class=" "><a href="{{ route('about') }}" >Le E-menew, c’est simple</a></li>
                                            <li class="current-menu-item"><a href="{{ route('reasons') }}">10 raisons de l’adopter</a></li>
                                           <li class=""><a href="{{ route('home') }}#section1">Tarification à la carte</a></li>
                                        </ul>                               
                                    </nav><!-- #site-navigation -->
                                </div>

                                <div class="octf-col text-right">
                                <!-- Call To Action -->
                                    <div class="octf-btn-cta">                                                                      
                                        <div class="octf-header-module">
                                            <div class="btn-cta-group btn-cta-header">
                                                <a class="octf-btn octf-btn-third" data-toggle="modal" data-target="#exampleModal">Je m'inscris</a>
                                            </div>
                                        </div>
                                        
                                    </div>                              
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Header mobile -->
            <div class="header_mobile">
                <div class="container">
                    <div class="mlogo_wrapper clearfix">

                        <!-- logo mobile start -->
                        <div class="mobile_logo">
                            <a href="{{ route('home') }}"><img src="{{asset('assets/main-site/images/logo-dark.png')}}" alt="E-MEnEW"></a>
                        </div>
                        <!-- logo mobile end -->

                       
                        <div id="mmenu_toggle">
                            <button></button>
                        </div>

                    </div>

                    <!-- nav mobile start -->
                    <div class="mmenu_wrapper">
                        <div class="mobile_nav">
                            <ul id="menu-main-menu" class="mobile_mainmenu">
								<li class=""><a href="{{ route('about') }}" >Le E-menew, c’est simple</a></li>
                                <li class=""><a href="{{ route('reasons') }}">10 raisons de l’adopter</a></li>
								<li class=""><a href="{{ route('home') }}#section1">Tarification à la carte</a></li>
                                <li class=""><a href="#" data-toggle="modal" data-target="#exampleModal">Je m’inscris</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- nav mobile end -->
                </div>
            </div>
        </header>

        <div id="content" class="site-content">
            <div class="page-header dtable text-center" style="background-image: url({{asset('assets/main-site/images/bg-page-header.jpg')}});">
                <div class="dcell">
                    <div class="container">
                        <h1 class="page-title">10 raisons de l’adopter</h1>
                    </div>
                </div>
            </div>
			
			
			
			<section class="p-tb90">
                <div class="container">
                    
					<div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">01</span>
                                <div class="number-box">01</div>
                                <div class="content-box">
                                    <h5>Hygiène</h5>
                                    <p>Le restaurant se préoccupe de la santé de ses clients. Les menus ne passent plus de main en main</p>
                                </div>
                            </div>
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">03</span>
                                <div class="number-box">03</div>
                                <div class="content-box">
                                    <h5>Productivité </h5>
                                    <p>Les commandes sont directement transmises en cuisine et le suivi des paiements centralisé.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">02</span>
                                <div class="number-box">02</div>
                                <div class="content-box">
                                    <h5>Digitalisation </h5>
                                    <p>L'établissement entre dans le monde numérique et se positionne auprès de la génération des Millennials.</p>
                                </div>
                            </div>
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">04</span>
                                <div class="number-box">04</div>
                                <div class="content-box">
                                    <h5>Polyglotte</h5>
                                    <p>Le client choisit sa langue et sa devise.</p>
                                </div>
                            </div>
                        </div>
						
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">05</span>
                                <div class="number-box">05</div>
                                <div class="content-box">
                                    <h5>Service</h5>
                                    <p>Moins de temps à prendre la commande, plus de temps à soigner le service et la relation client.  </p>
                                </div>
                            </div>
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">07</span>
                                <div class="number-box">07</div>
                                <div class="content-box">
                                    <h5>Augmentation du chiffre d’affaire </h5>
                                    <p>Permet de développer ventes à emporter et livraison au voisinage.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">06</span>
                                <div class="number-box">06</div>
                                <div class="content-box">
                                    <h5>Flexibilité </h5>
                                    <p>La carte peut être rapidement mise à jour et les suggestions désactivées ou remplacées lorsque devenues indisponibles.</p>
                                </div>
                            </div>
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">08</span>
                                <div class="number-box">08</div>
                                <div class="content-box">
                                    <h5>Clé en main</h5>
                                    <p>Déployée en quelques heures, la solution est simple et intuitives.</p>
                                </div>
                            </div>
                        </div>
						
						<div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">09</span>
                                <div class="number-box">09</div>
                                <div class="content-box">
                                    <h5>Design</h5>
                                    <p>Totalement personnalisable, le menu reflète l’identité de votre établissement. </p>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="service-box s-box m-b30">
                                <div class="overlay"></div>
                                <span class="big-number">10</span>
                                <div class="number-box">10</div>
                                <div class="content-box">
                                    <h5>Communication </h5>
                                    <p>Permet la mise en avant de plats du jour, spécialités, promotions…</p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
					
					
                   
                </div>
            </section>
			

        </div>

    @include('partials.footer') 

  
</div><!-- #page -->
<a id="back-to-top" href="#" class="show"><i class="flaticon-arrow-pointing-to-up"></i></a>
@include('partials.footer-js')
@include('partials.common_js')
</body>
</html>