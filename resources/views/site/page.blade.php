<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container">
                <h2>{{$page_detail->title}}</h2>
                <p>
                    {{$page_detail->description}}
                </p>
            </div>
        </div>
    </div>
    <!-- End container -->
    @include('partials.footer')
</div>
@include('partials.footer-js')
</body>
</html>

