<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
    <style>
        .form-horizontal .col-md-6:nth-child(2n) {
            padding-right: 16px !important;
            padding-left: 0 !important;
        }
    </style>
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container">
                <div class="page-contact">
                    <div class="content-text center">
                        <h3>CONTACT US</h3>
                        <p>Leave Us A Message</p>
                        @include('partials.flash-message')

                            <form method="post" action="{{ route('contact_submitted') }}" id="" autocomplete="off" class="form-horizontal space-50">
                                {{ csrf_field() }}

                            <div class="form-group col-md-6">
                                <input class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Name*" type="text">
                            </div>
                            <div class="form-group col-md-6">
                                <input class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email*" type="text">
                            </div>
                            <div class="form-group col-md-12">
                                <input class="form-control" id="phone" name="phone" value="{{ old('phone') }}" placeholder="Phone" type="text">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" id="message" name="message"
                                          placeholder="Comment">{{ old('message') }}</textarea>
                            </div>
                            <div class="box align-center">
                                <input type="submit" value="Send message" class="link-v1 rt" id="submit-contact">
                            </div>
                        </form>
                    </div>
                    <!-- End content-text -->
                </div>
            </div>
        </div>
    </div>
    <!-- End container -->
    @include('partials.footer')
</div>
@include('partials.footer-js')
</body>
</html>

