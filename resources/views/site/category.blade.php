<!DOCTYPE html>
<html lang="en">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <link href="{{asset('assets/site/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/css/style.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/vendor/owl-slider.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/vendor/settings.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/vendor/range-price.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/site/images/favicon.png')}}" rel="shortcut icon"/>
    <script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <title>Foodjet</title>
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
@include('partials.top-navbar')
    <!-- End Banner Grid -->
    <div class="container content-wrapper">
        <div class="main-content">
            <div class="container">
                <div class="col-xs-12 col-md-9" id="primary">
                    <div class="wrap-breadcrumb">
                        <div class="ordering">

                                 <div class="float-left">

                                     <p class="result-count">Category: <b>{{$lable}}</b></p>
                            </div>
                            <div class="float-right">
                                <form action="#" class="order-by" method="get">
                                    <p>Sort by :</p>
                                    <select class="orderby" name="orderby">
                                        <option value="default">Default Sorting</option>
                                        <option value="price-asc" @if(isset($_GET['sorting']) AND $_GET['sorting'] == 'price-asc')  {{'selected'}} @endif>price: low to high</option>
                                        <option value="price-desc" @if(isset($_GET['sorting']) AND $_GET['sorting'] == 'price-desc') {{'selected'}} @endif>price: high to low</option>
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="products ver2 grid_full grid_sidebar hover-shadow furniture" id="product_outer">
                        @if (count($products) > 0)
                            @foreach($products as $product)
                                <div class="item-inner">
                                      <div class="product">
                                    <div class="product-images" style="min-height: 275px;">
                                        <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="{{$product->product_name}}">
                                            @if(\Storage::disk('uploads')->exists('/products/' . $product->product_images[0]->image_name))
                                                <img class="primary_image"
                                                     src="{{ asset('uploads/products/'.$product->product_images[0]->image_name ) }}"
                                                     alt=""/>
                                            @else
                                                <img class="primary_image"
                                                      src="{{ asset('uploads/image-not-avilable.png' ) }}"
                                                     alt=""/>
                                            @endif

                                        </a>
                                        <div class="action">
                                            <a class="add-cart {{$product->is_cart=="no"?'':'active'}}" data-id="{{$product->id}}" data-is_cart="{{$product->is_cart}}" data-cart_id="{{$product->cart_id}}"
                                               title="{{$product->is_cart=="no"?'Add to cart':'Remove to cart'}}"></a>
                                            <a class="wish_list wish @if(in_array($product->id,wish_list())) {{'remove_wish'}} @endif" href="javascript:void(0)" title="@if(in_array($product->id,wish_list())) {{'Remove from wishlist'}} @else {{'Add to wishlist'}} @endif" data-id="{{$product->id}}"></a>

                                        </div>
                                        <!-- End action -->
                                    </div>
                                    <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="{{$product->product_name}}">
                                        <p class="product-title">{{$product->product_name}}</p>
                                    </a>
                                          <p class="product-price-old"></p>
                                    <p class="product-price">€{{$product->regular_price}} {{$product->weight}}</p>

                                </div>
                                </div>
                            @endforeach
                        @else
                            <p>Not found any Products</p>
                        @endif

                    </div>
                    <!-- End product-content products  -->
                    <div class="pagination-container">
                        <?php /*
                        <nav class="pagination align-center">
                            <a class="prev page-numbers" href="#"><i class="fa fa-angle-left"></i></a>
                            <span class="page-numbers current">1</span>
                            <a class="page-numbers" href="#">2</a>
                            <a class="page-numbers" href="#">3</a>
                            <a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a>
                        </nav> */ ?>
                            {!! $products->appends(request()->except('page')); !!}
                    </div>
                    <!-- End pagination-container -->
                </div>
                <!-- End Primary -->
                <div class="widget-area col-xs-12 col-md-3" id="secondary">
                    <aside class="widget widget_product_categories">
                        <h3 class="widget-title">Categories</h3>
                        <ul class="product-categories" id="product_categories">
                                @if(!$categories->isEmpty())
                                    @foreach($categories as $category)
                                    <li>
                                        <a href="{{route('category',['cat_id'=>$category->id])}}" title="">{{$category->name}}</a>
                                        <ul class="children" style="display: block !important;">
                                            @foreach($category->child_categories as $child_category)
                                                <li><a href="{{route('category',['cat_id'=>$child_category->id])}}" title="">{{$child_category->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                @endif
                        </ul>
                    </aside>
                    <aside class="widget widget_by_price">
                        <h3 class="widget-title">By Price</h3>
                        <div class="layout-slider">
                                <input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;" value="">
                                <input type="hidden" id="price_range" value="{{($filter_price['min_price'] ? $filter_price['min_price'] : '') }},{{($filter_price['max_price'] ? $filter_price['max_price'] : '')}}">

                            <div id="slider-range"></div>
                        </div>
                        <?php /*<a class="link-v1 space-30" href="#" title="fillter">fillter</a> */ ?>
                    </aside>
                    <?php /*
                    <aside class="widget widget_link">
                        <h3 class="widget-title">By Type</h3>
                        <ul>
                            <li><a href="#" title="Aeccaft">Apples</a><span class="count">(15)</span></li>
                            <li><a href="#" title="Artek">Blackberry</a><span class="count">(09)</span></li>
                            <li><a href="#" title="Bower">Avocado</a><span class="count">(12)</span></li>
                        </ul>
                    </aside> */ ?>
                    <aside class="widget widget_feature">
                        <h3 class="widget-title">Feature Products</h3>
                        <ul>
                            @foreach($feature_products as $product)

                                <li>

                                    <a href="{{ route('product_detail',['id'=>$product->id]) }}"  class="images" title="{{$product->product_name}}">

										@if(\Storage::disk('uploads')->exists('/products/' . $product->product_images[0]->image_name))
                                            <img alt="images" class="img-responsive"
                                                 src="{{ asset('uploads/products/'.$product->product_images[0]->image_name ) }}">
                                        @else
												 <img class="img-responsive"
                                                      src="{{ asset('uploads/image-not-avilable.png' ) }}"
                                                     alt=""/>
                                        @endif

                                    </a>
                                    <div class="text">
                                        <h2>
                                            <a href="{{ route('product_detail',['id'=>$product->id]) }}" title="{{$product->product_name}}">
                                               {{$product->product_name}}
                                            </a>
                                        </h2>
                                        <p class="product-price-old"></p>
                                        <p><span>€{{$product->regular_price}}</span></p>
                                    </div>
                                </li>
                            @endforeach

                        </ul>
                    </aside>
                </div>
                <!-- End Secondary -->
            </div>
        </div>
    </div>

    <!-- end product sidebar -->
    @include('partials.footer')
</div>
<!-- End wrappage -->
@include('partials.footer-js')
<<?php /*script src="{{asset('assets/site/js/pricess-range.js')}}" type="text/javascript"></script> */ ?>
<script>
    $( function() {
        //---------------------------------------------------------------------------------------

        $( "#slider-range" ).slider({
            range: true,
            min: parseFloat('{{$price_range['min_price']}}'),
            max: parseFloat('{{$price_range['max_price']}}'),
            values: [ '{{($filter_price['min_price'] ? $filter_price['min_price'] : '') }}', {{($filter_price['max_price'] ? $filter_price['max_price'] : '')}} ],
            slide: function( event, ui ) {
                // console.log(ui);
                $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
                $( "#price_range" ).val(ui.values[ 0 ] + ","+ui.values[ 1 ]);
            },
            change: function(event, ui) {
                apply_filters();
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) + " - $" + $( "#slider-range" ).slider( "values", 1 ) );

        //------------------------------------------------------------------------------------------------

        $('.orderby').on('change', function() {
            apply_filters();
        });
        $('.categories').click(function() {
            apply_filters();
        });
        //------------------------------------------------------------------------------------------------
        function apply_filters() {
            var sorting =  $('.orderby').val();
            var price_range = $('#price_range').val();
            var category = category_fun();
            var url = '{{route('category',['cat_id'=>$_GET['cat_id']])}}';

            if (price_range !== '')
                url += "&price=" + price_range;

            if (sorting !== 'default')
                url += "&sorting=" + sorting;

            window.location.href = url;
        }

        function category_fun() {
            var arr = [];
            $("input:checkbox.categories").each(function() {
                var ischecked = $(this).is(':checked');
                if (ischecked) {
                    val = $(this).val();
                    if ($.inArray(val, arr) !== '-1') {
                        arr.push(val);
                    }
                }
            });
            categoryIds = "";
            for (i = 0; i < arr.length; i++) {
                categoryIds += arr[i] + ",";
            }
            var categoryIds = categoryIds.replace(/^,|,$/g, '');
            return categoryIds;
        }
        //------------------------------------------------------------------------------------------------
    } );
</script>
</head>
<body>


</body>
</html>

