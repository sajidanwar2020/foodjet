<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    @include('partials.headers-style')
    <link href="{{ asset('/assets/css/sweetalert2.min.css') }}" rel="stylesheet">

</head>
<body>


<!-- End pushmenu -->
<div class="wrappage">
    @include('partials.top-navbar')
    <div class="container content-wrapper">
        <div class="main-content">

            <div class="cart-box-container">
                <!-- End container -->
                <div class="container">
                    <div class="box cart-container" id="cart_outer">
                        @if(count(Cart::content()) > 0)
                        <table class="table cart-table space-30">
                            <thead>
                            <tr>
                                <th class="product-photo" scope="col">Products</th>
                                <th class="produc-name" scope="col"></th>
                                <th class="produc-price" scope="col">Price</th>
                                <th class="units_per_box" scope="col">Units per box</th>
                                <th class="product-quantity" scope="col">Quantity</th>
                                <th class="total-price" scope="col">Total</th>
                                <th class="product-remove" scope="col">Remove</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach(Cart::content() as $row) :
                           // print_r($row->options->units_per_box);exit;
                            ?>
                            <tr class="item_cart" id="{{$row->rowId}}">
                                <td class="product-photo"><img src="{{ asset('uploads/products/'.findProductImage($row->id)  ) }}" alt="Futurelife"></td>
                                <td class="produc-name"><a href="{{ route('product_detail',['id'=>$row->id]) }}" title="">{{$row->name}}</a></td>
                                <td class="produc-price">€<?php echo $row->options->price2; ?></td>
                                <td class="units_per_box"><?php echo $row->options->units_per_box; ?></td>
                                <td class="product-quantity">
                                    <form enctype="multipart/form-data">
                                        <div class="product-signle-options product_15 clearfix">
                                            <div class="selector-wrapper size">
                                                <div class="quantity">
                                                    <div class="quantity">
                                                        <span class="minus"><i class="fa fa-minus"></i></span>
                                                        <input data-step="1" readonly value="<?php echo $row->qty; ?>" title="Qty" class="qty" size="4" type="text">
                                                        <span class="plus"><i class="fa fa-plus"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                                <td class="total-price">€<?php echo $row->total; ?></td>

                                <td class="col-sm-1 col-md-2 cart-remove">
                                    <a href="javascript:void(0)" class="btna btn-dangera remove_cart">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                     <?php /*
                                    <a href="javascript:void(0)" class="btna btn-dangera update_cart" data-pid="{{$row->rowId}}">
                                        <span class="glyphicon glyphicon-remove"></span> Update
                                    </a> */ ?>
                                </td>
                            </tr>
                            <?php endforeach;?>

                            </tbody>
                        </table>
                        <div class="row-total">
                            <div class="float-left">
                                <h3>Sub Total</h3>
                            </div>
                            <!--End align-left-->
                            <div class="float-right" id="cart_total">
                                <p>€<?php echo Cart::subtotal(); ?></p>
                            </div>
                            <!--End align-right-->
                        </div>
                        <div class="box space-20 set-seter">
                            <div class="float-right">
                                <a class="link-v1 lh-50 bg-brand" href="{{ route('check_out') }}" title="CONTINUS SHOPPING">CONTINUS SHOPPING</a>
                            </div>
                            <!-- End float-right -->
                            <div class="float-left">
                                <a class="link-v1 lh-50 margin-right-20 space-20" id="clear_shoping_cart" href="{{ route('clear_shoping_cart') }}" title="CLEAR SHOPPING CART">CLEAR SHOPPING CART</a>
                               <?php /* <a class="link-v1 lh-50 space-20" href="#" title="UPDATE SHOPPING CART">UPDATE SHOPPING CART</a> */ ?>
                            </div>
                            <!-- End float left -->

                        </div>
                        @else
                            <div class="cart_empty_wrapper">
                                <p>Your cart is currently empty.</p>
                                <p class="return-to-shop">
                                    <a class="button wc-backward" href="{{ route('home') }}"> Return to shop</a>
                                </p>
                            </div>
                    @endif


                    </div>
                    <div class="cart_empty_wrapper" style="display: none">
                        <p>Your cart is currently empty.</p>
                        <p class="return-to-shop">
                            <a class="button wc-backward" href="{{ route('home') }}"> Return to shop</a>
                        </p>
                    </div>
                </div>
                <!-- End cat-box-container -->
            </div>

        </div>
    </div>
    @include('partials.footer')
</div>
<!-- End wrappage -->
@include('partials.footer-js')
<script src="{{ asset('/assets/js/sweetalert2.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".plus,.minus").click(function () {
            var closest_tr = $(this).closest('tr');
            var quantity = closest_tr.find('.qty').val();
            var pid = closest_tr.attr('id');
            var _this  = $(this);

            $.ajax('{{ route('update_cart') }}', {
                method: 'POST',
                dataType: "json",
                data: {'_token': '{{ csrf_token() }}', pid: pid,quantity: quantity},
                success: function (data) {
                    //alert(data.current_product_total);
                    var current_product_total= (Math.round(data.current_product_total * 100) / 100).toFixed(2);
                    closest_tr.find('.total-price').text('€'+current_product_total);
                    $('.cart-count').html(data.cart_count);
                    $('#cart_total p').text('€'+data.total_cart_amount);
                }
            });
        });

        $(".remove_cart").click(function () {
            var closest_tr = $(this).closest('tr');
            var pid = closest_tr.attr('id');
            $.ajax('{{ route('remove_cart') }}', {
                method: 'POST',
                dataType: "json",
                data: {'_token': '{{ csrf_token() }}', pid: pid},
                success: function (data) {
                    closest_tr.remove();
                    $('.cart-count').html(data.cart_count);
                    $('#cart_total p').text('€'+data.total_cart_amount);
                    if(data.cart_count < 1) {
                        $('#cart_outer').css('display','none');
                        $('.cart_empty_wrapper').css('display','block');
                    }

                }
            });
        });

    });

</script>
</body>
</html>

