<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.headers-style')
</head>
<body>

<!-- End pushmenu -->
<div class="wrappage">
    <!-- <div id="rtl">RTL</div> -->
    @include('partials.top-navbar')

    <div class="container">
        <div class="title-text-v2">
            <h3>Cart</h3>
        </div>
        <div class="featured-products home_2 new-arrivals lastest">

            <div class="tab-container space-10">
                <div id="tab_0" class="tab-contentss">
                    <div class="products hover-shadow ver2 border-space-product">
                        <div class="col-sm-12 col-md-10 col-md-offset-1">
                            @if(count(Cart::content()) > 0)

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Quantity</th>
                                        <th class="text-center">Price</th>
                                        <th class="text-center">Total</th>
                                        <th> </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach(Cart::content() as $row) :  ?>
                                    <tr>
                                        <td class="col-sm-8 col-md-6">
                                            <div class="media">
                                                <a class="thumbnail pull-left" href="#"> <img class="media-object" src="{{ asset('uploads/products/'.findProductImage($row->id)  ) }}" style="width: 72px; height: 72px;"> </a>
                                                <div class="media-body">
                                                    <h4 class="media-heading"><a href="{{ route('product_detail',['id'=>$row->id]) }}">{{$row->name}}</a></h4>
                                                </div>
                                            </div></td>
                                        <td class="col-sm-1 col-md-1" style="text-align: center">
                                            <input type="text" class="form-control quantity"  value="<?php echo $row->qty; ?>">
                                        </td>
                                        <td class="col-sm-1 col-md-1 text-center"><strong>€<?php echo $row->price; ?></strong></td>
                                        <td class="col-sm-1 col-md-1 text-center"><strong>€<?php echo $row->total; ?></strong></td>
                                        <td class="col-sm-1 col-md-2">
                                            <a href="javascript:void(0)" class="btna btn-dangera remove_cart" data-pid="{{$row->rowId}}">
                                                <span class="glyphicon glyphicon-remove"></span> Remove
                                            </a>
                                            |
                                            <a href="javascript:void(0)" class="btna btn-dangera update_cart" data-pid="{{$row->rowId}}">
                                                <span class="glyphicon glyphicon-remove"></span> Update
                                            </a>
                                        </td>
                                    </tr>
                                    <?php endforeach;?>

                                    <tr>
                                        <td>   </td>
                                        <td>   </td>
                                        <td>   </td>
                                        <td><h5>Subtotal</h5></td>
                                        <td class="text-right"><h5><strong>€<?php echo Cart::subtotal(); ?></strong></h5></td>
                                    </tr>

                                    <tr>
                                        <td>   </td>
                                        <td>   </td>
                                        <td>   </td>
                                        <td><h3>Total</h3></td>
                                        <td class="text-right"><h3><strong>€<?php echo Cart::total(); ?></strong></h3></td>
                                    </tr>
                                    <tr>
                                        <td>   </td>
                                        <td>   </td>
                                        <td>   </td>
                                        <td>
                                            <button type="button" class="btn btn-default">
                                                <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                                            </button></td>
                                        <td>
                                            <a href="{{ route('check_out') }}" class="btn btn-success">
                                                Checkout <span class="glyphicon glyphicon-play"></span>
                                            </a></td>
                                    </tr>
                                    </tbody>
                                </table>

                            @else
                                <p>Your cart is currently empty.</p>
                                <p class="return-to-shop">
                                    <a class="button wc-backward" href="{{ route('home') }}"> Return to shop</a>
                                </p>
                            @endif
                        </div>

                    <!-- End item -->
                    </div>
                    <!-- End product-tab-content products -->
                </div>
            </div>
        </div>
        <div class="box center space-padding-tb-30 space-30" style="display: none">
            <a class="link-v1 color-brand font-300" href="#" title="View All">View All</a>
        </div>
    </div>


    <div id="back-to-top" style="">
        <i class="fa fa-long-arrow-up"></i>
    </div>

    @include('partials.footer')
</div>
<!-- End wrappage -->
@include('partials.footer-js')

<script type="text/javascript">
    $(document).ready(function () {
        $(".update_cart").click(function () {
            var pid = $(this).attr('data-pid');
            var quantity = $(this).closest('tr').find('.quantity').val();
            // alert(quantity);
            $.ajax('{{ route('update_cart') }}', {
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', pid: pid,quantity: quantity},
                success: function (data) {
                    location.reload();
                }
            });
        });

        $(".remove_cart").click(function () {
            var pid = $(this).attr('data-pid');
            $.ajax('{{ route('remove_cart') }}', {
                method: 'POST',
                data: {'_token': '{{ csrf_token() }}', pid: pid},
                success: function (data) {
                    location.reload();
                }
            });
        });

    });

</script>

</body>
</html>

