@if(!$categories->isEmpty())
@foreach($categories as $category)
            <option value="{{ $category->id }}" {{ old('child_category') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>

    </optgroup>
@endforeach
@else
    <option value="">No option</option>
@endif
