<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Product</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('productSubmitted') }}" method="post" id="form_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <input type="hidden"  name="user_type" value="2">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Product Name</label>
                                <input type="text" name="product_name" id="product_name" class="form-control required" placeholder="Product Name*" value="{{ old('product_name') }}">

                            </div>

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="pd" class="col-form-label">Product Description</label>
                                <textarea class="form-control required"  cols="4" rows="5" id="product_description" name="product_description">{{ old('product_description') }}</textarea>
                             </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4" >
                                <label for="up" class="col-form-label">Unit Regular Price(Optional)</label>
                                <input type="text" class="form-control required" name="regular_price" id="regular_price"  value="{{ old('regular_price') }}">

                            </div>

                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Sale Price</label>
                                <input type="text" class="form-control" name="sale_price" id="sale_price"  value="{{ old('sale_price') }}">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Quantity Unit</label>
                                <select name="quantity_unit" id="quantity_unit" class="form-control required">
                                    <option value="">Please select quantity unit</option>
                                    <option value="1" {{ old('quantity_unit') == '1' ? 'selected' : ''}}>KG</option>
                                    <option value="2" {{ old('quantity_unit') == '2' ? 'selected' : ''}}>Dozen</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Parent Category</label>
                                <select name="parent_category" id="parent_category" class="form-control required" onchange="get_child_category(this.value)">
                                    <option value="">Please select Parent Category</option>
                                    @if(!$categories->isEmpty())
                                        @foreach($categories as $category)
                                           <option value="{{ $category->id }}" {{ old('parent_category') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                                        @endforeach
                                    @else
                                        <option value="">No option</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Child Category</label>
                                <select name="child_category" id="child_category" class="form-control required">
                                </select>
                            </div>
                            <div class="form-group col-md-4"> </div>
                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Image*</label>
                                <input type="file" class="form-control-file product_img" id="product_image0" name="product_image0">
                                <div class="image_preview"></div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Add Product</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>
