<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                    <h4 class="page-title">My Product</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <a href="{{ route('add-product') }}" class="btn btn-sm btn-primary float-right">Add product</a>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Descrption</th>
                            <th>Price</th>
                            <th>Unit</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($products) > 0)
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->product_name}}</td>
                                    <td>{{$product->product_description}}</td>
                                    <td>{{$product->regular_price}}</td>
                                    <td>{{$product->weight}}</td>
                                    <td>{{getProductStatus($product->status)}}</td>
                                    <td>

                                        <a href="{{ route('edit_product',['product_id'=>base64_encode($product->id.":".$product->seller_id)]) }}" class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="Edit Offer">
                                            Edit
                                        </a>
                                        |
                                        <a href="{{ route('change_offer_status',['product_id'=>base64_encode($product->id.":".$product->seller_id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded product_status" data-toggle="tooltip"
                                           data-original-title="Delete product" type="warning" msg="Delete this Product">
                                            Delete
                                        </a>
                                        |
                                        @if($product->status == 0)
                                        <a href="{{ route('change_offer_status',['product_id'=>base64_encode($product->id.":".$product->seller_id.":1")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                           data-original-title="Publish Offer" type="warning" msg="Activate this Product">
                                            Activate
                                        </a>
                                            @else
                                            <a href="{{ route('change_offer_status',['product_id'=>base64_encode($product->id.":".$product->seller_id.":0")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                               data-original-title="Publish Offer" type="warning" msg="Activate this Product">
                                                DeActivate
                                            </a>
                                            @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Not found any product</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div style="float: right">
                        <div class="card-footer">
                            {{ $products->links('vendor.pagination.custom') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



