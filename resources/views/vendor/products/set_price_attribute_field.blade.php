@if(!$productCategoryAttribute->isEmpty())
	@foreach($productCategoryAttribute as $attribute_arr)
		<div class="form-group col-md-4">
			<label for="qu" class="col-form-label">{{ $attribute_arr->attribute_name }}</label>
			<input type="text" class="form-control attribute" name="attribute[{{$attribute_arr->id}}]" placeholder="Set price">
		</div>
	@endforeach
@endif