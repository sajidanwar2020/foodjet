<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Import Product</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('vendor-import-product-submitted') }}" method="post" id="form_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <div class="form-row">

                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Import product from excel*</label>
                                <input type="file" class="form-control-file" id="vendor-excel" name="vendor-excel">
                            </div>
                        </div>
						
						   <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Parent Category</label>
                                <select name="parent_category" id="parent_category" class="form-control required" onchange="get_child_category(this.value)">
                                    <option value="">Please select Parent Category</option>
                                    @if(!$categories->isEmpty())
                                        @foreach($categories as $category)
                                           <option value="{{ $category->id }}" {{ old('parent_category') == $category->id ? 'selected' : ''}}>{{ $category->name }}</option>
                                        @endforeach
                                    @else
                                        <option value="">No option</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Child Category</label>
                                <select name="child_category" id="child_category" class="form-control required">
                                </select>
                            </div>

                        <button type="submit" class="btn btn-primary">Import Product</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>
