<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">Ajouter une option</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Ajouter une option</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <a href="{{ route('add-nutrients') }}" class="btn btn-sm btn-primary float-right">Ajouter Nutrients label</a>
                    <div class=" table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Statut</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($nutrients) > 0)
                            @foreach($nutrients as $nutrient)
                                <tr>
                                    <td>{{$nutrient->label}}</td>
                                    <td></td>
                                    <td>

                                        <a href="{{ route('edit_nutrient',['product_id'=>base64_encode($nutrient->id.":".$nutrient->id)]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Éditer
                                        </a>
                                        |
                                        <a href="{{ route('change_nutrient_status',['product_id'=>base64_encode($nutrient->id.":".$nutrient->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded">
                                            Supprimer
                                        </a>
                                        |
                                        @if($nutrient->status == 0)
                                        <a href="{{ route('change_nutrient_status',['product_id'=>base64_encode($nutrient->id.":".$nutrient->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Activer
                                        </a>
                                            @else
                                            <a href="{{ route('change_nutrient_status',['product_id'=>base64_encode($nutrient->id.":".$nutrient->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                Désactiver
                                            </a>
                                            @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Aucune étiquette de nutriments trouvée</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                    <div>
                        {{ $nutrients->links('vendor.pagination.custom') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



