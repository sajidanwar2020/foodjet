 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                   <form action="{{ route('edit-nutrients-submitted') }}" method="post" enctype="multipart/form-data">
                       
                        {{ csrf_field() }}
                        <input type="hidden"  name="user_type" value="2">
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="pn" class="col-form-label">Nutrients labal</label>
                                <input type="text" name="nutrients_labal" id="nutrients_labal" class="form-control required" value="{{ $nutrients->label }}">
								<input type="hidden" name="id" id="id" class="form-control" value="{{ $nutrients->id }}">
                            </div>
							<div class="form-group col-md-8">
								 <label for="pn" class="col-form-label">La description</label>
								<textarea class="form-control" id="description" name="description">{{ $nutrients->description }}</textarea>
							</div>
						</div>	
                        <button type="submit" class="btn btn-primary">Ajouter label</button>
                    </form>
					
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>



