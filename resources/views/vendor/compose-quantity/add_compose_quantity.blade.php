<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
	<style>
		.container.m-3.sub_item_outer {margin: 8px 0 0 10px !important;}
		.dyanmic_fields.row {margin-bottom: 10px;}
	</style>
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title">Ajouter un produit Compose</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <form action="{{ route('compose-product-quantites-submit') }}" method="post" id="form_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}	
                        <input type="hidden" name="compose_product_type" id="compose_product_type" />
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Produit composé</label>
                                 <select name="composed_product" id="composed_product" class="form-control required" onchange="get_composed_quantity(this.value)">
                                    <option value="">Sélectionnez le produit composé</option>
                                    @if(count($composed_products))
                                        @foreach($composed_products as $composed_product)
                                           <option value="{{ $composed_product->id }}" data-type="{{ $composed_product->type }}" {{ old('composed_product') == $composed_product->id ? 'selected' : ''}}>{{ $composed_product->name }}</option>
                                        @endforeach
                                    @else
                                        <option value="">Pas d'option</option>
                                    @endif
                                </select>
                            </div>
                        </div>
						
						
						<div class="form-row" id="dyanmic_compose_quantites" style="display:none">
                            <input type="hidden" name="total_quantity" id="total_quantity" />
						    <div class="form-group col-md-12">
								<label class="control-label"><button class="add_field_quantity">Ajouter une quantité</button></label>
								<div class="quantity_fields_wrap">
									
								</div>
                            </div>
                        </div>
						
						
						<div class="form-row" id="dyanmic_compose_item" style="display:none">
                            <input type="hidden" name="product_items_quantity" id="product_items_quantity" />
							<input type="hidden" name="last_item_index" id="last_item_index" />
						    <div class="form-group col-md-12">
								<label class="control-label"><button class="add_field_item">Ajouter des articles de produit</button></label>
								<div class="items_fields_wrap">
									
								</div>
                            </div>
                        </div>
						
                        <button type="submit" class="btn btn-primary">Ajouter un produit de composition</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

<script>
	var counter_quantities = 	1;
	var item = 1;
	function get_composed_quantity (cpid) {
		if(cpid > 0) {
			$('.quantity_fields_wrap').html('');
			$('.items_fields_wrap').html('');
			
			composed_product = $('#composed_product').find(':selected').attr('data-type');
			$('#compose_product_type').val(composed_product);
			
			$.ajax({
				method: "GET",
				url: "{{ route('get-compose-product-quantites') }}",
				data: {cpid: cpid},
			    dataType: 'json',
			}).done(function (data) {
				$('#dyanmic_compose_quantites').html(data.message.quantityHtml);
				$('#dyanmic_compose_item').html(data.message.itemHtml);	
				
				item = $('.product_items_outer').length;
				counter_quantities = $('.quantity_fields_wrap .dyanmic_fields').length;
				$('#last_item_index').val(item);				
				
			});	
			
			$('#dyanmic_compose_quantites').css('display','block');
			$('#dyanmic_compose_item').css('display','block');
		} else {
			$('#dyanmic_compose_quantites').css('display','none');
			$('#dyanmic_compose_item').css('display','none');
		}			
	}

$(document).ready(function() {

	//------------------------------------------- Add more quantites ---------------------------------------------------
	
	var max_fields      = 100; 
	var wrapper   		= $(".quantity_fields_wrap"); 
	var add_button      = $(".add_field_quantity");
	
	 
	$(document).on("click",'.add_field_quantity',function(e) {
		//alert(counter_quantities);
		e.preventDefault();
		if(counter_quantities < max_fields){ 
			counter_quantities++;
			composed_product = $('#composed_product').find(':selected').attr('data-type');
			quantity_html = '';
			if( composed_product == '1') {
				quantity_html = '<div class="col-md-3 quantity_ingredient_hide"><input type="text" name="quantity_ingredient[]" class="form-control" placeholder="Quantité d\'ingrédient"/></div>';
			}
			
			dyanmic_html = '<div class="dyanmic_fields row"><div class="col-md-3"><input type="text" name="quantity_name[]" class="form-control" placeholder="Nom de la quantité"/></div><div class="col-md-3"><input type="text" name="quantity_price[]" class="form-control" placeholder="Quantité Prix"/></div>'+quantity_html+'<div class="col-md-3"><a href="#" class="remove_field">Retirer</a></div></div>';
			$('.quantity_fields_wrap').append(dyanmic_html);
			$('#total_quantity').val(counter_quantities);
		}
	});
	
	$(document).on("click",'.remove_field',function(e) {
		e.preventDefault(); 
		$(this).parent('div').parent('.dyanmic_fields').remove(); 
		counter_quantities--;
		$('#total_quantity').val(counter_quantities);
		// delete item or quantity from db when click on remove
		
		id = $(this).data('id');
		type = $(this).data('type');
		
		if(id != "" && type !="") {	
			$.ajax({
				method: "GET",
				url: "{{ route('delete-compose-items-quantites') }}",
				data: {id: id,type: type},
				dataType: 'json',
			}).done(function (data) {				
				
			});	
		}
		
	});
	
	// delete item image
	
	$(document).on("click",'.item_img .remove_item_img',function(e) {
		e.preventDefault(); 
		id = $(this).data('id');
		//alert(id);
		if(id != "" && id > 0) {	
			$.ajax({
				method: "GET",
				url: "{{ route('delete-compose-items-image') }}",
				data: {id: id},
			}).done(function (data) {				
				$('#item_id_'+data).remove();
			});	
		}
		
	});
	
	
	//-------------------------------------------- Add more product items ----------------------------------------------------
	
	var max_fields      = 100; 
	var items_fields_wrap   		= $(".items_fields_wrap"); 
	var add_item      = $(".add_field_item");
	
	
	$(document).on("click",'.add_field_item',function(e) {
		//alert();
		e.preventDefault();
		if(item < max_fields){ 
			item++;
			composed_product = $('#composed_product').find(':selected').attr('data-type');
			item_html = item_html2 = '';
			if(composed_product == 2) {
				//item_html = ' | <a href="javscript:void(0)" class="add_items">Ajouter</a>'; 
				//item_html2 = '<div class="col-md-2"><input type="text" name="item_ingredient['+item+']" class="form-control" placeholder="Ingrédient de l\'article de produit"/></div>';
			}
			dyanmic_html = '<div class="dyanmic_fields row product_items_outer" data-itemId="'+item+'">'+
							'<div class="col-md-3"> <input type="file" class="form-control-file" name="item_img['+item+']"></div>'+
							'<div class="col-md-2"><input type="text" name="item_name['+item+']" class="form-control" placeholder="Product item name"/></div>'+
							'<div class="col-md-2"><input type="text" name="item_price['+item+']" class="form-control" placeholder="Item price" value=""/></div>'
							+item_html2+
							'<div class="col-md-2"><a href="#" class="remove_field">Retirer</a> <span class="item_hide">'+item_html+'</div>'+
							'</div>';
			$('.items_fields_wrap').append(dyanmic_html);
			console.log(item);
			$('#product_items_quantity,#last_item_index').val(item);
		}
	});
	
	$(document).on("click",'.remove_field',function(e) {
	//$('.items_fields_wrap').on("click",".remove_field", function(e){ 
		e.preventDefault(); 
		$(this).parent('div').parent('.dyanmic_fields').remove(); 
		item--;
		$('#product_items_quantity').val(item);
	});
	
	//--------------------------------------------Add more product sub items----------------------------------------------------
	/*
	var max_fields      = 100; 
	var product_items_outer  = $(".product_items_outer"); 
	var add_sub_item      = $(".add_items");
	
	var sub_item = 0; 
	
	$(document).on("click",'.add_items',function(e) {
		e.preventDefault();
		if(sub_item < max_fields){ 
			sub_item++;
			__parent = $(this).parent('span').parent('div').parent('.product_items_outer');
			itemid = __parent.attr('data-itemid');
			product_item = '<div class="container m-3 sub_item_outer">'+
							'<div class="row">'+
							'<div class="col-md-3"><input type="file" class="form-control-file" name="sub_item_img['+itemid+'][]"></div>'+
							'<div class="col-md-2"> <input type="text" name="sub_item_name['+itemid+'][]" class="form-control" placeholder="Nom du sous-élément"></div>'+ 
							'<div class="col-md-2"> <a href="#" class="remove_field_sub_item">Retirer</a></div>'+
							'</div>'+ 
							'</div>';
			__parent.append(product_item);
		}
	}); 
	
	$(document).on("click",'.remove_field_sub_item',function(e) {
		e.preventDefault(); 
		$(this).parent('div').parent('div').parent('.sub_item_outer').remove(); 
		sub_item--;
	}); */
	
	//------------------------------------------------------------------------------------------------
	
	<?php  if(isset($_GET['product_id']) AND $_GET['product_id'] > 0) { ?>
			$('#composed_product').val('<?php echo $_GET['product_id'] ?>').trigger("change");
	<?php } ?>
});
	
</script>


</body>
</html>
