 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">Des produits</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Mettre à jour le produit</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('edit-compose-product-submit') }}" method="post" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                       
					    <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                        <input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}"/>
                       
					   <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Nom du produit</label>
                                <input type="text" name="_name" id="_name" class="form-control" placeholder="Product Name*" value="{{ $product->name }}">

                            </div>

                        </div>
						
						<div class="form-row">
							<div class="form-group col-md-4">
									<label for="qu" class="col-form-label">Type</label>
									<select name="type" id="type" class="form-control">
										<option value="">Please select type</option>
										<option value="1" {{ $product->type == 1 ? 'selected' : ''}}>Simple</option>
										<option value="2" {{ $product->type == 2 ? 'selected' : ''}}>Mixture</option>
									</select>
							</div>
						</div>
						
                        <button type="submit" class="btn btn-primary">Mettre à jour le produit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>



