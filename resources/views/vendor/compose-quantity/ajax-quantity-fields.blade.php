@if($data)
	<input type="hidden" name="total_quantity" id="total_quantity" value="{{count($data['composeQuantites'])}}"/>
	<div class="form-group col-md-12">
		<label class="control-label"><button class="add_field_quantity">Ajouter une quantité</button></label>
			<div class="quantity_fields_wrap">
			@foreach($data['composeQuantites'] as $composeQuantity)
			<?php
				$quantity_html = '';
				if($data['composed_products']['type'] == '1') {
					$quantity_html = '<div class="col-md-3 quantity_ingredient_hide"><input type="text" name="quantity_ingredient['.$composeQuantity->id .']" class="form-control" placeholder="Quantité d\'ingrédient" value="'.$composeQuantity->ingredients.'"/></div>';
				}
			?>
				<div class="dyanmic_fields row"><div class="col-md-3"><input type="text" name="quantity_name[<?php echo $composeQuantity->id ?>]" class="form-control" placeholder="Nom de la quantité" value="<?php echo $composeQuantity->quantity?>"/></div><div class="col-md-3"><input type="text" name="quantity_price[<?php echo $composeQuantity->id ?>]" class="form-control" placeholder="Quantité Prix" value="<?php echo $composeQuantity->price ?>"/></div><?php echo $quantity_html ?><div class="col-md-3"><a href="#" class="remove_field" data-id="<?php echo $composeQuantity['id'] ?>" data-type="quantity">Retirer</a></div></div>
			@endforeach
			</div>
	</div>
@endif