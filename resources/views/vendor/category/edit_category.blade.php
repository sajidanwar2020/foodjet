<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                            <form action="{{ route('editCategorySubmitted-v') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                                <input type="hidden" id="cat_id" name="cat_id" value="{{ $category->id }}"/>
								
                                @if(isset($_GET['parent_id']) AND $_GET['parent_id'] > 0)
                                    <div class="form-group">
                                        <label class="control-label">Sélectionnez la catégorie parent</label>
                                        <select class="form-control" id="parent_id" name="parent_id">
                                            <option value="">Sélectionnez la catégorie parent</option>
                                            @foreach($parent_category as $category1)
                                                <option value="{{ $category1->id }}" @if($category1->id == $category->parent_id) {{'selected'}} @endif >{{ $category1->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
								
								@if(!isset($_GET['parent_id']) AND empty($_GET['parent_id']))
									<div class="form-group">
                                        <label class="control-label">sélectionner le type de catégorie</label>
                                        <select class="form-control" id="categoryType" name="categoryType">
                                            <option value="">Sélectionnez la catégorie parent</option>
                                            @foreach($categoryTypes as $categoryType)
                                                <option value="{{ $categoryType->id }}" @if($categoryType->id == $category->category_type_id) {{'selected'}} @endif>{{ $categoryType->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
								@endif	
								
                                <div class="form-group">
                                    <label for="name">Nom de la catégorie*</label>
                                    <input type="text" name="name"  class="form-control" id="name" value="{{$category->name}}">
                                </div>

                                <div class="form-group">
                                    <label for="description">La description</label>
                                    <textarea rows="4" cols="50" name="description" id="description" class="form-control">{{$category->description}}</textarea>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="status">Statut*</label>
                                    <select id="cat_status" name="cat_status" class="form-control">
                                        <option value="1" {{$category->status == '1' ? 'selected' : ''}}>Activé</option>
                                        <option value="2" {{$category->status == '2' ? 'selected' : ''}}>Désactivé</option>
                                    </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="qu" class="col-form-label">Image catégorie*</label>
                                    @if(isset($category->image) AND !empty($category->image))
                                        <input type="hidden" id="old_image" name="old_image" value="{{ $category->image }}"/>
                                        <div class="cat_image_preview"> <img class="rounded avatars" id="avatar2" src="{{ url('uploads/categories/'.$category->image) }}" alt="avatar" style="width: 150px;height: 150px"> </div>
                                    @endif
                                    <input type="file" class="form-control-file file_preview" id="category" name="category" accept="image/*">
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="qu" class="col-form-label">Icône catégorie*</label>
                                    @if(isset($category->icon) AND !empty($category->icon))
                                        <div class="cat_image_preview">  <img class="rounded avatars" id="avatar2" src="{{ url('uploads/categories/icons/'.$category->icon) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
                                    @endif
                                    <input type="file" class="form-control-file file_preview" id="icon" name="icon" accept="image/*">
                                </div>
																
								<div class="form-group">
									<label class="control-label"><button class="add_field_button">Ajouter un attribut</button></label>
									@if(count($productCategoryAttribute) > 0) 
											<div class="input_fields_wrap">
												@foreach($productCategoryAttribute as $attribute)
													<input type="text" name="attribute_name[{{$attribute->id}}]" class="form-control file_preview" placeholder="Nom de l’attribut" value="{{$attribute->attribute_name}}">
													<br>
												@endforeach
											</div>
									@else
										<div class="input_fields_wrap">
											<input type="text" name="attribute_name[]" class="form-control file_preview" placeholder="Nom de l’attribut">
										</div>  
									@endif
								</div>
                              
								
                                <div class="form-group text-right mb-0">
                                    <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">
                                        Valider
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect waves-light">
                                        Annuler
                                    </button>
                                </div>

                            </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')
<script>

$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<br><div><input type="text" name="attribute_name[]" class="form-control" placeholder="Valeur / nom de la quantité"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});
</script>
<script src="{{ asset('public/js/texteditor.js') }}" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

</body>
</html>
