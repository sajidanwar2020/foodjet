 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <a  href="{{ route('add-category-type') }}" id="ac" class="btn btn-success octf-btn octf-btn1 mb-3">
                        Ajouter un type de catégorie
                    </a>
                <div class=" table-responsive">
                    <table class="table table-striped mb-0" id="tech-companies-1">
						<thead>
						<tr>
							<th>Icône</th>
							<th>Nom</th>
							<th>Actions</th>
						</tr>
						</thead>
							<tbody>

							@foreach($categoryTypes as $categoryType)
								
								<tr>
									<td>
										@if(isset($categoryType->icon))
											<img class="primary_image"
												 src="{{ url('uploads/categories/icons/'.$categoryType->icon) }}"
												 alt="" width="50" height="50"/>
									
										@endif
									</td>

									<td>{{ $categoryType->name }}</td>

									<?php
										$categoryStatusArr = array('1' =>'Activé','2' => 'Désactivé' ,'3' => 'Supprimé');
									?>
									<td>{{ $categoryStatusArr[$categoryType->status] }}</td>
									
									<td>
										<a href="{{ route('edit-category-type',['cat_type_id'=>$categoryType->id]) }}"  class="btn octf-btn btn-set" data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fas fa-edit" aria-hidden="true"></i>
										</a>

										@if($categoryType->status == 2 || $categoryType->status == 3)
											<a href="{{ route('category-type-status',['cat_type_id'=>base64_encode($categoryType->id.":".$categoryType->id.":1")]) }}"class="btn btn-set octf-btn octf2-btn " data-toggle="tooltip" data-original-title="Activate">
                                                <i class="fas fa-eye"></i>
											</a>
										@elseif($categoryType->status == 1)
											<a href="{{ route('category-type-status',['cat_type_id'=>base64_encode($categoryType->id.":".$categoryType->id.":2")]) }}"class="btn btn-set octf-btn octf3-btn" data-toggle="tooltip" data-original-title="DeActivate">
                                                <i class="fas fa-eye-slash"></i>
											</a>
										@endif
									</td>
								</tr>

							@endforeach
							</tbody>
                        </table>
                    </div>
			
                    
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



