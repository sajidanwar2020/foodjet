<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                   
                            <form action="{{ route('edit-category-type-submitted') }}" method="post" enctype="multipart/form-data">
                                
                                {{ csrf_field() }}

                                <input type="hidden" id="cat_type_id" name="cat_type_id" value="{{ $categoryType->id }}"/>

                                 <div class="form-group">
                                    <label for="name">Titre*</label>
                                    <input type="text" name="name"  class="form-control" id="name" value="{{$categoryType->name}}">
                                </div>


                                <div class="form-group col-md-6">
                                    <label for="qu" class="col-form-label">Icône catégorie*</label>
                                    @if(isset($categoryType->icon) AND !empty($categoryType->icon))
                                        <div class="cat_image_preview">  <img class="rounded avatars" id="avatar2" src="{{ url('uploads/categories/icons/'.$categoryType->icon) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
                                    @endif
                                    <input type="file" class="form-control-file file_preview" id="category_type_icon" name="category_type_icon" accept="image/*">
                                </div>
								
                                <div class="form-group text-right mb-0">
                                    <button class="btn btn-primary waves-effect waves-light mr-1" type="submit">
                                        Valider
                                    </button>
                                    <button type="reset" class="btn btn-secondary waves-effect waves-light">
                                        Annuler
                                    </button>
                                </div>

                            </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>
