<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                  
                            <form action="{{ route('categorySubmitted-v') }}" method="post"  enctype="multipart/form-data">
                               
                                {{ csrf_field() }}
                                @if(isset($_GET['parent_id']) AND $_GET['parent_id'] > 0)
                                    <div class="form-group">
                                        <label class="control-label">Sélectionnez la catégorie parent</label>
                                        <select class="form-control" id="parent_id" name="parent_id">
                                            <option value="">Sélectionnez la catégorie parent</option>
                                            @foreach($parent_category as $category)
                                                <option value="{{ $category->id }}" @if($category->id == $_GET['parent_id']) {{'selected'}} @endif >{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
								
								@if(!isset($_GET['parent_id']) AND empty($_GET['parent_id']))
									<div class="form-group">
                                        <label class="control-label">sélectionner le type de catégorie</label>
                                        <select class="form-control" id="categoryType" name="categoryType">
                                            <option value="">Sélectionnez la catégorie parent</option>
                                            @foreach($categoryTypes as $categoryType)
                                                <option value="{{ $categoryType->id }}" {{ old('categoryType') == $categoryType->id ? 'selected' : ''}}>{{ $categoryType->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
								@endif	 
                                <div class="form-group">
                                    <label for="userName">Nom de la catégorie*</label>
                                    <input type="text" name="name"  class="form-control" id="name" value="">
                                </div>

                                <div class="form-group">
                                    <label for="emailAddress">La description</label>
                                    <textarea rows="4" cols="50" name="description" id="description" class="form-control">{{ old('description')}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="pass1">Statut*</label>
                                    <select id="cat_status" name="cat_status" class="form-control">
                                        <option value="1">Activé</option>
                                        <option value="2">Désactivé</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="pass1">Image catégorie*</label>
                                    <input id="category" type="file" name="category" class="form-control file_preview">
                                    <div class="cat_image_preview"></div>
                                </div>

                                <div class="form-group">
                                    <label for="pass1">Icône catégorie*</label>
                                    <input id="category_icon" type="file" name="icon" class="form-control file_preview">
                                    <div class="cat_image_preview"></div>
                                </div>
																
								<div class="form-group">
									<label class="control-label"><button class="btn btn-success octf-btn octf-btn1 mb-3">Ajouter un attribut</button></label>
									<div class="input_fields_wrap">
										<input type="text" name="attribute_name[]" class="form-control file_preview" placeholder="Nom de l’attribut">
									</div>
								</div>
                             
								
								
                                <div class="form-group text-right mb-0">
                                    <button class="btn btn-success octf-btn octf-btn1 mr-1" type="submit">
                                        Valider
                                    </button>
                                    <button type="reset" class="btn btn-success octf-btn octf-btn1">
                                        Annuler
                                    </button>
                                </div>

                            </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')
<script>
$(document).ready(function() {
	var max_fields      = 10; //maximum input boxes allowed
	var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
	var add_button      = $(".add_field_button"); //Add button ID
	
	var x = 1; //initlal text box count
	$(add_button).click(function(e){ //on add input button click
		e.preventDefault();
		if(x < max_fields){ //max input box allowed
			x++; //text box increment
			$(wrapper).append('<br><div><input type="text" name="attribute_name[]" class="form-control" placeholder="Valeur / nom de la quantité"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
		}
	});
	
	$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
		e.preventDefault(); $(this).parent('div').remove(); x--;
	})
});
</script>
<script src="{{ asset('public/js/texteditor.js') }}" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

</body>
</html>
