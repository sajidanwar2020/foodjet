<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">Les catégories</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Les catégories</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <a  href="{{ route('add-category-v',['parent_id'=>$parent_id]) }}" id="ac" class="btn btn-success">
                       Ajout sous-catégorie
                    </a>
                <div class=" table-responsive"><table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>La description</th>
                                                <th>Catégorie</th>
                                                <th>Statut</th>
                                                <th>Ordre d’affichage</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @php $counter=1; @endphp
                                            @foreach($categories as $category)
                                                <tr>
                                                    <td>
                                                        @if(isset($category->image))
                                                            <img class="primary_image"
                                                                 src="{{ asset('uploads/categories/'.$category->image ) }}"
                                                                 alt="" width="50" height="50"/>
                                                        @else
                                                            <img class="primary_image"
                                                                 src="{{asset('assets/site/images/products/featured/2.jpg')}}"
                                                                 alt="" width="50" height="50"/>
                                                        @endif

                                                    </td>
                                                    <td>{{ $category->description }}</td>
                                                    <td>{{ $category->name }}</td>
                                                   <?php
                                                        $categoryStatusArr = array('1' =>'Activé','2' => 'Désactivé' ,'3' => 'Supprimé');
                                                    ?>
                                                    <td>{{ $categoryStatusArr[$category->status] }}</td>
                                                    <td>
													<input type="text" id="sort_order" value="{{ $category->sort_order }}" class="" onchange="updateSortOrder('{{ base64_encode($category->id) }}',this);">
                                                    </td>
                                                    <td>

                                                            <a href="{{ route('edit_category-v',['cat_id'=>$category->id,'parent_id'=>$_GET['parent_id']]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                Modifier
                                                            </a>
                                                            |
                                                            <a href="{{ route('change_category_status-v',['cat_id'=>base64_encode($category->id.":".$category->id.":3")]) }}" class="btn btn-sm btn-danger btn-rounded" >
                                                                Supprimer
                                                            </a>
                                                            |
                                                             @if($category->status == 2 || $category->status == 3)
                                                                <a href="{{ route('change_category_status-v',['cat_id'=>base64_encode($category->id.":".$category->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                    Activer
                                                                </a>
                                                            @elseif($category->status == 1)
                                                                <a href="{{ route('change_category_status-v',['cat_id'=>base64_encode($category->id.":".$category->id.":2")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                                    Désactiver
                                                                </a>
                                                            @endif
                                                    </td>
                                                </tr>
                                                @php $counter++; @endphp
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>
                    <div>
                            {{ $categories->links('vendor.pagination.custom') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



