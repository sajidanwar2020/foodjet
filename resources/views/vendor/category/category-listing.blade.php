 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">Les catégories</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Les catégories</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <a  href="{{ route('add-category-v') }}" id="ac" class="btn btn-success octf-btn octf-btn1 mb-3">
                        Ajout d’une catégorie
                    </a>
                <div class=" table-responsive">
                    <table class="table table-striped mb-0" id="tech-companies-1">
                                            <thead>
                                            <tr>
                                                <th>Image</th>
                                                <th>Ajout sous-catégorie</th>
                                                <th>La description</th>
                                                <th>Catégorie</th>
                                                <th>Sous-catégorie</th>
                                                <th>Statut</th>
                                                <th>Ordre d’affichage</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @php $counter = 1; @endphp
                                            @foreach($categories as $category)
                                                <?php
                                                    $child_count = getChildCategoriesIds($category->id);
                                                ?>
                                                <tr>
                                                    <td>
                                                        @if(isset($category->image))
                                                            <img class="primary_image"
                                                                 src="{{ asset('uploads/categories/'.$category->image ) }}"
                                                                 alt="" width="50" height="50"/>
                                                        @else
                                                            <img class="primary_image"
                                                                 src="{{asset('assets/site/images/products/featured/2.jpg')}}"
                                                                 alt="" width="50" height="50"/>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a  href="{{ route('add-category-v',['parent_id'=>$category->id]) }}" id="ac" class="btn btn-success octf-btn octf-btn1">
                                                           Ajouter un enfant
                                                        </a>
                                                    </td>
                                                    <td>{{ $category->description }}</td>
                                                    <td>{{ $category->name }}</td>
                                                    <td><a href="{{ route('categoryListing-v',['parent_id'=>$category->id]) }}">{{count($child_count)}}</a></td>
                                                    <?php
                                                    $categoryStatusArr = array('1' =>'Activé','2' => 'Désactivé' ,'3' => 'Supprimé');
                                                    ?>
                                                    <td>{{ $categoryStatusArr[$category->status] }}</td>
                                                    <td>
                                <input type="text" id="sort_order" value="{{ $category->sort_order }}" class="" onchange="updateSortOrder('{{ base64_encode($category->id) }}',this);">
            <!-- <button type="button" class="btn btn-sm btn-warning btn-rounded" data-toggle="modal" data-target="#sortModal{{ $category->id }}">
                Sort Order
            </button>
            <div class="modal fade" id="sortModal{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document" style="max-width: 550px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Sort Order Update</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body " style="display: flex; justify-content: center;">
                            <img src="{{asset('assets/img/sort.png')}}" style="width: 35px; height: 30px; margin-top: 3px; margin-right: 5px;">
                            <form action="{{ route('sort-categories-update',['cat_id'=>base64_encode($category->id)]) }}" method="post">
                                {{ csrf_field() }}
                                <h5>{{ $category->name }}</h5>
                                <select style="padding: 8px 8px 10px 8px;" name="sort_order">
                                    @for($i=1;$i<=$cat_count;$i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                                <button type="submit" class="btn btn-primary">Update Sort Order</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>  -->
                                                    </td>
                                                    <td>
                                                            <a href="{{ route('edit_category-v',['cat_id'=>$category->id]) }}" class="btn octf-btn btn-set" data-toggle="tooltip" data-original-title="Edit">
                                                                <i class="fas fa-edit" aria-hidden="true"></i>
                                                            </a>
                                                            |
                                                            <a href="{{ route('change_category_status-v',['cat_id'=>base64_encode($category->id.":".$category->id.":3")]) }}"class="btn octf1-btn btn-set" data-toggle="tooltip" data-original-title="Delete">
                                                                <i class="fas fa-trash"></i>
                                                            </a>

                                                            @if($category->status == 2 || $category->status == 3)
                                                                <a href="{{ route('change_category_status-v',['cat_id'=>base64_encode($category->id.":".$category->id.":1")]) }}"class="btn btn-set octf-btn octf2-btn " data-toggle="tooltip" data-original-title="Activate">
                                                                    <i class="fas fa-eye"></i>
                                                                </a>
                                                            @elseif($category->status == 1)
                                                                <a href="{{ route('change_category_status-v',['cat_id'=>base64_encode($category->id.":".$category->id.":2")]) }}" class="btn btn-set octf-btn octf3-btn" data-toggle="tooltip" data-original-title="DeActivate">
                                                                    <i class="fas fa-eye-slash"></i>
                                                                </a>
                                                            @endif

                                                    </td>
                                                </tr>
                                                @php $counter++; @endphp
                                            @endforeach
                                            </tbody>
                                        </table>
                            </div>

                        <div >
                            {{ $categories->links('vendor.pagination.custom') }}
                        </div>
                    
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



