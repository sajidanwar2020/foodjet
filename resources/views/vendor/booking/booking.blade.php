<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

		<div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">Booking</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Booking</h4>
                </div>
            </div>
        </div>

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
					
                    <div class=" table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Prénom</th>
							<th>Nom de famille</th>
							<th>Email</th>
							<th>Téléphone</th>
							<th>Date heure d'arrivée</th>
							<th>Nombre de personnes</th>
							<th>commentaire</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($bookings) > 0)
                            @foreach($bookings as $booking)
                                <tr>
                                    <td>{{$booking->first_name}}</td>
									<td>{{$booking->last_name}}</td>
									<td>{{$booking->email}}</td>
									<td>{{$booking->telephone}}</td>
									<td>{{$booking->date_time_arrival}}</td>
									<td>{{$booking->no_of_people}}</td> 
									<td>{{$booking->comments}}</td>
                                    <td>
										<a href="{{ route('delete-booking',['id'=>$booking->id]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Supprimer 
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Aucun tableau ajouté</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                    <div>
                        {{ $bookings->links('vendor.pagination.custom') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



