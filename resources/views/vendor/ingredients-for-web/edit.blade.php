 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                   <form action="{{ route('edit-ingredients-web-submitted') }}" method="post" >
						<input type="hidden" name="id" id="id" value="{{ $ingredients_for_web->id }}">
                     
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="pn" class="col-form-label">Nom</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $ingredients_for_web->name }}">
                            </div>
							
							<div class="form-group col-md-8">
                                <label for="pn" class="col-form-label">La description</label>
								<textarea id="description" name="description" rows="4" cols="50" class="form-control">{{ $ingredients_for_web->description }}</textarea>
                            </div>
							
						</div>	
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </form>
				
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>



