<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title --> 

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
					<a href="{{ route('add-ingredients-for-web') }}" class="btn btn-sm btn-primary">Ajouter une origine</a>
                    <div class=" table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nom</th>
							<th>La description</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($ingredients_for_webs) > 0)
                            @foreach($ingredients_for_webs as $ingredients_for_web)
                                <tr>
                                    <td>{{$ingredients_for_web->name}}</td>
									<td>{{$ingredients_for_web->description}}</td>
                                    <td>

                                        <a href="{{ route('edit-ingredients-web',['id'=>base64_encode($ingredients_for_web->id.":".$ingredients_for_web->id)]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Modifier
                                        </a>
                                        |
                                        @if($ingredients_for_web->status == 0)
											<a href="{{ route('change-ingredients-web',['id'=>base64_encode($ingredients_for_web->id.":".$ingredients_for_web->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
												Activer
											</a>
                                        @else
                                            <a href="{{ route('change-ingredients-web',['id'=>base64_encode($ingredients_for_web->id.":".$ingredients_for_web->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                Désactiver
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Aucun tableau ajouté</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                    <div>
                        {{ $ingredients_for_webs->links('vendor.pagination.custom') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



