 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Page d'accueil</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Page d'accueil</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    @if(count($homepages) <= 0)
                        <a href="{{ route('addhomepages') }}" class="btn btn-sm btn-primary float-right">Ajouter page d'accueil</a>
                    @endif

                    @if(Session::has('responseText'))
                      <div class="col-sm-12 text-center">
                        <p>{{ session('responseText') }}</p>
                      </div>
                    @endif

                    @if(count($homepages) <= 0)
                        <div class="col-sm-12 text-center text-warning">Homepages Not Found!</div>
                    @else
                            <div class=" table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Titre</th>
                            <!-- <th>Meta Title</th>
                            <th>Meta Keywords</th> -->
                            <th>La description</th>
                            <!-- <th>Status</th> -->
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($homepages as $page)
                                <tr>
                                    <td>{{ $page->title }}</td>
                                    <!-- <td>{{ $page->meta_title }}</td>
                                    <td>{{ $page->meta_keywords }}</td> -->
                                    <td>
                                        @php //100Character Pull
                                        $small = substr($page->description, 0, 100);
                                        @endphp
                                        {{ $small.'...' }}</td>
                                    <!-- <td>{{ $page->status}}</td> -->
                                    <td>

                                        <a href="{{ route('edithomepage',['page_id'=>base64_encode($page->id.":".$page->restaurant_id)]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Éditer
                                        </a>
                                        |
                                   
                                        @if($page->status == 0)
                                        <a href="{{ route('statusonhomepage',['page_id'=>base64_encode($page->id.":".$page->restaurant_id)]) }}" class="btn btn-sm btn-info btn-rounded" >
                                            Activer
                                        </a>
                                            @else
                                            <a href="{{ route('statusoffhomepage',['page_id'=>base64_encode($page->id.":".$page->restaurant_id)]) }}" class="btn btn-sm btn-info btn-rounded" >
                                                Désactiver
                                            </a>
                                            @endif

                                        |
                                        <a href="{{ route('deletehomepage',['page_id'=>base64_encode($page->id.":".$page->restaurant_id)]) }}" class="btn btn-sm btn-danger btn-rounded" >
                                            Supprimer
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                            </div>
                        @endif

                        <div >
                            {{ $homepages->links('vendor.pagination.custom') }}
                        </div>
                    
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



