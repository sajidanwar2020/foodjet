<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Mes pages</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Modifier la page</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('editpage') }}" method="post" id="form_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $page_id }}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Titre de la page</label>
                                <input type="text" name="page_title" id="page_title" class="form-control required" placeholder="Page Title*" value="{{ $page->title }}" required>

                            </div>
						<?php /*
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Page Meta Title</label>
                                <input type="text" name="meta_title" id="meta_title" class="form-control" value="{{ $page->meta_title }}" placeholder="Page Meta Title">
                            </div> */ ?>

                        </div>
						
						<?php /*
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Mots-clés de méta de page</label>
                                <input type="text" name="meta_keywords" id="meta_keywords" class="form-control" value="{{ $page->meta_keywords }}" placeholder="Page Meta Keywords">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Page Meta Description</label>
                                <input type="text" name="meta_description" id="meta_description" class="form-control" value="{{ $page->meta_description }}" placeholder="Page Meta Description">
                            </div>

                        </div> */ ?>
						
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="pd" class="col-form-label">Description de la page</label>
                                <textarea class="form-control required"  cols="4" rows="5" id="description" name="description">{{ $page->description }}</textarea>
                             </div>
                        </div>
                        <div class="form-row">
                            <label>Sélectionnez le statut*:</label>
						    <select class="form-group col-md-6" name="status" required>
                                <option value="1" @if($page->status=='1') {{ 'selected' }} @endif>Actif</option>
                                <option value="2" @if($page->status=='2') {{ 'selected' }} @endif>Désactiver</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Modifier la page</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

<script src="{{ asset('public/js/texteditor.js') }}" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

</body>
</html>
