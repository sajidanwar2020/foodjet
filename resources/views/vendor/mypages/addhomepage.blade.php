<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Homepages</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add homepage</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('addhomepages') }}" method="post" id="form_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Titre de la page</label>
                                <input type="text" name="page_title" id="page_title" class="form-control required" placeholder="Homepage Title*" required>

                            </div>

                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Page Meta Title </label>
                                <input type="text" name="meta_title" id="meta_title" class="form-control" placeholder="Page Meta Title">
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Mots-clés de méta de page</label>
                                <input type="text" name="meta_keywords" id="meta_keywords" class="form-control" placeholder="Page Meta Keywords">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Page Meta Description</label>
                                <input type="text" name="meta_description" id="meta_description" class="form-control" placeholder="Page Meta Description">
                            </div>

                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Header Title</label>
                                <input type="text" name="header_title" id="meta_keywords" class="form-control" placeholder="Page Header Title">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Header Sub-Title</label>
                                <input type="text" name="header_subtitle" id="header_subtitle" class="form-control" placeholder="Page Header Sub-Title">
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="pd" class="col-form-label">Description de la page</label>
                                <textarea class="form-control required"  cols="4" rows="5" id="description" name="description">{{ old('product_description') }}</textarea>
                             </div>
                        </div>
                        <div class="form-row">
                            <label>Sélectionnez le statut*:</label>
						    <select class="form-group col-md-6" name="status" required>
                                <option value="1">Actif</option>
                                <option value="2">Désactiver</option>
                            </select>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="qu" class="col-form-label">Logo</label>
                                <input type="file" class="form-control-file header_logo" id="header_logo" name="logo">
                                <div class="image_preview"></div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="qu" class="col-form-label">Header Image</label>
                                <input type="file" class="form-control-file header_image" id="header_image" name="header_image">
                                <div class="image_preview"></div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="qu" class="col-form-label">Footer Color</label>
                                <input type="color" class="form-control footer_color" id="footer_color" name="footer_color" value="#ff0000">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="qu" class="col-form-label">Footer Address</label>
                                <input type="text" class="form-control footer_address" id="footer_address" name="footer_address">
                            </div>
                            

                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="qu" class="col-form-label">Footer Phone No 1</label>
                                <input type="text" class="form-control footer_phone1" id="footer_phone1" name="footer_phone1">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="qu" class="col-form-label">Footer Phone No 2</label>
                                <input type="text" class="form-control footer_phone2" id="footer_phone2" name="footer_phone2">
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Footer Start Time</label>
                                <input type="text" class="form-control footer_starttime" id="footer_starttime" name="footer_starttime">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Footer Break Time</label>
                                <input type="text" class="form-control footer_breaktime" id="footer_breaktime" name="footer_breaktime">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Footer End Time</label>
                                <input type="text" class="form-control footer_endtime" id="footer_endtime" name="footer_endtime">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Ajouter une page</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

<script src="{{ asset('public/js/texteditor.js') }}" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

</body>
</html>
