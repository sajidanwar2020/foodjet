<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

       <div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<form action="{{ route('profile-update') }}" method="post" enctype="multipart/form-data">
						@if($errors->any())
							@foreach($errors->all() as $error)
								<span style="color: red">{{ @"*".$error }}</span><br>
							@endforeach
						@endif
						{{ csrf_field() }}
						
						<div class="form-group">
							<label>Nom de l'établissement</label>
							<input type="text" name="name" id="name" class="form-control" placeholder="Nom de l'établissement*" value="{{  $info->name }}">
						</div>
						
						<div class="form-group">
							<label>Nom de l'établissement ou Texte (en surimpression du logo/visuel)</label>
							<input type="text" name="main_heading" id="main_heading" class="form-control" placeholder="Nom de l'établissement ou Texte" value="{{  $info->main_heading }}">
						</div>

						<div class="form-group">
							<label>Slogan ou texte (en surimpression du logo/visuel)</label>
							<input type="text" name="above_heading_text" id="above_heading_text" class="form-control" placeholder="Slogan ou texte" value="{{  $info->above_heading_text }}">
						</div>
						
						<div class="form-group">
							<label>Post-texte</label>
							<input type="text" name="header_image_text" id="header_image_text" class="form-control" value="{{  $info->header_image_text }}">
						</div>
						
						<div class="form-group" style="display:none">
							<label>Heading Button Text</label>
							<input type="text" name="button_text" id="button_text" class="form-control" placeholder="ButtonText*" value="{{  $info->button_text }}">
						</div>

						<div class="form-group" style="display: none">
							<label>Texte de description (en surimpression du logo/visuel)</label>
							<textarea id="description" rows="4" cols="50"  name="description" class="form-control">{{  $info->description }}</textarea>
						</div>
						
						<div class="form-group">
							<label>Identifiant de connexion</label>
							<input type="email" class="form-control" name="email" id="email" placeholder="Identifiant de connexion" value="{{ $info->email }}" readonly>
						</div>	
						

						<div class="form-group">
							<label>Adresse e-mail de contact</label>
							<input type="email" class="form-control" name="contact_email" id="contact_email" placeholder="Adresse e-mail de contact" value="{{ $info->contact_email }}">
						</div>			
						
						<div class="form-group">
							<label for="location_mandatory">Emplacement obligatoire</label>
							<input type="checkbox" name="location_mandatory" id="location_mandatory" class="form-control2" value="1" <?php if($info->location_mandatory) echo "checked" ?>>
						</div> 
						
						<div class="form-group">
							<label>Adresse de l'établissement</label> 
							<input type="text" class="form-control" name="location" id="location" placeholder="Location*" value="{{ $info->location }}">
							<input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ $info->latitude }}">
							<input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ $info->longitude }}">
						</div>
						
						<div class="form-group">
							<label>Distance de commande en Meter (le client peut commander la distriace minimam)</label>
							<input type="text" name="order_distance" id="order_distance" class="form-control" value="{{  $info->order_distance }}" >
						</div>

						<div class="form-group">
							<label>Numéro de téléphone</label>
							<input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ $info->phone_number }}">
						</div>
						
						<div class="form-group">
							<div class="row">
								<div class="form-group col-md-6">
									<label for="qu" class="col-form-label">Logo haut de page</label>
									@if(isset($info->logo) AND !empty($info->logo))
										<input type="hidden" id="old_image" name="old_image" value="{{ $info->logo }}"/>
										<div class="cat_image_preview"> <img class="rounded avatars" id="avatar2" src="{{ url('uploads/vendor/logo/'.$info->logo) }}" alt="avatar" style="width: 150px;height: 150px"> </div>
									@endif
									<input type="file" class="form-control-file file_preview" id="logo" name="logo" accept="image/*">
								</div>
								<div class="form-group col-md-6">
									<label for="qu" class="col-form-label">Logo ou visuel principal(1920*860)</label>
									@if(isset($info->header_image) AND !empty($info->header_image))
										<div class="cat_image_preview">  <img class="rounded avatars" id="avatar2" src="{{ url('uploads/vendor/header_banner/'.$info->header_image) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
									@endif
									<input type="file" class="form-control-file file_preview" id="header_image" name="header_image" accept="image/*">
								</div>			
							</div>  
						</div>
						
						<div class="form-group">
							<label>Texte logo</label>
							<input type="text" name="logo_text" id="logo_text" class="form-control" placeholder="Texte logo*" value="{{  $info->logo_text }}">
						</div>

						<div class="form-group">
							<label>Couleur du thème</label>
							<input type="color" class="form-control theme_color" id="theme_color" name="theme_color" value="{{ $info->theme_color }}" >
                        </div>
						<div class="form-group">
							<label>Devise monétaire</label>
							<input type="text" class="form-control currency" id="currency" name="currency" value="{{ $info->currency }}" >
                        </div>
						
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block btn-lg">Mise à jour des paramètres</button>
						</div>
					</form>
				</div>
			</div>
        </div>

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCB59pcScbhhVj5SD6-m1is6v0Qq3cGwT4&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#location").keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });


       //-----------------------------------------------------------------

    });

    function initialize() {
       if(!$('#gmap_latitude').val()  ||  !$('#gmap_longitude').val())  {
           currentLocation();
       }
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            var address = place.address_components;
            var address_details = address[address.length - 1];
            document.getElementById('location').value = place.name;
            document.getElementById('gmap_latitude').value = place.geometry.location.lat();
            document.getElementById('gmap_longitude').value = place.geometry.location.lng();
            document.getElementById('country_iso').value = place['short_name'];
           // alert( place['short_name']);
        });
    }

    function currentLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $('#gmap_latitude').val('{{  isset($latitude) ? $latitude : '0' }}');
            $('#gmap_longitude').val('{{ isset($longitude) ? $longitude : '0' }}');
        }
    }

    function showPosition(position) {
        //console.log(position);
        $('#gmap_latitude').val(position.coords.latitude);
        $('#gmap_longitude').val(position.coords.longitude);
        let geocoder = new google.maps.Geocoder();
        let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                var address = results[results.length - 1].address_components[0];
                $('#location').attr("placeholder", results[0].formatted_address);
                $('#location').val(results[0].formatted_address);
                $('#country_iso').val(address['short_name']);
                //alert( address['short_name']);
            } else {
                alert('Geocode was not successful due to following reason: ' + status);
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

</body>
</html>
