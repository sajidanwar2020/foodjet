<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">
			<div class="col-sm-12">
				<div class="card-box">
					<form action="{{ route('doc-proof-update') }}" method="post" enctype="multipart/form-data">
						@if($errors->any())
							@foreach($errors->all() as $error)
								<span style="color: red">{{ @"*".$error }}</span><br>
							@endforeach
						@endif
						{{ csrf_field() }}
						
						<div class="form-group">
							<div class="row">
								<div class="form-group col-md-12">
									<label for="qu" class="col-form-label">ID</label>
									@if(isset($info->_ID) AND !empty($info->_ID))
										<div class="cat_image_preview"> <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->_ID) }}" alt="avatar" style="width: 150px;height: 150px"> </div>
									@endif
									<input type="file" class="form-control-file file_preview" id="_ID" name="_ID" accept="image/*">
								</div>
								
								<div class="form-group col-md-12">
									<label for="qu" class="col-form-label">Proof of Address</label>
									@if(isset($info->proof_of_address) AND !empty($info->proof_of_address))
										<div class="cat_image_preview">  <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->proof_of_address) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
									@endif
									<input type="file" class="form-control-file file_preview" id="proof_of_address" name="proof_of_address" accept="image/*">
								</div>
								
								<div class="form-group col-md-12">
									<label for="qu" class="col-form-label">Driver’s License</label>
									@if(isset($info->driver_License) AND !empty($info->driver_License))
										<div class="cat_image_preview">  <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->driver_License) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
									@endif
									<input type="file" class="form-control-file file_preview" id="driver_License" name="driver_License" accept="image/*">
								</div>	

								
								<div class="form-group col-md-12">
									<label for="qu" class="col-form-label">Articles of Association</label>
									@if(isset($info->articles_of_association) AND !empty($info->articles_of_association))
										<div class="cat_image_preview">  <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->articles_of_association) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
									@endif
									<input type="file" class="form-control-file file_preview" id="articles_of_association" name="articles_of_association" accept="image/*">
								</div>	
								
								<div class="form-group col-md-12">
									<label for="qu" class="col-form-label">EU Passport</label>
									@if(isset($info->eu_passport) AND !empty($info->eu_passport))
										<div class="cat_image_preview">  <img class="rounded avatars" src="{{ url('uploads/vendor/doc_proof/'.$info->eu_passport) }}" alt="avatar" style="width: 150px;height: 150px">  </div>
									@endif
									<input type="file" class="form-control-file file_preview" id="eu_passport" name="eu_passport" accept="image/*">
								</div>	
							</div>  
						</div>
						
						<div class="form-group">
							<button type="submit" class="btn btn-primary btn-block btn-lg">Update</button>
						</div>
						
					</form>
				</div>
			</div>
        </div>

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCB59pcScbhhVj5SD6-m1is6v0Qq3cGwT4&libraries=places" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#location").keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                return false;
            }
        });


       //-----------------------------------------------------------------

    });

    function initialize() {
       if(!$('#gmap_latitude').val()  ||  !$('#gmap_longitude').val())  {
           currentLocation();
       }
        var input = document.getElementById('location');
        var autocomplete = new google.maps.places.Autocomplete(input);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            var address = place.address_components;
            var address_details = address[address.length - 1];
            document.getElementById('location').value = place.name;
            document.getElementById('gmap_latitude').value = place.geometry.location.lat();
            document.getElementById('gmap_longitude').value = place.geometry.location.lng();
            document.getElementById('country_iso').value = place['short_name'];
           // alert( place['short_name']);
        });
    }

    function currentLocation() {

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            $('#gmap_latitude').val('{{  isset($latitude) ? $latitude : '0' }}');
            $('#gmap_longitude').val('{{ isset($longitude) ? $longitude : '0' }}');
        }
    }

    function showPosition(position) {
        //console.log(position);
        $('#gmap_latitude').val(position.coords.latitude);
        $('#gmap_longitude').val(position.coords.longitude);
        let geocoder = new google.maps.Geocoder();
        let latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        geocoder.geocode({'location': latlng}, function (results, status) {
            if (status === 'OK') {
                var address = results[results.length - 1].address_components[0];
                $('#location').attr("placeholder", results[0].formatted_address);
                $('#location').val(results[0].formatted_address);
                $('#country_iso').val(address['short_name']);
                //alert( address['short_name']);
            } else {
                alert('Geocode was not successful due to following reason: ' + status);
            }
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>

</body>
</html>
