
<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                        </ol>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <div class="row">

            <div class="col-xl-3 col-md-6">
                <div class="card-box">
                    <a href="{{ route('my_orders') }}">
                        <h4 class="header-title mt-0 mb-4">Total Orders</h4>
                        <div class="widget-chart-1">
                            <div class="widget-detail-1 text-right">
                                <h2 class="font-weight-normal pt-2 mb-1">{{$total_orders}} </h2>
                            </div>
                        </div>
                    </a>
                </div>

            </div>

            <div class="col-xl-3 col-md-6">
                <div class="card-box">
                    <a href="{{ route('my-products') }}">
                        <h4 class="header-title mt-0 mb-4">Total Products</h4>
                        <div class="widget-chart-1">
                            <div class="widget-detail-1 text-right">
                                <h2 class="font-weight-normal pt-2 mb-1"> {{$total_products}}  </h2>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

        </div>

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

@include('partials.vendor.footer-js')
</body>
</html>





