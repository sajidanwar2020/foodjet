<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Driver</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Update Driver</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('edit_driver_submitted') }}" method="post" id="driver_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                        <input type="hidden" id="driver_id" name="driver_id" value="{{ $driver->user_id }}"/>

                        <div class="form-group">
                            <label>Salutation*</label>
                            <select name="salutation" class="custom-select form-control" aria-invalid="false">
                                <option value="" selected="">Please select ...</option>
                                <option value="MRS" {{ $driver->salutation == 'MRS' ? 'selected' : ''}}>Mrs</option>
                                <option value="MR" {{ $driver->salutation == 'MR' ? 'selected' : ''}}>Mr</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>First name*</label>
                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name*" value="{{ $driver->first_name }}">
                        </div>

                        <div class="form-group">
                            <label>Last name*</label>
                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name*" value="{{ $driver->last_name }}">
                        </div>

                        <div class="form-group">
                            <label>Email Address*</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email*" value="{{ $driver->email }}">
                        </div>

                        <div class="form-group">
                            <label>Company*</label>
                            <input type="text" class="form-control" name="company" required="required" value="{{ $driver->company }}">
                        </div>

                        <div class="form-group">
                            <label>Vehicle no*</label>
                            <input type="text" class="form-control" name="vehicle_no" required="required" value="{{ $driver->vehicle_no }}">
                        </div>


                        <div class="form-group">
                            <label>Location*</label>
                            <input type="text" class="form-control" name="location" id="location" placeholder="Plot 9, F-11 Markaz F 11 Markaz F-11, Islamabad, Islamabad Capital Territory, Pakistan"  autocomplete="off" value="{{ $driver->location }}">
                            <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ $driver->latitude }}">
                            <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ $driver->longitude }}">
                            <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ $driver->country_id }}">
                        </div>



                        <div class="form-group">
                            <label>Date of birth*</label>
                            <input type="text" class="form-control" name="dob" id="dob"  required="required" value="{{ $driver->date_of_birth }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Mobile number*</label>
                            <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ $driver->phone_number }}">
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-lg">
                                Register
                            </button>
                        </div>
                    </form>


                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<script type="text/javascript" src="{{asset('assets/site/js/jquery-3.2.0.min.js')}}"></script>
@include('partials.footer-js')
@include('partials.common_js')
<script type="text/javascript">
    $(document).ready(function () {
        $("#dob").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
</body>
</html>



