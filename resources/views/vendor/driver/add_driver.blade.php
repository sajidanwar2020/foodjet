<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Driver</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Driver</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <form action="{{ route('driver_submitted') }}" method="post" id="driver_signup" enctype="multipart/form-data">

                    @include('partials.flash-message')
                        {{ csrf_field() }}
                        <input type="hidden" name="user_type" value="4">

                        <div class="form-group">
                            <label>Salutation*</label>
                            <select name="salutation" class="custom-select form-control" aria-invalid="false">
                                <option value="" selected="">Please select ...</option>
                                <option value="MRS" {{ old('salutation') == 'MRS' ? 'selected' : ''}}>Mrs</option>
                                <option value="MR" {{ old('salutation')== 'MR' ? 'selected' : ''}}>Mr</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>First name*</label>
                            <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name*" value="{{ old('first_name') }}">
                        </div>

                        <div class="form-group">
                            <label>Last name*</label>
                            <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name*" value="{{ old('last_name') }}">
                        </div>

                        <div class="form-group">
                            <label>Email Address*</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email*" value="{{ old('email') }}">
                        </div>

                        <div class="form-group">
                            <label>Company*</label>
                            <input type="text" class="form-control" name="company" required="required" value="{{ old('company') }}">
                        </div>

                        <div class="form-group">
                            <label>Vehicle no*</label>
                            <input type="text" class="form-control" name="vehicle_no" required="required" value="{{ old('vehicle_no') }}">
                        </div>


                        <div class="form-group">
                            <label>Location*</label>
                            <input type="text" class="form-control" name="location" id="location" placeholder="Plot 9, F-11 Markaz F 11 Markaz F-11, Islamabad, Islamabad Capital Territory, Pakistan" value="{{ old('location') }}" autocomplete="off">
                            <input type="hidden" class="form-control" name="gmap_latitude" id="gmap_latitude" value="{{ old('gmap_latitude') }}">
                            <input type="hidden" class="form-control" name="gmap_longitude" id="gmap_longitude" value="{{ old('gmap_longitude') }}">
                            <input type="hidden" class="form-control" name="country_iso" id="country_iso" value="{{ old('country_iso') }}">
                        </div>



                        <div class="form-group">
                            <label>Date of birth*</label>
                            <input type="text" class="form-control" name="dob" id="datepicker"  required="required" value="{{ old('dob') }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Mobile number*</label>
                            <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Mobile number" value="{{ old('phone_number') }}">
                        </div>


                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-lg">
                                Register
                            </button>
                        </div>
                    </form>

                    <?php /*
                    <form action="{{ route('driver_submitted') }}" method="post" id="driver_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Driver Name</label>
                                <input type="text" name="driver_name" id="driver_name" class="form-control required" placeholder="Driver Name*" >
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Phone#</label>
                                <input type="text" class="form-control required" name="phone" id="phone"  >
                            </div>

                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Email</label>
                                <input type="text" class="form-control required" name="email" id="email"  >
                            </div>

                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Vehicle no</label>
                                <input type="text" class="form-control required" name="vehicle_no" id="vehicle_no"  >
                            </div>


                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Image*</label>
                                @if(isset($driver->photo))
                                    <input type="hidden" id="old_id_0" name="old_image_id" class="old_image_id" value="{{ $driver->photo }}"/>
                                    <img class="rounded avatars" id="avatar2" src="{{ url('uploads/drivers/'.$driver->photo) }}" alt="avatar" style="width: 150px;height: 150px">
                                @endif
                                <input type="file" class="form-control-file" id="driver" name="driver" accept="image/*">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Add Driver</button>
                    </form>*/ ?>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>


<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<div class="rightbar-overlay"></div>
<script type="text/javascript" src="{{asset('assets/site/js/jquery-3.2.0.min.js')}}"></script>
@include('partials.footer-js')
@include('partials.common_js')

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
    });
</script>
</body>
</html>



