
 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12" style="display: none">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Drivers</li>
                        </ol>
                    </div>
                    <h4 class="page-title">My Driver</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <a href="{{ route('add_driver') }}" class="btn btn-sm btn-primary float-right">Add Driver</a>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Driver name</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Vehicle no</th>
                            <th>Photo</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($drivers) > 0)
                            @foreach($drivers as $driver)
                                <tr>
                                    <td>{{$driver->first_name}} {{$driver->last_name}}</td>
                                    <td>{{$driver->phone_number}}</td>
                                    <td>{{$driver->email}}</td>
                                    <td>{{$driver->vehicle_no}}</td>
                                    <td>{{$driver->photo}}</td>
                                    <td>{{($driver->status)}}</td>
                                    <td>

                                        <a href="{{ route('edit_driver',['driver_id'=>$driver->user_id]) }}" class="btn btn-sm btn-info btn-rounded" data-toggle="tooltip" data-original-title="Edit Driver">
                                            Edit
                                        </a>
                                        |
                                        <a href="{{ route('change_driver_status',['driver_id'=>base64_encode($driver->user_id.":".$driver->created_by.":2")]) }}" class="btn btn-sm btn-danger btn-rounded product_status" data-toggle="tooltip"
                                           data-original-title="Delete product" type="warning" msg="Delete this driver">
                                            Delete
                                        </a>
                                        |
                                        <a href="{{ route('change_driver_status',['driver_id'=>base64_encode($driver->user_id.":".$driver->created_by.":1")]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                           data-original-title="Publish Offer" type="warning" msg="Activate this driver">
                                            Activate
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Not found any product</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    <div style="float: right">
                        <div class="card-footer">
                            {{ $drivers->links('vendor.pagination.custom') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



