 <!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>

<!-- CSS only -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css2?family=Rubik&display=swap" rel="stylesheet">
	
	<style>
		
		body{
			padding:0;
			margin: 0;
			font-family: 'Rubik', sans-serif !important;
		}
		.container{
			height:100vh;
		}
		.tab-bg{
/*
			background: url("tag-print.png") no-repeat;
			width: 100%;
			height: 678px;
*/
			margin: 0px 0;
			position: relative;
		}
		.tab-bg h1{
			font-size:48px;
			text-align: center;
			position: absolute;
			top: 40px;
			width: 100%;
		}
		.tab-bg h2{
			font-size:48px;
			text-align: center;
			position: absolute;
			bottom: 30px;
			width: 100%;
		}
		.tab-bg .qr-code{
			position: absolute;
			top: 50%;
			left: 50%;
			width: 205px;
			height: 205px;
			margin: -100px 0 0 -88px;
		}
	
	</style>
	
</head>

<body>
	
	<div class="container d-flex justify-content-center align-items-center">
			<div class="tab-bg">
				<img src="{{asset('assets/restraurant-site/img/tag-print.png')}}" width="100%" alt=""/>
				<h1>Read the code to access our Menu</h1>
				<h2>Lire le code pour accéder au Menu</h2>
				
				<div class="qr-code">
					<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chco=3f48cc&choe=UTF-8&chld=M|0&chl=<?php echo urlencode(route('home-sub', ['id' =>  Session::get('is_vendor')['restaurant_code'],'table_id' => $id])); ?>"  width="100%" height="100%" alt=""/>
				</div><!--QR code-->
			</div><!--.tagbg-->  
	</div><!--.container-->

<script type="text/javascript">
        function CallPrint() {
            
           window.print();
        }
		CallPrint();
</script>	
	
</body>
</html>
