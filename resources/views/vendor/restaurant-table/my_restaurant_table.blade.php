<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">Gestion des tables</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Gestion des tables</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
					<a href="{{ route('add-restaurant-table') }}" class="btn btn-sm btn-primary">Ajouter une table</a>
					
					@if (count($restaurantTable) > 0)
						<a href="{{ route('export-restaurant-table') }}" class="btn btn-sm btn-primary float-right">Exportation</a> 
                    @endif
					
                    <div class=" table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Numéro de table</th>
							<th>Référence de tag</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($restaurantTable) > 0)
                            @foreach($restaurantTable as $restaurantTable1)
                                <tr>
                                    <td>{{$restaurantTable1->table_number}}</td>
									<td>{{$restaurantTable1->tag_reference}}</td>
                                    <td>

                                        <a href="{{ route('edit-restaurant-Table',['id'=>base64_encode($restaurantTable1->id.":".$restaurantTable1->id)]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Modifier
                                        </a>
                                        |
										<a href="{{ route('print-restaurant-table',['id'=>base64_encode($restaurantTable1->tag_reference.":".$restaurantTable1->tag_reference)]) }}" target="_blank" class="btn btn-sm btn-info btn-rounded">
                                            Imprimer 
                                        </a>
                                        |
                                        @if($restaurantTable1->status == 0)
											<a href="{{ route('change_table_status',['id'=>base64_encode($restaurantTable1->id.":".$restaurantTable1->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
												Activer
											</a>
                                        @else
                                            <a href="{{ route('change_table_status',['id'=>base64_encode($restaurantTable1->id.":".$restaurantTable1->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                Désactiver
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Aucun tableau ajouté</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                    <div>
                        {{ $restaurantTable->links('vendor.pagination.custom') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



