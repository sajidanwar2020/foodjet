<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                 <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">Identifiant de table</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Identifiant de table</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('table-submitted') }}" method="post" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <div class="form-row">
							
							<div class="form-group col-md-8">
                                <label for="pn" class="col-form-label">Référence de tag</label>
                                <input type="text" name="tag_reference" id="tag_reference" class="form-control required" value="{{ old('tag_reference') }}">
                            </div>
							
							<div class="form-group col-md-8">
                                <label for="pn" class="col-form-label">Numéro de table</label>
                                <input type="text" name="table_number" id="table_number" class="form-control required" value="{{ old('table_number') }}">
                            </div>
							
						</div>	
                        <button type="submit" class="btn btn-primary">Ajouter un identifiant de table</button>
                    </form>
					
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>
