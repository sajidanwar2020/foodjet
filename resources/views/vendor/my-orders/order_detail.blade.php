<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <?php
                    $url = route('my_orders');
                    if(Session::has('is_seller') && Session::get('is_seller')['role_id'] =='4') {
                       $url = route('driver_orders');
                    }
                    ?>
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{$url}}">Orders</a>
                            <li class="breadcrumb-item active">Order Detail</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Order Detail</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    @if (count($order_detail) > 0)
                    <div class="order_detail_wrapper">
                        <div class="row">
                            <div class="col-md-4">
                                Address : {{$order_detail->order_detail['address']}}
                            </div>
                            <div class="col-md-4">
                                Phone : {{$order_detail->order_detail['phone_number']}}
                            </div>
                        </div>
                    </div>

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Order#</th>
                            <th>Customer name</th>
                            <th>Product name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                            @foreach($order_detail as $order_det)
                                <tr>
                                    <td>{{$order_det->o_id}}</td>
                                    <td>{{$order_det->first_name}} {{$order_det->last_name}}</td>
                                    <td> {{$order_det->product_name}}</td>
                                    <td> {{$order_det->quantity}}</td>
                                    <td> {{$order_det->price}}</td>
                                    <td>{{getOrderItemStatus($order_det->item_status)}}</td>
                                    <td>
                                        @if($order_det->item_status == 1)
                                            <a href="{{ route('change_order_status',['item_id'=>$order_det->items_id,'order_id'=>$order_det->o_id,'status'=>2]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                               data-original-title="Process" type="warning" msg="Are you sure order to Process?">
                                                Process
                                            </a>
                                        @endif

                                        @if($order_det->item_status == 1)
                                            <a href="{{ route('change_order_status',['item_id'=>$order_det->items_id,'order_id'=>$order_det->o_id,'status'=>3]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                               data-original-title="Reject" type="warning" msg="Are you sure order to Reject?">
                                                Reject
                                            </a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    @else
                        <tr>
                            <td colspan="8">
                                <p>Not found any Orders</p>
                            </td>
                        </tr>
                    @endif
                </div>

            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>
