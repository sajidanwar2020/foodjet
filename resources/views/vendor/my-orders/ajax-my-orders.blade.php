@php
	$currency=isset($restaurant->currency)?$restaurant->currency:'CHF';
@endphp
@foreach($my_orders as $order)
	<tr class="new_order" style="background-color: #f2f1f1;">
	  <td>{{ $order->id }}</td>
	  <td>{{ $order->table_id }}</td>
	  <td>
		  @if($order->user_id != NULL)
			{{ $order->userDetails->first_name." ".$order->userDetails->last_name }}
		  @else
			{{ $order->phone_number }}
		  @endif
	  </td>
	  <td>{{$currency}} {{ $order->total_price }}</td>
	  

	  @php 
	  $statusClass =  'pending';
	  $statusVal =  'en attente';

	  if($order->status == 1) {
		$statusClass =  'pending';
		$statusVal =  'en attente';
	  }
	  if($order->status == 2) {
		$statusClass = 'inprocess';
		$statusVal =  'dans le traitement';
	  }
	  if($order->status == 3) {
		$statusClass =  'delivered';
		$statusVal =  'Livré';
	  }
	  
	  if($order->status == 4) {
		$statusClass =  'delivered';
		$statusVal =  'Livré';
	  }
	  
	  if($order->status == 5) {
		$statusClass =  'cancel';
		$statusVal =  'Annuler';
	  }
		
	  @endphp
	  <td class="status-col"><span class="{{ $statusClass }}">{{ $statusVal }}</span></td>
	  <td>
		
		   <?php /* <a href="{{ route('inprocess',['order_id'=>$order->id]) }}" class="status-col"><span class="inprocess">Processing</span></a> |
			<a href="{{ route('cancelorder',['order_id'=>$order->id]) }} }}" class="status-col"><span class="cancel">Cancel</span></a> */ ?>
		
		<?php /*
		@if($order->status == 2)
			<a href="{{ route('deliveredorder',['order_id'=>$order->id]) }}" class="status-col"><span class="delivered">Ready To Delivered</span></a>
		@endif
		@if($order->status == 3)
			<a href="{{ route('completedorder',['order_id'=>$order->id]) }}" class="status-col"><span class="delivered">Completed</span></a>
		@endif */ ?>
		  <a href="" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="modal" data-target="#infoWindow{{ $order->id }}" type="info" >
			  voir l'ordre
		  </a>
		 @if($order->status != 5 AND $order->status != 4)	
			<select name="__order-status" class="__order-status">
				   <option value="">Statut de la commande</option>
				   <option value="{{ route('inprocess',['order_id'=>$order->id]) }}" {{ $order->status == 2 ? 'selected' : ''}}>En traitement</option>
				   <option value="{{ route('deliveredorder',['order_id'=>$order->id]) }}" {{ $order->status == 3 ? 'selected' : ''}}>Prêt à livrer</option>
				   <option value="{{ route('completedorder',['order_id'=>$order->id]) }}" {{ $order->status == 4 ? 'selected' : ''}}>Terminé</option>
				   <option value="{{ route('cancelorder',['order_id'=>$order->id]) }} }}" {{ $order->status == 5 ? 'selected' : ''}}>Annuler</option>
			</select>
		 @endif
		<!-- Modal -->
		<div class="modal fade" id="infoWindow{{ $order->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">détails de la commande</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row pos-inline">
							<div class="col-md-6 ">
								<h4>Numéro de commande:</h4> <p>{{ $order->id }}</p>

							</div>

							<div class="col-md-6 mb-2">
								<h4>Numéro de table:</h4> <p>{{ $order->table_id }}</p>
							</div>
							<div class="col-md-12">
								<!-- Shopping cart table -->
								<div class="table-responsive">
									<table class="table">
										<thead>
										<tr>
											<th scope="col" class="border-0 bg-light">
												<div class="p-2 px-3 text-uppercase">PRODUIT</div>
											</th>
											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">PRIX</div>
											</th>
											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">QUANTITÉ</div>
											</th>
											<th scope="col" class="border-0 bg-light">
												<div class="py-2 text-uppercase">LES DÉTAILS DU PRODUIT</div>
											</th>
										</tr>
										</thead>
										<tbody>
											@foreach($order->orderDetail as $single)
											<tr>
											<th scope="row" >
												<div class="">
													<img src="{{ $single->image_url }}" alt="" width="70" class="img-fluid rounded shadow-sm">
													<div class="ml-1 d-inline-block ">
														<h5 class="mb-0 mt-0"> <a href="#" class="text-dark d-inline-block">{{ $single->product_name }}</a></h5>
													@if(isset($single->category))
														<span class="text-muted font-weight-normal font-italic d-block">Catégorie: {{ $single->category }}</span>
													@endif
													</div>

												</div>
											</th>

											<td ><strong>${{$single->price}}</strong></td>
											<td ><strong>{{ $single->quantity }}</strong></td>
												<td  style="width: 350px;"><span class="text-muted font-weight-normal font-italic ">
												@if(isset($single->composeProductItem->name)) 
													{{ $single->composeProductItem->name }}
												@else
													{{ '' }}
												@endif
												  @if(isset($single->nameOfComposeProdItems)) 
													{{ $single->nameOfComposeProdItems }}
												  @else
													{{ '' }}
												  @endif
												  </span>

												  <!-- @if(isset($single->nameOfComposeProd))
												  <span class="text-muted font-weight-normal font-italic">
												  {{ $single->nameOfComposeProd }}</span>
												  @endif -->
												  
												  @if(isset($single->compose_quantites_id))
												  <!-- <span class="text-muted font-weight-normal font-italic "> 
												  {{ $single->compose_quantites_id->quantity }}</span> -->
												  @endif
												  @if(isset($single->subItemsListName))
												  <span class="text-muted font-weight-normal font-italic">
												  {{ $single->subItemsListName }}</span>
												  @endif

												  @if(isset($single->special_notes))<br>
												 <span class="text-muted font-weight-normal font-italic d-block"><h5 style="display: inline;">Note spéciale: </h5>{{ $single->special_notes }}</span>
												  @endif
												</td>
										</tr>
										@endforeach

										</tbody>
									</table>
								</div>
							</div>
							@if($order->additional_notes)
								<div class="col-md-12">
									<h4>Notes complémentaires:</h4> <p>{{ $order->additional_notes }}</p>
								</div>
							@endif
							
						</div>
					</div>
				</div>
			</div>
		</div>

	  </td>
	</tr>
@endforeach
                           
