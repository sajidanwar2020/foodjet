<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
    <style>
        td.paid {color: green;font-weight: bold; }
        td.not_paid {color: red;font-weight: bold; }
    </style>
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right" style="display: none">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Orders</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Order#</th>
                                <th>Payment Method</th>
                                <th>Is paid</th>
                                <th>Customer name</th>
                                <th>Address</th>
                                <th>Phone#</th>
                                <th>Order Total</th>
                                <th>Additional notes</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($my_orders) > 0)
                                @foreach($my_orders as $my_order)
                                    <tr>
                                        <td>{{$my_order->o_id}}</td>
                                        <td>{{getPaymentMethod($my_order->payment_type)}}</td>
                                        <td class="{{($my_order->online_transaction['status'] == 1 ? 'paid': 'not_paid')}}">
                                            @if($my_order->payment_type == 1)
                                                -
                                                @else
                                                {{($my_order->online_transaction['status'] == 1 ? 'Yes': 'No')}}
                                            @endif
                                        </td>
                                        <td>{{$my_order->first_name}} {{$my_order->last_name}}</td>
                                        <td>{{$my_order->o_address}}</td>
                                        <td>{{$my_order->phone_number}}</td>
                                        <td>
                                            @if($my_order->o_status == 5)
                                                ---
                                            @else
                                                €{{ ($my_order->total_price + $my_order->distance_charges ) - $my_order->cancelled_amount }}
                                            @endif
                                        </td>
                                        <td>{{$my_order->additional_notes}}</td>
                                        <td>{{getOrderStatus($my_order->o_status)}} </td>

                                        <td>
                                            <?php
                                                $status_arr = array(4, 5);
                                            ?>
                                            @if($my_order->o_status == 3 AND !in_array($my_order->o_status, $status_arr))
                                                <a href="{{ route('change_main_order_status',['order_id'=>$my_order->o_id,'status'=>4]) }}" class="btn btn-success btn-rounded"
                                                   type="warning" msg="Are you sure order to complete?">
                                                    Complete
                                                </a>
                                            @endif

                                            @if($my_order->is_order_processed == 0 AND $my_order->o_status != 3 AND !in_array($my_order->o_status, $status_arr))
                                                <a href="{{ route('change_main_order_status',['order_id'=>$my_order->o_id,'status'=>3]) }}" class="btn btn-secondary btn-sm btn-rounded product_status"
                                                    type="warning" msg="Are you sure order to complete?" style="padding-left:4px;padding-right: 4px; ">
                                                    Ready for Delivery
                                                </a>

                                            @endif

                                            <?php
                                                $info = Session::get('is_seller');
                                                if($info['role_id'] == 3)
                                                    $redirect =  'vendor_order_detail';
                                                else if($info['role_id'] == 4)
                                                    $redirect =  'driver_order_detail';
                                            ?>

                                            <a href="{{ route($redirect,['order_id'=>$my_order->o_id]) }}" class="btn btn-sm btn-info btn-rounded" >
                                                Detail
                                            </a>

                                                @if(!in_array($my_order->o_status, $status_arr))
                                                    <a href="{{ route('change_main_order_status',['order_id'=>$my_order->o_id,'status'=>5]) }}" class="btn btn-danger btn-rounded"
                                                       type="warning" msg="Are you sure order to Cancelled?">
                                                        Cancel
                                                    </a>
                                                @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">
                                        <p>Not found any Orders</p>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div style="float: right">
                            <div class="card-footer">
                                {{ $my_orders->links('vendor.pagination.custom') }}
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>



<!-- Vendor js -->
<!-- Footer Start -->

<!-- end Footer -->

@include('partials.vendor.footer-js')

</body>
</html>
