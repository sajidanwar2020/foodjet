<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
    <style>
        td.paid {color: green;font-weight: bold; }
        td.not_paid {color: red;font-weight: bold; }
    </style>
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->
	@php
		$currency=isset($restaurant->currency)?$restaurant->currency:'CHF';
	@endphp
<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right" style="display: none">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Categories</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Categories</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class=" table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Sort Order #</th>
                                <th>Category Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $cat)
                                @if($cat->sort_order != 0)
                                <tr class="accordion-toggle collapsed draggable-y" id="accordion1" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne{{ $cat->id }}">
                                  <td>{{ $cat->sort_order }}</td>
                                  <td>{{ $cat->name }}</td>
                                  <td>
                                      <a href="" class="status-col"><span class="inprocess">Processing</span></a> |

                                      <a href="" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="modal" data-target="#infoWindow{{ $cat->id }}" type="info" >
                                          voir l'ordre
                                      </a>

									<!-- Modal -->
									<div class="modal fade" id="infoWindow{{ $cat->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLabel">détails de la commande</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">
													<div class="row pos-inline">
														<div class="col-md-6 ">
															<h4>Numéro de commande:</h4> <p></p>

														</div>

														<div class="col-md-6 mb-2">
															<h4>Numéro de table:</h4> <p>8</p>
														</div>
														<div class="col-md-12">
															<!-- Shopping cart table -->
															<div class="table-responsive">
                                <form action="{{ route('sort-categories-update') }}" method="post">
                                  {{ csrf_field() }}
                                  <input type="text" name="vendor_scategory_id" value="{{ $cat->id }}">
                                  <input type="text" name="sort_order_num" >
                                  <button type="submit">Update</button>
                                </form>
															</div>
														</div>
														
													</div>
												</div>
											</div>
										</div>
									</div>

                                  </td>
                                </tr>
            @if(isset($cat->child_categories))
              @foreach($cat->child_categories as $singleCate)
                <tr class="collapse childcategories draggable-y" id="collapseOne{{ $singleCate->id }}">
                  <td>{{ $singleCate->child_sort_order }}</td>
                  <td>{{ $singleCate->name }}</td>
                  <td> 
                    <a href="" class="status-col"><span class="inprocess">Processing</span></a> |
                    <a href="" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="modal" data-target="#infoWindow{{ $singleCate->id }}" type="info" >voir l'ordre
                   </a>
                  </td>
                </tr>
              @endforeach
              @endif
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>


<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>



<!-- Vendor js -->
<!-- Footer Start -->

<!-- end Footer -->

@include('partials.vendor.footer-js')

</body>
</html>
