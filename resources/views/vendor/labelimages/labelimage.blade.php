<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Tableau de bord</a>
                            <li class="breadcrumb-item active">icône</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Ajouter une icône</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <a href="" class="btn btn-sm btn-primary float-right octf-btn mb-3"  data-toggle="modal" data-target="#exampleModal">Ajouter des images d'étiquette</a>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Add Label Image</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('add-label-images') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="col-sm-6 form-group">
                                    <label>Label Name</label>
                                    <input type="text" class="form-control" name="labelname" required>
                                </div>
                                <div class="col-sm-6 form-group">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> 

            @if($errors->any())
                <div class="text-danger">
                    {{ $errors->first() }}
                </div>
            @endif


                    <div class=" table-responsive">
                    <table class="table table-striped mb-0">
                        <thead>
                        <tr>
                            <th>Nom de l'étiquette</th>
                            <th>Image</th>
                            <th>Statut</th>
                            <th>action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($images) > 0)
                            @foreach($images as $image)
                                <tr>
                                    <td>{{$image->label_name}}</td>
                                    <td><img src="uploads/labelimages/{{$image->image_path}}" class="img-fluid" width="10%" height="10%"></td>
                                    <td></td>
                                    <td>
                                        <a class="btn octf-btn btn-set" data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fas fa-edit" aria-hidden="true"></i>
                                        </a>

                                        <!-- Modal -->
										<div class="modal fade" id="exampleModal_{{ $image->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h4>Edit Label Image</h4>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<form action="{{ route('edit-label-images') }}" method="post" enctype="multipart/form-data">
															{{ csrf_field() }}
															<input type="text" name="labelid" value="{{ $image->id }}" hidden>
															<div class="col-sm-6 form-group">
																<label>Label Name</label>
																<input type="text" class="form-control" name="labelname" value="{{ $image->label_name }}" required>
															</div>
															<div class="col-sm-6 form-group">
																<label>Image</label>
																<input type="file" class="form-control" name="image" value="{{ $image->label_name }}">
															</div>
															<div class="form-group">
																<button type="submit" class="btn btn-primary">Edit</button>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
                                        |
                                        <a href="{{ route('delete-label-images',['labelid'=>$image->id]) }}" class="btn octf1-btn btn-set" data-toggle="tooltip" data-original-title="Delete">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Aucun produit trouvé</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                        <div >
                            {{-- $images->links('vendor.pagination.custom') --}}
                        </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



