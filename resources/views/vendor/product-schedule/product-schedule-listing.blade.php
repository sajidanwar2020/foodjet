<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
				<a href="{{ route('add-product-schedule') }}" class="btn btn-sm btn-primary">Ajouter un horaire</a>
                    <div class=" table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Nom</th>
							<th>Heure de début</th>
                            <th>Heure de fin</th>
							<th>Des produits</th>
							<th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
					
                        @if (count($shedule_lists) > 0)
							 @foreach($shedule_lists as $shedule_list)
							<?php 
							//echo "<pre>";
							//print_r($shedule_list->products);exit;
							?>
								<tr>
								<td>{{$shedule_list->name}}</td>
								<td>{{$shedule_list->start_time}}</td>
								<td>{{$shedule_list->end_time}}</td>
								<td>
								@if (count($shedule_list->products) > 0)
									@foreach($shedule_list->products as $product)
										{{$product['product_name'].' ,'}}
									@endforeach
								@endif 
								</td>
								<td>
									<a href="{{ route('edit-schedule',['id'=>base64_encode($shedule_list->id.":".$shedule_list->id)]) }}" class="btn btn-sm btn-info btn-rounded">
										Éditer
									</a>
									|
									
									<a href="{{ route('change-schedule-status',['id'=>base64_encode($shedule_list->id.":".$shedule_list->id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded">
										Supprimer
									</a>
									|
									@if($shedule_list->status == 0)
                                        <a href="{{ route('change-schedule-status',['id'=>base64_encode($shedule_list->id.":".$shedule_list->id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Activer
                                        </a>
                                    @else
                                        <a href="{{ route('change-schedule-status',['id'=>base64_encode($shedule_list->id.":".$shedule_list->id.":0")]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Désactiver
                                        </a>
                                    @endif
								</td>
							 </tr>	
							 @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Aucun produit trouvé</p>
                                </td>
                            </tr>
                        @endif 
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

	<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	  aria-hidden="true">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header text-center">
			<h4 class="modal-title w-100 font-weight-bold">Product Shedule </h4>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body mx-3">
		    <form method="POST" id="product_shedule" action="">
			  <div class="form-row">
				<div class="form-group col-md-6">
				  <label for="from_time">From Time</label>
				  <input type="text" class="form-control" id="start_time" name="start_time">
				</div>
				<div class="form-group col-md-6">
				  <label for="to_time">To Time</label>
				  <input type="text" class="form-control" id="end_time" name="end_time">
				</div>
			  </div>
			   <button type="submit" class="btn btn-primary">Save</button>
			</form>

      </div>
     
    </div>
  </div>
</div>


<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



