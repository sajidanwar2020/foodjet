<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"></a>
                            <li class="breadcrumb-item"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('edit-schedule-submitted') }}" method="post">
                  
                        {{ csrf_field() }}
						 <input type="hidden" name="id" id="id" class="form-control" value="{{ $productSchedule->id }}">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Heure de début</label>
                                <input type="time" name="start_time" id="start_time" class="form-control" value="{{ $productSchedule->start_time }}">
                            </div>
                        </div>
						
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Heure de fin</label>
                                <input type="time" name="end_time" id="end_time" class="form-control" value="{{ $productSchedule->end_time }}">
                            </div>
                        </div>
						
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Nom</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $productSchedule->name }}">
                            </div>
                        </div>
						<input type="hidden" name="product_ids" id="product_ids" class="form-control" value="{{ $productSchedule->product_ids }}">
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Produit</label>
								<select class="chzn-select" multiple="true" id="product" name="product" class="form-control">
									@if($products)
                                        @foreach($products as $product)
											<?php 
											$selected = '';
											if(in_array($product->id, $product_ids))
												$selected = 'selected';
											?>
											<option value="{{$product->id}}" {{$selected}}>{{$product->product_name}} {{$product->id}}</option>
									@endforeach
                                    @else
                                        <option value="">No option</option>
                                    @endif	
								</select>
                            </div>
                        </div>
						
                        <button type="submit" class="btn btn-primary">Modifier le calendrier</button> 
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.jquery.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.4.2/chosen.css">
<script type="text/javascript">
$(function(){
   // $(".chzn-select").chosen();
	$(".chzn-select").chosen().change(function(e, params){
		values = $(".chzn-select").chosen().val();
		$('#product_ids').val(values+',');
	});
	
	
});
</script>
</body>
</html>
