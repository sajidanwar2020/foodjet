 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('edit-compose-product-submit') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                       
					    <input type="hidden" name="is_edit" id="is_edit" value="1"/>
                        <input type="hidden" id="product_id" name="product_id" value="{{ $product->id }}"/>
                        
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label class="control-label">Sélectionnez la catégorie</label>
								<select class="form-control" id="category" name="category">
									<option value="">Sélectionnez la catégorie</option>
									@foreach($categories as $category)
										<option value="{{ $category->id }}" @if($category->id == $product->category_id) {{'selected'}} @endif>{{ $category->name }}</option>
									@endforeach
								</select>
                            </div>
                        </div>
						
					    <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Nom du produit</label>
                                <input type="text" name="_name" id="_name" class="form-control" placeholder="Product Name*" value="{{ $product->name }}">
                            </div>
                        </div>
						
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Description</label>
                                <textarea id="description" name="description" rows="4" cols="50" class="form-control required" >{{ $product->description }}</textarea>
                            </div>
                        </div>
						
						<div class="form-row">
							<label for="qu" class="col-form-label">Image*</label>
							<input type="file" class="form-control-file compose_product_img" id="compose_product_img" name="compose_product_img" accept="image/*">
							@if(isset($product->image))
								<input type="hidden" id="old_id_0" name="old_image_id" class="old_image_id" value="{{ $product->image }}"/>
								<div class="image_preview"><img class="rounded avatars " id="avatar2" src="{{ url('uploads/products/'.$product->image) }}" alt="avatar" style="width: 250px;height: 150px"></div>
							@endif
						</div>
						
						<div class="form-row">
							<div class="form-group col-md-4">
									<label for="qu" class="col-form-label">Type</label>
									<select name="type" id="type" class="form-control">
										<option value="">Please select type</option>
										<option value="1" {{ $product->type == 1 ? 'selected' : ''}}>Simple</option>
										<option value="2" {{ $product->type == 2 ? 'selected' : ''}}>Mixture</option>
									</select>
							</div>
						</div>
						
                        <br>
                        <div class="form-group ">
                            <label for="qu" class="">Label Images*</label>
                             <div class="row">
                                @php 
                                    $labelids = explode(',',$product->labelimages_ids);
                                @endphp
                                    
                                @foreach($labelimages as $image)
                                    <div class="form-group col-md-3">
                                        
                                        @if(in_array($image->id,$labelids))
                                        <input type="checkbox" class="" id="{{ $image->id }}" value="{{ $image->id }}" name="labelimages[]" width="30px" height="30px" checked>
                                        @else 
                                        <input type="checkbox" class="" id="{{ $image->id }}" value="{{ $image->id }}" name="labelimages[]" width="30px" height="30px" >
                                        @endif
                                        
                                        
                                        <img src="uploads/labelimages/{{$image->image_path}}" alt="" width="30px" height="30px">
                                        <label for="" class="">{{ $image->label_name }}</label> 
                                    </div>
                                @endforeach
                            </div>  
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Mettre à jour le produit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>



