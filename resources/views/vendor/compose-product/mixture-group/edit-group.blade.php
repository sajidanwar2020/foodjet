 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <form action="{{ route('edit-compose-item-group-submit') }}" method="post">
						<input type="hidden" name="group_id" value="{{ $composeGroup->id }}">
						<input type="hidden" name="item_id" value="{{ $composeGroup->cpiid }}">
                        @include('partials.flash-message')
                        {{ csrf_field() }}	
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">TITLE</label>
                                <input type="text" name="title" id="title" class="form-control required" value="{{ $composeGroup->title }}">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Ajouter un produit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>



