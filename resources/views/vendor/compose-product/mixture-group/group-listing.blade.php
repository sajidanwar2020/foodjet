<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <a href="{{ route('add-compose-item-group',['item-id' =>$_GET['item-id'] ]) }}" class="btn btn-sm btn-primary float-right">Add compose item group</a>
                    <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>title</th>
							<th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
						
                        @if (count($composeGroups) > 0)
							@foreach($composeGroups as $composeGroup)
                                <tr>
                                    <td>{{$composeGroup->title}}</td>
                                    <td>{{getStatus($composeGroup->status)}}</td>
                                    <td>

                                        <a href="{{ route('edit-compose-item-group',['group-id'=>base64_encode($composeGroup->id.":".$composeGroup->cpiid)]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Éditer
                                        </a>
										<?php /*
                                        |
                                        <a href="{{ route('change-compose-product-status',['product_id'=>base64_encode($composeGroup->id.":".$composeGroup->restaurant_id.":2")]) }}" class="btn btn-sm btn-danger btn-rounded">
                                            Supprimer
                                        </a>
                                        |
                                        @if($composeGroup->status == 0)
                                        <a href="{{ route('change-compose-product-status',['product_id'=>base64_encode($composeGroup->id.":".$composeGroup->restaurant_id.":1")]) }}" class="btn btn-sm btn-info btn-rounded">
                                            Activer
                                        </a>
                                            @else
                                            <a href="{{ route('change-compose-product-status',['product_id'=>base64_encode($composeGroup->id.":".$composeGroup->restaurant_id.":0")]) }}" class="btn btn-sm btn-info btn-rounded">
                                                Désactiver
                                            </a>
                                            @endif */ ?>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Aucun produit trouvé</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                    </div>
                    <div>
                        {{ $composeGroups->links('vendor.pagination.custom') }}
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')

</body>
</html>



