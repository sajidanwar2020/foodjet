<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Composer des produits</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Ajouter un produit Compose</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('add-compose-product-submit') }}" method="post" id="form_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}	
                        
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Nom du produit</label>
                                <input type="text" name="_name" id="_name" class="form-control required" placeholder="Nom du produit*" value="{{ old('_name') }}">
                            </div>
                        </div>
						
						<div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">La description</label>
                                <textarea id="description" name="description" rows="4" cols="50" class="form-control required" >{{ old('description') }}</textarea>
                            </div>
                        </div>
						
						<div class="form-row">
                            <div class="form-group col-md-6">
                                 <label for="qu" class="col-form-label">Image*</label>
                                <input type="file" class="form-control-file compose_product_img" id="compose_product_img" name="compose_product_img">
                                <div class="image_preview"></div>
                            </div>
                        </div>
						
						<div class="form-row">
							<div class="form-group col-md-4">
									<label for="qu" class="col-form-label">Type</label>
									<select name="type" id="type" class="form-control">
										<option value="">Veuillez sélectionner le type</option>
										<option value="1">Facile</option>
										<option value="2">Mélange</option>
									</select>
							</div>
						</div>

                        <div class="form-group ">
                            <label for="qu" class="">Images d'étiquettes*</label>
                            
                             <div class="row">
                            @foreach($labelimages as $image)
                                <div class="form-group col-md-3">
                                    
                                    <input type="checkbox" class="" id="{{ $image->id }}" value="{{ $image->id }}" name="labelimages[]" width="30px" height="30px">
                                    
                                    <img src="uploads/labelimages/{{$image->image_path}}" alt="" width="30px" height="30px">
                                    <label for="" class="">{{ $image->label_name }}</label> 
                                </div>
                            <?php /*
                            <label class="custom-control-label" for="{{ $image->id }}">
                            <div class="row">
                                <div class="col-xs-6 ml-6 mr-6">
                                    <img src="uploads/labelimages/{{$image->image_path}}" alt="">
                                </div>
                            </div>
                            </label> */ ?>
                            @endforeach
                        </div>  
                        </div>

                        <button type="submit" class="btn btn-primary">Ajouter un produit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>
