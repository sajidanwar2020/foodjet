<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"</a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <div class="table-responsive">
					
						@if (count($composeProductItems) > 0)
							@foreach($composeProductItems as $composeProductItem)
							<?php 
								$ingredients = json_decode($composeProductItem->ingredients,true);
								$ingredients_arr = $ingredients[$composeProductItem->id];
							?>
								<div class="col-md-12 item_wrapper item_wrapper_{{$composeProductItem->id}}"  data-id="{{$composeProductItem->id}}">
									<p>{{$composeProductItem->name}} <a href="{{ route('compose-item-group-listing',['item-id' =>$composeProductItem->id ]) }}" class="btn btn-sm btn-primary" target="_blank">Manage group</a> | <a href="{{ route('compose-sub-item-listing' ,['item-id' =>$composeProductItem->id]) }}" class="btn btn-sm btn-primary" target="_blank">Manage item</a></p>
									@if (count($composeQuantites) > 0)
											@foreach($composeQuantites as $composeQuantite)
											<div class="quantity_wrapper col-md-6" data-qid="{{$composeQuantite->id}}">
												<div class="form-group col-md-4">
													<label for="{{$composeQuantite->id}}">{{$composeQuantite->quantity}} {{$composeQuantite->price}}</label>
												</div>
												<div class="form-group col-md-4">
													<input type="text" name="compose_quantity" class="form-control compose_quantity" value="{{isset($ingredients_arr[$composeQuantite->id]) ? $ingredients_arr[$composeQuantite->id] : ''}}">
												</div>
											</div>
											@endforeach
									@else
										<p>Please create quantity</p>
									@endif
								</div>
							@endforeach
						@else
							<p>Please create items</p>
						@endif
							
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>
@include('partials.vendor.footer-js')
<script>
$(document).ready(function() {
	
	$(document).on("change", ".compose_quantity", function () {
		var id  = $(this).closest('.item_wrapper').attr('data-id');
		var qid  = $(this).closest('.quantity_wrapper').attr('data-qid');
		var item_arr = [];
		
		$('.item_wrapper_'+id+' .quantity_wrapper input.compose_quantity').each(function () {
			obj = {};
			obj['quantity_val'] = $(this).val(); 
			obj['quantity_id'] = $(this).closest('.quantity_wrapper').data('qid');
			item_arr.push(obj);	
		}); 
		
		$.ajax('{{ route('update-compose-items-quantites') }}', {
			method: 'get',
			data: {'_token': '{{ csrf_token() }}',item_arr:item_arr,id,id},
			success: function (data) {
				
			}
		});
    });
});
</script>
</body>
</html>



