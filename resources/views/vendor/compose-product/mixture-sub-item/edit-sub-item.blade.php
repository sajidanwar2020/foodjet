 <!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);"></a>
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                    <h4 class="page-title"></h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('edit-compose-sub-item-submit') }}" method="post" enctype="multipart/form-data">
                     
                        {{ csrf_field() }}
                       
					    <input type="hidden" name="item_id" id="item_id" value="{{ $composeProductSubItems->cpiid }}"/>
                        <input type="hidden" id="sub_item_id" name="sub_item_id" value="{{ $composeProductSubItems->id }}"/>
                        
						 @if($composeGroups)
							<div class="form-group col-md-6">
								<label for="qu" class="col-form-label">Group</label>
								<select name="group_id" id="group_id" class="form-control">
									<option value="">Select Group</option>
										@foreach($composeGroups as $composeGroups)
										   <option value="{{ $composeGroups->id }}" {{ $composeProductSubItems->cgid == $composeGroups->id ? 'selected' : ''}}>{{ $composeGroups->title }}</option>
										@endforeach
								</select>
							</div>
						@endif
						
					    <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Name</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{ $composeProductSubItems->name }}">
                            </div>
                        </div>
						
						<div class="form-group col-md-6">
                                <label for="qu" class="col-form-label">Image*</label>
                                <input type="file" class="form-control-file product_img" id="sub_item_img" name="sub_item_img" accept="image/*">
                                @if(isset($composeProductSubItems->image))
                                    <input type="hidden" id="old_image" name="old_image" class="old_image" value="{{ $composeProductSubItems->image }}"/>
                                    <div class="image_preview"><img class="rounded avatars " id="avatar2" src="{{ url('uploads/products/'.$composeProductSubItems->image) }}" alt="avatar" style="width: 250px;height: 150px"></div>
                                @endif
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Mettre à jour le produit</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>



