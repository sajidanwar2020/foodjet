<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Order cancelled</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body style="background-color: #e9ecef;">

<div class="card-box">

    @if (count($order_detail) > 0)
        <div class="order_detail_wrapper">
            <div class="row">
                <div class="col-md-4">
                    Address : {{$order_detail->order_detail['address']}}
                </div>
                <div class="col-md-4">
                    Phone : {{$order_detail->order_detail['phone_number']}}
                </div>
            </div>
        </div>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Order#</th>
                <th>Product name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>

            @foreach($order_detail as $order_det)
                <tr>
                    <td>{{$order_det->o_id}}</td>
                    <td> {{$order_det->product_name}}</td>
                    <td> {{$order_det->quantity}}</td>
                    <td> {{$order_det->price}}</td>
                    <td>{{getOrderItemStatus($order_det->item_status)}}</td>
                </tr>
            @endforeach

            </tbody>
        </table>
    @else
        <tr>
            <td colspan="8">
                <p>Not found any Orders</p>
            </td>
        </tr>
    @endif
</div>


</body>
</html>
