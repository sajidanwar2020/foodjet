@extends('layouts.mainlayout')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-red">
                <div class="alert alert-danger" role="alert">
                    You are not allowed to access this page
                    <p style="font-size: 15px;"><a href="{{ route('home') }}"><< Back</a></p>
                </div>

            </div>
        </div>
    </div>
@endsection
