<?php /*
@extends('layouts.mainlayout')
@section('content')
    @include('partials.flash-message')
    <div class="container">
        <h2>Order Detail</h2>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Order#</th>
                <th>Customer name</th>
                <th>Product name</th>
                <th>Quantity</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>
            @if (count($order_detail) > 0)
                @foreach($order_detail as $order_det)
                    <tr>
                        <td>{{$order_det->o_id}}</td>
                        <td>{{$order_det->first_name}} {{$order_det->last_name}}</td>
                        <td> {{$order_det->product_name}}</td>
                        <td> {{$order_det->quantity}}</td>
                        <td> {{$order_det->price}}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="8">
                        <p>Not found any Orders</p>
                    </td>
                </tr>
            @endif
            </tbody>
        </table>

    </div>

@endsection

*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a>
                            <li class="breadcrumb-item active">Order Detail</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Order Detail</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Order#</th>
                            <th>Customer name</th>
                            <th>Product name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (count($order_detail) > 0)
                            @foreach($order_detail as $order_det)
                                <tr>
                                    <td>{{$order_det->o_id}}</td>
                                    <td>{{$order_det->first_name}} {{$order_det->last_name}}</td>
                                    <td> {{$order_det->product_name}}</td>
                                    <td> {{$order_det->quantity}}</td>
                                    <td> {{$order_det->price}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">
                                    <p>Not found any Orders</p>
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>
