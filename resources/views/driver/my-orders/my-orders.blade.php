<?php /*
@extends('layouts.mainlayout')
@section('content')
    <div class="signup-form">

        <form action="{{ route('productSubmitted') }}" method="post" id="form_signup" enctype="multipart/form-data">
            @include('partials.flash-message')
            {{ csrf_field() }}
            <input type="hidden"  name="user_type" value="2">
            <div class="form-header">
                <h2>Add product</h2>
            </div>

            <div class="form-group">
                <label>Product name*</label>
                <input type="text" name="product_name" id="product_name" class="form-control required" placeholder="Product Name*" value="{{ old('product_name') }}">
            </div>

            <div class="form-group">
                <label>Product description*</label>
                <textarea class="form-control required" rows="5" id="product_description" name="product_description">{{ old('product_description') }}</textarea>
            </div>

            <div class="form-group">
                <label>Unit price*</label>
                <input type="text" class="form-control required" name="regular_price" id="regular_price"  value="{{ old('regular_price') }}">
            </div>

            <div class="form-group">
                <label>Quantity unit*</label>
                <select name="quantity_unit" id="quantity_unit" class="form-control required">
                    <option value="">Please select quantity unit</option>
                    <option value="1" {{ old('quantity_unit') == '1' ? 'selected' : ''}}>KG</option>
                    <option value="2" {{ old('quantity_unit') == '2' ? 'selected' : ''}}>Dozen</option>
                </select>
            </div>

            <div class="form-group">
                <label>Category</label>
                <select name="category" id="category" class="form-control required">
                    <option value="">Please select Category</option>
                    <?php foreach($categories as $category) :  ?>
                    <option value="{{$category->id}}" {{ $category->id ==  old('category')  ? 'selected' : ''}}>{{$category->name}}</option>
                    <?php endforeach;?>
                </select>
            </div>

            <div class="form-group">
                <label>Image*</label>
                <input type="file" class="form-control-file" id="product_image0" name="product_image0">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block btn-lg">Add Product</button>
            </div>
        </form>

    </div>

@endsection

@section('custom_scripts')
    @include('partials.common_js')
@stop
*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Orders</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Order#</th>
                                <th>Customer name</th>
                                <th>Address</th>
                                <th>Phone#</th>
                                <th>Total Order</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if (count($my_orders) > 0)
                                @foreach($my_orders as $my_order)
                                    <tr>
                                        <td>{{$my_order->o_id}}</td>
                                        <td>{{$my_order->first_name}} {{$my_order->last_name}}</td>
                                        <td>{{$my_order->o_address}}</td>
                                        <td>{{$my_order->phone_number}}</td>
                                        <td>€{{$my_order->total_price}}</td>
                                        <td>{{getOrderStatus($my_order->order_items_status)}}</td>
                                        <td>
                                            @if($my_order->order_items_status == 1)
                                                <a href="{{ route('change_order_status',['order_id'=>$my_order->o_id,'status'=>2]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                   data-original-title="Process" type="warning" msg="Are you sure order to Process?">
                                                    Process?
                                                </a>
                                            @endif

                                            @if($my_order->order_items_status == 2)
                                                <a href="{{ route('change_order_status',['order_id'=>$my_order->o_id,'status'=>4]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                   data-original-title="Completed" type="warning" msg="Are you sure order to complete?">
                                                    Complete?
                                                </a>
                                            @endif

                                            @if($my_order->order_items_status == 1)
                                                <a href="{{ route('change_order_status',['order_id'=>$my_order->o_id,'status'=>3]) }}" class="btn btn-sm btn-info btn-rounded product_status" data-toggle="tooltip"
                                                   data-original-title="Reject" type="warning" msg="Are you sure order to Reject?">
                                                    Reject?
                                                </a>
                                            @endif

                                            <a href="{{ route('vendor_order_detail',['order_id'=>$my_order->o_id]) }}" class="btn btn-sm btn-info btn-rounded" >
                                                Detail
                                            </a>

                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="8">
                                        <p>Not found any Orders</p>
                                    </td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <div style="float: right">
                            <div class="card-footer">
                                {{ $my_orders->links('vendor.pagination.custom') }}
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>



<!-- Vendor js -->
<!-- Footer Start -->

<!-- end Footer -->

@include('partials.vendor.footer-js')

</body>
</html>
