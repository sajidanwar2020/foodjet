<!DOCTYPE html>
<html lang="en">
<head>
    @include('partials.vendor.headers-style')
</head>

<body>

<!-- Navigation Bar-->
<header id="topnav">
    @include('partials.vendor.top-navbar')
</header>
<!-- End Navigation Bar-->

<!-- ============================================================== -->
<!-- Start Page Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <div class="container-fluid">

        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a>
                            <li class="breadcrumb-item active">Driver</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Add Driver</h4>
                </div>
            </div>
        </div>
        <!-- end page title -->

        <!-- Form row -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-box">

                    <form action="{{ route('driver_submitted') }}" method="post" id="driver_signup" enctype="multipart/form-data">
                        @include('partials.flash-message')
                        {{ csrf_field() }}
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="pn" class="col-form-label">Driver Name</label>
                                <input type="text" name="driver_name" id="driver_name" class="form-control required" placeholder="Driver Name*" >
                            </div>
                            
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Phone#</label>
                                <input type="text" class="form-control required" name="phone" id="phone"  >
                            </div>

                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Email</label>
                                <input type="text" class="form-control required" name="email" id="email"  >
                            </div>

                            <div class="form-group col-md-4">
                                <label for="up" class="col-form-label">Vehicle no</label>
                                <input type="text" class="form-control required" name="vehicle_no" id="vehicle_no"  >
                            </div>


                            <div class="form-group col-md-4">
                                <label for="qu" class="col-form-label">Image*</label>
                                @if(isset($driver->photo))
                                    <input type="hidden" id="old_id_0" name="old_image_id" class="old_image_id" value="{{ $driver->photo }}"/>
                                    <img class="rounded avatars" id="avatar2" src="{{ url('uploads/drivers/'.$driver->photo) }}" alt="avatar" style="width: 150px;height: 150px">
                                @endif
                                <input type="file" class="form-control-file" id="driver" name="driver" accept="image/*">
                            </div>

                        </div>
                        <button type="submit" class="btn btn-primary">Update Driver</button>
                    </form>
                </div>
            </div>
        </div>
        <!-- end row -->

    </div> <!-- end container -->
</div>
<!-- end wrapper -->

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->

<!-- Footer Start -->
@include('partials.vendor.footer')
<!-- end Footer -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('partials.vendor.footer-js')

</body>
</html>



