<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
    protected $table = 'countries';
	
	public function geLanguages()
    {

        $resp = array();
		
        $countries = Countries::query()->select("*")->get();
		/*
		//print_r($countries);exit;
       // $resp['en'] = 'CHF';
        if (!empty($countries)) {
           foreach($countries as $country) {
			   //print_r($country);exit;
               $resp[$country['id']] = $country['currency'];
           }
           //$resp = array_unique($resp);
        } */
		
        return $countries; 
    }
}
