<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MinimumOrder extends Model
{
    protected $table = 'minimum_order';
}
