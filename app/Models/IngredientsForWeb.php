<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IngredientsForWeb extends Model
{
    protected $table = 'ingredients_for_web';
}
