<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComposeProductItems extends Model
{
    protected $table = 'compose_product_items';
}
