<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqTypes extends Model
{
    protected $table = 'faq_type';
}
