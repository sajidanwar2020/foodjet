<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorSortCategories extends Model
{
    protected $table = 'vendor_sort_categories';
}
