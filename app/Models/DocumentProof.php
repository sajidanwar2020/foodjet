<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentProof extends Model
{
    protected $table = 'doc_proof';
}
