<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LabelImage extends Model
{
    protected $table = 'label_images';
}
