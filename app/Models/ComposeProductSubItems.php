<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComposeProductSubItems extends Model
{
    protected $table = 'compose_product_sub_items';
}
