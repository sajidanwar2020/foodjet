<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantNotification extends Model
{
    protected $table = 'restaurant_notifications';
}
