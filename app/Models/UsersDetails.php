<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UsersDetails extends Model
{
    protected $table = 'user_details';
}
