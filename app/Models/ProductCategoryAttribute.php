<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategoryAttribute extends Model
{
    protected $table = 'product_category_attribute';
}
