<?php

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use App\Models\ProductsImages;
use App\Models\Pages;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\UserWishList;
use App\Models\Products;
use App\Models\Category;
use App\Models\Orders;
use App\Models\OrderItems;
echo "hereeeee";exit;
function resp($success, $message, $data = [])
{
    $resp ['success'] = $success;
    $resp['response'] = $message;
    if (!empty($data))
        $resp['data'] = $data;
    echo json_encode($resp);
    exit();
}

function errorResp(Exception $exc, string $msg = '')
{
    $file = explode("/", $exc->getFile());
    $file = $file[sizeof($file) - 1];
    echo json_encode(array('success' => 0, 'response' => $msg . $exc->getMessage() . " in $file on line " . $exc->getLine() . $exc->getTraceAsString()));
    exit();
}

function getProductQuantityUnit($status)
{
    //echo $status;exit;
    $arr = array('1' =>'KG', '2' =>'Dozen');
    //print_r($arr[$status]);exit;
    return $arr[$status];
}

function getProductStatus($status)
{
    $arr = array('DeActivated', 'Active', 'Deleted');
    return $arr[$status];
}

function getOrderStatus($status)
{
    $arr = array('','Pending','Processing','Ready for Delivery', 'Completed','Cancelled');
    return $arr[$status];
}

function getOrderItemStatus($status)
{
    $arr = array('','Pending','Processing', 'Cancelled', 'Completed','Ready for delivery');
    return $arr[$status];
}

function getPaymentMethod($index)
{
    $arr = array('','Cash on delivery','Paypal');
    return $arr[$index];
}

function getLocation()
{
    $latitude = Config::get('constants.DEFAULT_MAP_LAT');
    $longitude = Config::get('constants.DEFAULT_MAP_LON');
    $listing_distance = Config::get('constants.DEFAULT_LISTING_DISTANCE');
    if (isset($_COOKIE['latitude']) && isset($_COOKIE['longitude'])) {
        $latitude = $_COOKIE['latitude'];
        $longitude = $_COOKIE['longitude'];
    }
    return array("latitude" => $latitude, "longitude" => $longitude, "distance" => $listing_distance);
}

function findProductImage($id='') {
    $products_image = ProductsImages::where('product_id', '=', $id)->first();
   // print_r($products_image);exit;
   if(\Storage::disk('uploads')->exists('/products/' . $products_image->image_name)) {
   // if($products_image) {
        return $products_image->image_name;
    }
    return 'image-not-avilable.png';
}

function findDistanceBetweenTwoPoint($lat1, $lon1, $lat2, $lon2) {

    $pi80 = M_PI / 180;
    $lat1 *= $pi80;
    $lon1 *= $pi80;
    $lat2 *= $pi80;
    $lon2 *= $pi80;

    $r = 6372.797; // mean radius of Earth in km
    $dlat = $lat2 - $lat1;
    $dlon = $lon2 - $lon1;
    $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $km = $r * $c;

    //echo '<br/>'.$km;
    return $km;
}

function orderDetail($order_id) {

    $order_detail = Orders::select('orders.id as o_id', 'orders.address as o_address', 'orders.phone_number','distance_charges',
        'user_details.first_name','user_details.last_name','order_items.quantity','order_items.price','order_items.status as item_status','order_items.id as items_id')
        ->join('order_items', 'orders.id', '=', 'order_items.order_id')
        ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
        ->where("orders.id", "=", $order_id)
        ->first();
    if($order_detail) {
        $order_detail->total_price = OrderItems::select('price','order_items.status')
            ->where("order_items.order_id", "=", $order_id)
            ->sum('price');
        // Is all order status has been chnanged?
        $order_detail->is_order_processed = OrderItems::select('status')
            ->where("order_items.order_id", "=", $order_id)
            ->where("order_items.status", "=",1)
            ->count('status');
        // Sum cancelled orders
        $order_detail->cancelled_amount = OrderItems::select('status')
            ->where("order_items.order_id", "=", $order_id)
            ->where("order_items.status", "=",3)
            ->sum('price');
    }

    return $order_detail;
}
function orderDetailWithProductAndStatus($order_id) {
    $order_item_detail = Orders::select('orders.id as o_id', 'orders.address as o_address', 'orders.phone_number',
        'user_details.first_name','user_details.last_name','users.email','order_items.quantity','order_items.price','p.product_name','order_items.status as item_status','order_items.id as items_id')
        ->join('order_items', 'orders.id', '=', 'order_items.order_id')
        ->join('products as p', 'p.id', '=', 'order_items.product_id')
        ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
        ->join('users', 'users.id', '=', 'orders.user_id')
        ->where("orders.id", "=", $order_id)
        //->groupBy('order_items.order_id')
        ->get();

    $order_detail_arr = array();
    if($order_item_detail) {
        foreach($order_item_detail as $order_detail) {
            $order_detail_arr['address'] = $order_detail->o_address;
            $order_detail_arr['phone_number'] = $order_detail->phone_number;
            $order_detail_arr['email'] = $order_detail->email;
            $order_detail_arr['order_id'] = $order_id;
        }
    }
    $order_item_detail->order_detail = $order_detail_arr;
    return $order_item_detail;

}
function is_order_cancelled($order_id) {
    if($order_id) {
        // Count total items in order
        $total_items = OrderItems::select('id')
            ->where("order_items.order_id", "=", $order_id)
            ->count('id');
        // Is all order status has been chnanged to cancel?
        $cancelled_items = OrderItems::select('status')
            ->where("order_items.order_id", "=", $order_id)
            ->where("order_items.status", "=",3)
            ->count('status');
        if($total_items == $cancelled_items) {
            return 'yes';
        }
        return 'no';
    }
    return 'no';
}

function getParentCategoryByChildId($childId) {
    $parent_category = Category::select('*')
        ->where("id", "=", $childId)
        ->first();
    return $parent_category;
}

function getParentCategory() {
    $parent_categories = Category::select('*')
        ->where("parent_id", "=", 0)
        ->where("status", "=", 1)
        ->orderBY('id', 'ASC')
        ->get();
    return $parent_categories;
}

function getParentChildCategory() {
    $parent_categories = Category::select('*')
        ->where("status", "=", 1)
        ->orderBY('id', 'DESC')
        ->get();
    if($parent_categories) {
        foreach($parent_categories as $parent_category) {
            $child_categories = Category::select('*')
                //->where("status", "=", 1)
                ->where("parent_id", "=", $parent_category->id)
                ->orderBY('id', 'DESC')
                ->get();
            $parent_category->child_categories = $child_categories;
        }
    }
    return $parent_categories;
}

function getCategoryhiercharchy($id) {
    //echo $id;exit;
    $parent_categories = Category::select('*')
        ->where("id", "=", $id)
        ->where("status", "=", 1)
        ->where("parent_id", "=", 0)
        ->orderBY('id', 'DESC')
        ->get();
    if(count($parent_categories) == 0) {
       // echo $id;exit;
        $child_category = Category::select('*')
            ->where("id", "=", $id)
            ->where("parent_id", "!=", 0)
            ->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->first();
        if($child_category) {
            $parent_categories = Category::select('*')
                ->where("id", "=", $child_category->parent_id)
                ->where("parent_id", "=", 0)
                ->where("status", "=", 1)
                ->orderBY('id', 'DESC')
                ->get();
        }

    }
    //print_r($parent_categories);exit;
    if($parent_categories) {
        foreach($parent_categories as $parent_category) {
            $child_categories = Category::select('*')
                //->where("status", "=", 1)
                ->where("parent_id", "=", $parent_category->id)
                ->orderBY('id', 'DESC')
                ->get();
            //print_r($child_categories);exit;
            $parent_category->child_categories = $child_categories;
        }
    }
    return $parent_categories;
}

function getChildCategoriesIds($cat_id) {
    $child_cat_ids = array();
    if($cat_id) {
            $child_categories = Category::select('*')
                ->where("status", "=", 1)
                ->where("parent_id", "=", $cat_id)
                ->orderBY('id', 'DESC')
                ->get();

            foreach($child_categories as $child_category) {
                $child_cat_ids[] = $child_category->id;
            }
        return $child_cat_ids;
    }
    return $child_cat_ids;
}

function getChildCategories($cat_id) {
    $child_categories = array();
    if($cat_id) {
        $child_categories = Category::select('*')
            ->where("status", "=", 1)
            ->where("parent_id", "=", $cat_id)
            ->orderBY('id', 'DESC')
            ->get();
        return $child_categories;
    }
    return $child_categories;
}

function trim_words($content , $num_words) {
    $content = strip_tags($content);
    $dotted = '';
    if(strlen($content) > $num_words)
        $dotted = '...';
    $content = substr($content, 0, $num_words);
    return $content.$dotted;
}

function wish_list() {
    $info = Session::get("is_logged_in");
   // print_r($info);exit;
    $fav_results = UserWishList::select("*")
        ->where("user_id", "=", $info['user_id'])
        ->get()->all();
    $wish_list_arr = array();
    foreach($fav_results as $fav_result) {
        $wish_list_arr[] = $fav_result->product_id;
    }
   return $wish_list_arr;
}

function getPageSlug($title, $lang = 'en')
{
    $slug = Str::slug($title, '-', $lang);
    $page = Pages::query()->where('slug', $slug)->exists();
    if ($page)
        return getPageSlug($title . '-' . rand(1, 1000), $lang);
    else
        return $slug;
}

function checkPromotions($product_id) {

}
