<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lemonway\LemonWay;

// $wkURL = "https://sandbox-webkit.lemonway.fr/tryngo/dev/payment-page/?fId=".$response->FORM->id;
// $fullURL = $wkURL.'?moneyInToken='.$moneyInToken.'&p='.$p.'&lang=en';
// echo "<a href='".$fullURL."'>Click Me ".$fullURL."</a>";
// header('Location: ');

class Lemon extends Controller
{
    public function Payment(Request $request){

    	$response = LemonWay::callService("GetMoneyInTransDetails", array(
		    "transactionId"=>"45"
		));
		echo json_encode($response, JSON_PRETTY_PRINT);
		return;
    }

    public function checkpay(Request $request){

    	include(app_path() . '\Functions\LemonWay.php');
    	$response = callService("GetMoneyInTransDetails", array(
		    "transactionId"=>"45"
		));
		echo json_encode($response, JSON_PRETTY_PRINT);
		return;
    }

    public function doPayment(Request $request){

    	include(app_path() . '\Functions\LemonWay.php');

    	$response = callService("MoneyInWebInit", array(
	        'wallet'=>'MKP',
	        'amountTot'=>'1.02',
	        'amountCom'=>'0.00',
	        'comment'=>'comment',
      		"returnUrl" => "https://www.yoursite.com/thankyou.php",
	  		"errorUrl" => "https://www.yoursite.com/oops.php",
	  		"cancelUrl" => "https://www.yoursite.com/seeYouNextTime.php",
	        'autoCommission'=>1,
	        'payerOrBeneficiary' => 2
	    ));
		print_r(json_encode($response));

		$result = $response;
		$wkURL = "https://sandbox-webkit.lemonway.fr/tryngo/dev/payment-page/?fId=".$result->MONEYINWEB->TOKEN;
		$fullURL = $wkURL;

		echo "<a href='".$fullURL."'>Click Me ".$fullURL."</a>";
		// header('Location: ');
		echo "<br><br><a href='https://sandbox-webkit.lemonway.fr/tryngo/dev/?moneyintoken=".$result->MONEYINWEB->TOKEN."'>https://sandbox-webkit.lemonway.fr/tryngo/dev/?moneyintoken=".$result->MONEYINWEB->TOKEN."</a>";
		// header('Location: ');GetMoneyInTransDetails

		$this->printCardForm($result->MONEYINWEB->TOKEN);
		return;
    }

    public function fileUploadPayment(Request $request){
    	$buffer = base64_encode(file_get_contents(public_path().'/images/test.jpg', true));
		$response = callService("UploadFile", array(
		    "wallet" => "Test",
	        "fileName" => "test.jpg",
	        "type" => "3",
	        "buffer" => $buffer
		));
		echo "\n<pre>\n".json_encode($response, JSON_PRETTY_PRINT)."\n</pre>\n";
    }

    public function printCardForm($moneyInToken,$wkUrl='https://sandbox-webkit.lemonway.fr/tryngo/dev/',$sslVerification = false, $cssUrl = '', $language = 'en')
    {
        // If Payxpert
        // echo("<script>location.href = '".$this->config->wkUrl . "?moneyintoken=" . $moneyInToken . '&p=' . urlencode($cssUrl) . '&lang=' . $language."';</script>");

        // If Atos
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $wkUrl . "?moneyintoken=" . $moneyInToken . '&p=' . urlencode($cssUrl) . '&lang=' . $language);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $sslVerification);

        $server_output = curl_exec($ch);
        if (curl_errno($ch)) {
            error_log('curl_err : ' . curl_error($ch));
            throw new Exception(curl_error($ch), Exception::UNKNOWN_ERROR);
        } else {
            $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
            switch ($returnCode) {
                case 200:
                    curl_close($ch);
                    $parsedUrl = parse_url($wkUrl);
                    $root = strstr($wkUrl, $parsedUrl['path'], true);
                    $server_output = preg_replace("/src=\"([a-zA-Z\/\.]*)\"/i", "src=\"" . $root . "$1\"", $server_output);
                    echo($server_output);
                    break;
                default:
                    throw new Exception("An error has occured for HTTP code $returnCode", $returnCode);
                    break;
            }
        }
    }

}
