<?php

namespace App\Http\Controllers;

use App\Mail\ForgotPassword;
use App\Models\Products;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\UserWishList;
use App\Models\NewsLetter;
use App\Models\Seller;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Redirect;
use Cart;

class UsersController extends Controller
{

    private $exclude_urls;

    public function __construct()
    {

        $this->exclude_urls = [route('customer_signup'),route('vendor_signup') ,route('reset_password')];
    }

    function signin()
    {
        //print_r(Session::get('is_customer'));exit;
       // echo "hereeeeee";exit;
	    $location = getLocation();
        $data['cart_count'] = Cart::count();
        $data['latitude'] = $location['latitude'];
        $data['longitude'] = $location['longitude'];
        if (isset($_GET['url'])) {
            Session::put("goback_url", url($_GET['url']));
        }
        if (session('is_logged_in')) {
            return redirect(route('home'));
        }
        return view('users/signin',$data);
    }

    function signin_vendor()
    {
        if (session('is_seller')) {
            return redirect('/vendor_dashboard');
        }
        return view('users/signin-vendor');
    }

    public function signin_submitted(Request $request)
    {
        $data = $request->all();
        $email = $data['email'];
        $password = $data['password'];

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $password = crypt($password, md5($password));
        $user = Users::where(['email' => $email, 'password' => $password,'role_id' => 2])
            ->join('user_details as details', 'users.id', '=', 'details.user_id')
            ->first();
        if ($user) {
            // 2 for driver
            if(isset($user['role_id']) AND $user['role_id'] == '2') {
                \session(['is_logged_in' => null]);
                $user = $user->toArray();
                Session::put("is_logged_in",$user);
                return redirect($request->get('prev'));
            }
            else
                return redirect($request->get('prev'));
        }
        return Redirect::back()->withErrors(['error'=> 'No account found for this email/password!'])->withInput();
       // return redirect(route('signin'))->withErrors(['error' => "No account found for this email/password!"]);
    }

    public function signin_vendor_submitted(Request $request)
    {
        $data = $request->all();
        $email = $data['email'];
        $password = $data['password'];

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $password = crypt($password, md5($password));
        $user = Users::where(['email' => $email, 'password' => $password])
            ->whereIn('role_id', array('3','4'))
            ->join('user_details as details', 'users.id', '=', 'details.user_id')
            ->first();
        if ($user) {
            // 3 for vendor
            if(isset($user['role_id']) AND ($user['role_id'] == '3')) {
                $seller_detail = Users::where(['users.id' => $user['user_id']])
                    ->join('user_details as details', 'users.id', '=', 'details.user_id')
                    ->join('seller', 'users.id', '=', 'seller.user_id')
                    ->first();
                Session::put("is_seller",$seller_detail);
                return redirect('/vendor_dashboard');
            } else if(isset($user['role_id']) AND $user['role_id'] == '4') {
                $seller_detail = Users::where(['users.id' => $user['user_id']])
                    ->join('user_details as details', 'users.id', '=', 'details.user_id')
                    ->first();
                Session::put("is_seller",$seller_detail);
                return redirect('/driver_orders');
            }
            // 2 for normal customer
            else if(isset($user['role_id']) AND $user['role_id'] == '2') {
                \session(['is_logged_in' => null]);
                $user = $user->toArray();
                Session::put("is_logged_in",$user);
                return redirect($request->get('prev'));
            }
            else
                return redirect($request->get('prev'));
        }
        return Redirect::back()->withErrors(['error'=> 'No account found for this email/password!'])->withInput();
    }

    function customer_signup()
    {
        if(Session::has('is_logged_in') && !empty(Session::get('is_logged_in'))){
            return redirect(route('home'));
        }
        $data['cart_count'] = Cart::count();
        $data['sub_header'] = 'Sign up';
        $location = getLocation();
        $data['latitude'] = $location['latitude'];
        $data['longitude'] = $location['longitude'];
        return view('users/customer_signup', $data);
    }

    function vendor_signup()
    {
        if(Session::has('is_logged_in') && !empty(Session::get('is_logged_in'))){
            return redirect(route('home'));
        }
        $data['cart_count'] = Cart::count();
        $data['sub_header'] = 'Sign up';
        $location = getLocation();
        $data['latitude'] = $location['latitude'];
        $data['longitude'] = $location['longitude'];
        return view('users/vendor_signup', $data);
    }

    function customer_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|unique:users|min:3|max:255',
            'phone_number' => 'required|unique:users|min:5|max:255',
            'location' => 'required|min:1|max:255',
            //  'password' => 'required|confirmed|min:3|max:255',
            'salutation' => 'required|min:1|max:255',
            //'company' => 'required|min:3|max:255',
            'street' => 'required|min:3|max:255',
            'house_number' => 'required|min:3|max:255',
            'post_code' => 'required|min:3|max:255',
            //'dob' => 'required|min:3|max:255',
        ]);


        $password = crypt($data['password'], md5($data['password']));
        $user = new Users();
        $user->email = $data['email'];
        $user->phone_number = $data['phone_number'];
        $user->role_id = $data['user_type'];
        $user->password = $password;
        $user->role_id = $data['user_type'];
        //$user->verification_status = '1';
        $user->status = '1';
        $user->save();

        $profile = new UsersDetails();
       // $currlanguage = (new Countries())->getCurrencyLanguage($data['country_iso']);
        $profile->user_id = $user->id;
        $profile->first_name = $data['first_name'];
        $profile->last_name = $data['last_name'];
        $profile->location = $data['location'];
        //$profile->date_of_birth = '2020-01-07';
        $profile->latitude = '0';
        if(isset($data['gmap_latitude']))
            $profile->latitude = $data['gmap_latitude'];
        $profile->longitude = '0';
        if(isset($data['gmap_longitude']))
            $profile->longitude = $data['gmap_longitude'];
        //$profile->country = '';
        if(isset($data['country_iso']))
            $profile->country = $data['country_iso'];

        $profile->salutation = $data['salutation'];
       // $profile->company = $data['company'];
        $profile->house_number = $data['house_number'];
        $profile->street = $data['street'];
        $profile->post_code = $data['post_code'];
        $profile->country_id = $data['country'];
        $profile->phone_number = $data['phone_number'];
        $profile->tax_id = '0';
        $profile->save();
        $user2 = Users::where(['users.id' => $user->id])
            ->join('user_details as details', 'users.id', '=', 'details.user_id')
            ->first();
        //print_r($user2);exit;
        if(isset($user2['role_id']) AND $user2['role_id'] == '2') {
            \session(['is_logged_in' => null]);
            $user3 = $user2->toArray();
            Session::put("is_logged_in",$user3);
            return redirect(route('signin'))->with('info', "Congratulations!: Your account is created");
        }

        return redirect(route('signin'))->with('info', "Congratulations!: Your account is created");
    }

    function vendor_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'first_name' => 'required|min:5|max:255',
            'last_name' => 'required|min:5|max:255',
            'email' => 'required|unique:users|min:3|max:255',
            'phone_number' => 'required|unique:users|min:5|max:255',
            //  'password' => 'required|confirmed|min:3|max:255',
            'tax_id'=>'digits_between:2,10',
            'location' => 'required|min:1|max:255',
            'salutation' => 'required|min:1|max:255',
            //'company' => 'required|min:3|max:255',
            'street' => 'required|min:3|max:255',
            'house_number' => 'required|min:3|max:255',
            'post_code' => 'required|min:3|max:255',
            //'dob' => 'required|min:3|max:255',
        ]);

        $password = crypt($data['password'], md5($data['password']));
        $user = new Users();
        $user->email = $data['email'];
        $user->phone_number = $data['phone_number'];
        $user->role_id = $data['user_type'];
        $user->password = $password;
        $user->role_id = $data['user_type'];
        //$user->verification_status = '1';
        $user->status = '1';
        $user->save();

        $profile = new UsersDetails();
        // $currlanguage = (new Countries())->getCurrencyLanguage($data['country_iso']);
        $profile->user_id = $user->id;
        $profile->first_name = $data['first_name'];
        $profile->last_name = $data['last_name'];
        $profile->location = $data['location'];
        $profile->date_of_birth = '2020-01-07';
        $profile->latitude = '0';
        if(isset($data['gmap_latitude']))
            $profile->latitude = $data['gmap_latitude'];
        $profile->longitude = '0';
        if(isset($data['gmap_longitude']))
            $profile->longitude = $data['gmap_longitude'];
        //$profile->country = '';
        if(isset($data['country_iso']))
            $profile->country = $data['country_iso'];
        $profile->salutation = $data['salutation'];
       // $profile->company = $data['company'];
        $profile->house_number = $data['house_number'];
        $profile->street = $data['street'];
        $profile->post_code = $data['post_code'];
        $profile->country_id = $data['country'];
        $profile->phone_number = $data['phone_number'];
        $profile->tax_id = $data['tax_id'];
        $profile->save();

        $seller = new Seller();
        $seller->user_id = $user->id;
        $seller->logo = 'no-logo.png';
        $seller->save();

        return redirect(route('signin'))->with('info', "Congratulations!: Your account is created");
    }

    function change_password()
    {
        $data['cart_count'] = Cart::count();
        return view('users/change_password',$data);
    }

    function update_password(Request $request)
    {

        $this->validate($request, [
            'old_password' => 'min:6',
            'new_password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);

        $data = $request->all();
       // print_r($data);exit;

        $info = Session::get('is_logged_in');
        $user_id = $info['user_id'];

        $user = Users::select("id", "password")
            ->where(['id' => $user_id])
            ->first();
        $old_password = $data['old_password'];
        $old_password = crypt($old_password, md5($old_password));
        if($old_password == $user->password) {
            $new_password = crypt($data['new_password'], md5($data['new_password']));
            $user->password = $new_password;
            if($user->save()){
                return redirect(route('change_password', ['status' => 'deleted']))->with('success', "Password has been changed");
            }
        } else {
            return redirect(route('change_password'))->withErrors('Old password did not match');
        }
        return redirect()->back();
    }

    function profile()
    {
        $info = Session::get('is_logged_in');
        $user_id = $info['user_id'];
        $info = Users::select('*')
            ->where(['users.id' => $user_id])
            ->join('user_details as d', 'users.id', '=', 'd.user_id')
            ->first();
        $data['info'] = $info;
        $data['cart_count'] = Cart::count();
        return view('users/profile', $data);
    }

    function profile_update(Request $request)
    {
        /*
        $this->validate($request, [
            'first_name' => 'required',
        ]); */

        $data = $request->all();

        $info = Session::get("is_logged_in");
        $user = Users::where("id", "=", $info['user_id'])
            ->first();
       // $user->phone_number = $data['mobile_number'];
        $user->save();

        $profile = UsersDetails::where("user_id", "=", $info['user_id'])
            ->first();
        $profile->first_name = $data['first_name'];
        $profile->last_name = $data['last_name'];
        $profile->location = $data['location'];
        //$profile->date_of_birth = $data['dob'];
        if(isset($data['gmap_latitude']))
            $profile->latitude = $data['gmap_latitude'];
        if(isset($data['gmap_longitude']))
            $profile->longitude = $data['gmap_longitude'];
        if(isset($data['country_iso']))
            $profile->country = $data['country_iso'];
        $profile->salutation = $data['salutation'];
       // $profile->company = $data['company'];
        $profile->house_number = $data['house_number'];
        $profile->street = $data['street'];
        $profile->post_code = $data['post_code'];
        $profile->country_id = $data['country'];
        //$profile->phone_number = $data['mobile_number'];
       // $profile->tax_id = $data['tax_id'];
        $profile->save();
        return redirect(route('profile'))->with('info', "Updated profile");
    }

    function forgot_password(Request $request)
    {
        $email = $request->email;
        $user = Users::select("id", "password", 'verification_code')
            ->where(['email' => $email])
            ->first();
       // print_r($user);exit;
        if (!empty($user)) {
            $code = mt_rand(100000, 999999);
            $user->verification_code = $code;
            $user->save();
            $details = UsersDetails::query()
                ->select('id', 'first_name', 'last_name')
                ->where('user_id', '=', $user['id'])
                ->first();
            //print_r($details);exit;
            $token = base64_encode($email . ':' . $code);
            $info = array(
                'first_name' => $details->first_name,
                'last_name' => $details->last_name,
                'link' => route('reset_password', ['token' => $token])
            );
            //print_r($info);exit;
            Mail::to($email)->send(new ForgotPassword($info));
            //Send Email
            return 1;
        }
        return "No such email are found.";
    }

    function reset_password(Request $request)
    {
        $data = $request->all();
        $info['token'] = $data['token'];
        return view('users.reset_password', $info);
    }

    function reset_update_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);
        $data = $request->all();
        $info = base64_decode($data['token']);
        $info = explode(":", $info);
       // echo "<pre>";
        //print_r($info);
        //exit;
        //exit;
        $email = $info[0];
        $code = $info[1];
        //echo $email.' '.$code;exit;
        if ($email != '' && $code != '') {
            $user = Users::select("id", "password", 'verification_code')
                ->where(['email' => $email, 'verification_code' => $code])
                ->first();
           // print_r($user);exit;
            if (empty($user)) {
                return redirect(route('reset_password', ['token' => $data['token']]))->with('error', 'Sorry your password reset link is expired');
            } else {
                $password = crypt($data['password'], md5($data['password']));
                $user->password = $password;
                $user->verification_code = '';
                if ($user->save()) {
                    return redirect(route('signin'))->with('success', 'Your password has been changed successfully');
                } else {
                    return redirect(route('reset_password', ['token' => $data['token']]))->with('error', 'Opps something went wrong');
                }
            }
        } else {
            return redirect(route('reset_password', ['token' => $data['token']]));
        }
    }


    function update_wish_list(Request $request)
    {
        $data = $request->all();
        if(!Session::has('is_logged_in') && empty(Session::get('is_logged_in'))) {
            return 11;
        }
        $info = Session::get("is_logged_in");
        $user_id = $info['user_id'];
        $product_id = $data['product_id'];
        $fav = new UserWishlist();

        //------------------------------------------ delete wishlist if already
        $fav_result = $fav::select("*")
            ->where("user_id", "=", $user_id)
            ->where("product_id", "=", $product_id)
            ->first();
        if(!empty($fav_result)) {
            $deleted = $fav::select("*")
                ->where("user_id", "=", $user_id)
                ->where("product_id", "=", $product_id)
                ->delete();
            if($deleted)
                return 'add_wish';
            else
                return 'no';
        }

        //----------------------------------------------------------------------------------end delete

        $fav->status = 1;
        $fav->user_id = $user_id;
        $fav->product_id = $product_id;
        if($fav->save())
            return 'remove_wish';
        else
            return 'no';
    }
    function wish_list()
    {
        $info = Session::get("is_logged_in");
        $fav_results = UserWishList::select("*")
            ->join('products as p', 'p.id', '=', 'wishlist.product_id')
            ->where("wishlist.user_id", "=", $info['user_id'])
            ->get()->all();
        $data['wish_list'] = $fav_results;
        return view('users.wishlist',$data);
    }
    function add_to_cart_remove_wish(Request $request)
    {
        $data = $request->all();

        $id = $request->product_id;
        $products_detail = Products::find($id);
        $quantity = 1;
        if($data['remove'] == "no") {
            Cart::add($products_detail->id, $products_detail->product_name, $quantity, $products_detail->regular_price);
        }
        //print_r($data);exit;
        $info = Session::get("is_logged_in");
        $user_id = $info['user_id'];
        $fav = new UserWishlist();
        $fav_result = $fav::select("*")
            ->where("user_id", "=", $user_id)
            ->where("product_id", "=", $id)
            ->first();

        if(!empty($fav_result)) {
            $deleted = $fav::select("*")
                ->where("user_id", "=", $user_id)
                ->where("product_id", "=", $id)
                ->delete();
            echo $id;
        }
    }

    function subscribe(Request $request) {
        $data = $request->all();

        $newsLetter = NewsLetter::query()
            ->select('id', 'email')
            ->where('email', '=', $data['email'])
            ->first();
        if($newsLetter) {
            return 1;
        }
        $newsLetter = new NewsLetter();
        $newsLetter->email = $data['email'];
        $newsLetter->status = '1';
        $newsLetter->save();
        return 0;
    }

    function logout()
    {
      //  print_r(Session::get('my_cart'));exit;
        \session(['is_logged_in' => null]);
        \session(['is_seller' => null]);
        //Session::flush();
        return redirect(route('home'));
    }

}
