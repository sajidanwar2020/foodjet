<?php

namespace App\Http\Controllers;

use App\Mail\OrderPlaced;
use Cart;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\MinimumOrder;
use App\Models\Orders;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use DB;

class CheckOutController extends Controller
{
  function check_out() {
      $data['user_detail'] = array();
      if(Session::has('is_logged_in') && !empty(Session::get('is_logged_in'))){
          $data['user_detail'] =  Session::get('is_logged_in');
      }
     // print_r($data['user_detail']);exit;
      if(Cart::count() > 0) {
          $data['cart_count'] = Cart::count();
          return view('checkout/checkout', $data);
      }
      return redirect(route('show_cart'));
  }

    function distance_detail(Request $request) {
        $data = $request->all();
        if(isset($data['charges']) AND $data['charges'] > 0)
            Session::put("distance_detail",$data['charges']);
        if(isset($data['distance_km']) AND $data['distance_km'] > 0) {
            $minimumOrder = DB::select("SELECT order_value FROM minimum_order WHERE ".$data['distance_km']." BETWEEN km_from AND  km_to");
           // print_r($minimumOrder);
            if($minimumOrder[0]->order_value)
                echo $minimumOrder[0]->order_value;
             else
                echo "no";
        } else {
            echo "no";
        }

    }

   function proceed_check_out(Request $request) {
        $this->validate($request, [
            'phone_number' => 'required',
            'location' => 'required',
            'payment_type' => 'required',
        ]);
      //  if($data['shiping_charges'] < 1)
       // return back()->withErrors('your error message');

        $data = $request->all();
        $user_detail =  Session::get('is_logged_in');
        if(Cart::count() > 0) {
            $orders = new Orders();

            if(Session::has('distance_detail') && !empty(Session::get('distance_detail')) ){
                $shiping_charges = Session::get('distance_detail');
            } else {
                return back()->withErrors('Shiping charges error');
            }
            $orders->address = $data['location'];
            $orders->latitude = $data['gmap_latitude'];
            $orders->longitude = $data['gmap_longitude'];
            $orders->user_id = $user_detail['user_id'];
            $orders->phone_number = $data['phone_number'];
            $orders->payment_type = $data['payment_type'];
            $orders->additional_notes = $data['additional_notes'];
            $orders->distance_charges = $shiping_charges;
            $orders->status = '1';
            $orders->save();
            $orders_id = $orders->id;
			//echo "<pre>";
			//print_r(Cart::content());
			//exit;
            foreach(Cart::content() as $row)  {
               // print_r($row);exit;
                $products_detail = Products::find($row->id);
                $orderItems = new OrderItems();
                $orderItems->product_id = $products_detail->id;
                $orderItems->order_id = $orders_id;
                $orderItems->seller_id = $products_detail->seller_id;
                $orderItems->quantity = $row->qty;
                $orderItems->price =    $row->total;
                $orderItems->sale_price = $products_detail->sale_price;
                $orderItems->regular_price = $products_detail->regular_price;
                $orderItems->status = '1';
                $orderItems->save();
            }
            $order_info = array("order_detail"=>$orders);
            Mail::to($user_detail['email'])->send(new OrderPlaced($order_info));
            Cart::destroy();
            return  redirect(route('order_success'))->with('success', 'Your order has been placed successfully. Order# '.$orders_id);
        } else {
            return  redirect(route('home'));
        }
  }

}
