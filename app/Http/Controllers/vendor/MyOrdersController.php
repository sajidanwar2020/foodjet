<?php

namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use App\Mail\OrderCancelled;
use App\Mail\OrderReady;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\CreditTransactions;
use App\Models\Orders;
use App\Models\OrderItems;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Storage;
class MyOrdersController extends Controller
{

    public function my_orders()
    {
        $data['title'] = 'My Orders';
        $data['breadcrums'] = 'My Orders';
        $info = Session::get('is_logged_in');
        $is_seller = Session::get('is_seller');
        //print_r($is_seller);exit;
        $my_orders = Orders::select('orders.id as o_id', 'orders.address as o_address','additional_notes', 'payment_type',
            'distance_charges','orders.phone_number','orders.status as o_status',
            'user_details.first_name','user_details.last_name')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            //->join('products', 'products.id', '=', 'order_items.product_id')
           ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->where("order_items.seller_id", "=", $is_seller->id)
            ->orderBY('orders.id', 'DESC')
            ->groupBy('order_items.order_id')
            ->paginate(15);
      //  print_r($my_orders);exit;
        // echo $is_seller->id;exit;
        if($my_orders) {
            foreach($my_orders as $my_order) {
                $my_order->total_price = OrderItems::select('price','order_items.status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.seller_id", "=", $is_seller->id)
                    ->sum('price');
                // Is all order status has been chnanged?
                $my_order->is_order_processed = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.seller_id", "=", $is_seller->id)
                    ->where("order_items.status", "=",1)
                    ->count('status');
                // Sum cancelled orders
                $my_order->cancelled_amount = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.status", "=",3)
                    ->where("order_items.seller_id", "=", $is_seller->id)
                    ->sum('price');
                $my_order->online_transaction = CreditTransactions::select('*')
                    ->where("order_id", "=", $my_order->o_id)
                    ->where("status", "=",1)
                    ->first();
            }
        }

        $data['my_orders'] = $my_orders;
        return view('vendor/my-orders/my-orders',$data);
    }

    public function driver_orders()
    {
        $data['title'] = 'My Orders';
        $data['breadcrums'] = 'My Orders';
        $info = Session::get('is_logged_in');
        $is_seller = Session::get('is_seller');
        $my_orders = Orders::select('orders.id as o_id', 'orders.address as o_address','additional_notes','payment_type',
            'distance_charges' ,'orders.phone_number','orders.status as o_status','user_details.first_name','user_details.last_name')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            //->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->orderBY('orders.id', 'DESC')
            ->groupBy('order_items.order_id')
            ->paginate(15);
        // echo $is_seller->id;exit;
        if($my_orders) {
            foreach($my_orders as $my_order) {
                $my_order->total_price = OrderItems::select('price','order_items.status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->sum('price');
                // Is all order status has been chnanged?
                $my_order->is_order_processed = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.status", "=",1)
                    ->count('status');
                // Sum cancelled orders
                $my_order->cancelled_amount = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.status", "=",3)
                    ->sum('price');
                // Check if online transaction has been completed
                $my_order->online_transaction = CreditTransactions::select('*')
                    ->where("order_id", "=", $my_order->o_id)
                    ->where("status", "=",1)
                    ->first();
            }
        }
        $data['my_orders'] = $my_orders;
        return view('vendor/my-orders/my-orders',$data);
    }

    function change_order_status(Request $request)
    {
        $data = $request->all();
        $status = $data['status'];
        //exit;
        OrderItems::where('id', '=', $data['item_id'])
            ->update(['status' => $status]);

        $info = Session::get('is_seller');
        //-----------------------------------------------------------
        if($info['role_id'] == 3)
            $redirect=  'vendor_order_detail';
        else if($info['role_id'] == 4)
            $redirect=  'driver_order_detail';
        //-----------------------------------------------------------
        // If all order item has been cancelled
        $order_status = is_order_cancelled($data['order_id']);
        if($order_status == 'yes'){
            Orders::where('id', '=', $data['order_id'])
                ->update(['status' => 5]);
            $orderDetailWithProduct = orderDetailWithProductAndStatus($data['order_id']);
            Mail::to($orderDetailWithProduct->order_detail['email'])->send(new OrderCancelled($orderDetailWithProduct));
        }
        //-----------------------------------------------------------
        if ($status == 2) {
            return redirect(route($redirect, ['status' => $status,'order_id'=>$data['order_id']]))->with('success', 'Processed');
        } else if ($status == 4) {
            return redirect(route($redirect, ['status' => $status,'order_id'=>$data['order_id']]))->with('success', 'Completed');
        } else if ($status == 3) {
            return redirect(route($redirect, ['status' => $status,'order_id'=>$data['order_id']]))->with('error', 'Rejected');
        }
        //-----------------------------------------------------------
        if($info['user_id'] == 3)
            return  redirect(route($redirect));
        else if($info['user_id'] == 4)
            return  redirect(route($redirect));
    }

    function change_main_order_status(Request $request)
    {
        $data = $request->all();
        $is_completed = OrderItems::select('status')
            ->where("order_items.order_id", "=", $data['order_id'])
            ->where("order_items.status", "=",1)
            ->count('status');
        //echo $is_completed;exit;
        $info = Session::get('is_seller');
        if($info['role_id'] == 3)
            $redirect=  'my_orders';
        else if($info['role_id'] == 4)
            $redirect=  'driver_orders';

        if($is_completed == 0) {
            // Change status of all items which is ready to deleviry
            if($data['status'] == 3) {
                OrderItems::where('order_id', '=', $data['order_id'])
                     ->where('status', '=', 2)
                    ->update(['status' => 5 ]);
                $orderDetailWithProduct = orderDetailWithProductAndStatus($data['order_id']);
                Mail::to($orderDetailWithProduct->order_detail['email'])->send(new OrderReady($orderDetailWithProduct));
            }
            // Change status to completed of all items
            if($data['status'] == 4) {
                OrderItems::where('order_id', '=', $data['order_id'])
                    ->where('status', '=', 5)
                    ->update(['status' => 4 ]);
            }
            Orders::where('id', '=', $data['order_id'])
                ->update(['status' => $data['status']]);
            return redirect(route($redirect, ['status' => $data['status']]))->with('success', $data['status']);
        }
        return redirect(route($redirect))->with('success', $data['status']);
    }

    public function vendor_order_detail(Request $request)
    {
        $data = $request->all();
        $order_id = $data['order_id'];
        $data['title'] = 'Order Detail';
        $data['breadcrums'] = 'Order Detail';
        $info = Session::get('is_logged_in');
        $is_seller = Session::get('is_seller');
      //  print_r($is_seller);exit;
        $order_item_detail = Orders::select('orders.id as o_id', 'orders.address as o_address', 'orders.phone_number',
            'user_details.first_name','user_details.last_name','order_items.quantity','order_items.price','p.product_name','order_items.status as item_status','order_items.id as items_id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products as p', 'p.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->where("order_items.seller_id", "=", $is_seller->id)
            ->where("orders.id", "=", $order_id)
            //->groupBy('order_items.order_id')
            ->get();

        $order_detail_arr = array();
        if($order_item_detail) {
            foreach($order_item_detail as $order_detail) {
                $order_detail_arr['address'] = $order_detail->o_address;
                $order_detail_arr['phone_number'] = $order_detail->phone_number;
            }
        }
        $order_item_detail->order_detail = $order_detail_arr;
       // echo "<pre>";
        //print_r($order_item_detail);
        //exit;

        $data['order_detail'] = $order_item_detail;
        return view('vendor/my-orders/order_detail',$data);
    }

    public function driver_order_detail(Request $request)
    {
        $data = $request->all();
        $order_id = $data['order_id'];
        $data['title'] = 'Order Detail';
        $data['breadcrums'] = 'Order Detail';
        //$info = Session::get('is_logged_in');
        //$is_seller = Session::get('is_seller');
        //  print_r($is_seller);exit;
        $order_item_detail = Orders::select('orders.id as o_id', 'orders.address as o_address', 'orders.phone_number',
            'user_details.first_name','user_details.last_name','order_items.quantity','order_items.price','p.product_name','order_items.status as item_status','order_items.id as items_id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products as p', 'p.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->where("orders.id", "=", $order_id)
            //->groupBy('order_items.order_id')
            ->get();

        $order_detail_arr = array();
        if($order_item_detail) {
            foreach($order_item_detail as $order_detail) {
                $order_detail_arr['address'] = $order_detail->o_address;
                $order_detail_arr['phone_number'] = $order_detail->phone_number;
            }
        }
        $order_item_detail->order_detail = $order_detail_arr;
       // echo "<pre>";
       // print_r($order_item_detail);
       // exit;
        $data['order_detail'] = $order_item_detail;
        return view('vendor/my-orders/order_detail',$data);
    }
}
