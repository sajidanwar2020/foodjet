<?php
namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use App\Models\Restaurant;
use App\Models\Users;
use App\Models\Products;
use App\Models\Nutrients;
use App\Models\OrderItems;
use App\Models\UsersDetails;
use http\Client\Curl\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Storage;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Validator;

class NutrientsController extends Controller
{
   public function addNutrients()
    {
        $data['title'] = 'Add Nutrients';
        $data['breadcrums'] = 'Add Nutrients';
        return view('vendor/nutrients/add_nutrients',$data);
    }

    function nutrientsSubmitted(Request $request)
    {
        $data = $request->all();
        $is_vendor = Session::get('is_vendor');
         $this->validate($request, [
            'nutrients_labal' => 'required',
        ]);
        $nutrients = new Nutrients();
		$nutrients->restaurant_id = $is_vendor->id;
        $nutrients->label = $data['nutrients_labal'];
		$nutrients->description = $data['description'];
        $nutrients->status = '1';
        $nutrients->save();
        $nutrients_id = $nutrients->id;
        return redirect(route('nutrients-label'))->with('info', "Nutrients created");
    }

    function editNutrientsSubmitted(Request $request)
    {
        $data = $request->all();
        $is_vendor = Session::get('is_vendor');
        $this->validate($request, [
            'nutrients_labal' => 'required',
        ]);
		$nutrients = Nutrients::find($request->id);
        $nutrients->label = $data['nutrients_labal'];
		$nutrients->description = $data['description'];
        $nutrients->save();
        return back();
    }

    public function nutrientsLabel()
    {
        $data['title'] = 'All Nutrients';
        $data['breadcrums'] = 'All Nutrients';
		$is_vendor = Session::get('is_vendor');
        $nutrients = Nutrients::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $data['nutrients'] = $nutrients;
        return view('vendor/nutrients/my_nutrients',$data);
    }

    public function change_nutrient_status(Request $request)
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
        $id = $_GET['product_id'];
        $data = $request->product_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $product_id = $data[0];
        $status = $data[2];
        $nutrients = Nutrients::query()
            ->select("id", "status")
            ->where("id", "=", $product_id)
            ->first();
        $nutrients->status = $status;
        $nutrients->save();
        if ($status == 2) {
            return redirect(route('nutrients-label'))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('nutrients-label'))->with('success', 'Published');
        } else if ($status == 0) {
            return redirect(route('nutrients-label'))->with('success', 'Deactivated');
        }
          return redirect(route('nutrients-label'))->with('info', "Nutrients");
    }

    public function edit_nutrient()
    {
        $data['title'] = 'Edit Product';
        $data['breadcrums'] = 'Edit Product';
		$is_vendor = Session::get('is_vendor');
		$id = $_GET['product_id'];
		$id = base64_decode($id);
        $id = explode(":", $id)[0];
        $data['nutrients'] = Nutrients::select("*")->where('id','=',$id)->first();
        return view('vendor/nutrients/edit_nutrients',$data);
    }

}
