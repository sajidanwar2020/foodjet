<?php

namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\LabelImage;
use App\Models\ComposeProduct;
use App\Models\ComposeQuantites;
use App\Models\Category;
use App\Models\ComposeProductItems;
use App\Models\ComposeProductSubItems;
use App\Models\composeGroup;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use DB;
use Storage;
class ComposeProductController extends Controller
{
    public function addComposeProduct()
    {
        $data['title'] = 'Add Compose Product';
        $data['breadcrums'] = 'Add Compose Product';
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
		$categories3 = Category::select('*')
		->where("parent_id", "=", 0)
		->where("restaurant_id", "=", $restaurant_id)
        ->where("status", "=", 1)
        ->orderBY('id', 'ASC')
		->get()
		->all();
		$data['categories'] = $categories3;
        $data['labelimages'] = LabelImage::select("*")->where('restaurant_id','=',$restaurant_id)->get();
        return view('vendor/compose-product/add_compose_product',$data);
    }

    function addComposeProductSubmit(Request $request)
    {
        $data = $request->all();
        $is_vendor = Session::get('is_vendor');
		
		$this->validate(
			$request, 
			[   
				'_name' => 'required|min:2|max:255',
				'type' => 'required',
				'category' => 'required',
			],
			[   
				'_name.required'    => 'Le nom est obligatoire.',
				'type.required'      => 'Le type de rédaction est obligatoire.',
				'category.required'      => 'La catégorie est obligatoire.',
			]
		);
		
		$file_name = '';
		$file = $request->file("compose_product_img");
        if ($file) {
            $file_name = "product_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("products", $file_name, 'uploads');
        }
		
        $composeProduct = new ComposeProduct();
		$composeProduct->category_id = $data['category'];
        $composeProduct->name = $data['_name'];
		$composeProduct->description = $data['description'];
		$composeProduct->image = $file_name;
        $composeProduct->restaurant_id = $is_vendor->id;
        $composeProduct->type = $data['type'];
        $composeProduct->status = '1';


        $alllabelsI = $request->labelimages;
        $finalIds = "";
		if($alllabelsI) {
			foreach($alllabelsI as $single){
				$finalIds .= $single.",";
			}
			$finalIds = rtrim($finalIds, ",");
		}
        $composeProduct->labelimages_ids = $finalIds;

        $composeProduct->save();
        $product_id = $composeProduct->id;
		
        return redirect(route('compose-product-listing'))->with('info', "Compose product created");
    }
	
	public function editComposeProduct()
    {
        $data['title'] = 'Edit Product';
        $data['breadcrums'] = 'Edit Product';
		$is_vendor = Session::get('is_vendor');
        if(isset($is_vendor['role_id']) AND $is_vendor['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
		
        $product_id = $_GET['product_id'];
        $product_id = base64_decode($product_id);
        $product_id = explode(":", $product_id)[0];
        $products = ComposeProduct::select('*')
            ->where("id", "=", $product_id)
            ->where('restaurant_id', '=',$is_vendor->id)
            ->first();
        if(!$products) {
            return view('errors/404');
            exit;
        }
        $data['product'] = $products;
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
		$categories3 = Category::select('*')
		->where("parent_id", "=", 0)
		->where("restaurant_id", "=", $restaurant_id)
        ->where("status", "=", 1)
        ->orderBY('id', 'ASC')
		->get()
		->all();
		$data['categories'] = $categories3;
        $data['labelimages'] = LabelImage::select("*")->where('restaurant_id','=',$restaurant_id)->get();
        return view('vendor/compose-product/edit_compose_product',$data);
    }
	
    function editComposeProductSubmit(Request $request)
    {
        $data = $request->all();
        $is_vendor = Session::get('is_vendor');
		
        $this->validate(
			$request, 
			[   
				'_name' => 'required|min:2|max:255',
				'type' => 'required',
				'category' => 'required',
			],
			[   
				'_name.required'    => 'Le nom est obligatoire.',
				'type.required'      => 'Le type de rédaction est obligatoire.',
				'category.required'      => 'La catégorie est obligatoire.',
			]
		);
		
        $product_id = $data['product_id'];
		//print_r($is_vendor);exit;
        if ($product_id != '') {
            $composeProduct = ComposeProduct::select("*")
                ->where("id", "=", $product_id)
                ->first();
        }
		
		$size = count($_FILES);
        $file = $_FILES['compose_product_img']['tmp_name'];
        if ($file) {
            if(isset($data['old_image_id'])) {
                $olds =  $data['old_image_id'];
                Storage::disk('uploads')->delete('products/' . $composeProduct->image);
            }
	
			$file = $request->file("compose_product_img");
            $file_name = "product" . $product_id . "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("products", $file_name, 'uploads');
			$composeProduct->image = $file_name;
        }
		$composeProduct->category_id = $data['category'];
        $composeProduct->name = $data['_name'];
		$composeProduct->description = $data['description'];
        $composeProduct->restaurant_id = $is_vendor->id;
        $composeProduct->type = $data['type'];
        $composeProduct->status = '1';

        $alllabelsI = $request->labelimages;
        $finalIds = "";
		if($alllabelsI) {
			foreach($alllabelsI as $single){
				$finalIds .= $single.",";
			}
			$finalIds = rtrim($finalIds, ",");
		}
        $composeProduct->labelimages_ids = $finalIds;
        
        $composeProduct->save();

        return back();
    }

    public function composeProductListing()
    {
        $data['title'] = 'All products';
        $data['breadcrums'] = 'All products';

		$is_vendor = Session::get('is_vendor');
		 
        $products = ComposeProduct::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
            ->orderBY('id', 'DESC')
            ->paginate(15);
			
        $data['products'] = $products;
        return view('vendor/compose-product/my_compose_products',$data);
    }


    function changeComposeProductSubmit(Request $request)
    {
        $is_vendor = Session::get('is_vendor');
        if(isset($is_vendor['role_id']) AND $is_vendor['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data = $request->product_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $product_id = $data[0];
        $status = $data[2];
        $composeProduct = ComposeProduct::query()
            ->select("id", "status")
            ->where("id", "=", $product_id)
            ->first();
        $composeProduct->status = $status;
        $composeProduct->save();
        if ($status == 2) {
            return redirect(route('compose-product-listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('compose-product-listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('compose-product-listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('compose-product-listing'));
    }
	
	//----------------------------------------------------------------------------------------------------------
	
	function addComposeProductQuantites(Request $request)
    {
        $data['title'] = 'Add Compose Product';
        $data['breadcrums'] = 'Add Compose Product';
		$is_vendor = Session::get('is_vendor');
		$composed_products = ComposeProduct::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
            ->orderBY('id', 'DESC')
            ->get()
			->all();
			//print_r($composed_products);exit;
		$data['composed_products'] = $composed_products;	
        return view('vendor/compose-quantity/add_compose_quantity',$data);
    }
	
	function composeProductQuantitesSubmit(Request $request)
    {
        $data['title'] = 'Add Compose Product';
        $data['breadcrums'] = 'Add Compose Product';
        $data = $request->all();
		
		//echo "<pre>";
		//print_r( $data);exit;
		
        $is_vendor = Session::get('is_vendor');
		// Mixture product
		/*
		if(isset($data['compose_product_type']) AND $data['compose_product_type'] == 2) {
			if(isset($data['total_quantity']) AND $data['total_quantity'] > 0) {
				ComposeQuantites::where('cpid',$data['composed_product'])->delete();
				for($i=0; $i <= $data['total_quantity']; $i++) {
					if(!empty($data['quantity_name'][$i]) AND !empty($data['quantity_price'][$i])) {
						$composeQuantites = new ComposeQuantites();
						$composeQuantites->restaurant_id = $is_vendor->id;
						$composeQuantites->quantity = $data['quantity_name'][$i];
						$composeQuantites->price = $data['quantity_price'][$i];
						//$composeQuantites->ingredients = $data['quantity_name'][$i];
						$composeQuantites->cpid = $data['composed_product'];
						$composeQuantites->save();
						$composeQuantites_id = $composeQuantites->id;
					}
				}
			}
			
			//----------------------------------------------------------------------------------------------
			
			if(isset($data['product_items_quantity']) AND $data['product_items_quantity'] > 0) {
				ComposeProductItems::where('cpid',$data['composed_product'])->delete();
				for($i=1; $i <= $data['last_item_index']; $i++) {
					if(!empty($data['item_name'][$i]) AND !empty($data['item_ingredient'][$i])) {
						$item_img = $request->file("item_img");
						//print_r($item_img);exit;
						$file_name = '';
						if(isset($item_img ) AND $item_img) {
							if(isset($item_img[$i])) {
								$item_img_name = $item_img[$i];
								$file_name = str_random(5)."-".date('his')."-".str_random(3)."." . $item_img_name->getClientOriginalExtension();
								$item_img[$i]->storeAs("products", $file_name, 'uploads');
							}
						}
						$composeProductItems = new ComposeProductItems();
						$composeProductItems->restaurant_id = $is_vendor->id;
						$composeProductItems->name = $data['item_name'][$i];
						$composeProductItems->image = $file_name;
						$composeProductItems->ingredients = $data['item_ingredient'][$i];
						$composeProductItems->cpid = $data['composed_product'];
						$composeProductItems->save();
						$composeProductItems_id = $composeProductItems->id;
						
							if(isset($data['sub_item_name'][$i]) AND count($data['sub_item_name'][$i]) > 0) {
								$counter = 0;
								foreach ($data['sub_item_name'][$i] as $sub_item_name) {
									if(empty($sub_item_name)) {
										$sub_item_name  = '  HIOHI';
									}
											$file_nameSub = '';
										$sub_item_arr = $request->file('sub_item_img');
										//print_r($sub_item_arr);exit;
										if(isset($sub_item_arr[$i][$counter])){
											$extension = $sub_item_arr[$i][$counter]->getClientOriginalExtension();
											$imageSubItem = $sub_item_arr[$i][$counter];
											if(isset($imageSubItem)){
												$file_nameSub = str_random(5)."-".date('his')."-".str_random(3)."." . $extension;
												//echo $file_nameSub;exit;
												$sub_item_arr[$i][$counter]->storeAs("products", $file_nameSub, 'uploads');
											}
										}
										//print_r($item_img);exit;
										
										
										$composeProductSubItems = new ComposeProductSubItems();
										$composeProductSubItems->restaurant_id = $is_vendor->id;
										$composeProductSubItems->name = $sub_item_name;
										$composeProductSubItems->image = $file_nameSub;
										$composeProductSubItems->cpiid = $composeProductItems_id;
										$composeProductSubItems->save();
										$composeProductSubItems_id = $composeProductSubItems->id;
								
									$counter++;
								}
							} 
					}
				}
			}
			//----------------------------------------------------------------------------------------------	
		} */
		
		// Simple product 
		
		if(isset($data['compose_product_type'])) {
			if(isset($data['quantity_name']) AND count($data['quantity_name']) > 0) {
				//ComposeQuantites::where('cpid',$data['composed_product'])->delete();
				//echo $data['composed_product'];exit;
				foreach($data['quantity_name'] as $quantity_key => $quantity) {
					if(!empty($quantity)) {
						
						$composeQuantites = ComposeQuantites::select("*")
							->where("id", "=", $quantity_key)
							->first();
						if(!$composeQuantites) {
							$composeQuantites = new ComposeQuantites();
						}
						
						$composeQuantites->restaurant_id = $is_vendor->id;
						$composeQuantites->quantity = $quantity;
						$composeQuantites->price = $data['quantity_price'][$quantity_key];
						$ingredients = isset($data['quantity_ingredient'][$quantity_key]) ? $data['quantity_ingredient'][$quantity_key] : '';
						$composeQuantites->ingredients = $ingredients;
						$composeQuantites->cpid = $data['composed_product'];
						$composeQuantites->save();
						$composeQuantites_id = $composeQuantites->id;
					}
				}
			}
			
			//----------------------------------------------------------------------------------------------
			
			if(isset($data['item_name']) AND count($data['item_name']) > 0) {
				//ComposeProductItems::where('cpid',$data['composed_product'])->delete();
				foreach($data['item_name'] as $item_key => $item) {
				//for($i=1; $i <= $data['item_name']; $i++) {
					if(!empty($item)) {
						
						$composeProductItems = composeProductItems::select("*")
							->where("id", "=", $item_key)
							->first();
						if(!$composeProductItems) {
							$composeProductItems = new ComposeProductItems();
						}
						
						$item_img = $request->file("item_img");
						
						if(isset($item_img[$item_key])) {
							$item_img_name = $item_img[$item_key];
							$file_name = str_random(5)."-".date('his')."-".str_random(3)."." . $item_img_name->getClientOriginalExtension();
							$item_img[$item_key]->storeAs("products", $file_name, 'uploads');
						} else {
							$file_name = "";
							if(isset($composeProductItems->image) AND !empty($composeProductItems->image))
								$file_name = $composeProductItems->image;
						}
						
						$composeProductItems->restaurant_id = $is_vendor->id;
						$composeProductItems->name = $item;
						$composeProductItems->price = $data['item_price'][$item_key];
						$composeProductItems->image = $file_name;
						$composeProductItems->cpid = $data['composed_product'];
						$composeProductItems->save();
						$composeProductItems_id = $composeProductItems->id;
					}
				}
			}
			//----------------------------------------------------------------------------------------------	
		}
		
		  return  redirect(route('add-compose-quantites',['product_id'=>$data['composed_product']]));
		
    }
	function getAjaxComposeProductQuantites(Request $request)
    {
		$data['title'] = 'Add Compose Product';
        $data['breadcrums'] = 'Add Compose Product';
        $data = $request->all();
        //print_r($data['cpid']);exit;
		$is_vendor = Session::get('is_vendor');
		if($data['cpid'] > 0) {
			$composeProduct = ComposeProduct::query()
            ->select("id", "status","type")
            ->where("id", "=", $data['cpid'])
            ->first();
			$data['composed_products'] = $composeProduct;	
			
			if($composeProduct) {
				$composeQuantites = ComposeQuantites::select('*')
					->where("restaurant_id", "=", $is_vendor->id)
					->where("cpid", "=", $data['cpid'])
					->orderBY('id', 'ASC')
					->get()
					->all();
				$data['composeQuantites'] = $composeQuantites;	
				
				$composeProductItems = ComposeProductItems::select('*')
					->where("restaurant_id", "=", $is_vendor->id)
					->where("cpid", "=", $data['cpid'])
					->orderBY('id', 'ASC')
					->get()
					->all();
				// Incase of mixture product	
				if($composeProduct['type'] == 2) {
					foreach($composeProductItems as $composeProductItem) {
						$composeProductSubItems = ComposeProductSubItems::select('*')
						->where("restaurant_id", "=", $is_vendor->id)
						->where("cpiid", "=", $composeProductItem->id)
						->orderBY('id', 'ASC')
						->get()
						->all();
						$composeProductItem->sub_item = $composeProductSubItems;
					}
				}
				
				$data['composeProductItems'] = $composeProductItems;		
			}
		}
		//$html = view('vendor/compose-quantity/ajax-product-item-fields',$data);
		$returnItemHTML = view('vendor/compose-quantity/ajax-product-item-fields')->with('data22',$data)->render();
		$returnQuantityHTML = view('vendor/compose-quantity/ajax-quantity-fields')->with('data',$data)->render();
		$msg = array(
              'status' => 'success',
			  'quantityHtml' => $returnQuantityHTML,
			  'itemHtml' => $returnItemHTML,
          );
       return response()->json(['message' => $msg],200);
       // return ['success' => true, 'data' => $msg];
	}
	function mixture_item_setting(Request $request)
    {
        
		$data = $request->all();
		$is_vendor = Session::get('is_vendor');
		$product_id = $_GET['product_id'];
		$product_id = $product_id = base64_decode($product_id);
        $product_id = explode(":", $product_id)[0];
		if($product_id > 0) {	
			$composeQuantites = ComposeQuantites::select('*')
				->where("restaurant_id", "=", $is_vendor->id)
				->where("cpid", "=", $product_id)
				->orderBY('id', 'ASC')
				->get()
				->all();
			$data['composeQuantites'] = $composeQuantites;	
			
			$composeProductItems = ComposeProductItems::select('*')
				->where("restaurant_id", "=", $is_vendor->id)
				->where("cpid", "=", $product_id)
				->orderBY('id', 'ASC')
				->get()
				->all();
			$data['composeProductItems'] = $composeProductItems;		
			
		}
		return view('vendor/compose-product/mixture-item/item-listing',$data); 
	}
	
	function deleteComposeItemsQuantites(Request $request)
    {
		$data = $request->all();
		if(isset($data['id']) AND $data['id'] > 0) {
			if(isset($data['type']) AND $data['type'] == "item") {
				ComposeProductItems::where('id',$data['id'])->delete();
			}
			if(isset($data['type']) AND $data['type'] == "quantity") {
				ComposeQuantites::where('id',$data['id'])->delete();
			}
		}
	}
	
	function deleteComposeItemsImage(Request $request)
    {
		$data = $request->all();
		if(isset($data['id']) AND $data['id'] > 0) {
			DB::table('compose_product_items')
            ->where('id', $data['id'])
            ->update(['image' => ""]);
			return $data['id']; 
		}
	}
	
	function updateComposeItemsQuantites(Request $request)
    {
		$data = $request->all();
		//print_r($data['item_arr']);exit;
		if(count($data['item_arr']) > 0 AND $data['id'] > 0) {
			foreach($data['item_arr'] as $item) {
				//echo $item['quantity_id']."----------".$item['quantity_val'];
				$quantity_arr[$item['quantity_id']] = $item['quantity_val'];
			}
			$final_arr[$data['id']] = $quantity_arr;
				DB::table('compose_product_items')
				->where('id', $data['id'])
				->update(['ingredients' => json_encode($final_arr)]);
		}
		
	}
	
	//----------------------------------------- Mixture item groups
	
	public function composeItemGroupListing(Request $request)
    {
        $data = $request->all();
		$data['title'] = '';
        $data['breadcrums'] = '';
		
		$is_vendor = Session::get('is_vendor');
		 
        $composeGroups = composeGroup::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
			->where("cpiid", "=", $data['item-id'])
            ->orderBY('id', 'DESC')
            ->paginate(15);	
        $data['composeGroups'] = $composeGroups;
        return view('vendor/compose-product/mixture-group/group-listing',$data);
    }
	public function addComposeItemGroup()
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
		//echo "hereeeeee";exit;
       return view('vendor/compose-product/mixture-group/add-group',$data);
    }
	function addComposeItemGroupSubmit(Request $request)
    {
        $data = $request->all();
		//print_r($is_vendor);exit;
        $this->validate($request, [
            'title' => 'required|min:2|max:255',
        ]);
		if($data['item-id'] > 0) {
			$is_vendor = Session::get('is_vendor');
			$composeGroup = new composeGroup();
			$composeGroup->title = $data['title'];
			$composeGroup->cpiid = isset($data['item-id']) ? $data['item-id'] : '';
			$composeGroup->restaurant_id = $is_vendor->id;
			$composeGroup->save();
			return redirect(route('compose-item-group-listing', ['item-id' => $data['item-id']]))->with('success', 'Created successfully');
		}
    }
	function editComposeItemGroup(Request $request)
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
		$is_vendor = Session::get('is_vendor');

        $group_id = $_GET['group-id'];
        $group_id = base64_decode($group_id);
        $group_id = explode(":", $group_id);
        $composeGroup = composeGroup::select('*')
            ->where("id", "=", $group_id[0])
            ->where('restaurant_id', '=',$is_vendor->id)
            ->first();
        if(!$composeGroup) {
            return view('errors/404');
            exit;
        }
        $data['composeGroup'] = $composeGroup;
        return view('vendor/compose-product/mixture-group/edit-group',$data);
    }
	
	 function editComposeItemGroupSubmit(Request $request)
    {
        $data = $request->all();
        $is_vendor = Session::get('is_vendor');
        $this->validate($request, [
            'title' => 'required|min:2|max:255',
        ]);
		
        $group_id = $data['group_id'];
		$item_id = $data['item_id'];

        if ($group_id != '') {
            $composeGroup = composeGroup::select("*")
                ->where("id", "=", $group_id)
                ->first();
        }
		
		$composeGroup->title = $data['title'];
        $composeGroup->save();
       return redirect(route('compose-item-group-listing', ['item-id' => $item_id]))->with('success', 'Updated successfully');
    }
	
	//------------------------------------------- Mixture sub item  -------------------------------------------------- 
	
	public function composeSubItemListing(Request $request)
    {
        $data = $request->all();
		$data['title'] = '';
        $data['breadcrums'] = '';
		
		$is_vendor = Session::get('is_vendor');
		 
        $composeProductSubItems = ComposeProductSubItems::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
			->where("cpiid", "=", $data['item-id'])
            ->orderBY('id', 'DESC')
            ->get();	
        $data['composeProductSubItems'] = $composeProductSubItems;
        return view('vendor/compose-product/mixture-sub-item/sub-item-listing',$data);
    }
	public function addComposeSubItem(Request $request)
    {
        $data = $request->all();
		$is_vendor = Session::get('is_vendor');
        $composeGroups = composeGroup::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
			->where("cpiid", "=", $data['item-id'])
            ->orderBY('id', 'DESC')
            ->get()
			->all();	
        $data['composeGroups'] = $composeGroups;
		$data['title'] = '';
        $data['breadcrums'] = '';
       return view('vendor/compose-product/mixture-sub-item/add-sub-item',$data);
    }
	function addComposeSubItemSubmit(Request $request)
    {
        $data = $request->all();
		//print_r($is_vendor);exit;
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
        ]);
		if($data['item_id'] > 0) {
			$is_vendor = Session::get('is_vendor');
			
				$file_name = '';
				$file = $request->file("sub_item_img");
				if ($file) {
					$file_name = "product_" . time() . "." . $file->getClientOriginalExtension();
					$file->storeAs("products", $file_name, 'uploads');
				}
				
				
				$composeProductSubItems = new ComposeProductSubItems();
				$composeProductSubItems->restaurant_id = $is_vendor->id;
				$composeProductSubItems->cgid = $data['group_id']; 
				$composeProductSubItems->name = $data['name'];
				$composeProductSubItems->image = $file_name;
				$composeProductSubItems->cpiid = $data['item_id'];
				$composeProductSubItems->save();
				$composeProductSubItems_id = $composeProductSubItems->id;
			
			
			return redirect(route('compose-sub-item-listing', ['item-id' => $data['item_id']]))->with('success', 'Created successfully');
		}
    }
	function editComposeSubItem(Request $request)
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
		$data = $request->all();
		$is_vendor = Session::get('is_vendor');
         $item_id = $_GET['item_id'];
        $item_id = base64_decode($item_id);
        $item_id = explode(":", $item_id);
		
		$composeGroups = composeGroup::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
			->where("cpiid", "=",  $item_id[1])
            ->orderBY('id', 'DESC')
            ->get()
			->all();	
        $data['composeGroups'] = $composeGroups;
		
		
        $composeProductSubItems = ComposeProductSubItems::select('*')
            ->where("id", "=", $item_id[0])
            ->where('restaurant_id', '=',$is_vendor->id)
            ->first();
			
        if(!$composeProductSubItems) {
            return view('errors/404');
            exit;
        }
        $data['composeProductSubItems'] = $composeProductSubItems;
        return view('vendor/compose-product/mixture-sub-item/edit-sub-item',$data);
    }
	
	function editComposeSubItemSubmit(Request $request)
    {
		
		   $data = $request->all();
		$is_vendor = Session::get('is_vendor');
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
        ]);
		if($data['sub_item_id'] > 0) {
			
			$composeProductSubItems = ComposeProductSubItems::select('*')
            ->where("id", "=", $data['sub_item_id'])
            ->where('restaurant_id', '=',$is_vendor->id)
            ->first();
			
			
		
			$file_name = '';
			$file = $request->file("sub_item_img");
			if ($file) {
				if(isset($data['old_image'])) {
					$olds =  $data['old_image'];
					Storage::disk('uploads')->delete('products/' . $olds);
				}
				
				$file_name = "product_" . time() . "." . $file->getClientOriginalExtension();
				$file->storeAs("products", $file_name, 'uploads');
			} else {
				$file_name = $composeProductSubItems->image;
			}
			$composeProductSubItems->cgid = $data['group_id']; 
			$composeProductSubItems->name = $data['name'];
			$composeProductSubItems->image = $file_name;
			$composeProductSubItems->save();
			return redirect(route('compose-sub-item-listing', ['item-id' => $data['item_id']]))->with('success', 'Updated successfully');
		}
    }
	
}
