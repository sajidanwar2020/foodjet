<?php
namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use App\Models\Restaurant;
use App\Models\Users;
use App\Models\Products;
use App\Models\Orders;
use App\Models\Pages;
use App\Models\Homepages;
use App\Models\OrderItems;
use App\Models\UsersDetails;
use http\Client\Curl\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Storage;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Validator;

class HomePagesController extends Controller
{
    public function homePages(Request $request){
    	if($request->IsMethod("get")){
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];
	    	$homepage = Homepages::select('*')->where('restaurant_id','=',$restaurant_id)
	            ->paginate(15);

        	return view('vendor/mypages/homepage',array('homepages'=> $homepage));
    	}
    }

    public function addHomepage(Request $request){
    	if($request->IsMethod("get")){
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];
	    	$homepage = Homepages::select('*')->where('restaurant_id','=',$restaurant_id)
	            ->get();
	        /*check for having only one occurence of homepage*/
	        if(count($homepage) <= 0){
        		return view('vendor/mypages/addhomepage',array());
	        }
	        else{
	        	$responseText = "Homepage Already Exist!";
	        	\Session::flash('responseText',$responseText);

	        	return redirect(route('homepages'));
        	}
    	}
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'page_title' => 'required',
                'description' => 'required',
	            'status' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}

	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

	        $logo_image = NULL;
	        $headerImage = NULL;

	        if($request->hasfile('logo')){
	        	$file = $request->file("logo");
                //print_r($file);exit;
                $logo_image = "logo" . "_" . time() . "." . $file->getClientOriginalExtension();
                $file->storeAs("vendor/logo", $logo_image, 'uploads');
	        }
	        if($request->hasfile('header_image')){
	        	$file = $request->file("header_image");
                //print_r($file);exit;
                $headerImage = "header-image" . "_" . time() . "." . $file->getClientOriginalExtension();
                $file->storeAs("vendor/header_banner", $headerImage, 'uploads');
	        }

        	$newPage = new Homepages();
        	$newPage->restaurant_id = $restaurant_id;
        	$newPage->title = $request->page_title;
        	$newPage->logo = $logo_image;
        	$newPage->meta_title = $request->meta_title;
        	$newPage->meta_keywords = $request->meta_keywords;
        	$newPage->meta_description = $request->meta_description;
        	$newPage->description = $request->description;
        	$newPage->header_image = $headerImage;
        	$newPage->header_uppertitle = $request->header_uppertitle;
        	$newPage->header_title = $request->header_title;
        	$newPage->header_subtitle = $request->header_subtitle;
        	$newPage->header_subdescriptions = $request->header_subdescriptions;
        	$newPage->footer_color = $request->footer_color;
        	$newPage->footer_phone1 = $request->footer_phone1;
        	$newPage->footer_phone2 = $request->footer_phone2;
        	$newPage->footer_address = $request->footer_address;
        	$newPage->footer_html = $request->footer_html;
	        $newPage->footer_starttime = $request->footer_starttime;
	        $newPage->footer_breaktime = $request->footer_breaktime;
	        $newPage->footer_endtime = $request->footer_endtime;
        	$newPage->status = $request->status;
        	$newPage->save();

        	$responseText = "New homepage has added successfully";
        	\Session::flash('responseText',$responseText);

        	return redirect(route('homepages'));
    	}
    }



    public function statusOn(Request $request)
    {
    	if($request->IsMethod("get")){
	        $page_id = $_GET['page_id'];
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];

	    	$pages = Homepages::find($page_id);
	    	if($pages){
	    		$pages->status = 1;
	    		$pages->save();

	    		$responseText = "Homepage Activated Successfully!";
	    		\Session::flash('responseText',$responseText);
	    	}
	    	else{
	    		$responseText = "Homepage Activation Failed!";
	    		\Session::flash('responseText',$responseText);
	    	}
	        return redirect(route('homepages'));
    	}
    }

    public function statusOff(Request $request)
    {
    	if($request->IsMethod("get")){
	        $page_id = $_GET['page_id'];
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];

	    	$pages = Homepages::find($page_id);
	    	if($pages){
	    		$pages->status = 0;
	    		$pages->save();

	    		$responseText = "Homepage De-Activated Successfully!";
	    		\Session::flash('responseText',$responseText);
	    	}
	    	else{
	    		$responseText = "Homepage De-Activation Failed!";
	    		\Session::flash('responseText',$responseText);
	    	}
	        return redirect(route('homepages'));
    	}
    }

    public function editPage(Request $request){
    	if($request->IsMethod("get")){
	        $Encpage_id = $_GET['page_id'];
	        $page_id = base64_decode($Encpage_id);
	        $page_id = explode(":", $page_id)[0];

	        $findPage = Homepages::find($page_id);
	        if($findPage){
        		$page = $findPage;

        		return view('vendor/mypages/edithomepage',array('page_id'=>$Encpage_id,'page'=>$page));
	        }
	        else{
            	return redirect()->back()->withErrors(['error' => 'Unable to open page for edition.']);
	        }

    	}
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'id' => 'required',
	            'page_title' => 'required',
                'description' => 'required',
	            'status' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}

	        $page_id = $request->id;
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];
	        
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

        	$editPage = Homepages::find($page_id);
        	if($editPage){

		        $logo_image = NULL;
		        $headerImage = NULL;

		        if($request->hasfile('logo')){
		        	$file = $request->file("logo");
	                //print_r($file);exit;
	                $logo_image = "logo" . "_" . time() . "." . $file->getClientOriginalExtension();
	                $file->storeAs("vendor/logo", $logo_image, 'uploads');
		        }
		        if($request->hasfile('header_image')){
		        	$file = $request->file("header_image");
	                //print_r($file);exit;
	                $headerImage = "header-image" . "_" . time() . "." . $file->getClientOriginalExtension();
	                $file->storeAs("vendor/header_banner", $headerImage, 'uploads');
		        }

	        	$editPage->restaurant_id = $restaurant_id;
	        	$editPage->title = $request->page_title;
        		$editPage->logo = $logo_image;
	        	$editPage->meta_title = $request->meta_title;
	        	$editPage->meta_keywords = $request->meta_keywords;
	        	$editPage->meta_description = $request->meta_description;
	        	$editPage->description = $request->description;
        		$editPage->header_image = $headerImage;
	        	$editPage->header_uppertitle = $request->header_uppertitle;
	        	$editPage->header_title = $request->header_title;
	        	$editPage->header_subtitle = $request->header_subtitle;
	        	$editPage->header_subdescriptions = $request->header_subdescriptions;
	        	$editPage->footer_color = $request->footer_color;
	        	$editPage->footer_phone1 = $request->footer_phone1;
	        	$editPage->footer_phone2 = $request->footer_phone2;
	        	$editPage->footer_address = $request->footer_address;
	        	$editPage->footer_html = $request->footer_html;
	        	$editPage->footer_starttime = $request->footer_starttime;
	        	$editPage->footer_breaktime = $request->footer_breaktime;
	        	$editPage->footer_endtime = $request->footer_endtime;
	        	$editPage->status = $request->status;
	        	$editPage->save();

	        	$responseText = "Homepage has updated successfully";
	        	\Session::flash('responseText',$responseText);

	        	return redirect(route('homepages'));
        	}
        	else{
            	return redirect()->back()->withErrors(['error' => 'Unable to update page.']);
        	}
    	}
    }

    public function deletePage(Request $request){
    	if($request->IsMethod("get")){
	        $Encpage_id = $_GET['page_id'];
	        $page_id = base64_decode($Encpage_id);
	        $page_id = explode(":", $page_id)[0];

	        $findPage = Homepages::find($page_id);
	        if($findPage){
	        	$findPage->delete();

	        	$responseText = "Page deleted successfully";
	        	\Session::flash('responseText',$responseText);

        		return redirect(route('homepages'));
	        }
	        else{
            	return redirect()->back()->withErrors(['error' => 'Unable to find page.']);
	        }
    	}
    }
}
