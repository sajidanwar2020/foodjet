<?php
namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use App\Models\Restaurant;
use App\Models\Users;
use App\Models\Products;
use App\Models\Orders;
use App\Models\Pages;
use App\Models\Homepages;
use App\Models\OrderItems;
use App\Models\UsersDetails;
use http\Client\Curl\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Storage;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Validator;

class PagesController extends Controller
{
    public function index(Request $request)
    {
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
    	$pages = Pages::select('*')->where('restaurant_id','=',$restaurant_id)
            ->paginate(15);

        $data['pages'] = $pages;
        return view('vendor/mypages/my-pages',$data);
    }
	

    public function statusOn(Request $request)
    {
    	if($request->IsMethod("get")){
	        $page_id = $_GET['page_id'];
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];

	    	$pages = Pages::find($page_id);
	    	if($pages){
	    		$pages->status = 1;
	    		$pages->save();

	    		$responseText = "Page Activated Successfully!";
	    		\Session::flash('responseText',$responseText);
	    	}
	    	else{
	    		$responseText = "Page Activation Failed!";
	    		\Session::flash('responseText',$responseText);
	    	}
	        return redirect(route('mypages'));
    	}
    }

    public function statusOff(Request $request)
    {
    	if($request->IsMethod("get")){
	        $page_id = $_GET['page_id'];
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];

	    	$pages = Pages::find($page_id);
	    	if($pages){
	    		$pages->status = 0;
	    		$pages->save();

	    		$responseText = "Page De-Activated Successfully!";
	    		\Session::flash('responseText',$responseText);
	    	}
	    	else{
	    		$responseText = "Page De-Activation Failed!";
	    		\Session::flash('responseText',$responseText);
	    	}
	        return redirect(route('mypages'));
    	}
    }

    public function addPage(Request $request){
    	if($request->IsMethod("get")){
        	return view('vendor/mypages/add_page');
    	}
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'page_title' => 'required',
                'description' => 'required',
	            'status' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}

	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

        	$newPage = new Pages();
        	$newPage->restaurant_id = $restaurant_id;
        	$newPage->title = $request->page_title;
        	$slug = str_replace(' ', '-', $request->page_title);
        	$newPage->slug = $slug;
        	$newPage->meta_title = $request->meta_title;
        	$newPage->meta_keywords = $request->meta_keywords;
        	$newPage->meta_description = $request->meta_description;
        	$newPage->description = $request->description;
        	$newPage->status = $request->status;
        	$newPage->save();

        	$responseText = "New Page has added successfully";
        	\Session::flash('responseText',$responseText);

        	return redirect(route('mypages'));
    	}
    }

    public function editPage(Request $request){
    	if($request->IsMethod("get")){
	        $Encpage_id = $_GET['page_id'];
	        $page_id = base64_decode($Encpage_id);
	        $page_id = explode(":", $page_id)[0];

	        $findPage = Pages::find($page_id);
	        if($findPage){
        		$page = $findPage;
        		return view('vendor/mypages/edit_page',array('page_id'=>$Encpage_id,'page'=>$page));
	        }
	        else{
            	return view('vendor/mypages/add_page');
	        }

    	}
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'id' => 'required',
	            'page_title' => 'required',
                'description' => 'required',
	            'status' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}


	        $page_id = $request->id;
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];
	        
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

        	$editPage = Pages::find($page_id);
        	if($editPage){
        		$editPage->restaurant_id = $restaurant_id;
	        	$editPage->title = $request->page_title;
	        	$slug = str_replace(' ', '-', $request->page_title);
	        	$editPage->slug = $slug;
	        	//$editPage->meta_title = $request->meta_title;
	        	//$editPage->meta_keywords = $request->meta_keywords;
	        	//$editPage->meta_description = $request->meta_description;
	        	$editPage->description = $request->description;
	        	$editPage->status = $request->status;
	        	$editPage->save();

	        	$responseText = "Page has updated successfully";
	        	\Session::flash('responseText',$responseText);

	        	return redirect(route('mypages'));
        	}
        	else{
            	return redirect()->back()->withErrors(['error' => 'Unable to update page.']);
        	}
    	}
    }


    public function deletePage(Request $request){
    	if($request->IsMethod("get")){
	        $Encpage_id = $_GET['page_id'];
	        $page_id = base64_decode($Encpage_id);
	        $page_id = explode(":", $page_id)[0];

	        $findPage = Pages::find($page_id);
	        if($findPage){
	        	$findPage->delete();

	        	$responseText = "Page deleted successfully";
	        	\Session::flash('responseText',$responseText);

        		return redirect(route('mypages'));
	        }
	        else{
            	return redirect()->back()->withErrors(['error' => 'Unable to find page.']);
	        }
    	}
    }
}
