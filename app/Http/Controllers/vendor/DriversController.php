<?php

namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Drivers;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Category;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Storage;
class DriversController extends Controller
{
    public function add_driver()
    {
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data['title'] = 'Add driver';
        $data['breadcrums'] = 'Add driver';
        return view('vendor/driver/add_driver',$data);
    }

    function driver_submitted(Request $request)
    {
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data = $request->all();
        $this->validate($request, [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|unique:users|min:3|max:255',
            'phone_number' => 'required|unique:users|min:5|max:255',
            'location' => 'required|min:1|max:255',
            'salutation' => 'required|min:1|max:255',
            'dob' => 'required|min:3|max:255',
        ]);

        $data['password'] = '123123';
        $password = crypt($data['password'], md5($data['password']));
        $user = new Users();
        $user->email = $data['email'];
        $user->phone_number = $data['phone_number'];
        $user->role_id = $data['user_type'];
        $user->password = $password;
        $user->role_id = $data['user_type'];
        //$user->verification_status = '1';
        $user->status = '1';
        $user->save();

        $profile = new UsersDetails();
        // $currlanguage = (new Countries())->getCurrencyLanguage($data['country_iso']);
        $profile->user_id = $user->id;
        $profile->first_name = $data['first_name'];
        $profile->last_name = $data['last_name'];
        $profile->location = $data['location'];
        $profile->date_of_birth = $data['dob'];
        $profile->latitude = '0';
        if(isset($data['gmap_latitude']))
            $profile->latitude = $data['gmap_latitude'];
        $profile->longitude = '0';
        if(isset($data['gmap_longitude']))
            $profile->longitude = $data['gmap_longitude'];
        //$profile->country = '';
        if(isset($data['country_iso']))
            $profile->country = $data['country_iso'];

        $profile->salutation = $data['salutation'];
        $profile->company = $data['company'];
        $profile->country_id = 1;
        $profile->phone_number = $data['phone_number'];
        $profile->vehicle_no = $data['vehicle_no'];
        $profile->tax_id = '0';
        $profile->save();
        return redirect(route('driver_listing'))->with('info', "User is created");
    }

    function edit_driver_submitted(Request $request)
    {
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data = $request->all();
        $this->validate($request, [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|min:3|max:255',
            'phone_number' => 'required||min:5|max:255',
            'location' => 'required|min:1|max:255',
            'salutation' => 'required|min:1|max:255',
            'dob' => 'required|min:3|max:255',
        ]);
          //  echo "here";exit;
        //$data['password'] = '123123';
       // $password = crypt($data['password'], md5($data['password']));
        //$user = new Users();
        $driver_id = $data['driver_id'];

        $user = Users::select('*')
            ->where("id", "=", $driver_id)
            ->first();
        $profile = UsersDetails::select('*')
            ->where("user_id", "=", $driver_id)
            ->first();
        //  print_r($driver);exit;
        if(!$user) {
            $data['title'] = 'driver';
            $data['breadcrumbs'] = 'driver';
            return view('vendor/products/unauthorized', $data);
        }
        $user->email = $data['email'];
        $user->phone_number = $data['phone_number'];
        //$user->verification_status = '1';
        $user->status = '1';
        $user->save();

        $profile->first_name = $data['first_name'];
        $profile->last_name = $data['last_name'];
        $profile->location = $data['location'];
        $profile->date_of_birth = $data['dob'];
        $profile->latitude = '0';
        if(isset($data['gmap_latitude']))
            $profile->latitude = $data['gmap_latitude'];
        $profile->longitude = '0';
        if(isset($data['gmap_longitude']))
            $profile->longitude = $data['gmap_longitude'];
        //$profile->country = '';
        if(isset($data['country_iso']))
            $profile->country = $data['country_iso'];

        $profile->salutation = $data['salutation'];
        $profile->company = $data['company'];
        $profile->country_id = 1;
        $profile->phone_number = $data['phone_number'];
        $profile->vehicle_no = $data['vehicle_no'];
        $profile->save();
        return back()->with('success', "Driver updated");
    }

    public function driver_listing()
    {
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data['title'] = 'All Driver';
        $data['breadcrums'] = 'All Driver';
       // $status = isset($_GET['status']) ? $_GET['status'] : "0,1,2,3";
        //$status = explode(",", $status);
       // $info = Session::get('is_logged_in');
        $is_seller = Session::get('is_seller');
        $drivers = Users::select('*')
            ->where("role_id", "=", 4)
            ->join('user_details as d', 'users.id', '=', 'd.user_id')
            ->orderBY('users.id', 'DESC')
            ->paginate(15);
        $data['drivers'] = $drivers;
        return view('vendor/driver/driver-listing',$data);
    }

    public function edit_driver()
    {
      // echo "hereeeee";exit;
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data['title'] = 'Edit driver';
        $data['breadcrums'] = 'Edit driver';
        //echo "hereeee";exit;
        //$info = Session::get('is_logged_in');
        //$is_seller = Session::get('is_seller');
        $driver_id = $_GET['driver_id'];
        $driver = Users::select('*')
            ->join('user_details as d', 'users.id', '=', 'd.user_id')
            ->where("users.id", "=", $driver_id)
            //->where('created_by', '=',$is_seller->id)
            ->first();
      //  print_r($driver);exit;
        if(!$driver) {
            $data['title'] = 'driver';
            $data['breadcrumbs'] = 'driver';
            return view('vendor/products/unauthorized', $data);
        }
        $data['driver'] = $driver;
        return view('vendor/driver/edit_driver',$data);
    }

    function change_driver_status(Request $request)
    {
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data = $request->driver_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $driver_id = $data[0];
        $status = $data[2];
        $driver = Users::query()
            ->select("id", "status")
            ->where("id", "=", $driver_id)
            ->first();
        $driver->status = $status;
        $driver->save();
        if ($status == 2) {
            return redirect(route('driver_listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('driver_listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 0) {
            return redirect(route('driver_listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('driver_listing'));
    }


}
