<?php

namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Category;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Storage;
class ProductsController extends Controller
{
    public function addProduct()
    {
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data['title'] = 'Add Product';
        $data['breadcrums'] = 'Add Product';
        $categories = getParentChildCategory();
        $data['categories'] = $categories;
        return view('vendor/products/add_product',$data);
    }

    function productSubmitted(Request $request)
    {
        $data = $request->all();
       // $info = Session::get("is_logged_in");
        $is_seller = Session::get('is_seller');
        $this->validate($request, [
            'product_name' => 'required|min:2|max:255',
            'product_description' => 'required|min:10|max:1000',
            'regular_price' => 'required',
            'quantity_unit' => 'required',
            'parent_category' => 'required',
            'child_category' => 'required',
        ]);

        $products = new Products();
        $products->product_name = $data['product_name'];
        $products->product_description = $data['product_description'];
        $products->seller_id = $is_seller->id;
        $products->regular_price = $data['regular_price'];
        $products->sale_price = $data['sale_price'];
        $products->quantity_unit = $data['quantity_unit'];
        $products->parent_category_id = $data['parent_category'];
        $products->category_id = $data['child_category'];
        $products->status = '0';
        $products->save();

        $product_id = $products->id;

        $size = count($_FILES);

        if ($size > 0) {
           // print_r($size);exit;
            for ($x = 0; $x < $size; $x++) {
                $file = $request->file("product_image$x");
                //print_r($file);exit;
                $file_name = "product" . $product_id . "_" . time() . "." . $file->getClientOriginalExtension();
                $file->storeAs("products", $file_name, 'uploads');
                $productsImages = new ProductsImages();
                $productsImages->product_id = $product_id;
                $productsImages->image_name = $file_name;
                $productsImages->is_default = $x == 0 ? 1 : 0;
                $productsImages->save();
            }
        }

        return redirect(route('my-products'))->with('info', "Product created");
       // return redirect(route('edit_product',['product_id'=>base64_encode($product_id.":".$info['user_id'])]))->with('info', "Product created");
    }

    function editProductSubmitted(Request $request)
    {
        $data = $request->all();
       // $info = Session::get("is_logged_in");
        //print_r($info);exit;
        $is_seller = Session::get('is_seller');
        if($is_seller['role_id'] != 3) {
            return redirect(route('home'));
        }

        $this->validate($request, [
            'product_name' => 'required|min:2|max:255',
            'product_description' => 'required|min:10|max:1000',
            'regular_price' => 'required',
            'quantity_unit' => 'required',
            'parent_category' => 'required',
            'child_category' => 'required',
        ]);
        $product_id = $data['product_id'];

        if ($product_id != '') {
            $products = Products::select("*")
                ->where("id", "=", $product_id)
                ->first();
        }

        $products->product_name = $data['product_name'];
        $products->product_description = $data['product_description'];
        $products->seller_id = $is_seller->id;
        $products->regular_price = $data['regular_price'];
        $products->sale_price = $data['sale_price'];
        $products->quantity_unit = $data['quantity_unit'];
        $products->parent_category_id = $data['parent_category'];
        $products->category_id = $data['child_category'];
       // $products->status = '1';
        $products->save();

        $product_id = $products->id;
        $size = count($_FILES);
        $file = $_FILES['product_image0']['tmp_name'];
        if ($file) {
            if(isset($data['old_image_id'])) {
                $olds =  $data['old_image_id'];
                $imgs = ProductsImages::query()->selectRaw("id, image_name")->where("product_id", $olds)->get();
                //print_r($imgs);exit;
                foreach ($imgs as $img) {
                        Storage::disk('uploads')->delete('products/' . $img->image);

                }
                ProductsImages::query()->where('product_id', $product_id)
                    ->where('id', $olds)
                    ->delete();
            }

            for ($x = 0; $x < $size; $x++) {
                $file = $request->file("product_image$x");
                if($file) {
                    $file_name = "product" . $product_id . "_" . time() . "." . $file->getClientOriginalExtension();
                    $file->storeAs("products", $file_name, 'uploads');
                    $productsImages = new ProductsImages();
                    $productsImages->product_id = $product_id;
                    $productsImages->image_name = $file_name;
                    $productsImages->is_default = $x == 0 ? 1 : 0;
                    $productsImages->save();
                }

            }
        }

        return back();
    }

    public function MyProducts()
    {
        $data['title'] = 'All Product';
        $data['breadcrums'] = 'All Product';
        $status = isset($_GET['status']) ? $_GET['status'] : "0,1,2,3";
        $status = explode(",", $status);
       // $info = Session::get('is_logged_in');
        $is_seller =  Session::get("is_seller");
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
       // $is_seller = Session::get('is_seller');
        $products = Products::select('*')
            //->where("status", "=", 1)
            ->where("seller_id", "=", $is_seller->id)
            ->orderBY('products.id', 'DESC')
            ->paginate(15);

        if($products) {
            foreach($products as $product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()->all();
                $product->product_images = $product_images;
            }
        }
        $data['products'] = $products;
        return view('vendor/products/my_products',$data);
    }

    public function change_product_status()
    {
        $data['title'] = 'Add Product';
        $data['breadcrums'] = 'Add Product';
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        return view('vendor/products/add_product',$data);
    }

    public function edit_product()
    {
        $data['title'] = 'Edit Product';
        $data['breadcrums'] = 'Edit Product';
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $info = Session::get('is_logged_in');
        $is_seller = Session::get('is_seller');
        $product_id = $_GET['product_id'];
        $product_id = base64_decode($product_id);
        $product_id = explode(":", $product_id)[0];
        //print_r($product_id);exit;
         /* $category = Category::select('*')
            ->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->get(); */

        $categories = getParentChildCategory();
        $data['categories'] = $categories;
        $products = Products::select('*')
            ->where("id", "=", $product_id)
            ->where('seller_id', '=',$is_seller->id)
            ->first();
        if(!$products) {
            $data['title'] = 'Products';
            $data['breadcrumbs'] = 'Products';
            return view('vendor/products/unauthorized', $data);
        }
        $data['product'] = $products;
        $data['product_images'] = ProductsImages::select("id", "image_name")
            ->where("product_id", "=", $product_id)
            ->orderBy("is_default", "DESC")
            ->get()->all();
        $childCategories = getChildCategories($products->parent_category_id);
        $data['child_categories'] = $childCategories;
        //print_r($data);exit;

        return view('vendor/products/edit_product',$data);
    }

    function change_offer_status(Request $request)
    {
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data = $request->product_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $product_id = $data[0];
        $status = $data[2];
        $products = Products::query()
            ->select("id", "status")
            ->where("id", "=", $product_id)
            ->first();
        $products->status = $status;
        $products->save();
        if ($status == 2) {
            return redirect(route('my-products', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('my-products', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('my-products', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('my-products'));
    }
    function child_category()
    {
        $parent_id = $_GET['parent_id'];
        $categories = Category::select('id', 'name')
            ->where('parent_id', '=', $parent_id)
            ->where('status', '=', '1')
            ->orderBY('id', 'ASC')
            ->get();
        $data['categories'] = $categories;
       // return view('Offers/ajax_categories', $data);
        return view('vendor/products/ajax_categories',$data);
    }
	function vendorImportProduct(Request $request)
    {
		$data['title'] = 'Import Product';
        $data['breadcrums'] = 'Import Product';
		$categories = Category::select('*')
        ->where("status", "=", 1)
		->where("parent_id", "=", 0)
        ->orderBY('id', 'DESC')
        ->get();
        $data['categories'] = $categories;
        return view('vendor/products/import-product',$data);
	}
	
	function vendorImportProductSubmitted(Request $request)
    {
		$data['title'] = 'Import Product';
        $data['breadcrums'] = 'Import Product';
		$data = $request->all();
		//echo "<pre>";
		//print_r($data);
		//exit;
		$file_dir = base_path('uploads').'/import-product/';
		//echo $inputFileName;exit;
		
		$file = $request->file("vendor-excel");
		$file_name = date("m-d-y").date("h-i-sa") . "." . $file->getClientOriginalExtension();
        $file->storeAs("import-product", $file_name, 'uploads');
		$file_path = $file_dir.$file_name;
		
		//echo $file_path;exit;
		//echo "<pre>";
		//print_r($file_name);
		//exit;
		$is_seller = Session::get('is_seller');
		$seller_id = $is_seller->id;
		//$file_path = $file_dir.'DryProductsTestforUpload.xlsx';
		try {
			$inputFileType = \PHPExcel_IOFactory::identify($file_path);
			$objReader = \PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($file_path);
		  } catch (Exception $e) {
			die('Error loading file "' . pathinfo($file_path, PATHINFO_BASENAME) . '": ' . 
				$e->getMessage());
		  }
			//echo "hereeeeee";exit;
		  $sheet = $objPHPExcel->getSheet(0);
		  $highestRow = $sheet->getHighestRow();
		  $highestColumn = $sheet->getHighestColumn();
		
		  for ($row = 1; $row <= $highestRow; $row++) { 
			$rowDatas = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, 
											null, true, false);
			$incrent = 10000 + $row;

				if( is_int((int)$row[0][0]) AND $rowDatas[0][4] > 0) {
					//echo "<pre>";
					//print_r($rowDatas);
					//exit;
					$myArray = explode(',', $rowDatas[0][2]);
					$products = new Products();
					$products->product_code = $rowDatas[0][0];
					$products->units_per_box = $myArray[0];
					$products->product_name = $rowDatas[0][1];
					$products->quantity = $rowDatas[0][3];
					$products->product_description = '';
					$products->seller_id = $seller_id;
					$products->regular_price = $rowDatas[0][4];
					$products->sale_price = 0.0;
					$products->quantity_unit = '';
					$products->parent_category_id = $data['parent_category'];
					$products->category_id = $data['child_category'];;
					$products->status = '1';
					$products->save();
					
					$product_id = $products->id;
					$productsImages = new ProductsImages();
					$productsImages->product_id = $product_id;
					$productsImages->image_name = $rowDatas[0][0].'.jpg';
					$productsImages->is_default = 1;
					$productsImages->save();
				}
				
			//}

		  }
		  //exit;
        return  redirect(route('my-products'));
	}
	
}
