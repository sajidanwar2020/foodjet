<?php

namespace App\Http\Controllers\vendor;

use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\IngredientsForWeb;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class IngredientsForWebController extends Controller
{
   	//---------------------------Start restaurant table ------------------------------------
	
	public function addIngredientsForWeb()
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
        return view('vendor/ingredients-for-web/add',$data);
    }

    function addIngredientsWebSubmitted(Request $request)
    {
        $data = $request->all();
        $is_vendor = Session::get('is_vendor');
         $this->validate($request, [
            'name' => 'required',
			'description' => 'required',
        ]);
        $ingredientsForWeb = new IngredientsForWeb();
		$ingredientsForWeb->restaurant_id = $is_vendor->id;
        $ingredientsForWeb->name = $data['name'];
		$ingredientsForWeb->description = $data['description'];
        $ingredientsForWeb->status = '1';
        $ingredientsForWeb->save();
        return redirect(route('ingredients-web'))->with('info', "ingredients-web created");
    }

    function editIngredientsWebSubmitted(Request $request)
    {
        $data = $request->all();
        $is_vendor = Session::get('is_vendor');
        $this->validate($request, [
            'name' => 'required',
			'description' => 'required',
        ]);
		$ingredientsForWeb = IngredientsForWeb::find($request->id);
        $ingredientsForWeb->name = $data['name'];
		$ingredientsForWeb->description = $data['description'];
        $ingredientsForWeb->status = '1';
        $ingredientsForWeb->save();
        //$restaurantTable->status = '1';
        $ingredientsForWeb->save();
        return back();
    }

    public function ingredientsWeb()
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
		$is_vendor = Session::get('is_vendor');
        $ingredients_for_webs = IngredientsForWeb::select('*')
            //->where("status", "=", 1)
            ->where("restaurant_id", "=", $is_vendor->id)
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $data['ingredients_for_webs'] = $ingredients_for_webs;
        return view('vendor/ingredients-for-web/ingredients-for-web',$data);
    }

    public function changeIngredientsWeb()
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
		$id = $_GET['id'];
		$id = base64_decode($id);
		//echo "<pre>";
        $id_arr = explode(":", $id);
		$ingredientsForWeb = IngredientsForWeb::find($id_arr[0]);
        $ingredientsForWeb->status = $id_arr[2];
        $ingredientsForWeb->save();
		return redirect(route('ingredients-web'));
    }

    public function editIngredientsWeb(Request $request)
    {
        $data['title'] = '';
        $data['breadcrums'] = '';
		$is_vendor = Session::get('is_vendor');
		$id = $_GET['id'];
		$id = base64_decode($id);
        $id = explode(":", $id)[0];
        $data['ingredients_for_web'] = IngredientsForWeb::select("*")->where('id','=',$id)->first();
        return view('vendor/ingredients-for-web/edit',$data);
    }
}
