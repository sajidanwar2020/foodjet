<?php
namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use App\Models\Restaurant;
use App\Models\Users;
use App\Models\Products;
use App\Models\Orders;
use App\Models\Pages;
use App\Models\Homepages;
use App\Models\LabelImage;
use App\Models\OrderItems;
use App\Models\UsersDetails;
use http\Client\Curl\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Storage;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Validator;

class LabelImagesController extends Controller
{
    public function labelImages(Request $request){
    	if($request->IsMethod("get")){
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];
	    	$homepage = LabelImage::select('*')->where('restaurant_id','=',$restaurant_id)
	            ->get();

        	return view('vendor/labelimages/labelimage',array('images'=> $homepage));
    	}
    }

    public function addLabelImages(Request $request){
    	if($request->IsMethod("get")){
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

        	return view('vendor/labelimages/labelimage',array('images'=> $homepage));

    	}
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'labelname' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}

	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

	        $logo_image = NULL;

	        if($request->hasfile('image')){
	        	$file = $request->file("image");

                $logo_image = "label-Images" . "_" . time() . "." . $file->getClientOriginalExtension();
                $file->storeAs("labelimages", $logo_image, 'uploads');
	        }

        	$newPage = new LabelImage();
        	$newPage->label_name = $request->labelname;
        	$newPage->restaurant_id = $restaurant_id;
        	$newPage->image_path = $logo_image;
        	$newPage->status = $request->status;
        	$newPage->save();

        	$responseText = "New Label Image has added successfully!";
        	\Session::flash('responseText',$responseText);

        	return redirect(route('label-images'));
    	}
    }


    public function editLabelImages(Request $request){
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'labelname' => 'required',
	            'labelid' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}

	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

	        $logo_image = NULL;

        	$newPage = LabelImage::find($request->labelid);
        	if($newPage){

		        if($request->hasfile('image')){
		        	$file = $request->file("image");
		        	
	                $logo_image = "label-Images" . "_" . time() . "." . $file->getClientOriginalExtension();
	                $file->storeAs("labelimages", $logo_image, 'uploads');
		        }
		        else{
		        	$logo_image = NULL;
		        }

	        	$newPage->label_name = $request->labelname;
	        	$newPage->restaurant_id = $restaurant_id;
	        	$newPage->image_path = ($logo_image) ? $logo_image : $newPage->image_path;
	        	$newPage->save();

	        	$responseText = "Label Image has updated successfully!";
	        	\Session::flash('responseText',$responseText);

	        	return redirect(route('label-images'));
        	}
        	else{
            	return redirect()->back()->withErrors(['error' => 'Couldn\'t update database!']);
        	}
    	}
    }

	public function deleteLabelImage(Request $request){
    	if($request->IsMethod("get")){
	        $validator = Validator::make($request->all(), [
	            'labelid' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}

	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

	        $logo_image = NULL;

        	$newPage = LabelImage::find($request->labelid);
        	if($newPage){

	        	$newPage->delete();

	        	$responseText = "Label Image deleted successfully!";
	        	\Session::flash('responseText',$responseText);

	        	return redirect(route('label-images'));
        	}
        	else{
            	return redirect()->back()->withErrors(['error' => 'Couldn\'t delete from database!']);
        	}
    	}
    }

    public function statusOn(Request $request)
    {
    	if($request->IsMethod("get")){
	        $page_id = $_GET['page_id'];
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];

	    	$pages = Homepages::find($page_id);
	    	if($pages){
	    		$pages->status = 1;
	    		$pages->save();

	    		$responseText = "Homepage Activated Successfully!";
	    		\Session::flash('responseText',$responseText);
	    	}
	    	else{
	    		$responseText = "Homepage Activation Failed!";
	    		\Session::flash('responseText',$responseText);
	    	}
	        return redirect(route('homepages'));
    	}
    }

    public function statusOff(Request $request)
    {
    	if($request->IsMethod("get")){
	        $page_id = $_GET['page_id'];
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];

	    	$pages = Homepages::find($page_id);
	    	if($pages){
	    		$pages->status = 0;
	    		$pages->save();

	    		$responseText = "Homepage De-Activated Successfully!";
	    		\Session::flash('responseText',$responseText);
	    	}
	    	else{
	    		$responseText = "Homepage De-Activation Failed!";
	    		\Session::flash('responseText',$responseText);
	    	}
	        return redirect(route('homepages'));
    	}
    }

    public function editPage(Request $request){
    	if($request->IsMethod("get")){
	        $Encpage_id = $_GET['page_id'];
	        $page_id = base64_decode($Encpage_id);
	        $page_id = explode(":", $page_id)[0];

	        $findPage = Homepages::find($page_id);
	        if($findPage){
        		$page = $findPage;

        		return view('vendor/mypages/edithomepage',array('page_id'=>$Encpage_id,'page'=>$page));
	        }
	        else{
            	return redirect()->back()->withErrors(['error' => 'Unable to open page for edition.']);
	        }

    	}
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'id' => 'required',
	            'page_title' => 'required',
                'description' => 'required',
	            'status' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}

	        $page_id = $request->id;
	        $page_id = base64_decode($page_id);
	        $page_id = explode(":", $page_id)[0];
	        
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

        	$editPage = Homepages::find($page_id);
        	if($editPage){

		        $logo_image = NULL;
		        $headerImage = NULL;

		        if($request->hasfile('logo')){
		        	$file = $request->file("logo");
	                //print_r($file);exit;
	                $logo_image = "logo" . "_" . time() . "." . $file->getClientOriginalExtension();
	                $file->storeAs("vendor/logo", $logo_image, 'uploads');
		        }
		        if($request->hasfile('header_image')){
		        	$file = $request->file("header_image");
	                //print_r($file);exit;
	                $headerImage = "header-image" . "_" . time() . "." . $file->getClientOriginalExtension();
	                $file->storeAs("vendor/header_banner", $headerImage, 'uploads');
		        }

	        	$editPage->restaurant_id = $restaurant_id;
	        	$editPage->title = $request->page_title;
        		$editPage->logo = $logo_image;
	        	$editPage->meta_title = $request->meta_title;
	        	$editPage->meta_keywords = $request->meta_keywords;
	        	$editPage->meta_description = $request->meta_description;
	        	$editPage->description = $request->description;
        		$editPage->header_image = $headerImage;
	        	$editPage->header_uppertitle = $request->header_uppertitle;
	        	$editPage->header_title = $request->header_title;
	        	$editPage->header_subtitle = $request->header_subtitle;
	        	$editPage->header_subdescriptions = $request->header_subdescriptions;
	        	$editPage->footer_color = $request->footer_color;
	        	$editPage->footer_phone1 = $request->footer_phone1;
	        	$editPage->footer_phone2 = $request->footer_phone2;
	        	$editPage->footer_address = $request->footer_address;
	        	$editPage->footer_html = $request->footer_html;
	        	$editPage->footer_starttime = $request->footer_starttime;
	        	$editPage->footer_breaktime = $request->footer_breaktime;
	        	$editPage->footer_endtime = $request->footer_endtime;
	        	$editPage->status = $request->status;
	        	$editPage->save();

	        	$responseText = "Homepage has updated successfully";
	        	\Session::flash('responseText',$responseText);

	        	return redirect(route('homepages'));
        	}
        	else{
            	return redirect()->back()->withErrors(['error' => 'Unable to update page.']);
        	}
    	}
    }

    public function deletePage(Request $request){
    	if($request->IsMethod("get")){
	        $Encpage_id = $_GET['page_id'];
	        $page_id = base64_decode($Encpage_id);
	        $page_id = explode(":", $page_id)[0];

	        $findPage = Homepages::find($page_id);
	        if($findPage){
	        	$findPage->delete();

	        	$responseText = "Page deleted successfully";
	        	\Session::flash('responseText',$responseText);

        		return redirect(route('homepages'));
	        }
	        else{
            	return redirect()->back()->withErrors(['error' => 'Unable to find page.']);
	        }
    	}
    }
}
