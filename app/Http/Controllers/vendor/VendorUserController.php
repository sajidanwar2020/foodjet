<?php

namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use App\Models\Users;
use App\Models\Products;
use App\Models\Orders;
use App\Models\OrderItems;
use App\Models\UsersDetails;
use http\Client\Curl\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;

class VendorUserController extends Controller
{
    public function vendor_dashboard()
    {
        $data['title'] = 'Dashboard';
        $data['breadcrums'] = 'Dashboard';
       // $is_logged_in =  Session::get("is_logged_in");
        $is_seller =  Session::get("is_seller");
        //print_r($is_logged_in);
        //exit;
        //print_r($is_seller->user_id);exit;
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] == 4)
        {
            return redirect('/driver_orders');
            exit;
        }
       // echo "here";exit;
        $data['total_products'] =  Products::where('seller_id','=',$is_seller->id)->count();
        $data['total_orders'] = DB::table('order_items')->where('seller_id','=',$is_seller->id)->count(DB::raw('DISTINCT order_id'));
        return view('vendor/dashboard/dashboard',$data);
    }
}
