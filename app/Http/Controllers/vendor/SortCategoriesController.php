<?php
namespace App\Http\Controllers\vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use App\Models\Restaurant;
use App\Models\Users;
use App\Models\Products;
use App\Models\Orders;
use App\Models\Pages;
use App\Models\Homepages;
use App\Models\VendorSortCategories;
use App\Models\LabelImage;
use App\Models\OrderItems;
use App\Models\UsersDetails;
use App\Models\Category;
use http\Client\Curl\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Storage;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Validator;

class SortCategoriesController extends Controller
{
    public function sortCategories(Request $request){
    	if($request->IsMethod("get")){
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];
	        $vendorCat = VendorSortCategories::select("*")->orderBy('sort_order','asc')->get();
	        if(!empty($vendorCat[0])){
	        	foreach($vendorCat as $eachCateg){
	        		$name = Category::select('*')->where('id','=',$eachCateg->category_id)->first();

	        		$eachCateg->name = (isset($name->name)) ? $name->name : NULL;

	        		$vendorChildCateg = VendorSortCategories::select("*")->where('child_of',$eachCateg->category_id)->where('restaurant_id','=',$restaurant_id)->where('sort_order','=',0)->orderBy('child_sort_order','asc')->get();

	        		if(!empty($vendorChildCateg) && isset($vendorChildCateg[0])){
	        			foreach($vendorChildCateg as $vChild){
	        				$name = Category::select('*')->where('id','=',$vChild->category_id)->first()->name;
	        				$vChild->name = $name;
	        			}
	        			$eachCateg->child_categories = $vendorChildCateg;
	        		}
	        		else{
	        			$eachCateg->child_categories = NULL;
	        		}
	        	}
        		return view('vendor/sort-categories/sortcategories',array('categories'=> $vendorCat));
	        }
	        else{
	        	$parent_categories = Category::select('*')
			        ->where("parent_id", "=", 0)
			        ->where("status", "=", 1)
			        ->orderBY('id', 'ASC')
			        ->get();
			    if($parent_categories) {
			        foreach($parent_categories as $parent_category) {
			            $child_categories = Category::select('*')
			                ->where("parent_id", "=", $parent_category->id)
			                ->where("status", "=", 1)
			                ->orderBY('id', 'ASC')
			                ->get();
			            $parent_category->child_categories = $child_categories;
			        }
			    }
			    
			    $sortOrder = 1;
			    foreach($parent_categories as $entry){
			    	if($entry->parent_id == 0){
				    	$eachCat = new VendorSortCategories();
				    	$eachCat->category_id = $entry->id;
				    	$eachCat->restaurant_id = $restaurant_id;
				    	$eachCat->sort_order = $sortOrder;
				    	$eachCat->child_of = 0;
				    	$eachCat->child_sort_order = 0;
				    	$eachCat->save();
				    	if(!empty($entry->child_categories) && isset($entry->child_categories[0]))
				    	{

				    		$childOrder = 1;
				    		foreach($entry->child_categories as $childCat){
				    			$eachChildCat = new VendorSortCategories();
						    	$eachChildCat->category_id = $childCat->id;
						    	$eachChildCat->restaurant_id = $restaurant_id;
						    	$eachChildCat->sort_order = 0;
						    	$eachChildCat->child_of = $eachCat->category_id;
						    	$eachChildCat->child_sort_order = $childOrder;
						    	$eachChildCat->save();

						    	$childOrder++;
				    		}
				    	}
			    	}
			    	$sortOrder++;
			    }
	        }
        	return view('vendor/sort-categories/sortcategories',array('categories'=> $vendorCat));
    	}
    }


    public function sortCategoriesUpdate(Request $request){
    	if($request->IsMethod("post")){
    		$validator = Validator::make($request->all(), [
	            'vendor_scategory_id' => 'required',
	            'sort_order_num' => 'required'
        	]);
        	if ($validator->fails()) {
            	return redirect()->back()->withErrors(['error' => '(*) Fields Required!.']);
        	}
	        $is_vendor =  Session::get("is_vendor");
	        $restaurant_id = $is_vendor['restaurant_id'];

	        $vendorCat = VendorSortCategories::select("*")->where('restaurant_id',$restaurant_id)->where('id',$request->vendor_scategory_id)->first();
	        if($vendorCat->sort_order != 0 && $vendorCat->child_of == 0){//Parent
	        	$allChild = VendorSortCategories::select("*")->where('restaurant_id',$restaurant_id)->where('child_of',0)->where('sort_order','!=',0)->orderBy('sort_order','asc')->get();
	        	$allToArray = $allChild->toArray();
	        	$arrCounter = 0;
	        	$myIndex = 0;
	        	foreach($allChild as $sing){
	        		if($sing->id == $request->vendor_scategory_id){
	        			$myIndex = $arrCounter;
	        		}
	        		$arrCounter++;
	        		$del = VendorSortCategories::find($sing->id)->delete();
	        	}
	        	$sortOrderNum = $request->sort_order_num-1;
	        	if($sortOrderNum > $arrCounter || $sortOrderNum < 0){
            		return redirect()->back()->withErrors(['error' => 'Invalid Range.']);
	        	}
	        	else{
	        		$myIndex;
	        		$temp = $allToArray[$sortOrderNum];
	        		$temp['sort_order'] = $myIndex;
	        		$allToArray[$sortOrderNum] = $allToArray[$myIndex];
	        		$allToArray[$sortOrderNum]['sort_order'] = $request->sort_order_num;
	        		$allToArray[$myIndex] =$temp;

	        		foreach($allToArray as $ithis){
	        			$val = new VendorSortCategories();
	        			$val->category_id = $ithis['category_id'];
	        			$val->restaurant_id = $ithis['restaurant_id'];
	        			$val->sort_order = $ithis['sort_order'];
	        			$val->child_of = $ithis['child_of'];
	        			$val->child_sort_order = $ithis['child_sort_order'];
	        			$val->save();
	        		}
	        		
	        	}
				//print_r(json_encode($allToArray));exit;
	        }
	        if($vendorCat->sort_order == 0){//Then Parent

	        }
        	return redirect(route('sort-categories'));
    	}
    }
    
}
