<?php

namespace App\Http\Controllers\vendor;

use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\UserWishList;
use App\Models\Category;
use App\Models\categoryType;
use App\Models\ProductCategoryAttribute;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\ComposeProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Validator;

class CategoryController extends Controller
{
    public function add_category(Request $request)
    {
        $data = $request->all();
        $data['title'] = 'Add Category';
        $data['breadcrums'] = 'Add Category';
		$is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
		
        $data['parent_category'] = Category::select('*')
								->where("parent_id", "=", 0)
								->where("restaurant_id", "=", $restaurant_id)
								->where("status", "=", 1)
								->orderBY('id', 'ASC')
								->get()
								->all();
								
		$data['categoryTypes'] = categoryType::select('*')
								->where("restaurant_id", "=", $restaurant_id)
								->where("status", "=", 1)
								->orderBY('id', 'ASC')
								->get()
								->all();
								
        return view('vendor/category/add_category',$data);
    }

    function categorySubmitted(Request $request)
    {
        $data = $request->all();
        
		if(!isset($data['parent_id']) AND $data['categoryType'] =="") {
			$error = \Illuminate\Validation\ValidationException::withMessages([
			   'categoryType' => ['Le type de catégorie est requis'],
			]);
			throw $error;
		}
		
		$this->validate(
			$request, 
			['name' => 'required'],
			['name.required' => 'Il est requis de compléter le champ correspondant au nom']
		);
				
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];

        $category = new Category();
        $category->name = $data['name'];
        $category->description = $data['description'];
		$category->category_type_id = isset($data['categoryType']) ? $data['categoryType'] : '';
        $category->parent_id = 0;
		
        if(isset($data['parent_id']) AND !empty($data['parent_id']))
            $category->parent_id = $data['parent_id'];
		
		$category->restaurant_id = $restaurant_id;
        $category->status = $data['cat_status'];
        $category->image = '';
		
        if ($_FILES['category']['size'] != 0 && $_FILES['category']['error'] == 0) {
            $file = $request->file("category");
            $file_name = "categories". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories", $file_name, 'uploads');
            $category->image = $file_name;
        }

        if ($_FILES['icon']['size'] != 0 && $_FILES['icon']['error'] == 0) {
            $file = $request->file("icon");
            $file_name = "icon". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories/icons", $file_name, 'uploads');
            $category->icon = $file_name;
        }

        $category->save();
		
		if(isset($data['attribute_name']) AND count($data['attribute_name']) > 0) {
			foreach($data['attribute_name'] as $attribute) {
				$productCategoryAttribute = new  ProductCategoryAttribute();
				$productCategoryAttribute->category_id = $category->id;
				if(empty($attribute))
					$attribute = 'Prix';
				$productCategoryAttribute->attribute_name =  $attribute;
				$productCategoryAttribute->status = 1;
				$productCategoryAttribute->save();
			}
		}
		
        return redirect(route('categoryListing-v'))->with('info', "Category created");
    }

    function editCategorySubmitted(Request $request)
    {
        $data = $request->all();
		if(!isset($data['parent_id']) AND $data['categoryType'] =="") {
			$error = \Illuminate\Validation\ValidationException::withMessages([
			   'categoryType' => ['Le type de catégorie est requis'],
			]);
			throw $error;
		}
       $this->validate(
			$request, 
			['name' => 'required'],
			['name.required' => 'Il est requis de compléter le champ correspondant au nom']
		);
        $cat_id = $data['cat_id'];
        $category = new Category();
        if ($cat_id != '') {
            $category = Category::select("*")
                ->where("id", "=", $cat_id)
                ->first();
        }
		
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];


        $category->name = $data['name'];
        $category->description = $data['description'];
		$category->category_type_id = $data['categoryType'];
        $category->parent_id = 0;
        if(isset($data['parent_id']) AND !empty($data['parent_id']))
            $category->parent_id = $data['parent_id'];
		
        $category->status = $data['cat_status'];
		$category->restaurant_id = $restaurant_id;
		
        if ($_FILES['category']['size'] != 0 && $_FILES['category']['error'] == 0) {
            $file = $request->file("category");
            $file_name = "categories". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories", $file_name, 'uploads');
            $category->image = $file_name;
        }

        if ($_FILES['icon']['size'] != 0 && $_FILES['icon']['error'] == 0) {
            $file = $request->file("icon");
            $file_name = "icon". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories/icons", $file_name, 'uploads');
            $category->icon = $file_name;
        }
        $category->save();
		
		if(isset($data['attribute_name']) AND count($data['attribute_name']) > 0) {
			//ProductCategoryAttribute::where('category_id',$cat_id)->delete();
			foreach($data['attribute_name'] as $key2 => $attribute) {
				
				$productCategoryAttribute = ProductCategoryAttribute::select("*")
						->where("id", "=", $key2)
						->where("category_id", "=", $category->id)
						->first();
				if(!$productCategoryAttribute) {
					$productCategoryAttribute = new  ProductCategoryAttribute();
				}					
				
				$productCategoryAttribute->category_id = $category->id;
				if(empty($attribute))
					$attribute = 'Prix';
				$productCategoryAttribute->attribute_name =  $attribute;
				$productCategoryAttribute->status = 1;
				$productCategoryAttribute->save();
			}
		}
		
        return back();
    }

    public function categoryListing(Request $request)
    {
        $data = $request->all();
       // print_r($data);exit;
        $data['title'] = 'All Category';
        $data['breadcrums'] = 'All Category';
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
        $parent_id = 0;
        //$view = 'admin/category/category-listing';
        $view = 'vendor/category/category-listing';
        if(isset($data['parent_id']) AND $data['parent_id'] > 0) {
            $parent_id = $data['parent_id'];
            $data['parent_id'] = $parent_id;
			//echo $data['parent_id'];exit;
            $view = 'vendor/category/child-category-listing';
        }
        $category_qry = Category::select('*');
       // if(isset)
        $category_qry->where("parent_id", "=", $parent_id);
		$category_qry->where("restaurant_id", "=", $restaurant_id);
		$category_qry->where('status', '!=',3);
        $category_qry->orderBY('sort_order', 'ASC');
        $category  =  $category_qry->paginate(15);
		//echo "<pre>";
		//print_r($category);
		//exit;		
        $data['categories'] = $category;
        $data['cat_count'] = count($data['categories']);
        return view($view,$data);
    }

    public function change_category_status(Request $request)
    {
     //   $data = $request->all();
        $data = $request->cat_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $cat_id = $data[0];
        $status = $data[2];
      
        if ($status == 1) {
			  $category = Category::query()
            ->select("id", "status")
            ->where("id", "=", $cat_id)
            ->first();
			//print_r($category);exit;
			$category->status = $status;
			$category->save();
            return redirect(route('categoryListing-v'))->with('success', 'Activated');
        } else if ($status == 2) {
			  $category = Category::query()
            ->select("id", "status")
            ->where("id", "=", $cat_id)
            ->first();
			//print_r($category);exit;
			$category->status = $status;
			$category->save();
            return redirect(route('categoryListing-v'))->with('success', 'Deactivated');
        } else if ($status == 3) {
			
		 $products = Products::select("*")
                ->where("parent_category_id", "=", $cat_id)
				->orWhere("category_id", "=", $cat_id)
                ->count();
				
		$compsed_product = ComposeProduct::select('*')
            ->where("category_id", "=", $cat_id)
            ->count(); 
			
		if($products > 0 OR $compsed_product > 0) {
			return redirect(route('categoryListing-v'))->with('success', 'Category have products So unable to delete');
		} else {
			$category = Category::query()
            ->select("id", "status")
            ->where("id", "=", $cat_id)
            ->first();
			//print_r($category);exit;
			$category->status = $status;
			$category->save();
            return redirect(route('categoryListing-v'))->with('success', 'Category has been deleted successfully');
		}
    }
        return  redirect(route('categoryListing-v'));
    }

    public function edit_category()
    {
        $data['title'] = 'Edit Category';
        $data['breadcrums'] = 'Edit Category';
		$is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
		
        $cat_id = $_GET['cat_id'];
        $category = Category::select('*')
            ->where("id", "=", $cat_id)
			->where("restaurant_id", "=", $restaurant_id)
            ->first();
		
        if(!$category) {
            return view('errors/404', $data);
        }
		
		$data['productCategoryAttribute'] = array();	
		//if($category['parent_id'] > 0) {	
			$data['productCategoryAttribute'] = ProductCategoryAttribute::select('*')
				->where("category_id", "=", $cat_id)
				->get()
				->all();
		//}
		
        $data['parent_category'] = Category::select('*')
								->where("parent_id", "=", 0)
								->where("restaurant_id", "=", $restaurant_id)
								->where("status", "=", 1)
								->orderBY('id', 'ASC')
								->get()
								->all();
		$data['categoryTypes'] = categoryType::select('*')
								->where("restaurant_id", "=", $restaurant_id)
								->where("status", "=", 1)
								->orderBY('id', 'ASC')
								->get()
								->all();
								
        $data['category'] = $category;
       

        return view('vendor/category/edit_category',$data);
    }

     public function change_category_sort_order(Request $request)
    {
        if($request->ajax()){
            $data = $request->cat_id;
            $data = base64_decode($data);
            $cat_id = $data;

            $is_vendor =  Session::get("is_vendor");
            $restaurant_id = $is_vendor['restaurant_id'];

            $category = Category::select('*')->where('id',$cat_id)->first();
            if($category){
                $category->sort_order = $request->sort_order;
                $category->save();
                
                return response()->json(['response'=>'Sort Updated']);
            }
            return response()->json(['response'=>'Sort Update Failed']);
        }
    }
	
	// Category type start
	public function addCategoryType(Request $request)
    {
        $data = $request->all();
        $data['title'] = 'Add Category type';
        $data['breadcrums'] = 'Add Category type';
        return view('vendor/category/category-type/add',$data);
    }
	 function categoryTypeSubmitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|min:2',
        ]);

        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];

        $categoryType = new categoryType();
        $categoryType->name = $data['name'];
		$categoryType->restaurant_id = $restaurant_id;
        $categoryType->icon = '';
        if ($_FILES['category_type_icon']['size'] != 0 && $_FILES['category_type_icon']['error'] == 0) {
            $file = $request->file("category_type_icon");
            $file_name = "icon". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories/icons", $file_name, 'uploads');
            $categoryType->icon = $file_name;
        }

        $categoryType->save();

		
        return redirect(route('category-type'))->with('info', "Category Type created");
    }

    function editCategoryTypeSubmitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
        ]);
        $cat_type_id = $data['cat_type_id'];
        $categoryType = new categoryType();
        if ($cat_type_id != '') {
            $categoryType = categoryType::select("*")
                ->where("id", "=", $cat_type_id)
                ->first();
        }
		
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
        $categoryType->name = $data['name'];
		$categoryType->restaurant_id = $restaurant_id;
		//$categoryType->icon = '';
		//echo "<pre>";
		//print_r($_FILES['category_type_icon']);exit;
        if ($_FILES['category_type_icon']['name'] != "") {
            $file = $request->file("category_type_icon");
            $file_name = "icon". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories/icons", $file_name, 'uploads');
            $categoryType->icon = $file_name;
        }
        $categoryType->save();
        return back();
    }

    public function categoryTypeListing(Request $request)
    {
        $data = $request->all();
        $data['title'] = 'All Category Type';
        $data['breadcrums'] = 'All Category Type';
        $is_vendor =  Session::get("is_vendor");
        $restaurant_id = $is_vendor['restaurant_id'];
        $category_qry = categoryType::select('*');
		$category_qry->where("restaurant_id", "=", $restaurant_id);
        $category_qry->orderBY('id', 'DESC');
        $category  =  $category_qry->paginate(15);	
        $data['categoryTypes'] = $category;
        return view('vendor/category/category-type/category-type-listing',$data);
    }

    public function categoryTypeStatus(Request $request)
    {
        $data = $request->cat_type_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $cat_id = $data[0];
        $status = $data[2];
		$categoryType = categoryType::query()
            ->select("id", "status")
            ->where("id", "=", $cat_id)
            ->first();
			//print_r($category);exit;
			$categoryType->status = $status;
			$categoryType->save();
        return redirect(route('category-type'))->with('success', 'Activated');
    }

    public function editCategoryType()
    {
        $data['title'] = 'Edit Category Type';
        $data['breadcrums'] = 'Edit Category Type';

        $cat_type_id = $_GET['cat_type_id'];
        $categoryType = categoryType::select('*')
            ->where("id", "=", $cat_type_id)
            ->first();
			
        if(!$categoryType) {
            return view('errors/404', $data);
        }

        $data['categoryType'] = $categoryType;
      
        return view('vendor/category/category-type/edit',$data);
    }	
}
