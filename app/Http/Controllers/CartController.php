<?php

namespace App\Http\Controllers;

use Cart;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    function add_to_cart(Request $request)
    {
        $id = $request->product_id;
        $products_detail = Products::find($id);
        $quantity = 1;
        if(isset($request->quantity) AND !empty($request->quantity)) {
            $quantity = $request->quantity;
        }

        if(!empty($products_detail->sale_price) AND $products_detail->sale_price > 0)
            $products_price = $products_detail->sale_price;
        else
             $products_price = $products_detail->regular_price;

        $products_price_temp = $products_price;

        if($products_detail->units_per_box > 0)
            $products_price = $products_detail->units_per_box * $products_price;

        $cart_detail = Cart::add($products_detail->id, $products_detail->product_name, $quantity, $products_price, ['units_per_box' => $products_detail->units_per_box,'price2'=>$products_price_temp]);
        return $cart_detail->rowId;
    }

    function show_cart()
    {
        $cart_contents = Cart::content();
        $data['cart_count'] = Cart::count();
        return view('cart/cart-new',$data);
    }

    function update_cart(Request $request)
    {
        //echo "hereee update";exit;
        //print_r(Cart::content());
        $data = $request->all();
        $rowId = $data['pid'];
        $cart_detail = Cart::get($rowId);
        $products_detail = Products::find($cart_detail->id);

        if(!empty($products_detail->sale_price) AND $products_detail->sale_price > 0)
            $products_price = $products_detail->sale_price;
        else
            $products_price = $products_detail->regular_price;

        if($products_detail->units_per_box > 0)
            $products_price = $products_detail->units_per_box * $products_price;

        $quantity = $data['quantity'];

        $item_detail = Cart::update(
            $rowId, [
            'qty' => $quantity,
            'price' => $products_price
        ]);
        $_item_detail = $item_detail->total;
        return response()->json(array('cart_count'=>Cart::count(),'current_product_total'=>$item_detail->total,'total_cart_amount'=>Cart::total()), 200, [], JSON_NUMERIC_CHECK);
       // echo json_encode(array('cart_count'=>Cart::count(),'aaa'=>$_item_detail,'current_product_total'=>$item_detail->total,'total_cart_amount'=>Cart::total()));
    }
    function remove_cart(Request $request)
    {
        $data = $request->all();
        Cart::remove($data['pid']);
        echo json_encode(array('cart_count'=>Cart::count(),'total_cart_amount'=>Cart::total()));
    }
    function clear_shoping_cart() {
         Cart::destroy();
        return redirect(route('home'));
    }
}
