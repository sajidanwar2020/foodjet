<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Cart;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;

class ShopController extends Controller
{
    function shop(Request $request)
    {
        $data = $request->all();
        $data['title'] = 'Shop';
        $data['breadcrums'] = 'Shop';
        $cart_arr = $this->searchCart1();
        $web_lable = "";
        //---------------------------------------------Category----------------------------------------------
        $cat_ids = array();
        if(isset($data['cat_id']) AND $data['cat_id'] !="") {
            $cat_ids = explode(',',$data['cat_id']);
        }
        $data['cat_ids'] = $cat_ids;
        //------------------------------------------Price range----------------------------------------------------
        $price_range = $this->getPriceRange();
        $data['price_range'] = $price_range;
        $filter_price = array();
        $filter_price['min_price'] = $price_range['min_price'];
        $filter_price['max_price'] = $price_range['max_price'];
        if(isset($data['price']) AND $data['price'] !="") {
            $_price = explode(',',$data['price']);
            $filter_price['min_price'] = $_price[0];
            $filter_price['max_price'] = $_price[1];
        }
        $data['filter_price'] = $filter_price;
        //--------------------------------------------End price----------------------------------------------------
        $products_query = Products::select('*')
            ->where("status", "=", 1);
        //---------------------------------------------Filter -----------------------------------------------------
        if (isset($data['c']) && !empty($data['c'])) {
            $child_cat_id = getChildCategoriesIds($data['c']);
            $products_query->whereIn('category_id', $child_cat_id);
            $category = Category::select('name')->where('id', '=', $data['c'])->first();
            if (!empty($category))
                $web_lable = "All products of <b>" . $category->name . "</b>";
        }
        if(isset($data['price']) AND $data['price'] !="") {
            $products_query->whereBetween('regular_price', [$filter_price['min_price'], $filter_price['max_price']]);
        }
        if(isset($data['cat_id']) AND $data['cat_id'] !="") {
            $products_query->whereIn('category_id', $cat_ids);
        }
        if(isset($data['sorting']) AND $data['sorting'] !="") {
              if($data['sorting'] == 'price-asc')
                  $products_query->orderBy('regular_price', 'ASC');
            else if($data['sorting'] == 'price-desc')
                 $products_query->orderBy('regular_price', 'DESC');
            else
                 $products_query->orderBY('products.id', 'ASC');
        }
        //-------------------------------------------- End filter-------------------------------------------------
        $products = $products_query->paginate(15);
        if ($products) {
            foreach ($products as $product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()
                    ->all();
                $product->product_images = $product_images;
                $product->is_cart = 'no';
                if (array_key_exists($product['id'], $cart_arr)) {
                    $product->cart_id = $cart_arr[$product['id']];
                    $product->is_cart = 'yes';
                }
            }
        }
        //--------------------------------------------------------------------------------------------------------------
        $categories = getParentChildCategory();
        $data['categories'] = $categories;
        //--------------------------------------------------------------------------------------------------------------
        $feature_products_query = Products::select('*')
            ->where("status", "=", 1);
        $feature_products_query->orderBY('products.id', 'ASC');
        $feature_products = $feature_products_query->limit(5)->get();
        $data['feature_products'] = $feature_products;
        //--------------------------------------------------------------------------------------------------------------
        //echo "<pre>";
        // print_r($categories);
        //exit;
        $data['products'] = $products;
        //echo Cart::count();exit;
        $data['cart_count'] = Cart::count();
        $data['lable'] = $web_lable;
        return view('shop/shop', $data);
    }

    function search(Request $request)
    {
        $data = $request->all();
        //print_r($data);exit;
        $data['title'] = 'Shop';
        $data['breadcrums'] = 'Shop';
        $cart_arr = $this->searchCart1();
        $web_lable = "";
        $products_query = Products::select('*')
            ->where("status", "=", 1);
        if (isset($data['s']) AND $data['s'] != "") {
            $products_query->where('product_name', 'LIKE', '%' . $data['s'] . '%');
            $web_lable = "Search For <b>" . $data['s'] . "</b>";
        } else if (isset($data['c']) && !empty($data['c'])) {
            $products_query->where('category_id', '=', $data['c']);
            $category = Category::select('name')->where('id', '=', $data['c'])->first();
            if (!empty($category))
                $web_lable = "All products of <b>" . $category->name . "</b>";
        }
        $products_query->orderBY('products.id', 'ASC');
        $products = $products_query->get()->all();

        if ($products) {
            foreach ($products as $product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()->all();
                $product->product_images = $product_images;
                $product->is_cart = 'no';
                if (array_key_exists($product['id'], $cart_arr)) {
                    $product->cart_id = $cart_arr[$product['id']];
                    $product->is_cart = 'yes';
                }
            }
        }

        //echo "<pre>";
        // print_r($categories);
        //exit;
        $data['products'] = $products;
        //echo Cart::count();exit;
        $data['cart_count'] = Cart::count();
        $data['lable'] = $web_lable;
        return view('shop/search', $data);
    }

    function searchCart($rowId)
    {
        return Cart::search(function ($cartItem, $rowId) {
            return $cartItem->rowId === $rowId;
        });
    }

    function searchCart1()
    {
        $product_arr = array();
        if (count(Cart::content()) > 0) {
            foreach (Cart::content() as $row) {
                $product_arr[$row->id] = $row->rowId;
            }
        }
        return $product_arr;
    }

    public function getPriceRange() {
        $price_range_arr = array();
        $priceRange = DB::table('products')->where("status", "=", 1);
        $price_range_arr['max_price'] = $priceRange->max('regular_price');
        $price_range_arr['min_price'] = $priceRange->min('regular_price');
        return $price_range_arr;
    }

    function product_detail(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $products_detail = Products::find($id);
        if (!$products_detail) {
            return redirect(route('home'));
        }
        $cart_arr = $this->searchCart1();

        $products_detail->is_cart = 'no';
        $products_detail->cart_detail = '';

        if (array_key_exists($id, $cart_arr)) {
            $products_detail->cart_detail = Cart::get($cart_arr[$id]);
            $products_detail->is_cart = 'yes';
        }

        if ($products_detail) {
            $product_images = ProductsImages::select('*')
                //->where("status", "=", 1)
                ->where("product_id", "=", $id)
                ->orderBY('product_images.id', 'ASC')
                ->get()->all();
            $products_detail->product_images = $product_images;
        }

        $products_query = Products::select('*')
            ->where("status", "=", 1);
        $products_query->where("category_id", "=", $products_detail->category_id);
        $products_query->orderBY('products.id', 'ASC');
        $products = $products_query->limit(10)->get()->all();

        if ($products) {
            foreach ($products as $product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()->all();
                $product->product_images = $product_images;
                $product->is_cart = 'no';
                if (array_key_exists($product['id'], $cart_arr)) {
                    $product->cart_id = $cart_arr[$product['id']];
                    $product->is_cart = 'yes';
                }
            }
        }


        $data['products_detail'] = $products_detail;
        $data['related_products'] = $products;
        $data['cart_count'] = Cart::count();
        return view('shop/product_detail', $data);
    }
}
