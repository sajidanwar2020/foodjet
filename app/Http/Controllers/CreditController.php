<?php

namespace App\Http\Controllers;

use App\Models\CreditTransactions;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Orders;
use App\Models\OrderItems;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use mysql_xdevapi\Exception;

class CreditController extends Controller
{
    private $admin_mail = '';
    private $paypal_mail = '';
    private $is_sandbox = true;
    private $use_local_certs = false;
    private $ipn_verify_url = 'https://ipnpb.paypal.com/cgi-bin/webscr';
    private $paypal_url = "https://www.paypal.com/cgi-bin/webscr";
    private $is_valid = false;

    function __construct()
    {
        $this->admin_mail = "hashmatkhattaka@gmail.com";
        $this->paypal_mail = "business@verbingo.com";
        if ($this->is_sandbox) {
            $this->paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            $this->paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
            $this->ipn_verify_url = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';
        }
    }

    function index(Request $request)
    {
        $data['title'] = 'Balance';
        $data['breadcrumbs'] = 'Balance';
        $order_id = (int)$request->order_id;
        if(!$order_id) {
            return redirect(route('customer_orders'));
        }

        $orderDetail = orderDetail($order_id);
        if(!$orderDetail) {
            return redirect(route('customer_orders'));
        }
        $final_amount = ($orderDetail->total_price + $orderDetail->distance_charges ) - $orderDetail->cancelled_amount;
        if($final_amount <= 0) {
            return redirect(route('customer_orders'));
        }
        //exit;
        $info =  Session::get('is_logged_in');
        $data['paypal_url'] = $this->paypal_url;
        $elements = array(
            "business" => $this->paypal_mail,
            "first_name" => $info['first_name'],
            "last_name" => $info['last_name'],
            "custom" => $order_id.','.$info['user_id'],
            "email" => $info['email'],
            "currency_code" => 'EUR',
            "cmd" => '_xclick',
            "rm" => '2',
            "item_name" => 'FoodJet',
            "amount" => $final_amount,
            "return" => route('paypal_return'),
            "cancel_return" => route('paypal_return', ['type' => 'cancel']),
            "notify_url" => route('verify_paypal_ipn')
        );
        $data['elements'] = $elements;
        return view('credits/paypal', $data);
    }
    function credits_success() {
        $data['cart_count'] = Cart::count();
        return view('credits/credits_success',$data);
    }
    function paypal_return(Request $request)
    {
        $data = $request->all();
        $info = Session::get('is_customer');
        $type = isset($data['type']) ? $data['type'] : '';

        if ($type == 'cancel') {
            return redirect(route('credits'))->with('warning', "Your payment has been cancelled");
        } elseif (isset($data['payment_status']) && $data['payment_status'] == 'Completed') {
            $message = "Congratulations,your account has been topup " . $data['mc_gross'] . " " . $data['mc_currency'] . " the account will be added once we verify it";
            return redirect(route('credits_success'))->with('success', $message);
        }

    }

    public function verify_paypal_ipn()
    {
        $user_info =  Session::get('is_logged_in');
        try {
            $raw_post_data = file_get_contents('php://input');
            $raw_post_array = explode('&', $raw_post_data);
            $myPost = array();
            foreach ($raw_post_array as $keyval) {
                $keyval = explode('=', $keyval);
                if (count($keyval) == 2) {
                    // Since we do not want the plus in the datetime string to be encoded to a space, we manually encode it.
                    if ($keyval[0] === 'payment_date') {
                        if (substr_count($keyval[1], '+') === 1) {
                            $keyval[1] = str_replace('+', '%2B', $keyval[1]);
                        }
                    }
                    $myPost[$keyval[0]] = urldecode($keyval[1]);
                }
            }
            file_put_contents("paypal.txt", json_encode($myPost));

            // Build the body of the verification post request, adding the _notify-validate command.
            $req = 'cmd=_notify-validate';
            $get_magic_quotes_exists = false;
            if (function_exists('get_magic_quotes_gpc')) {
                $get_magic_quotes_exists = true;
            }
            foreach ($myPost as $key => $value) {
                if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                    $value = urlencode(stripslashes($value));
                } else {
                    $value = urlencode($value);
                }
                $req .= "&$key=$value";
            }

            // Post the data back to paypal, using curl. Throw exceptions if errors occur.
            $ch = curl_init($this->ipn_verify_url);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
            curl_setopt($ch, CURLOPT_SSLVERSION, 6);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

            // This is often required if the server is missing a global cert bundle, or is using an outdated one.
            if ($this->use_local_certs) {
                curl_setopt($ch, CURLOPT_CAINFO, __DIR__ . "/cert/cacert.pem");
            }

            curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'User-Agent: PHP-IPN-Verification-Script',
                'Connection: Close',
            ));            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $http_code = $info['http_code'];
            if ($http_code != 200) {
                throw new \Exception("PayPal responded with http code $http_code");
            }

            if (!($res)) {
                $errno = curl_errno($ch);
                $errstr = curl_error($ch);
                curl_close($ch);
                throw new \Exception("cURL error: [$errno] $errstr");
            }
            curl_close($ch);

            // Check if paypal verfifes the IPN data, and if so, return true.
            if ($res == 'VERIFIED') {
                if ($myPost['payment_status'] == 'Completed') {
                    $pattern = ",";
                    $pieces = explode($pattern,$myPost['custom'] );
                    $custom1 = $pieces[0];
                    $custom2 = $pieces[1];
                    $user = UsersDetails::select("id")
                        ->where("user_id", "=", $custom2)
                        ->first();
                    if (!empty($user)) {
                       $transaction = new CreditTransactions();
                        $transaction->payment_type  = 2;
                        $transaction->payment_response = json_encode($myPost);
                        $transaction->order_id = $custom1;
                        $transaction->user_id = $custom2;
                        $transaction->transaction_id = $myPost['txn_id'];
                        $transaction->amount = $myPost['mc_gross'];
                        $transaction->currency = $myPost['mc_currency'];
                        //$transaction->type = '1';
                        $transaction->status = '1';
                        $transaction->status_text = $myPost['payment_status'];
                        $transaction->save();
                        //mail('sajidanwar2020@gmail.com', 'Success', 'Success:' . "Ipn is  verified successfully");
                    }
                }
            } else {
                mail('sajidanwar2020@gmail.com', 'Success', 'Success:' . "Ipn is not verified successfully");
            }
        } catch (\Exception $exception) {
            mail('sajidanwar2020@gmail.com', 'Error', 'ERROR:' . $exception->getMessage());

        }
    }
}
