<?php

namespace App\Http\Controllers\admin;

use App\Mail\ForgotPassword;
use App\Models\MinimumOrder;
use App\Models\Promotions;
use App\Models\Category;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Seller;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\UserWishList;
use App\Models\HomeSlider;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class PromotionsController extends Controller
{

    public function add_promotions()
    {
        $data['title'] = 'Add promotion';
        $categories = getParentChildCategory();
        $data['categories'] = $categories;
        $data['breadcrums'] = 'Add promotion';
        return view('admin/promotions/add_promotions',$data);
    }

    function add_promotions_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'start_datetime' => 'required',
            'end_datetime' => 'required',
            'discount_percent' => 'required',
            'parent_category' => 'required',
            'child_category' => 'required',
        ]);
        $promotions = new Promotions();
        $promotions->start_datetime = $data['start_datetime'];
        $promotions->end_datetime = $data['end_datetime'];
        $promotions->discount_percent = $data['discount_percent'];
        $promotions->category_idd = $data['child_category'];
        $promotions->status = 0;
        $promotions->save();


        return redirect(route('promotions-listing'))->with('info', "promotions created");
    }

    function edit_promotions()
    {
        $data['title'] = 'Edit promotion';
        $data['breadcrums'] = 'Edit promotion';

        $categories = getParentChildCategory();
        $data['categories'] = $categories;
        $id = $_GET['id'];
        $id = base64_decode($id);
        $id = explode(":", $id)[0];
        $promotions = Promotions::select('*')
            ->where("id", "=", $id)
            ->first();
        //print_r($promotions);exit;
        if(!$promotions) {
            $data['title'] = 'promotion';
            $data['breadcrumbs'] = 'promotion';
            return view('vendor/products/unauthorized', $data);
        }
       // echo $promotions->category_idd;exit;
        $parent_category = getParentCategoryByChildId($promotions->category_idd);
        $childCategories = getChildCategories($parent_category->parent_id);
        $promotions->parent_category_id = $parent_category->parent_id;
        $data['child_categories'] = $childCategories;
        $data['promotion'] = $promotions;
        return view('admin/promotions/edit_promotions',$data);
    }

    function edit_promotions_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'parent_category' => 'required',
            'child_category' => 'required',
            'start_datetime' => 'required',
            'end_datetime' => 'required',
            'discount_percent' => 'required'
        ]);

        $id = $data['promotion_id'];
        if ($id != '') {
            $promotions = Promotions::select("*")
                ->where("id", "=", $id)
                ->first();
        }

        $promotions->start_datetime = $data['start_datetime'];
        $promotions->end_datetime = $data['end_datetime'];
        $promotions->discount_percent = $data['discount_percent'];
        $promotions->discount_percent = $data['discount_percent'];
        $promotions->category_idd = $data['child_category'];
        $promotions->save();

        return redirect(route('promotions-listing'))->with('info', "promotions updated");
    }

    function promotions_listing()
    {
        $data['title'] = 'All promotion';
        $data['breadcrums'] = 'All promotion';
        $promotions = Promotions::select('*')
            //->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $data['promotions'] = $promotions;
        return view('admin/promotions/promotions-listing',$data);
    }

    function change_promotions_status(Request $request)
    {
        $data = $request->id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $id = $data[0];
        $status = $data[2];
        $promotions = Promotions::query()
            ->select("id", "status")
            ->where("id", "=", $id)
            ->first();
        $promotions->status = $status;
        $promotions->save();
        if ($status == 2) {
            return redirect(route('promotions-listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('promotions-listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('promotions-listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('promotions-listing'));
    }
}
