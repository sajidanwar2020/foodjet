<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Category;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Storage;
class ProductsController extends Controller
{
    public function adminAddProduct()
    {
        $data['title'] = 'Add Product';
        $data['breadcrums'] = 'Add Product';
        $categories = getParentChildCategory();
        $data['categories'] = $categories;
        return view('admin/products/add_product',$data);
    }

    function adminProductSubmitted(Request $request)
    {
        $data = $request->all();
        $info = Session::get("is_logged_in");
        $is_seller = Session::get('is_seller');
        $this->validate($request, [
            'product_name' => 'required|min:2|max:255',
            'product_description' => 'required|min:10|max:255',
            'regular_price' => 'required',
            'quantity_unit' => 'required',
            'category' => 'required',
        ]);

        $products = new Products();
        $products->product_name = $data['product_name'];
        $products->product_description = $data['product_description'];
        $products->seller_id = 3;
        $products->regular_price = $data['regular_price'];
        $products->sale_price = $data['sale_price'];
        $products->quantity_unit = $data['quantity_unit'];
        $products->category_id = $data['category'];
        $products->status = '1';
        $products->save();

        $product_id = $products->id;

        $size = count($_FILES);

        if ($size > 0) {
           // print_r($size);exit;
            for ($x = 0; $x < $size; $x++) {
                $file = $request->file("product_image$x");
                //print_r($file);exit;
                $file_name = "product" . $product_id . "_" . time() . "." . $file->getClientOriginalExtension();
                $file->storeAs("products", $file_name, 'uploads');
                $productsImages = new ProductsImages();
                $productsImages->product_id = $product_id;
                $productsImages->image_name = $file_name;
                $productsImages->is_default = $x == 0 ? 1 : 0;
                $productsImages->save();
            }
        }

        return redirect(route('products_listing'))->with('info', "Product created");
       // return redirect(route('edit_product',['product_id'=>base64_encode($product_id.":".$info['user_id'])]))->with('info', "Product created");
    }

    function adminEditProductSubmitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'product_name' => 'required|min:2|max:255',
            'product_description' => 'required|min:10|max:255',
            'regular_price' => 'required',
            'quantity_unit' => 'required',
            'category' => 'required',
        ]);
        $product_id = $data['product_id'];

        if ($product_id != '') {
            $products = Products::select("*")
                ->where("id", "=", $product_id)
                ->first();
        }

        $products->product_name = $data['product_name'];
        $products->product_description = $data['product_description'];
        $products->seller_id =3;
        $products->regular_price = $data['regular_price'];
        $products->sale_price = $data['sale_price'];
        $products->quantity_unit = $data['quantity_unit'];
        $products->category_id = $data['category'];
        $products->status = '1';
        $products->save();

        $product_id = $products->id;
        $size = count($_FILES);
        $file = $_FILES['product_image0']['tmp_name'];
        if ($file) {
            if(isset($data['old_image_id'])) {
                $olds =  $data['old_image_id'];
                $imgs = ProductsImages::query()->selectRaw("id, image_name")->where("product_id", $olds)->get();
                //print_r($imgs);exit;
                foreach ($imgs as $img) {
                        Storage::disk('uploads')->delete('products/' . $img->image);

                }
                ProductsImages::query()->where('product_id', $product_id)
                    ->where('id', $olds)
                    ->delete();
            }

            for ($x = 0; $x < $size; $x++) {
                $file = $request->file("product_image$x");
                if($file) {
                    $file_name = "product" . $product_id . "_" . time() . "." . $file->getClientOriginalExtension();
                    $file->storeAs("products", $file_name, 'uploads');
                    $productsImages = new ProductsImages();
                    $productsImages->product_id = $product_id;
                    $productsImages->image_name = $file_name;
                    $productsImages->is_default = $x == 0 ? 1 : 0;
                    $productsImages->save();
                }

            }
        }

        return back();
    }

    public function products_listing()
    {
        $data['title'] = 'All Product';
        $data['breadcrums'] = 'All Product';
        //$status = isset($_GET['status']) ? $_GET['status'] : "0,1,2,3";
      //  $status = explode(",", $status);
       // $info = Session::get('is_logged_in');


        $products = Products::select('*')
            //->where("status", "=", 1)
            ->orderBY('products.id', 'DESC')
            ->paginate(15);

        if($products) {
            foreach($products as $product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()->all();
                $product->product_images = $product_images;
            }
        }
        $data['products'] = $products;
        return view('admin/products/my_products',$data);
    }

    public function admin_edit_product()
    {
        $data['title'] = 'Edit Product';
        $data['breadcrums'] = 'Edit Product';

        $product_id = $_GET['product_id'];
        $product_id = base64_decode($product_id);
        $product_id = explode(":", $product_id)[0];
        //print_r($product_id);exit;
        $category = Category::select('*')
            ->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->get();
        $categories = getParentChildCategory();
        $data['categories'] = $categories;
        $products = Products::select('*')
            ->where("id", "=", $product_id)
            ->first();
        if(!$products) {
            $data['title'] = 'Products';
            $data['breadcrumbs'] = 'Products';
            return view('vendor/products/unauthorized', $data);
        }
        $data['product'] = $products;
        $data['product_images'] = ProductsImages::select("id", "image_name")
            ->where("product_id", "=", $product_id)
            ->orderBy("is_default", "DESC")
            ->get()->all();

        //print_r($data);exit;

        return view('admin/products/edit_product',$data);
    }

    function admin_change_product_status(Request $request)
    {
        $is_seller = Session::get('is_seller');
        if(isset($is_seller['role_id']) AND $is_seller['role_id'] != 3)
        {
            return view('errors/404');
            exit;
        }
        $data = $request->product_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $product_id = $data[0];
        $status = $data[2];
        $products = Products::query()
            ->select("id", "status")
            ->where("id", "=", $product_id)
            ->first();
        $products->status = $status;
        $products->save();
        if ($status == 2) {
            return redirect(route('products_listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('products_listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('products_listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('products_listing'));
    }
	


}
