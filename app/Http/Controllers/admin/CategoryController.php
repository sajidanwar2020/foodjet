<?php

namespace App\Http\Controllers\admin;

use App\Models\Category;
use App\Models\Products;
use App\Models\ProductsImages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function add_category(Request $request)
    {
        $data = $request->all();
        $data['title'] = 'Add Category';
        $data['breadcrums'] = 'Add Category';
        $data['parent_category'] =  getParentCategory();
        return view('admin/category/add_category',$data);
    }

    function categorySubmitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
        ]);

        $category = new Category();
        $category->name = $data['name'];
        $category->description = $data['description'];
        $category->parent_id = 0;
        if(isset($data['parent_id']) AND !empty($data['parent_id']))
            $category->parent_id = $data['parent_id'];

        $category->status = $data['cat_status'];
        $category->image = '';
        if ($_FILES['category']['size'] != 0 && $_FILES['category']['error'] == 0) {
            $file = $request->file("category");
            $file_name = "categories". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories", $file_name, 'uploads');
            $category->image = $file_name;
        }

        if ($_FILES['icon']['size'] != 0 && $_FILES['icon']['error'] == 0) {
            $file = $request->file("icon");
            $file_name = "icon". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories/icons", $file_name, 'uploads');
            $category->icon = $file_name;
        }

        $category->save();

        return redirect(route('categoryListing'))->with('info', "Category created");
    }

    function editCategorySubmitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
        ]);
        $cat_id = $data['cat_id'];
        $category = new Category();
        if ($cat_id != '') {
            $category = Category::select("*")
                ->where("id", "=", $cat_id)
                ->first();
        }
       // echo "<pre>";
      //  print_r($category);
       // exit;

        $category->name = $data['name'];
        $category->description = $data['description'];
        $category->parent_id = 0;
        if(isset($data['parent_id']) AND !empty($data['parent_id']))
            $category->parent_id = $data['parent_id'];
        $category->status = $data['cat_status'];

        if ($_FILES['category']['size'] != 0 && $_FILES['category']['error'] == 0) {
            $file = $request->file("category");
            $file_name = "categories". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories", $file_name, 'uploads');
            $category->image = $file_name;
        }

        if ($_FILES['icon']['size'] != 0 && $_FILES['icon']['error'] == 0) {
            $file = $request->file("icon");
            $file_name = "icon". "_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("categories/icons", $file_name, 'uploads');
            $category->icon = $file_name;
        }
        $category->save();
        return back();
    }

    public function categoryListing(Request $request)
    {
        $data = $request->all();
       // print_r($data);exit;
        $data['title'] = 'All Category';
        $data['breadcrums'] = 'All Category';
       // $status = isset($_GET['status']) ? $_GET['status'] : "0,1,2,3";
       // $status = explode(",", $status);
        $parent_id = 0;
        $view = 'admin/category/category-listing';
        if(isset($data['parent_id']) AND $data['parent_id'] > 0) {
            $parent_id = $data['parent_id'];
            $view = 'admin/category/child-category-listing';
        }
        $category_qry = Category::select('*');
       // if(isset)
        $category_qry->where("parent_id", "=", $parent_id);
        $category_qry->orderBY('id', 'DESC');
        $category  =   $category_qry->paginate(15);

        $data['categories'] = $category;
        return view($view,$data);
    }

    public function change_category_status(Request $request)
    {
     //   $data = $request->all();
        $data = $request->cat_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $cat_id = $data[0];
        $status = $data[2];
        $category = Category::query()
            ->select("id", "status")
            ->where("id", "=", $cat_id)
            ->first();
        //print_r($category);exit;
        $category->status = $status;
        $category->save();
        if ($status == 1) {
            return redirect(route('categoryListing'))->with('success', 'Activated');
        } else if ($status == 2) {
            return redirect(route('categoryListing'))->with('success', 'Deactivated');
        } else if ($status == 3) {
            return redirect(route('categoryListing'))->with('success', 'Deleted');
         }
        return  redirect(route('categoryListing'));
    }

    public function edit_category()
    {
        $data['title'] = 'Edit Category';
        $data['breadcrums'] = 'Edit Category';

        $cat_id = $_GET['cat_id'];
       // echo $cat_id;exit;
        $category = Category::select('*')
            ->where("id", "=", $cat_id)
            ->first();
      //  print_r($category);exit;
        if(!$category) {
            return view('errors/404', $data);
        }
        $data['parent_category'] =  getParentCategory();
       // print_r($category);exit;
        $data['category'] = $category;
       // print_r(  $data['category']);exit;

        return view('admin/category/edit_category',$data);
    }
}
