<?php

namespace App\Http\Controllers\admin;

use App\Mail\ForgotPassword;
use App\Models\Category;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Seller;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\UserWishList;
use App\Models\HomeSlider;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{

     public function admin()
    {
        return view('admin/users/signin');
    }

    public function admin_submitted(Request $request)
    {
      // echo "hereeeee";exit;

        $data = $request->all();
        $email = $data['email'];
        $password = $data['password'];

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $password = crypt($password, md5($password));
       // echo $password;exit;
        $user = Users::where(['email' => $email, 'password' => $password])
            ->join('user_details as details', 'users.id', '=', 'details.user_id')
            ->first();
       // print_r($user);exit;
        if ($user) {
            if(isset($user['role_id']) AND ($user['role_id'] == '1')) {
                $admin_detail = Users::where(['users.id' => $user['user_id']])
                    ->join('user_details as details', 'users.id', '=', 'details.user_id')
                    ->first();
                Session::put("is_admin",$admin_detail);
                return redirect('/admin-dashboard');
            }
            else
                return redirect('/admin-dashboard');
        }
        return redirect(route('admin-dashboard'))->withErrors(['error' => "No account found for this email/password!"]);
    }

    function change_password()
    {
        $data['cart_count'] = Cart::count();
        return view('users/change_password',$data);
    }

    function update_password(Request $request)
    {

        $this->validate($request, [
            'old_password' => 'min:6',
            'new_password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);

        $data = $request->all();
        // print_r($data);exit;

        $info = Session::get('is_logged_in');
        $user_id = $info['user_id'];

        $user = Users::select("id", "password")
            ->where(['id' => $user_id])
            ->first();
        $old_password = $data['old_password'];
        $old_password = crypt($old_password, md5($old_password));
        if($old_password == $user->password) {
            $new_password = crypt($data['new_password'], md5($data['new_password']));
            $user->password = $new_password;
            if($user->save()){
                return redirect(route('change_password', ['status' => 'deleted']))->with('success', "Password has been changed");
            }
        } else {
            return redirect(route('change_password'))->withErrors('Old password did not match');
        }
        return redirect()->back();
    }

    function profile()
    {
        $info = Session::get('is_logged_in');
        $user_id = $info['user_id'];
        $info = Users::select('*')
            ->where(['users.id' => $user_id])
            ->join('user_details as d', 'users.id', '=', 'd.user_id')
            ->first();
        $data['info'] = $info;
        $data['cart_count'] = Cart::count();
        return view('users/profile', $data);
    }

    function profile_update(Request $request)
    {
        /*
        $this->validate($request, [
            'first_name' => 'required',
        ]); */

        $data = $request->all();

        $info = Session::get("is_logged_in");
        $user = Users::where("id", "=", $info['user_id'])
            ->first();
        // $user->phone_number = $data['mobile_number'];
        $user->save();

        $profile = UsersDetails::where("user_id", "=", $info['user_id'])
            ->first();
        $profile->first_name = $data['first_name'];
        $profile->last_name = $data['last_name'];
        $profile->location = $data['location'];
        //$profile->date_of_birth = $data['dob'];
        if(isset($data['gmap_latitude']))
            $profile->latitude = $data['gmap_latitude'];
        if(isset($data['gmap_longitude']))
            $profile->longitude = $data['gmap_longitude'];
        if(isset($data['country_iso']))
            $profile->country = $data['country_iso'];
        $profile->salutation = $data['salutation'];
        // $profile->company = $data['company'];
        $profile->house_number = $data['house_number'];
        $profile->street = $data['street'];
        $profile->post_code = $data['post_code'];
        $profile->country_id = $data['country'];
        //$profile->phone_number = $data['mobile_number'];
        // $profile->tax_id = $data['tax_id'];
        $profile->save();
        return redirect(route('profile'))->with('info', "Updated profile");
    }

    function forgot_password(Request $request)
    {
        $email = $request->email;
        $user = Users::select("id", "password", 'verification_code')
            ->where(['email' => $email])
            ->first();
        // print_r($user);exit;
        if (!empty($user)) {
            $code = mt_rand(100000, 999999);
            $user->verification_code = $code;
            $user->save();
            $details = UsersDetails::query()
                ->select('id', 'first_name', 'last_name')
                ->where('user_id', '=', $user['id'])
                ->first();
            //print_r($details);exit;
            $token = base64_encode($email . ':' . $code);
            $info = array(
                'first_name' => $details->first_name,
                'last_name' => $details->last_name,
                'link' => route('reset_password', ['token' => $token])
            );
            //print_r($info);exit;
            Mail::to($email)->send(new ForgotPassword($info));
            //Send Email
            return 1;
        }
        return "No such email are found.";
    }

    function reset_password(Request $request)
    {
        $data = $request->all();
        $info['token'] = $data['token'];
        return view('users.reset_password', $info);
    }

    function reset_update_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);
        $data = $request->all();
        $info = base64_decode($data['token']);
        $info = explode(":", $info);
        // echo "<pre>";
        //print_r($info);
        //exit;
        //exit;
        $email = $info[0];
        $code = $info[1];
        //echo $email.' '.$code;exit;
        if ($email != '' && $code != '') {
            $user = Users::select("id", "password", 'verification_code')
                ->where(['email' => $email, 'verification_code' => $code])
                ->first();
            // print_r($user);exit;
            if (empty($user)) {
                return redirect(route('reset_password', ['token' => $data['token']]))->with('error', 'Sorry your password reset link is expired');
            } else {
                $password = crypt($data['password'], md5($data['password']));
                $user->password = $password;
                $user->verification_code = '';
                if ($user->save()) {
                    return redirect(route('signin'))->with('success', 'Your password has been changed successfully');
                } else {
                    return redirect(route('reset_password', ['token' => $data['token']]))->with('error', 'Opps something went wrong');
                }
            }
        } else {
            return redirect(route('reset_password', ['token' => $data['token']]));
        }
    }

    public function customer_listing()
    {
        $data['title'] = 'All Customer';
        $data['breadcrums'] = 'All Customer';

        $customers = Users::select('*')
            ->join('user_details as details', 'users.id', '=', 'details.user_id')
            ->where("status", "=", 1)
            ->where("role_id", "=", 2)
            ->orderBY('users.id', 'DESC')
            ->paginate(15);
        $data['customers'] = $customers;
        return view('admin/users/customer-listing',$data);
    }

    public function vendor_listing()
    {
        $data['title'] = 'All Vendor';
        $data['breadcrums'] = 'All Vendor';

        $vendors = Users::select('*')
            ->join('user_details as details', 'users.id', '=', 'details.user_id')
            ->where("status", "=", 1)
            ->where("role_id", "=", 3)
            ->orderBY('users.id', 'DESC')
            ->paginate(15);
        $data['vendors'] = $vendors;
        return view('admin/users/vendor-listing',$data);
    }

    function admin_logout()
    {
        \session(['is_admin' => null]);
        return redirect(route('admin'));
    }

    public function add_listing()
    {
        $data['title'] = 'Add Slide';
        $data['breadcrums'] = 'Add Slide';
        return view('admin/setting/home_slider/add_image',$data);
    }

    function add_slide_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'slide_image0' => 'required',
        ]);

        $size = count($_FILES);
        if ($size > 0) {
            $file = $request->file("slide_image0");
            $file_name = "slide_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("home_slider", $file_name, 'uploads');
            $homeSlider = new HomeSlider();
            $homeSlider->text = $data['text'];
            $homeSlider->image = $file_name;
            $homeSlider->status = 0;
            $homeSlider->save();
        }

        return redirect(route('slide-listing'))->with('info', "Slide created");
        // return redirect(route('edit_product',['product_id'=>base64_encode($product_id.":".$info['user_id'])]))->with('info', "Product created");
    }

    function edit_slide()
    {
        $data['title'] = 'Edit Slide';
        $data['breadcrums'] = 'Edit Slide';

        $id = $_GET['id'];
        $id = base64_decode($id);
        $id = explode(":", $id)[0];
        $homeSlider = HomeSlider::select('*')
            ->where("id", "=", $id)
            ->first();
        if(!$homeSlider) {
            $data['title'] = 'Slide';
            $data['breadcrumbs'] = 'Slide';
            return view('vendor/products/unauthorized', $data);
        }
        $data['homeSlider'] = $homeSlider;
        return view('admin/setting/home_slider/edit_image',$data);
    }

    function editSlideSubmitted(Request $request)
    {
        $data = $request->all();

        $id = $data['slide_id'];

        if ($id != '') {
            $homeSlider = HomeSlider::select("*")
                ->where("id", "=", $id)
                ->first();
        }

        $size = count($_FILES);
        $file = $_FILES['slide_image0']['tmp_name'];
        if ($file) {
            if(isset($data['old_image_id'])) {
                $olds =  $data['old_image_id'];
                Storage::disk('uploads')->delete('home_slider/' . $homeSlider->image);
                HomeSlider::query()->where('id', $olds)
                    ->delete();
            }

            $file = $request->file("slide_image0");
            $file_name = "slide_" . time() . "." . $file->getClientOriginalExtension();
            $file->storeAs("home_slider", $file_name, 'uploads');
            $homeSlider = new HomeSlider();
            $homeSlider->text = $data['text'];
            $homeSlider->image = $file_name;
            $homeSlider->status = 1;
            $homeSlider->save();
        }

        return redirect(route('slide-listing'))->with('info', "Slide updated");
    }

    function slide_listing()
    {
        $data['title'] = 'All Slide';
        $data['breadcrums'] = 'All Slide';
        $slider = HomeSlider::select('*')
            //->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $data['sliders'] = $slider;
        return view('admin/setting/home_slider/slider-listing',$data);
    }

    function change_slide_status(Request $request)
    {
        $data = $request->id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $id = $data[0];
        $status = $data[2];
        $HomeSlider = HomeSlider::query()
            ->select("id", "status")
            ->where("id", "=", $id)
            ->first();
        $HomeSlider->status = $status;
        $HomeSlider->save();
        if ($status == 2) {
            return redirect(route('slide-listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('slide-listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('slide-listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('slide-listing'));
    }
}
