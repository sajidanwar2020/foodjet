<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Pages;
use App\Models\Category;
use App\Models\Faqs;
use App\Models\FaqTypes;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Storage;

class PagesController extends Controller
{
    public function add_page()
    {
        $data['title'] = 'Add Page';
        $data['breadcrums'] = 'Add Page';
        return view('admin/pages/add_page',$data);
    }

    function add_page_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'title' => 'required|min:2|max:255',
            'description' => 'required|min:10',
        ]);

        $pages = new Pages();
        $pages->title = $data['title'];
        $pages->slug = getPageSlug($data['title']);
        $pages->description = $data['description'];
        $pages->status = '0';
        $pages->save();
        return redirect(route('pages-listing'))->with('info', "Page created");
    }

    public function edit_page()
    {
        $data['title'] = 'Edit Page';
        $data['breadcrums'] = 'Edit page';
       // echo $page_id;exit;
        $page_id = $_GET['page_id'];
        $page_id = base64_decode($page_id);
        $page_id = explode(":", $page_id)[0];
        //echo $page_id;exit;
        $page = Pages::select('*')
            ->where("id", "=", $page_id)
            ->first();
        if(!$page) {
            $data['title'] = 'Page';
            $data['breadcrumbs'] = 'Page';
            return view('vendor/products/unauthorized', $data);
        }
        $data['page'] = $page;
        return view('admin/pages/edit_page',$data);
    }

    function editPageSubmitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'title' => 'required|min:2|max:255',
            'description' => 'required|min:10',
        ]);
        $page_id = $data['page_id'];

        if ($page_id != '') {
            $page = Pages::select("*")
                ->where("id", "=", $page_id)
                ->first();
        }

        $page->title = $data['title'];
        $page->description = $data['description'];
        //$page->status = '0';
        $page->save();
        return back();
    }

    public function pages_listing()
    {
        $data['title'] = 'All Pages';
        $data['breadcrums'] = 'All Pages';
        $pages = Pages::select('*')
            //->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $data['pages'] = $pages;
        return view('admin/pages/pages-listing',$data);
    }

    function change_page_status(Request $request)
    {

        $data = $request->page_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $page_id = $data[0];
        $status = $data[2];
        $pages = Pages::query()
            ->select("id", "status")
            ->where("id", "=", $page_id)
            ->first();
        $pages->status = $status;
        $pages->save();
        if ($status == 2) {
            return redirect(route('pages-listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('pages-listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('pages-listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('pages-listing'));
    }

    // Faqs start

    public function add_faq()
    {
        $data['title'] = 'Add Faq';
        $data['breadcrums'] = 'Add Faq';
        $faqTypes = FaqTypes::select("*")
            ->where("status", "=", 1)
            ->get();
        $data['faqTypes'] =  $faqTypes;
        return view('admin/pages/faq/add_faq',$data);
    }

    function add_faq_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'question' => 'required|min:2|max:255',
            'answer' => 'required|min:5',
            'type_id' => 'required',
        ]);

        $faqs = new Faqs();
        $faqs->question = $data['question'];
        $faqs->answer = $data['answer'];
        $faqs->type_id = $data['type_id'];
        $faqs->status = '1';
        $faqs->save();
        return redirect(route('faq-listing'))->with('info', "Faq created");
    }

    public function edit_faq()
    {
        $data['title'] = 'Edit Page';
        $data['breadcrums'] = 'Edit page';
        // echo $page_id;exit;
        $faq_id = $_GET['faq_id'];
        $faq_id = base64_decode($faq_id);
        $faq_id = explode(":", $faq_id)[0];
        //echo $page_id;exit;
        $faq = Faqs::select('*')
            ->where("id", "=", $faq_id)
            ->first();
        if(!$faq) {
            return abort(404);
        }
        $faqTypes = FaqTypes::select("*")
            ->where("status", "=", 1)
            ->get();
        $data['faqTypes'] =  $faqTypes;
        $data['faq'] = $faq;
        return view('admin/pages/faq/edit_faq',$data);
    }

    function editFaqSubmitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'question' => 'required|min:2',
            'answer' => 'required|min:5',
            'type_id' => 'required',
        ]);
        $faq_id = $data['faq_id'];

        if ($faq_id != '') {
            $faqs = Faqs::select("*")

                ->where("id", "=", $faq_id)
                ->first();
        }

        $faqs->question = $data['question'];
        $faqs->answer = $data['answer'];
        $faqs->type_id = $data['type_id'];
       // $faqs->status = '1';
        $faqs->save();
        return back();
    }

    public function faq_listing()
    {
        $data['title'] = 'All Faq';
        $data['breadcrums'] = 'All Faq';
        $Faqs = Faqs::select('*')
            //->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $Faqs = Faqs::select('faq.id as id', 'answer','question','title','faq.status as status','type_id')
            ->join('faq_type', 'faq.type_id', '=', 'faq_type.id')
            ->orderBY('faq.id', 'DESC')
            ->paginate(15);
        $data['faqs'] = $Faqs;
        return view('admin/pages/faq/faq-listing',$data);
    }

    function change_faq_status(Request $request)
    {

        $data = $request->faq_id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        //print_r($data);exit;
        $faq_id = $data[0];
        $status = $data[2];
        $faqs = Faqs::query()
            ->select("id", "status")
            ->where("id", "=", $faq_id)
            ->first();
       // print_r($faqs);exit;
        $faqs->status = $status;
        $faqs->save();
        if ($status == 2) {
            return redirect(route('faq-listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('faq-listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('faq-listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('faq-listing'));
    }


}
