<?php

namespace App\Http\Controllers\admin;
use App\Models\Users;
use App\Models\Countries;
use App\Models\PricePlan;
use App\Models\UsersDetails;
use App\Models\Restaurant;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class PricePlanController extends Controller
{
    function update_price_plan()
    {
        $data['title'] = 'Price plan';
        $data['breadcrums'] = 'Price plan';
		$countriesModal = new Countries();
        $languages = $countriesModal->geLanguages();
		$data['languages'] =  $languages;
        return view('admin/price-plan/update_price_plan',$data);
    }

    function price_plan_submitted(Request $request)
    {
        $data = $request->all();
		//echo "<pre>";
		//print_r($data);
		//exit;
		// Activation
		
		if($data['price_plan'][1]['economic'] || $data['price_plan'][1]['standered'] || $data['price_plan'][1]['premium']) {
				$pricePlan = PricePlan::select("*")
						->where("id", "=",$data['price_plan'][1]['id'])
						->where("type", "=",1)
						->first();
				if(!$pricePlan) {
					$pricePlan = new  PricePlan();
				}					
				
				$pricePlan->economic  =  		 isset($data['price_plan'][1]['economic']) ? trim($data['price_plan'][1]['economic']) : '';
				$pricePlan->economic_status  =   isset(($data['price_plan'][1]['economic_status'])) ? trim($data['price_plan'][1]['economic_status']) : '';
				$pricePlan->standered =   		 isset(($data['price_plan'][1]['standered'])) ? trim($data['price_plan'][1]['standered']) : '';
				$pricePlan->standered_status =   isset(($data['price_plan'][1]['standered_status'])) ? trim($data['price_plan'][1]['standered_status']) : '';
				$pricePlan->premium   =   		 isset(($data['price_plan'][1]['premium'])) ? trim($data['price_plan'][1]['premium']) : '';
				$pricePlan->premium_status   =   isset(($data['price_plan'][1]['premium_status'])) ? trim($data['price_plan'][1]['premium_status']) : '';
				$pricePlan->cid       =   $data['cid'];
				$pricePlan->type       =  1;
				$pricePlan->status    = 1;
				$pricePlan->save();
		}
        
		// Subscription
		
		if($data['price_plan'][2]['economic'] || $data['price_plan'][2]['standered'] || $data['price_plan'][2]['premium']) {
				$pricePlan = PricePlan::select("*")
						->where("id", "=",$data['price_plan'][2]['id'])
						->first();
				if(!$pricePlan) {
					$pricePlan = new  PricePlan();
				}					
				
				$pricePlan->economic  =    		  isset(($data['price_plan'][2]['economic'])) ? trim($data['price_plan'][2]['economic']) : '';
				$pricePlan->economic_status  =    isset(($data['price_plan'][2]['economic_status'])) ? trim($data['price_plan'][2]['economic_status']) : '';
				$pricePlan->standered =    		  isset(($data['price_plan'][2]['standered'])) ? trim($data['price_plan'][2]['standered']) : '';
				$pricePlan->standered_status =    isset(($data['price_plan'][2]['standered_status'])) ? trim($data['price_plan'][2]['standered_status']) : '';
				$pricePlan->premium   =    		  isset(($data['price_plan'][2]['premium'])) ? trim($data['price_plan'][2]['premium']) : '';
				$pricePlan->premium_status   =    isset(($data['price_plan'][2]['premium_status'])) ? trim($data['price_plan'][2]['premium_status']) : '';
				$pricePlan->cid       =  $data['cid'];
				$pricePlan->type       =  2;
				$pricePlan->status    = 1;
				$pricePlan->save();
		}
		
		// Activation
		
		if($data['price_plan'][3]['economic'] || $data['price_plan'][3]['standered'] || $data['price_plan'][3]['premium']) {
				$pricePlan = PricePlan::select("*")
						->where("id", "=",$data['price_plan'][3]['id'])
						->first();
				if(!$pricePlan) {
					$pricePlan = new  PricePlan();
				}					
				
				$pricePlan->economic  =   		 isset(($data['price_plan'][3]['economic'])) ? trim($data['price_plan'][3]['economic']) : '';
				$pricePlan->economic_status  =   isset(($data['price_plan'][3]['economic_status'])) ? trim($data['price_plan'][3]['economic_status']) : '';
				$pricePlan->standered =   		 isset(($data['price_plan'][3]['standered'])) ? trim($data['price_plan'][3]['standered']) : '';
				$pricePlan->standered_status =   isset(($data['price_plan'][3]['standered_status'])) ? trim($data['price_plan'][3]['standered_status']) : '';
				$pricePlan->premium   =   		 isset(($data['price_plan'][3]['premium'])) ? trim($data['price_plan'][3]['premium']) : '';
				$pricePlan->premium_status   =   isset(($data['price_plan'][3]['premium_status'])) ? trim($data['price_plan'][3]['premium_status']) : '';
				$pricePlan->cid       =   $data['cid'];
				$pricePlan->type       = 3;
				$pricePlan->status    = 1;
				$pricePlan->save();
		}
 
       
        return redirect()->back();
    }
	
	 function ajaxPriceData()
    {
        $data['title'] = 'Price plan';
        $data['breadcrums'] = 'Price plan';
		$cid = $_GET['cid'];
		$language = Countries::select("*")
						->where("id", "=",$cid)
						->get()
						->first();
		$data['language'] =  $language;
		
		$data['actvation'] = PricePlan::select("*")
						->where("cid", "=",$cid)
						->where("type", "=",1)
						->first();
		$data['subscription'] = PricePlan::select("*")
						->where("cid", "=",$cid)
						->where("type", "=",2)
						->first();	
		$data['transaction'] = PricePlan::select("*")
						->where("cid", "=",$cid)
						->where("type", "=",3)
						->first();
		//print_r($data);
		//exit;		
		
        return view('admin/price-plan/ajax_price_data',$data);
    }

}
