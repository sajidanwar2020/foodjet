<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\HomeSlider;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\MinimumOrder;
use App\Models\CreditTransactions;
use App\Models\Orders;
use App\Models\OrderItems;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Storage;
class OrdersController extends Controller
{


    public function order_listing()
    {
        $data['title'] = 'Orders';
        $data['breadcrums'] = 'Orders';
      //  $info = Session::get('is_logged_in');
       // $is_seller = Session::get('is_seller');
        $my_orders = Orders::select('orders.id as o_id', 'orders.address as o_address','additional_notes','payment_type',
            'distance_charges' ,'orders.phone_number','orders.status as o_status','user_details.first_name','user_details.last_name')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            //->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->orderBY('orders.id', 'DESC')
            ->groupBy('order_items.order_id')
            ->paginate(15);
        // echo $is_seller->id;exit;
        if($my_orders) {
            foreach($my_orders as $my_order) {
                $my_order->total_price = OrderItems::select('price','order_items.status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->sum('price');
                // Is all order status has been chnanged?
                $my_order->is_order_processed = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.status", "=",1)
                    ->count('status');
                // Sum cancelled orders
                $my_order->cancelled_amount = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.status", "=",3)
                    ->sum('price');
                // Check if online transaction has been completed
                $my_order->online_transaction = CreditTransactions::select('*')
                    ->where("order_id", "=", $my_order->o_id)
                    ->where("status", "=",1)
                    ->first();
            }
        }
        $data['orders'] = $my_orders;
        return view('admin/orders/orders',$data);
    }
    public function order_detail(Request $request)
    {
        $data = $request->all();
        $order_id = $data['order_id'];
        $data['title'] = 'Order Detail';
        $data['breadcrums'] = 'Order Detail';
        $info = Session::get('is_logged_in');
        $is_seller = Session::get('is_seller');
        //  print_r($is_seller);exit;
        $order_item_detail = Orders::select('orders.id as o_id', 'orders.address as o_address', 'orders.phone_number',
            'user_details.first_name','user_details.last_name','order_items.quantity','order_items.price','p.product_name','order_items.status as item_status','order_items.id as items_id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products as p', 'p.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->where("orders.id", "=", $order_id)
            //->groupBy('order_items.order_id')
            ->get();

        $order_detail_arr = array();
        if($order_item_detail) {
            foreach($order_item_detail as $order_detail) {
                $order_detail_arr['address'] = $order_detail->o_address;
                $order_detail_arr['phone_number'] = $order_detail->phone_number;
            }
        }
        $order_item_detail->order_detail = $order_detail_arr;
       // echo "<pre>";
       // print_r($order_item_detail);
       // exit;
        $data['order_detail'] = $order_item_detail;
        return view('admin/orders/order_detail',$data);
    }

    // Minimam order
    public function add_minimum_order()
    {
        $data['title'] = 'Add order range';
        $data['breadcrums'] = 'Add order range';
        return view('admin/setting/minimum_order/add_minimum_order',$data);
    }

    function add_minimum_order_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'km_from' => 'required',
            'km_to' => 'required',
            'order_value' => 'required',
        ]);
            $minimumOrder = new MinimumOrder();
            $minimumOrder->km_from = $data['km_from'];
            $minimumOrder->km_to = $data['km_to'];
            $minimumOrder->order_value = $data['order_value'];
            $minimumOrder->status = 0;
            $minimumOrder->save();


        return redirect(route('minimum-order-listing'))->with('info', "Order range created");

    }

    function edit_minimum_order()
    {
        $data['title'] = 'Edit order range';
        $data['breadcrums'] = 'Edit order range';

        $id = $_GET['id'];
        $id = base64_decode($id);
        $id = explode(":", $id)[0];
        $minimumOrder = MinimumOrder::select('*')
            ->where("id", "=", $id)
            ->first();
        if(!$minimumOrder) {
            $data['title'] = 'Order range';
            $data['breadcrumbs'] = 'Order range';
            return view('vendor/products/unauthorized', $data);
        }
        $data['minimum_order'] = $minimumOrder;
        return view('admin/setting/minimum_order/edit_minimum_order',$data);
    }

    function edit_minimum_order_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'km_from' => 'required',
            'km_to' => 'required',
            'order_value' => 'required',
        ]);

        $id = $data['minimum_order_id'];
        if ($id != '') {
            $minimumOrder = MinimumOrder::select("*")
                ->where("id", "=", $id)
                ->first();
        }

       // $minimumOrder = new MinimumOrder();
        $minimumOrder->km_from = $data['km_from'];
        $minimumOrder->km_to = $data['km_to'];
        $minimumOrder->order_value = $data['order_value'];
        $minimumOrder->save();

        return redirect(route('minimum-order-listing'))->with('info', "Order range updated");
    }

    function minimum_order_listing()
    {
        $data['title'] = 'All Order range';
        $data['breadcrums'] = 'All Order range';
        $minimumOrder = MinimumOrder::select('*')
            //->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $data['minimum_orders'] = $minimumOrder;
        return view('admin/setting/minimum_order/minimum_order-listing',$data);
    }

    function change_minimum_order_status(Request $request)
    {
        $data = $request->id;
        $data = base64_decode($data);
        $data = explode(":", $data);
        $id = $data[0];
        $status = $data[2];
        $minimumOrder = MinimumOrder::query()
            ->select("id", "status")
            ->where("id", "=", $id)
            ->first();
        $minimumOrder->status = $status;
        $minimumOrder->save();
        if ($status == 2) {
            return redirect(route('minimum-order-listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('minimum-order-listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('minimum-order-listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('minimum-order-listing'));
    }
}
