<?php

namespace App\Http\Controllers\admin;

use App\Models\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
     public function admin_dashboard()
    {
        $data['title'] = 'Dashboard';
        $data['breadcrums'] = 'Dashboard';
        $data['total_products'] =  Products::count();
        $data['total_orders'] = DB::table('order_items')->count(DB::raw('DISTINCT order_id'));
        return view('admin/dashboard/dashboard',$data);
    }
}
