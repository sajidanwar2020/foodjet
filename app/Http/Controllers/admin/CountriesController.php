<?php

namespace App\Http\Controllers\admin;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Restaurant; 
use App\Models\Countries;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CountriesController extends Controller
{

    public function country_listing()
    {
        $data['title'] = 'All country';
        $data['breadcrums'] = 'All country';

        $countries= Countries::select('*')
            ->orderBY('id', 'DESC')
            ->paginate(15);
        $data['countries'] = $countries;
        return view('admin/country/country-listing',$data);
    }

    public function add_country()
    {
        $data['title'] = 'Add country';
        $data['breadcrums'] = 'Add country';
        return view('admin/country/add_country',$data);
    }

    function add_country_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required',
            'iso_code' => 'required',
            'currency' => 'required'
        ]);
		
		//---------------------------------------------------------
		
		$countries = new Countries();
        $countries->name = $data['name'];
		$countries->iso_code = $data['iso_code'];
        $countries->currency = $data['currency'];
        $countries->save();
		
        return redirect(route('countries-listing'))->with('info', "countries created");
    }

    function edit_country()
    {
        $data['title'] = 'Edit country';
        $data['breadcrums'] = 'Edit country';

        $id = $_GET['cid'];
        $country = Countries::select('*')
            ->where("id", "=", $id)
            ->first();
			
        if(!$country) {
            return view('vendor/products/unauthorized', $data);
        }

        $data['country'] = $country;
        return view('admin/country/edit_country',$data);
    }

    function edit_country_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required',
        ]);

        $c_id = $data['cid'];

        if ($c_id != '') {
			$countries = Countries::select('*')
			->where("id", "=", $c_id)
			->first();
        }
		//----------------------------------------------------------------
		
		
        $countries->name = $data['name'];
		$countries->iso_code = $data['iso_code'];
        $countries->currency = $data['currency'];
        $countries->save();
		
		//----------------------------------------------------------------
		
        return redirect(route('countries-listing'))->with('info', "Updated");
    }

    function change_country_status(Request $request)
    {
        $data = $request->all();
      // print_r($data);exit;
        $data = base64_decode($data['cid']);
        $data = explode(":", $data);
        $id = $data[0];
        $status = $data[2];
        $countries = Countries::query()
            ->select("id", "status")
            ->where("id", "=", $id)
            ->first();
        $countries->status = $status;
        $countries->save();
        if ($status == 2) {
            return redirect(route('countries-listing', ['status' => $status]))->with('success', 'Deleted');
        } else if ($status == 1) {
            return redirect(route('countries-listing', ['status' => $status]))->with('success', 'Published');
        } else if ($status == 4) {
            return redirect(route('countries-listing', ['status' => $status]))->with('success', 'Deactivated');
        }
        return  redirect(route('countries-listing'));
    }
}
