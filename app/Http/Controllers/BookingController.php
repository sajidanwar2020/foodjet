<?php

namespace App\Http\Controllers;

use App\Models\Users;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Models\Booking;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use libphonenumber\NumberParseException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Cart;

class BookingController extends Controller
{

    function booking()
    {
        $data['title'] = 'Booking';
        $data['breadcrums'] = 'Booking';
        return view('sub-domin/booking/booking',$data);
    }
	
	function bookingSubmitted(Request $request)
    {
        $data['title'] = 'Booking';
        $data['breadcrums'] = 'Booking';
        $data = $request->all();
		//echo "<pre>";
		//print_r($data);
		//exit;
		$restaurantDetail = Session::get('restaurantDetail');
		$restaurant_code = $restaurantDetail->restaurant_code;
		
		$this->validate( $request, [   
				'first_name' => 'required',
				'last_name' => 'required',
				'email' => 'required',
				'phone_number' => 'required|regex:/^\+?[0-9]{3}-?[0-9]{6,12}$/',
				'datetimepicker' => 'required',
				'no_of_people' => 'required',
				'comments' => 'required',
			],
			[   
				'first_name.required'   => 'Le champ du prénom est obligatoire.',
				'last_name.required'    => 'Le champ du nom de famille est obligatoire.',
				'email.required' 		=> 'Le champ email est obligatoire.',
				'phone_number.required' => 'Le champ du numéro de téléphone est obligatoire.',
				'phone_number.regex'	=> 'Le format du numéro de téléphone est incorrect',
				'datetimepicker.required'    => 'Le champ de date et d\'heure est obligatoire.',
				'no_of_people.required' => 'Le champ nombre de personnes est obligatoire.',
				'comments.required'     => 'Le champ de commentaire est requis.',
			]
		);
		
		$new_date = date('Y-m-d H:i:s', strtotime($data['datetimepicker']));
		
        $booking = new Booking();
        $booking->first_name = $data['first_name'];
        $booking->last_name = $data['last_name'];
        $booking->email = $data['email'];
        $booking->telephone = $data['phone_number'];
		$booking->date_time_arrival =   $new_date;
		$booking->no_of_people =   $data['no_of_people'];
		$booking->comments = $data['comments'];
        $booking->restaurant_id = $restaurantDetail->id;
        $booking->save();
		
		return redirect(route('booking', ['id' => $restaurantDetail->restaurant_code]))->with('success', 'La réservation a été soumise avec succès');
    }
	
	 public function myBooking(Request $request)
    {
        $data = $request->all();
		$data['title'] = 'All Booking';
        $data['breadcrums'] = 'All Booking';
		 $is_vendor = Session::get('is_vendor');
        $restaurant_id = $is_vendor['restaurant_id'];
		
        $bookings = Booking::select('*')
            ->where("restaurant_id", "=", $restaurant_id)
			->orderBY('id', 'DESC')
            ->paginate(15);
		
        $data['bookings'] = $bookings;
        return view('vendor/booking/booking',$data);
    }
	
	public function deleteBooking(Request $request)
    {
		$id = $request->id;
		Booking::where('id',$id)->delete();
		return back();
	}
	
}
