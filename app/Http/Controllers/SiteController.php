<?php

namespace App\Http\Controllers;


use App\Mail\ContactUs;
use App\Models\Category;
use App\Models\Faqs;
use App\Models\FaqTypes;
use App\Models\HomeSlider;
use App\Models\Orders;
use App\Models\Pages;
use Cart;
use App\Models\Products;
use App\Models\ProductsImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class SiteController extends Controller
{
    private $group;
    private $to;

    function __construct()
    {
        $this->to = 'sajidanwar2020@gmail.com';
    }
    public function index()
    {
        $data['title'] = 'All Product';
        $data['breadcrums'] = 'All Product';
        $cart_arr = $this->searchCart1();
        $cid = isset($_GET['c']) ? $_GET['c'] : 0;
        $all_products_query = Products::select('*')
            ->where("status", "=", 1);
        if (!empty($cid))
            $all_products_query->where("category_id", "=", $cid);

        $all_products_query->orderBY('products.id', 'ASC');
        $all_products = $all_products_query->get()->all();

        $categories = Category::select('*')
            ->where("status", "=", 1)
            ->where("parent_id", "=", 0)
            ->orderBY('id', 'DESC')
            ->get()->all();

        if ($categories) {
            foreach ($categories as $category) {
                $category->sub_categories = Category::select('*')
                    ->where("status", "=", 1)
                    ->where("parent_id", "=", $category->id)
                    ->orderBY('id', 'DESC')
                    ->get()->all();

                $products = Products::select('*')
                    ->where("status", "=", 1)
                    ->where("category_id", "=", $category->id)
                    ->orderBY('products.id', 'ASC')
                    ->get()->all();
                foreach ($products as $product) {
                    $product_images = ProductsImages::select('*')
                        //->where("status", "=", 1)
                        ->where("product_id", "=", $product['id'])
                        ->orderBY('product_images.id', 'ASC')
                        ->get()->all();
                    $product->product_images = $product_images;
                    $product->is_cart = 'no';
                    if (array_key_exists($product['id'], $cart_arr)) {
                        $product->cart_id = $cart_arr[$product['id']];
                        $product->is_cart = 'yes';
                    }
                }
                $category['products_arr'] = $products;
            }
        }
        //echo "<pre>";
        // print_r($categories);
        //exit;
        $data['categories'] = $categories;
        $data['all_products'] = $all_products;
        $data['cid'] = $cid;
        //echo Cart::count();exit;
        $data['cart_count'] = Cart::count();
        return view('site/home_new', $data);
    }

    public function index1()
    {
        $data['title'] = 'All Product';
        $data['breadcrums'] = 'All Product';
		//$objPHPExcel = new \PHPExcel();
		//echo "hereeeeeee";exit;
        //-----------------------------------------------------------------------------
        //$data['parent_child_category'] = getParentCategory();
        //-----------------------------------------------------------------------------
        $child_categories = Category::select('*')
            ->where("status", "=", 1)
            ->where("parent_id", "!=", 0)
            ->orderBY('id', 'DESC')
            ->get();

        $cart_arr = $this->searchCart1();
        //-----------------------------------------------------------------------------
        if ($child_categories) {
            foreach ($child_categories as $category) {
                //$child_cat_id = getChildCategoriesIds($category->id);
                $products = Products::select('*')
                    ->where("status", "=", 1)
                    ->where('category_id',"=", $category->id)
                    ->inRandomOrder()
					->limit(10)
                    ->get();

                foreach ($products as $product) {
                    $product_images = ProductsImages::select('*')
                        //->where("status", "=", 1)
                        ->where("product_id", "=", $product['id'])
                        ->orderBY('product_images.id', 'ASC')
                        ->get()
						->all();
                    $product->product_images = $product_images;
                    $product->is_cart = 'no';
                    if (array_key_exists($product['id'], $cart_arr)) {
                        $product->cart_id = $cart_arr[$product['id']];
                        $product->is_cart = 'yes';
                    }
                }
                $category['products_arr'] = $products;
            }
        }

        $homeSlider = HomeSlider::select('*')
            ->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->get();

        //echo "<pre>";
        //print_r($child_categories);
        //exit;
        $data['homeSlider'] = $homeSlider;
        $data['categories'] = $child_categories;
        $data['cart_count'] = Cart::count();
        return view('site/home_new', $data);
    }

    function searchCart($rowId)
    {
        return Cart::search(function ($cartItem, $rowId) {
            // print_r($cartItem);exit;
            // print_r($rowId);exit;
            return $cartItem->rowId === $rowId;
        });
    }

    function searchCart1()
    {
        $product_arr = array();
        if (count(Cart::content()) > 0) {
            foreach (Cart::content() as $row) {
                $product_arr[$row->id] = $row->rowId;
            }
        }
        return $product_arr;
    }

    public function page($slug)
    {
        $data['cart_count'] = Cart::count();
        $page_detail = Pages::select('*')
            ->where("slug", "=", $slug)
            ->first();
        if(!$page_detail) {
            return abort(404);
        }
        $data['page_detail'] = $page_detail;
        return view('site.page', $data);
    }

    function contact_us()
    {
        $data['cart_count'] = Cart::count();
        return view('site.contact-us', $data);
    }
    function faq()
    {
        $data['cart_count'] = Cart::count();


        $faqTypes = FaqTypes::select('*')
            ->where("status", "=", 1)
            ->orderBY('id', 'DESC')
            ->get()->all();

        if ($faqTypes) {
            foreach ($faqTypes as $faqType) {
                $faqs = Faqs::select('*')
                    ->where("status", "=", 1)
                    ->where("type_id", "=", $faqType->id)
                    ->orderBY('id', 'DESC')
                    ->get()
                    ->all();
                $faqType->faqs = $faqs;
            }
        }
        $data['faqs'] = $faqTypes;
        return view('site.faq', $data);
    }

    function contact_submitted(Request $request)
    {
        $data = $request->all();
        $this->validate($request, [
            'name' => 'required|min:5|max:255',
            'email' => 'required|email|min:5|max:255',
            'phone' => 'required|min:5|max:255',
            'message' => 'required|min:5|max:255',
        ]);
        $info['name'] = $data['name'];
        $info['email'] = $data['email'];
        $info['phone'] = $data['phone'];
        $info['message'] = $data['message'];
        Mail::to($this->to)->send(new ContactUs($info));
        return redirect(route('contact_us'))->with('success', "Thank you for getting in touch,our support team will get back in touch with you soon!");
    }

    function category(Request $request)
    {
        $data = $request->all();
        $data['title'] = 'Category';
        $data['breadcrums'] = 'Category';
        //-------------------------------------------getChildCategoryByParentID-----------------------------------
        $categories = getCategoryhiercharchy($data['cat_id']);
        $catgeory_detail = Category::select('*')
            ->where("id", "=", $data['cat_id'])
            ->first();
        $data['lable'] = '';
        if($catgeory_detail)
            $data['lable'] = $catgeory_detail->name;
        $data['categories'] = $categories;
        $cart_arr = $this->searchCart1();
        //------------------------------------------Price range----------------------------------------------------
        $price_range = $this->getPriceRange();
        $data['price_range'] = $price_range;
        $filter_price = array();
        $filter_price['min_price'] = $price_range['min_price'];
        $filter_price['max_price'] = $price_range['max_price'];
        if(isset($data['price']) AND $data['price'] !="") {
            $_price = explode(',',$data['price']);
            $filter_price['min_price'] = $_price[0];
            $filter_price['max_price'] = $_price[1];
        }
        $data['filter_price'] = $filter_price;
        //--------------------------------------------End price----------------------------------------------------
        $products_query = Products::select('*')
            ->where("status", "=", 1);
        //---------------------------------------------Filter -----------------------------------------------------

        if(isset($data['price']) AND $data['price'] !="") {
            $products_query->whereBetween('regular_price', [$filter_price['min_price'], $filter_price['max_price']]);
        }
        if(isset($data['cat_id']) AND $data['cat_id'] !="") {
            $cat_ids = getChildCategoriesIds($data['cat_id']);
           // print_r($cat_ids);exit;
            if($cat_ids) {
                $products_query->whereIn('category_id', $cat_ids);
            }
            else {
                $products_query->where('category_id','=', $data['cat_id']);
            }
        }
        if(isset($data['sorting']) AND $data['sorting'] !="") {
            if($data['sorting'] == 'price-asc')
                $products_query->orderBy('regular_price', 'ASC');
            else if($data['sorting'] == 'price-desc')
                $products_query->orderBy('regular_price', 'DESC');
            else
                $products_query->orderBY('products.id', 'ASC');
        }
        //-------------------------------------------- End filter-------------------------------------------------
        $products = $products_query->paginate(15);
        if ($products) {
            foreach ($products as $product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()
                    ->all();
                $product->product_images = $product_images;
                $product->is_cart = 'no';
                if (array_key_exists($product['id'], $cart_arr)) {
                    $product->cart_id = $cart_arr[$product['id']];
                    $product->is_cart = 'yes';
                }
            }
        }
        //--------------------------------------------------------------------------------------------------------------
        $feature_products_query = Products::select('*')
            ->where("status", "=", 1);
        $feature_products_query->orderByRaw('RAND()');
        $feature_products = $feature_products_query->limit(5)->get();
		  if ($feature_products) {
            foreach ($feature_products as $feature_product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $feature_product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()
                    ->all();
                $feature_product->product_images = $product_images;
            }
        }
        $data['feature_products'] = $feature_products;
        //--------------------------------------------------------------------------------------------------------------

        $data['products'] = $products;
        //echo Cart::count();exit;
        $data['cart_count'] = Cart::count();
        //print_r($data);exit;
        return view('site/category', $data);
    }

    public function getPriceRange() {
        $price_range_arr = array();
        $priceRange = DB::table('products')->where("status", "=", 1);
        $price_range_arr['max_price'] = $priceRange->max('regular_price');
        $price_range_arr['min_price'] = $priceRange->min('regular_price');
        return $price_range_arr;
    }
}
