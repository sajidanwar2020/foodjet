<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Drivers;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\CreditTransactions;
use App\Models\Orders;
use App\Models\OrderItems;
use App\Models\Category;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Storage;
class DriversController extends Controller
{
   
    function get_orders(Request $request)
    {
        $get_inputs = $request->all();
		$driver_id = (int)$get_inputs['driver_id'];
		if(!$driver_id || $driver_id < 1) {
			resp(0, "Driver id is missing", ['driver_id' => $driver_id]);
		}
		$my_orders = Orders::select('orders.id as o_id', 'orders.address as o_address','orders.driver_id as 0_driver_id','orders.latitude','orders.longitude','additional_notes','payment_type',
            'distance_charges' ,'orders.phone_number','orders.status as o_status','user_details.first_name','user_details.last_name')
            //->join('order_items', 'orders.id', '=', 'order_items.order_id')
            //->join('products', 'products.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
			->where("orders.driver_id", "=",$driver_id)
			//->where("o_status", "!=",5) 
			->orwhereNull('orders.driver_id', '=' ,null)
            ->orderBY('orders.id', 'DESC')
            //->groupBy('order_items.order_id')
            ->get()->all();  
        // echo $is_seller->id;exit;
        if($my_orders) {
            foreach($my_orders as $my_order) {
                // Is all order status has been chnanged?
                //$my_order->is_order_processed = OrderItems::select('status')
                    //->where("order_items.order_id", "=", $my_order->o_id)
                    //->where("order_items.status", "=", 2)
                    //->count('status');
				$order_calc_ret =  $this->order_calc($my_order->o_id);
				$my_order->order_items_amount = 0;
				if(count($order_calc_ret) > 0) {
					$my_order->order_items_amount =$order_calc_ret['accpeted'] - $order_calc_ret['cancelled'];
				}
				
                // Sum accepted items amount
				$my_order->accepted_items_amount = 0;
				if(count($order_calc_ret) > 0)
					$my_order->accepted_items_amount = $order_calc_ret['accpeted'] - $order_calc_ret['cancelled'];

                // Sum cancelled items amount
				/*
                $my_order->cancelled_items_amount = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $my_order->o_id)
                    ->where("order_items.status", "=", 3)
                    ->sum('price'); */
				$my_order->cancelled_items_amount = 0;
					if(count($order_calc_ret) > 0)
						$my_order->cancelled_items_amount = $order_calc_ret['cancelled'];   	

                // Check if online transaction has been completed
                $my_order->online_transaction = CreditTransactions::select('*')
                    ->where("order_id", "=", $my_order->o_id)
                    ->where("status", "=",1)
                    ->first();
            }
        }
		
		resp(1, "New order detail", ['order_detail' => $my_orders]);
       
	}

	function order_calc($order_id)
    {
		//echo "hereeee";exit; 
		$_orders = OrderItems::select('*')
                    ->where("order_items.order_id", "=", $order_id)
                    //->where("order_items.status", "=", 2)
                    ->get()->all();  
		$order_calc = [];
		$accpeted = $cancelled_amount = 0;		
		if($_orders) {
            foreach($_orders as $_order) {
				//if($_order->status == 2)
					$accpeted += $_order->quantity * $_order->price;
				if($_order->status == 3)
					$cancelled_amount += $_order->quantity * $_order->price;
			}
			$order_calc['accpeted'] = $accpeted;
			$order_calc['cancelled'] = $cancelled_amount;
		}			
		return $order_calc;
	}
	
	function accept_item(Request $request)
    {
		
		$get_inputs = $request->all();
		
		$driver_id = (int)$get_inputs['driver_id'];
        if(!$driver_id || $driver_id < 1) {
			resp(0, "Driver not found", ['driver_id' => $driver_id]);
		}
        $driver = Users::select('*')
			->where("id", "=", $driver_id)
			->where("role_id", "=", 4)
			->first();
        if(!$driver || !$driver->id) {
			resp(0, "Driver not found", ['driver_id' => $driver_id]);
		}

		$order_id = (int)$get_inputs['order_id'];
        if(!$order_id || $order_id < 1) {
			resp(0, "Order not found", ['order_id' => $order_id]);
		}
		$order = Orders::select('*')
			->where("id", "=", $order_id)
			->first();
		if(!$order || !$order->id) {
			resp(0, "Order not found", ['order_id' => $order_id]);
		}
		if($order->status != 1 && $order->driver_id != $driver_id){
			resp(0, "Order already processed", ['order_id' => $order_id]);				
		}
		$order->driver_id = $driver_id;
		$order->status = 2;
		$order->save();

		$order_item = null;
		$order_item_id = (int)$get_inputs['item_id'];
		if($order_item_id && $order_item_id > 0) {
			$order_item = OrderItems::select('*')
				->where("id", "=", $order_item_id)
				->first();
			if(!$order_item || !$order_item->id) {
				resp(0, "Order Item not found", ['item_id' => $order_item_id]);
			}
			if($order_item->order_id != $order->id){
				resp(0, "Order Item not found", ['item_id' => $order_item_id]);
			}
			
			$item_status = (int)$get_inputs['item_status'];
			if(!$item_status || $item_status < 1 || $item_status > 5){
				$item_status = 1;
			}
			$order_item->status = $item_status;
			$order_item->save();
		}
		
		$order_item_check = OrderItems::select('*')
			->where("status", "=", 1)
			->where("order_id", "=", $order_id)
			->first();

		if(!$order_item_check || !$order_item_check->id){
			//$order->status = 3;
			//$order->save();			
		}
		$message = 'Order status has been changed successfully';
		if($item_status == 2)
			$message = 'Item has been approved for delivery';
		if($item_status == 3)
			$message = 'Item has been rejected from order';
		
		resp(1, $message, ['order' => $order, 'order_item' => $order_item]);
    }
		
    

	
	function reject_order(Request $request)
    {
		
		$get_inputs = $request->all();
		
		$driver_id = (int)$get_inputs['driver_id'];
        if(!$driver_id || $driver_id < 1) {
			resp(0, "Driver not found", ['driver_id' => $driver_id]);
		}
        $driver = Users::select('*')
			->where("id", "=", $driver_id)
			->where("role_id", "=", 4)
			->first();
        if(!$driver || !$driver->id) {
			resp(0, "Driver not found", ['driver_id' => $driver_id]);
		}

		$order_id = (int)$get_inputs['order_id'];
        if(!$order_id || $order_id < 1) {
			resp(0, "Order not found", ['order_id' => $order_id]);
		}
		$order = Orders::select('*')
			->where("id", "=", $order_id)
			->first();
		$driver_ids_rejected_arr = explode(",", trim($order['driver_ids_rejected'],","));
		//print_r($driver_ids_rejected);exit;
			//print_r( $order['driver_ids_rejected']);exit;
		if(!$order || !$order->id) {
			resp(0, "Order not found", ['order_id' => $order_id]); 
		}
		if (in_array($driver_id, $driver_ids_rejected_arr))
		{
			
		}

		$order->driver_ids_rejected = $order['driver_ids_rejected'].','.$driver_id;
		$order->save();
		
		resp(1, "Order has been rejected", ['order' => $order]);
		
    }
	
	
	
	function change_order_status(Request $request)
    {
		
		$get_inputs = $request->all();
		
		$driver_id = (int)$get_inputs['driver_id'];
        if(!$driver_id || $driver_id < 1) {
			resp(0, "Driver not found", ['driver_id' => $driver_id]);
		}
        $driver = Users::select('*')
			->where("id", "=", $driver_id)
			->where("role_id", "=", 4)
			->first();
        if(!$driver || !$driver->id) {
			resp(0, "Driver not found", ['driver_id' => $driver_id]);
		}

		$order_id = (int)$get_inputs['order_id'];
        if(!$order_id || $order_id < 1) {
			resp(0, "Order not found", ['order_id' => $order_id]);
		}
		
		$order_item_check = OrderItems::select('*')
			->where("status", "=", 1)
			->where("order_id", "=", $order_id)
			->first();

		if($order_item_check || $order_item_check['id']){
			resp(0, "All item has been not processed", ['order_id' => $order_id]);		
		}
		
		$order = Orders::select('*')
			->where("id", "=", $order_id)
			->first();
		$order_status = (int)$get_inputs['order_status'];
		$order->status = $order_status;
		$order->save();
		
		resp(1, "Order status has been changed", ['order' => $order]);
		
    }
	
	 public function _orders_detail(Request $request)
    {
        $data = $request->all();

		 if(!$data['order_id'] || $data['order_id'] < 1) {
			resp(0, "Order not found", ['order_id' => $data['order_id']]);
		}
		//echo $data['order_id'];exit;  
        $order_item_detail = Orders::select('orders.id as o_id', 'orders.address as o_address','distance_charges','orders.created_at as o_created_at', 'orders.phone_number','payment_type',
            'user_details.first_name','user_details.last_name','order_items.quantity','order_items.price','order_items.product_id','p.product_name','p.regular_price','order_items.status as item_status','order_items.id as items_id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products as p', 'p.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->where("orders.id", "=", $data['order_id'])
            ->get();

        $order_detail_arr = array();
        if($order_item_detail) {
            foreach($order_item_detail as $order_detail) {
				$product_images = ProductsImages::select('*')
						->where("product_id", "=",$order_detail->product_id)
                        ->orderBY('product_images.id', 'ASC')
                        ->get()->all();
                $order_detail->product_images = $product_images;
				  
				
				 $order_detail->order_items_amount = OrderItems::select('price','order_items.status')
                    ->where("order_items.order_id", "=", $order_detail->o_id)
                    ->sum('price');
				//echo $data['order_id'];exit;  
				$order_calc_ret =  $this->order_calc($data['order_id']);
				
                // Sum accepted items amount
				$order_detail->accepted_items_amount = 0;
				if(count($order_calc_ret) > 0)
					$order_detail->accepted_items_amount = $order_calc_ret['accpeted'];
	
                // Sum accepted items amount
				/* 
                $order_detail->accepted_items_amount = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $order_detail->o_id)
                    ->where("order_items.status", "=", 2)
                    ->sum('price'); */

                // Sum cancelled items amount
                $order_detail->cancelled_items_amount = OrderItems::select('status')
                    ->where("order_items.order_id", "=", $order_detail->o_id)
                    ->where("order_items.status", "=", 3)
                    ->sum('price');
				
				
				
                $order_detail_arr['address'] = $order_detail->o_address;
                $order_detail_arr['phone_number'] = $order_detail->phone_number;
                $order_detail_arr['shiping_cost'] = $order_detail->distance_charges;
            }
        }
        $order_item_detail->order_detail = $order_detail_arr;
		
		resp(1, "Order item detail", ['item_detail' => $order_item_detail]);
  
    }
	
	
	function get($key, $def = NULL)
    {
        if (isset($this->input[$key])) {
            return $this->input[$key];
        } elseif (isset($this->data[$key])) {
            return $this->data[$key];
        } elseif ($def !== NULL) {
            return $def;
        } else {
            return resp(0, "Please provide all required data. Missing : $key");
        }
    }


}
