<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Mail\OrderPlaced;
use App\Models\Category;
use App\Models\CreditTransactions;
use App\Models\HomeSlider;
use App\Models\OrderItems;
use App\Models\Orders;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Users;
use App\Models\UsersDetails;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Storage;

/**
 * Class OfferApiController
 * @package App\Http\Controllers\Api
 */
class CheckoutController extends Controller
{
    /**
     *  stores all the data received from get/post
     */
    private $input;
    /**
     * stores all the extra data sent in the data array
     */
    private $data;

    public function __construct()
    {

    }

    function proceed_check_out(Request $request) {

        try {
            $this->input = $request->all();
            $json_data = $request->all();
           // print_r($json_data);exit;
           // $email = $this->get('email');
           // $password = $this->get('password');
            if(count($json_data) > 0) {

                $orders = new Orders();
                $orders->address = $json_data['location'];
                $orders->latitude = $json_data['latitude'];
                $orders->longitude = $json_data['longitude'];
                $orders->user_id = $json_data['user_id'];
                $orders->phone_number = $json_data['phone_number'];
                $orders->payment_type = $json_data['payment_type'];
                $orders->additional_notes = $json_data['additional_notes'];
                $orders->distance_charges = $json_data['distance_charges'];
                $orders->status = '1';
                $orders->save();
                $orders_id = $orders->id;

                foreach($json_data['order_items'] as $row)  {
                    // print_r($row);exit;
                    $products_detail = Products::find($row['p_id']);
                    $orderItems = new OrderItems();
                    $orderItems->product_id = $products_detail->id;
                    $orderItems->order_id = $orders_id;
                    $orderItems->seller_id = $products_detail->seller_id;
                    $orderItems->quantity = $row['quantity'];
                    $orderItems->price =    $row['price'];
					$orderItems->sale_price = 0;
					if(isset($products_detail->sale_price))
						$orderItems->sale_price = $products_detail->sale_price;
					$orderItems->regular_price = 0;
					if(isset($products_detail->regular_price))
						$orderItems->regular_price = $products_detail->regular_price;
                    $orderItems->status = '1';
                    $orderItems->save();
                }
               // $order_info = array("order_detail"=>$orders);
               // Mail::to($user_detail['email'])->send(new OrderPlaced($order_info));
                resp(1, "Your order has been placed successfully", ['order_detail' => $orders]);
            }

        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function distance_detail(Request $request) {
        $data = $request->all();
        if(isset($data['distance_km']) AND $data['distance_km'] > 0) {
            $minimumOrder = DB::select("SELECT order_value FROM minimum_order WHERE ".$data['distance_km']." BETWEEN km_from AND  km_to");
            // print_r($minimumOrder);
            if(isset($minimumOrder[0]->order_value))
                resp(1, "Minimum Order", ['minimumOrder' => $minimumOrder[0]->order_value]);
            else
                resp(0, "Not found", ['minimumOrder' => []]);
        } else {
            resp(0, "Distance not set", ['minimumOrder' => []]);
        }
    }

    function get($key, $def = NULL)
    {
        if (isset($this->input[$key])) {
            return $this->input[$key];
        } elseif (isset($this->data[$key])) {
            return $this->data[$key];
        } elseif ($def !== NULL) {
            return $def;
        } else {
            return resp(0, "Please provide all required data. Missing : $key");
        }
    }
}
