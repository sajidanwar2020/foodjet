<?php

namespace App\Http\Controllers\Api;

use App\Models\Categories;
use App\Models\CategoriesTypes;
use App\Models\Countries;
use App\Models\CreditTransactions;
use App\Models\Groups;
use App\Models\Offers;
use App\Models\TimeZones;
use App\Models\Users;
use App\Models\UsersDetails;
use Braintree_Gateway;
use Braintree_PaymentInstrumentType;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PaymentsApiController extends Controller
{
    private $data;
    private $gateway;

    public function __construct()
    {
        $this->gateway = new Braintree_Gateway([
            'environment' => 'sandbox',
            'merchantId' => '55drgdjybck3hns8',
            'publicKey' => 'j9k4zk9hknrpcwbg',
            'privateKey' => 'ac85f14b4cb61d9aee06fd4b73943f7d'
        ]);
    }

    function getPayPalClientToken(Request $request)
    {
        $this->data = $request->all();
        try {
            $user_id = $this->get('user_id');
            $clientToken = $this->gateway->clientToken()->generate();
            resp(1, "Client token", ['token' => $clientToken]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function makePaymentFromNonce(Request $request)
    {
        $this->data = $request->all();
        try {
            $user_id = $this->get('user_id');
            $nonce = json_decode($this->get('nonce'), true);
            $amount = $this->get('amount');
            $amount = number_format((float)$amount, 2, '.', '');

            $user = UsersDetails::query()->where('user_id', $user_id)->first();
//            print_r($user);
            $nonce_str = $nonce['mNonce'];
            $result = $this->gateway->transaction()->sale([
                'amount' => $amount,
                'paymentMethodNonce' => $nonce_str,
                'options' => [
                    'submitForSettlement' => True
                ]
            ]);
            $result = json_decode(json_encode($result));
            if ($result->success) {
                $credit = new CreditTransactions();
                $transaction = $result->transaction;
                if (in_array($transaction->status, ['settled', 'submitted_for_settlement', 'settling', 'settlement_pending'])) {
                    $is_paypal = $transaction->paymentInstrumentType == Braintree_PaymentInstrumentType::PAYPAL_ACCOUNT;
                    if ($is_paypal) {
                        $email = $transaction->paypal->payerEmail;
                        $credit->payment_source = $email;
                    } else {
                        $credit->payment_source = $result->transaction->creditCard->cardType . '-' . $result->transaction->creditCard->last4;
                    }

                    $credit->user_id = $user_id;
                    $credit->trans_id = $transaction->id;
                    $credit->amount = $amount;
                    $credit->currency = $transaction->currencyIsoCode;
                    $credit->status = $transaction->status;
                    $credit->payment_response = json_encode($result);
                    $credit->new_credit = $user->balance + floatval($amount);
                    $credit->type = 3;
                    $credit->status = 1;
                    $credit->status_text = $transaction->status;
                    $credit->save();

                    $user->balance = $user->balance + floatval($amount);
                    $user->save();
                    resp(1, "Payment successfully completed.", ['result' => $result, 'balance' => $user->balance]);
                }
            }
            resp(1, "Payment failed", ['result' => $result, 'balance' => $user->balance]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function resp($success, $message, $data = [])
    {
        $resp ['success'] = $success;
        $resp['response'] = $message;
        if (!empty($data))
            $resp['data'] = $data;
        echo json_encode($resp);
    }

    function get($key, $def = null)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        } elseif ($def !== null) {
            return $def;
        } else {
            echo $this->resp(0, "Please provide all required data.");
            exit();
        }
    }
}
