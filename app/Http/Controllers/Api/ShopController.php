<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CreditTransactions;
use App\Models\HomeSlider;
use App\Models\Products;
use App\Models\ProductsImages;
use App\Models\Users;
use App\Models\UsersDetails;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Storage;

/**
 * Class OfferApiController
 * @package App\Http\Controllers\Api
 */
class ShopController extends Controller
{
    /**
     *  stores all the data received from get/post
     */
    private $input;
    /**
     * stores all the extra data sent in the data array
     */
    private $data;
    /**
     * @var Offers Object of Offers model
     */
   // private $offer;

    /**
     * OfferApiController constructor.
     */
    public function __construct()
    {
      //  $this->offer = new Offers();
    }

    function getProductByCategory() {
        //echo "hereeeee";exit;
        try {
        $child_categories = Category::select('*')
            ->where("status", "=", 1)
            ->where("parent_id", "!=", 0)
            ->orderBY('id', 'DESC')
            ->get();

        //-----------------------------------------------------------------------------
        if ($child_categories) {
            foreach ($child_categories as $category) {
                //$child_cat_id = getChildCategoriesIds($category->id);
                $products = Products::select('*')
                    ->where("status", "=", 1)
                    ->where('category_id',"=", $category->id)
                    ->orderBY('products.id', 'ASC')
                    ->get()->all();

                foreach ($products as $product) {
                    $product_images = ProductsImages::select('*')
                        //->where("status", "=", 1)
                        ->where("product_id", "=", $product['id'])
                        ->orderBY('product_images.id', 'ASC')
                        ->get()->all();
					if($product->weight === NULL)	
						$product->weight = 	'';	
                    $product->product_images = $product_images;
                }
            }
        }
        //echo "<pre>";
        //print_r($child_categories);
        //exit;
        //$data['categories'] = $child_categories;
            resp(1, "Shop data", ['shop' => $child_categories]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function homeSlider() {
        try {
            $homeSlider = HomeSlider::select('*')
                ->where("status", "=", 1)
                ->orderBY('id', 'DESC')
                ->get();
            resp(1, "slider", ['slider' => $homeSlider]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function productByCategoryId(Request $request)
    {
        try {
            $data = $request->all();
			$page = $request->page;
            //-----------------------------------------------------------------------------
            if ($data['cat-id']) {
				$products = Products::select('*')
					->where("status", "=", 1)
					->where('category_id',"=", $data['cat-id'])
					->orderBY('products.id', 'ASC')
					->paginate(30)
					->appends(request()->except('page'));
					//->get()->all();

				foreach ($products as $product) {
					$product_images = ProductsImages::select('*')
						//->where("status", "=", 1)
						->where("product_id", "=", $product['id'])
						->orderBY('product_images.id', 'ASC')
						->get()->all();
					if($product->weight === NULL)	
						$product->weight = 	'';
					
					$product->product_images = $product_images;
				}
            }
            if(count($products) > 0)
                resp(1, "Shop data", ['products' => $products]);
            else
                    resp(0, "Not found any record", ['products' => []]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }
	 function search(Request $request)
    {
	
	   $data = $request->all();
		//echo $data['s'];exit;
        $products_query = Products::select('*')
            ->where("status", "=", 1);
        if (isset($data['s']) AND $data['s'] != "") {
            $products_query->where('product_name', 'LIKE', '%' . $data['s'] . '%');
    
        } else if (isset($data['c']) && !empty($data['c'])) {
            $products_query->where('category_id', '=', $data['c']);
            $category = Category::select('name')->where('id', '=', $data['c'])->first();
           
        }
        $products_query->orderBY('products.id', 'ASC');
        $products = $products_query->get()->all();

        if ($products) {
            foreach ($products as $product) {
                $product_images = ProductsImages::select('*')
                    //->where("status", "=", 1)
                    ->where("product_id", "=", $product['id'])
                    ->orderBY('product_images.id', 'ASC')
                    ->get()->all();
				if($product->weight === NULL)	
						$product->weight = 	'';	
                $product->product_images = $product_images;
            }
        }

       // $data['products'] = $products;
	   if($products)
			resp(1, "search", ['product' => $products]);
		else   
			resp(0, "search", ['product' => []]);
	}
	
    function get($key, $def = NULL)
    {
        if (isset($this->input[$key])) {
            return $this->input[$key];
        } elseif (isset($this->data[$key])) {
            return $this->data[$key];
        } elseif ($def !== NULL) {
            return $def;
        } else {
            return resp(0, "Please provide all required data. Missing : $key");
        }
    }
}
