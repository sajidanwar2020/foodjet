<?php

namespace App\Http\Controllers\Api;

use App\Mail\ForgotPassword;
use App\Models\Category;
use App\Models\CreditTransactions;
use App\Models\Users;
use App\Models\UsersDetails;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class UserApiController extends Controller
{
    private $input;
    private $data;
    private $object;

    public function __construct()
    {
       // $this->object = new Offers();
    }

    function splashData(Request $request)
    {
        $parentChildCategory = getParentChildCategory();
        $unitArray  =  array('1' =>'KG', '2' =>'Dozen');
        $minimumOrder = DB::select("SELECT * FROM minimum_order order by id ASC");
        $shipping_cost = DB::select("SELECT * FROM shipping_cost order by id ASC");
        $vendor_detail = Users::query()->select('latitude','longitude')->where(['role_id' => '2'])
            ->join('user_details as details', 'users.id', '=', 'details.user_id')
            ->first();
        $appData = [
					'category' => $parentChildCategory,
					'units' =>$unitArray,
					'minimumOrder' =>$minimumOrder,
					'vendor_detail'=>$vendor_detail,
					'shipping_cost'=>$shipping_cost,
					"currency_symbol" => '€'
					];
        resp(1, 'Data', $appData);
    }

    function login(Request $request)
    {
        $this->input = $request->all();
        try {
            $email = $this->get('email');
            $password = $this->get('password');
            $password = crypt($password, md5($password));
            $user = Users::query()->where(['email' => $email, 'password' => $password, 'role_id' => '2'])->first();
            if (!empty($user)) {

                $profile = UsersDetails::query()->where('user_id', $user->id)->first();
                $profile->device_token = $this->get('device_token', '');
                $profile->device_id = $this->get('device_id', '');
                $profile->device_model = $this->get('device_model', '');
                $profile->app_version = $this->get('app_version', '');
                $profile->save();

                $user = Users::query()->where(['email' => $email, 'password' => $password, 'role_id' => '2'])
                    ->join('user_details as details', 'users.id', '=', 'details.user_id')
                    ->first();
                resp(1, "Login successful", ['user' => $user]);
            } else {
                resp(0, "Invalid email/password combination");
            }
        } catch (Exception $e) {
            errorResp($e);
        }
    }

	
	function signin_driver(Request $request)
    {
        $this->input = $request->all();
		//echo "hereeeeeee";exit;
        try {
            $email = $this->get('email');
            $password = $this->get('password');
            $password = crypt($password, md5($password));
            $user = Users::query()->where(['email' => $email, 'password' => $password])->whereIn('role_id', array('3','4'))->first();
            if (!empty($user)) {

                $profile = UsersDetails::query()->where('user_id', $user->id)->first();
                $profile->device_token = $this->get('device_token', '');
                $profile->device_id = $this->get('device_id', '');
                $profile->device_model = $this->get('device_model', '');
                $profile->app_version = $this->get('app_version', '');
                $profile->save();
				$user = Users::query()->where(['email' => $email, 'password' => $password])->whereIn('role_id', array('3','4'))
                    ->join('user_details as details', 'users.id', '=', 'details.user_id')
                    ->first();
                resp(1, "Login successful", ['user' => $user]);
            } else {
                resp(0, "Invalid email/password combination");
            }
        } catch (Exception $e) {
            errorResp($e);
        }
    }
	
    function register(Request $request)
    {
        $this->input = $request->all();
        try {
           // $this->data = json_decode($this->get('data'), true);
            $email = $this->get('email');
            $phone = $this->get('phone');

            $res = Users::query()->where('email', $email)->orWhere('phone_number', $phone)->first();
            if (!empty($res)) {
                resp(0, $email == $res->email ? "Email is already registered, please use another email." :
                    "Phone number is already registered, please use another number");
            }

            $password = $this->get('password');
            $first_name = $this->get('first_name');
            $last_name = $this->get('last_name');
            $current_location = $this->get('location');
            $latitude = $this->get('latitude');
            $longitude = $this->get('longitude');
            $country_code = $this->get('country_code');
            $salutation = $this->get('salutation');
            $house_number = $this->get('house_number');
            $street = $this->get('street');
            $post_code = $this->get('post_code');
            $password = crypt($password, md5($password));

            $user = new Users();
            $user->email = $email;
            $user->phone_number = $phone;
            $user->password = $password;
            $user->role_id = '2';
            $user->status = '1';
            $user->save();

            $profile = new UsersDetails();
            $profile->user_id = $user->id;
            $profile->first_name = $first_name;
            $profile->last_name = $last_name;
            $profile->location = $current_location;
            $profile->latitude = $latitude;
            $profile->longitude = $longitude;
            $profile->country = $country_code;
            $profile->salutation = $salutation;
            $profile->house_number = $house_number;
            $profile->street = $street;
            $profile->post_code = $post_code;
            $profile->country_id = 1;
            $profile->phone_number = $phone;
            $profile->tax_id = '0';
            $profile->device_token = $this->get('device_token', '');
            $profile->device_id = $this->get('device_id', '');
            $profile->device_model = $this->get('device_model', '');
            $profile->app_version = $this->get('app_version', '');
            $profile->save();
            $user = Users::query()->where('users.id', $user->id)
                ->join('user_details as details', 'users.id', '=', 'details.user_id')
                ->first();
            resp(1, "You have successfully registered.", ['user' => $user]);
        } catch (Exception $e) {
            try {
                if (!empty($user)) {
                    $user->delete();
                }
            } catch (Exception $e) {
            }
            errorResp($e);
        }
    }
	
	function update_driver_location(Request $request)
    {
        $this->input = $request->all();
        try {
           // $this->data = json_decode($this->get('data'), true);
            $driver_id = $this->get('driver_id');
			$latitude = $this->get('latitude');
            $longitude = $this->get('longitude');

            $profile = UsersDetails::query()->where('user_id', $driver_id)->first();
			//echo $driver_id;exit;
			//print_r($profile);exit;
            if (empty($profile)) {
                resp(0, "Sorry driver not found",['driver' => []]);
            }
			
            $profile->latitude = $latitude;
            $profile->longitude = $longitude;
            $profile->save();
            resp(1, "Updated driver location.", ['user' => $profile]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function customerUpdateProfile(Request $request)
    {
        $this->input = $request->all();
		//echo "hereeeeeeee";exit;
        try {
           // $this->data = json_decode($this->get('data'), true);
			$phone = $this->get('phone');
            $user_id = $this->get('user_id');
            $user = Users::query()->select('id')->where('id', $user_id)->first();
			
			// Check if phone is already exist
			$check_phone = Users::query()->select('phone_number')->where('phone_number', $phone)->first();
			if($check_phone) {
				resp(0, "Phone number is already exist!", ['user' => []]);
			}
			
            $user->phone_number = $this->get('phone');
            $user->save(); 

            $profile = UsersDetails::query()->select('id', 'photo')->where('user_id', $user_id)->first();

            $profile->first_name = $this->get('first_name');
            $profile->last_name = $this->get('last_name');
           // $profile->description = $this->get('description');
            $profile->location = $this->get('location');
            $profile->latitude = $this->get('latitude');
            $profile->longitude = $this->get('longitude');
            $profile->street = $this->get('street');
            $profile->post_code = $this->get('post_code');
            $profile->phone_number = $this->get('phone');
            $profile->house_number = $this->get('house_number');
            $profile->salutation = $this->get('salutation');
            $profile->save();

            $upd_user = UsersDetails::query()->where('user_id', $user_id)->first();
            resp(1, "Profile update successfully!", ['user' => $upd_user]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function forgotPassword(Request $request)
    {
        try {
            $this->input = $request->all();
            $email = $this->get('email');
            $user = Users::query()->where('email', $email)->first();
            if (!empty($user)) {
                $code = mt_rand(100000, 999999);
                $user->verification_code = $code;
                $user->save();
                $details = UsersDetails::query()
                    ->select('id', 'first_name', 'last_name')
                    ->where('user_id', '=', $user->id)
                    ->first();
                $token = base64_encode($email . ':' . $code);
                $info = array(
                    'first_name' => $details->first_name,
                    'last_name' => $details->last_name,
                    'link' => route('reset_password', ['token' => $token])
                );
                Mail::to($email)->send(new ForgotPassword($info));
                resp(1, 'Reset password link has been sent to your email.');
            } else {
                resp(0, 'This email is not linked with any account. Please enter correct email.');
            }
        } catch (Exception $e) {
            errorResp($e);
        }
    }


    function getProfile(Request $request)
    {
        $this->input = $request->all();
        print_r($this->input);
        exit;
        $user_id = $this->get('user_id');

        try {
            $user_detail = UsersDetails::getUserDetails($user_id);
            resp(1, "Profile data", ['user' => $user_detail]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }

    function get($key, $def = NULL)
    {
        if (isset($this->input[$key])) {
            return $this->input[$key];
        } elseif (isset($this->data[$key])) {
            return $this->data[$key];
        } elseif ($def !== NULL) {
            return $def;
        } else {
            resp(0, "Please provide all required data. Missing : $key");
            exit();
        }
    }
}
