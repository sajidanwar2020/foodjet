<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\OrderItems;
use App\Models\Orders;
use Cart;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Storage;

class CustomerOrderController extends Controller
{
    /**
     *  stores all the data received from get/post
     */
    private $input;
    /**
     * stores all the extra data sent in the data array
     */
    private $data;

    public function __construct()
    {

    }

    function customer_orders(Request $request) {
        $this->input = $request->all();
        try {
            $user_id = $this->get('user_id');
           $my_orders = Orders::select('orders.id as o_id', 'orders.address as o_address','orders.driver_id as 0_driver_id','distance_charges','payment_type','orders.status as o_status')
               ->join('order_items', 'orders.id', '=', 'order_items.order_id')
               //->join('products', 'products.id', '=', 'order_items.product_id')
               ->where("orders.user_id", "=", $user_id)
               ->groupBy('order_items.order_id')
               ->orderBy('orders.id','DESC')
               ->get();
                //->paginate(15);
           //echo "<pre>";
          // print_r($my_orders);exit;
           if($my_orders) {
               foreach($my_orders as $my_order) {
                 //  echo "<pre>";
                  // print_r($my_order);
                  // exit;
				  
				  $order_calc_ret =  $this->order_calc($my_order->o_id);
				
                // Sum accepted items amount
				$my_order->total_price = 0;
				if(count($order_calc_ret) > 0)
					$my_order->total_price = $order_calc_ret['accpeted'] - $order_calc_ret['cancelled'];
					/*
                   // Total order price
                   $my_order->total_price = OrderItems::select('price','order_items.status')
                       ->where("order_items.order_id", "=", $my_order->o_id)
                       ->sum('price'); */

                   // Is all order status has been chnanged?
                   $my_order->is_order_processed = OrderItems::select('status')
                       ->where("order_items.order_id", "=", $my_order->o_id)
                       ->where("order_items.status", "=",1)
                       ->count('status');

                   $my_order->count_item = OrderItems::select('status')
                       ->where("order_items.order_id", "=", $my_order->o_id)
                       //->where("order_items.status", "=",1)
                       ->count('status');

                    // Sum cancelled orders
					/*
                   $my_order->cancelled_amount = OrderItems::select('status')
                       ->where("order_items.order_id", "=", $my_order->o_id)
                       ->where("order_items.status", "=",3)
                       ->sum('price'); */
					$my_order->cancelled_amount = 0;
					if(count($order_calc_ret) > 0)
						$my_order->cancelled_amount = $order_calc_ret['cancelled'];   
					   
               }
           }
          // print_r($my_orders);exit;
           if(count($my_orders) > 0)
                resp(1, "Customer order detail", ['my_orders' => $my_orders]);
           else
               resp(0, "Customer order not found", ['my_orders' => $my_orders]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }
	
	function order_calc($order_id)
    {
		//echo "hereeee";exit; 
		$_orders = OrderItems::select('*')
                    ->where("order_items.order_id", "=", $order_id)
                    //->where("order_items.status", "=", 2)
                    ->get()->all();  
		$order_calc = [];
		$accpeted = $cancelled_amount = 0;		
		if($_orders) {
            foreach($_orders as $_order) {
				//if($_order->status == 2)
					$accpeted += $_order->quantity * $_order->price;
				if($_order->status == 3)
					$cancelled_amount += $_order->quantity * $_order->price;
			}
			$order_calc['accpeted'] = $accpeted;
			$order_calc['cancelled'] = $cancelled_amount;
		}			
		return $order_calc;
	}
	
	
    public function customer_orders_detail(Request $request)
    {
        $this->input = $request->all();
        try {
             $data = $request->all();
                $order_id = $this->get('order_id');
                $user_id = $this->get('user_id');
                $order_item_detail = Orders::select('orders.id as o_id', 'orders.address as o_address','distance_charges', 'orders.phone_number','payment_type',
                    'user_details.first_name','user_details.last_name','order_items.quantity','order_items.price as regular_price',
					'p.product_name','p.id as product','order_items.status as item_status','order_items.id as items_id')
                    ->join('order_items', 'orders.id', '=', 'order_items.order_id')
                    ->join('products as p', 'p.id', '=', 'order_items.product_id')
                    ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
                    ->where("orders.user_id", "=", $user_id)
                    ->where("orders.id", "=", $order_id)
                    ->get();
					
					
						for ($i=0;$i<sizeof($order_item_detail);$i++) {
				$image = ProductsImages::select('*')
					//->where("status", "=", 1)
					->where("product_id", "=", $order_item_detail[$i]['product'])
					->orderBY('product_images.id', 'ASC')
					->first();
					
					
				if(!empty($image))
					$order_item_detail[$i]['product_image'] = $image->image_name;
				else
                    $order_item_detail[$i]['product_image'] = '';
				unset($order_item_detail[$i]['product']);
                }
				/*echo '<pre/>';
				print_r($image);
				print_r($order_item_detail);
				exit();
*/

                $order_detail_arr = array();
                if($order_item_detail) {
                    foreach($order_item_detail as $order_detail) {
                        $order_detail_arr['address'] = $order_detail->o_address;
                        $order_detail_arr['phone_number'] = $order_detail->phone_number;
                        $order_detail_arr['shiping_cost'] = $order_detail->distance_charges;
                    }
                }

               // $order_item_detail->order_detail = $order_detail_arr;
               //print_r($order_item_detail);exit;
              if(count($order_item_detail) > 0)
                    resp(1, "Customer order item detail", ['order_detail' => $order_item_detail,'customer_info'=>$order_detail_arr]);
              else
                    resp(0, "Customer order items detail not found", ['order_detail' => $order_item_detail]);
            } catch (Exception $e) {
                errorResp($e);
            }
    }


    function change_order_status_by_customer(Request $request)
    {
		$this->input = $request->all();
		//$data = $request->all();
		try {	
			//echo $data['order_id'];exit;
			$order_id = $this->get('order_id');
			//echo "hereeeeeeeeeee";exit;
			$is_order_processed = OrderItems::select('status')
				->where("order_items.order_id", "=", $order_id)
				->where("order_items.status", "=",1)
				->count('status');
				//print_r($is_order_processed);exit;

		   $count_item = OrderItems::select('status')
				->where("order_items.order_id", "=", $order_id)
				//->where("order_items.status", "=",1)
				->count('status');
			if($is_order_processed == $count_item) {
				OrderItems::where('order_id', '=', $order_id)
					->update(['status' => 3 ]);
				Orders::where('id', '=', $order_id)
					->update(['status' =>5]);
				 resp(1, "Order has been cancelled", ['order_status' => []]);
			} else {
				resp(0, "Order is already cancelled or can't cancelled", ['order_status' => []]);
			}
        } catch (Exception $e) {
            errorResp($e);
        }
    }
    
	function getDriverLocation(Request $request) {
       $this->input = $request->all();
        try {
			$driver_id = $this->get('driver_id');
			$order_id = $this->get('order_id');
		  $profile = UsersDetails::query()->select('latitude', 'longitude')->where('user_id', $driver_id)->first();
            resp(1, "Driver current location", ['driver_loc' => $profile]);
        } catch (Exception $e) {
            errorResp($e);
        }
    }
	
    function get($key, $def = NULL)
    {
        if (isset($this->input[$key])) {
            return $this->input[$key];
        } elseif (isset($this->data[$key])) {
            return $this->data[$key];
        } elseif ($def !== NULL) {
            return $def;
        } else {
            return resp(0, "Please provide all required data. Missing : $key");
        }
    }
}
