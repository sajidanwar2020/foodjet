<?php

namespace App\Http\Controllers;

use App\Models\OrderItems;
use App\Models\Orders;
use Cart;
use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Products;
use App\Models\ProductsImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CustomerOrderController extends Controller
{
   function customer_orders() {
       $data['title'] = 'My Orders';
       $data['breadcrums'] = 'My Orders';
       $data['cart_count'] = Cart::count();
       $info = Session::get('is_logged_in');
       //print_r($info);exit;
       $my_orders = Orders::select('orders.id as o_id', 'orders.address as o_address','distance_charges','payment_type','orders.status as o_status','orders.created_at as o_created_at')
           ->join('order_items', 'orders.id', '=', 'order_items.order_id')
           //->join('products', 'products.id', '=', 'order_items.product_id')
           ->where("orders.user_id", "=", $info['user_id'])
           ->groupBy('order_items.order_id')
           ->orderBy('orders.id','DESC')
            ->paginate(15);
       //echo "<pre>";
      // print_r($my_orders);exit;
       if($my_orders) {
           foreach($my_orders as $my_order) {
             //  echo "<pre>";
              // print_r($my_order);
              // exit;
               // Total order price
               $my_order->total_price = OrderItems::select('price','order_items.status')
                   ->where("order_items.order_id", "=", $my_order->o_id)
                   ->sum('price');

               // Is all order status has been chnanged?
               $my_order->is_order_processed = OrderItems::select('status')
                   ->where("order_items.order_id", "=", $my_order->o_id)
                   ->where("order_items.status", "=",1)
                   ->count('status');

               $my_order->count_item = OrderItems::select('status')
                   ->where("order_items.order_id", "=", $my_order->o_id)
                   //->where("order_items.status", "=",1)
                   ->count('status');

                // Sum cancelled orders
               $my_order->cancelled_amount = OrderItems::select('status')
                   ->where("order_items.order_id", "=", $my_order->o_id)
                   ->where("order_items.status", "=",3)
                   ->sum('price');
           }
       }


       $data['my_orders'] = $my_orders;
       return view('customer-order/customer-order',$data);
   }
    function order_success() {
        $data['cart_count'] = Cart::count();
        return view('checkout/order-success',$data);
    }

    public function customer_orders_detail(Request $request)
    {
        $data = $request->all();

        if(!isset($data['order_id'])) {
            abort(404,'Page not found.');
        }
        $order_id = $data['order_id'];
        $data['title'] = 'Order Detail';
        $data['breadcrums'] = 'Order Detail';
        $info = Session::get('is_logged_in');
        $data['cart_count'] = Cart::count();
      //  echo "hereeee";exit;

        $order_item_detail = Orders::select('orders.id as o_id', 'orders.address as o_address','distance_charges', 'orders.phone_number','payment_type',
            'user_details.first_name','user_details.last_name','order_items.quantity','order_items.price','p.product_name','order_items.status as item_status','order_items.id as items_id')
            ->join('order_items', 'orders.id', '=', 'order_items.order_id')
            ->join('products as p', 'p.id', '=', 'order_items.product_id')
            ->join('user_details', 'orders.user_id', '=', 'user_details.user_id')
            ->where("orders.user_id", "=", $info['user_id'])
            ->where("orders.id", "=", $order_id)
            ->get();

        $order_detail_arr = array();
        if($order_item_detail) {
            foreach($order_item_detail as $order_detail) {
                $order_detail_arr['address'] = $order_detail->o_address;
                $order_detail_arr['phone_number'] = $order_detail->phone_number;
                $order_detail_arr['shiping_cost'] = $order_detail->distance_charges;
            }
        }
        $order_item_detail->order_detail = $order_detail_arr;

        $data['order_detail'] = $order_item_detail;
        return view('customer-order/customer-order-detail',$data);
    }


    function change_main_order_status_by_customer(Request $request)
    {
        $data = $request->all();

        $is_order_processed = OrderItems::select('status')
            ->where("order_items.order_id", "=", $data['order_id'])
            ->where("order_items.status", "=",1)
            ->count('status');

       $count_item = OrderItems::select('status')
            ->where("order_items.order_id", "=", $data['order_id'])
            //->where("order_items.status", "=",1)
            ->count('status');
        if($is_order_processed == $count_item) {
            OrderItems::where('order_id', '=', $data['order_id'])
                ->update(['status' => 3 ]);
            Orders::where('id', '=', $data['order_id'])
                ->update(['status' =>5]);
            return redirect(route('customer_orders'))->with('success', 'Cancelled');
        }
        return redirect(route('customer_orders'))->with('success', 'Cancelled');
    }
}
