<?php

namespace App\Http\Middleware;

use App\Models\Users;
use Closure;
use Illuminate\Support\Facades\Session;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       // print_r(Session::get('is_logged_in'));exit;
		if(Session::has('is_admin') && !empty(Session::get('is_admin')) && (Session::get('is_admin')['role_id'] == '1')){
           /*
            $user = Users::where(['users.id' => Session::get('is_logged_in')['user_id']])
                    ->join('user_details as details', 'users.id', '=', 'details.user_id')
                    ->first();
			if (!empty($user)) {
                $user = $user->toArray();
                Session::put('is_logged_in', $user);
            }
            return $next($request); */
           //------------------
            return $next($request);
        }
        return redirect(route('admin'));
    }
}
