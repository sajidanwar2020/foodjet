<?php

namespace App\Http\Middleware;

use App\Models\Users;
use Closure;
use Illuminate\Support\Facades\Session;

class IsVendor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       // print_r(Session::get('is_logged_in'));exit;
		if(Session::has('is_seller') && !empty(Session::get('is_seller')) && (Session::get('is_seller')['role_id'] == '3' OR Session::get('is_seller')['role_id'] == '4')){
           /*
            $user = Users::where(['users.id' => Session::get('is_logged_in')['user_id']])
                    ->join('user_details as details', 'users.id', '=', 'details.user_id')
                    ->first();
			if (!empty($user)) {
                $user = $user->toArray();
                Session::put('is_logged_in', $user);
            }
            return $next($request); */
           //------------------
            return $next($request);
        }
        return redirect(route('signin'));
    }
}
