<?php

namespace App\Http\Middleware;

use App\Models\Users;
use App\Models\UsersDetails;
use App\Models\Restaurant;
use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class IsRestaurant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		/*$url = $request->url();
        //dd($url);
        $restaurantCodeArr = explode('/',$url);
        $restaurantCode = $restaurantCodeArr[count($restaurantCodeArr)-2];
        //print($restaurantCode);
		$restaurantGot = Restaurant::select('*')->where("restaurant_code", "=", $restaurantCode)->first();
        //dd($restaurantGot);
        if($restaurantGot){
            Session::put("restaurantDetail",$restaurantGot);
            return $next($request);
        }else{
            abort(404);
        }*/
		//\session(['restaurantDetail' => null]);
		if(!isset($request->id) AND empty($request->id))
			abort(404);
		$restaurantCode =  $request->id;
		//echo "heree";exit;
        $restaurant =  Users::select('*')
					->join('user_details as details', 'users.id', '=', 'details.user_id')
					->join('restaurant', 'users.restaurant_id', '=', 'restaurant.id')
					->where("role_id", "=", 3)
					->where("users.status", "=", 1)
					->where("restaurant_code", "=", $restaurantCode)
					->first();	
		//echo "<pre>";
		//print_r($restaurant );exit;					
		if($restaurant){
            Session::put('restaurantDetail', $restaurant);
			View::share('restaurant', $restaurant);
			return $next($request);
		} 
		abort(404);
    }
}
