<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

///User
Route::post('user/checkPhoneEmail', 'Api\UserApiController@checkPhoneEmail');
Route::post('user/checkPhoneOrEmail', 'Api\UserApiController@checkPhoneOrEmail');
Route::post('user/register', 'Api\UserApiController@register');
Route::post('user/login', 'Api\UserApiController@login');
Route::post('user/signin-driver', 'Api\UserApiController@signin_driver');
Route::post('user/forgotPass', 'Api\UserApiController@forgotPassword');
Route::get('user/splashData', "Api\UserApiController@splashData");
Route::get('user/profile', "Api\UserApiController@getProfile");
Route::post('user/updateProfile', "Api\UserApiController@customerUpdateProfile");
Route::post('user/updatePhone', "Api\UserApiController@updatePhoneNumber");
Route::post('user/updateImage', "Api\UserApiController@updateImage");
Route::post('user/update-driver-location', "Api\UserApiController@update_driver_location");
Route::get('credit/topup', 'Api\CreditApiController@topupCredit');
Route::post('credit/verify_paypal_ipn_mobile', 'Api\CreditApiController@verify_paypal_ipn')->name('verify_paypal_ipn_mobile');
Route::any('credit/paypal_return_mobile', 'Api\CreditApiController@paypal_return')->name('paypal_return_mobile');
Route::any('credit/completed', 'Api\CreditApiController@completed')->name('completed');

// Shop
Route::get('shop/get-product-by-category', 'Api\ShopController@getProductByCategory')->name('get-product-by-category');
Route::get('shop/product-by-category-id', 'Api\ShopController@productByCategoryId')->name('product-by-category-id');
Route::get('shop/search', 'Api\ShopController@search')->name('search');
Route::get('shop/home-slider', 'Api\ShopController@homeSlider')->name('home-slider');


// Checkout
Route::post('checkout/proceed_check_out', 'Api\CheckoutController@proceed_check_out')->name('proceed_check_out');
Route::post('checkout/distance-detail', 'Api\CheckoutController@distance_detail')->name('distance_detail');


// Customer order
Route::get('customer/customer-orders', 'Api\CustomerOrderController@customer_orders')->name('customer_orders');
Route::get('customer/get-driver-location', 'Api\CustomerOrderController@getDriverLocation')->name('getDriverLocation');
Route::get('customer/customer-orders-detail', 'Api\CustomerOrderController@customer_orders_detail')->name('customer_orders_detail');
Route::get('customer/change-order-status-by-customer', 'Api\CustomerOrderController@change_order_status_by_customer')->name('change-order-status-by-customer');

// Driver 
Route::get('driver/get-orders', 'Api\DriversController@get_orders')->name('get_orders');
Route::get('driver/get-orders1', 'Api\DriversController@get_orders1')->name('get_orders1');
Route::get('driver/_orders-detail', 'Api\DriversController@_orders_detail')->name('_orders_detail');
Route::post('driver/accept-item', 'Api\DriversController@accept_item')->name('accept_item');
Route::post('driver/reject-order', 'Api\DriversController@reject_order')->name('reject_order');
Route::post('driver/change-order-status', 'Api\DriversController@change_order_status')->name('change_order_status');