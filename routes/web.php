<?php

/*
|--------------------------------------------------------------------------
| Web Routes sss
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get("add", "CartController@add_to_cart");
//Route::get("showCart", "CartController@showCart");


//echo "hereeeee";exit;

Route::get('/signin', 'UsersController@signin')->name('signin');
Route::post('/signin_submitted', 'UsersController@signin_submitted')->name('signin_submitted');
Route::get('/signin-v', 'UsersController@signin_vendor')->name('signin-v');
Route::post('/signin-v-submitted', 'UsersController@signin_vendor_submitted')->name('signin-v-submitted');
// Common
Route::get('/customer_signup', 'UsersController@customer_signup')->name('customer_signup');
Route::get('/vendor_signup', 'UsersController@vendor_signup')->name('vendor_signup');
Route::post('/customer_submitted', 'UsersController@customer_submitted')->name('customer_submitted');
Route::post('/vendor_submitted', 'UsersController@vendor_submitted')->name('vendor_submitted');
Route::get('/forgot_password', 'UsersController@forgot_password')->name('forgot_password');
Route::get('/reset_password', 'UsersController@reset_password')->name('reset_password');
Route::post('/reset_update_password', 'UsersController@reset_update_password')->name('reset_update_password');
Route::get('/logout', 'UsersController@logout')->name('logout');
Route::get('/', 'SiteController@index1')->name('home');
Route::get('/home', 'SiteController@index1')->name('home');
//Route::get('/about-us', 'SiteController@about_us')->name('about_us');
Route::get('page/{slug}', 'SiteController@page')->name('page');
Route::get('/contact-us', 'SiteController@contact_us')->name('contact_us');
Route::post('/contact_submitted', 'SiteController@contact_submitted')->name('contact_submitted');
Route::get('/faq', 'SiteController@faq')->name('faq');
Route::get('/category', 'SiteController@category')->name('category');
Route::get('/search', 'ShopController@search')->name('search');
// Cart
Route::get('/add_to_cart', 'CartController@add_to_cart')->name('add_to_cart');
Route::post('/add_to_cart', 'CartController@add_to_cart')->name('add_to_cart');
//Route::get('/get_cart_content', 'CartController@get_cart_content')->name('get_cart_content');
Route::get('/show_cart', 'CartController@show_cart')->name('show_cart');
Route::post('/update_cart', 'CartController@update_cart')->name('update_cart');
Route::post('/remove_cart', 'CartController@remove_cart')->name('remove_cart');
Route::get('/clear_shoping_cart', 'CartController@clear_shoping_cart')->name('clear_shoping_cart');

// Product
Route::get('/shop', 'ShopController@shop')->name('shop');
Route::get('/product_detail', 'ShopController@product_detail')->name('product_detail');

//Wishlist
Route::get('/update_wish_list', 'UsersController@update_wish_list')->name('update_wish_list');

Route::post('/subscribe', 'UsersController@subscribe')->name('subscribe');

Route::group(['middleware' => ['IsCustomer']], function () {
    // Common -> user
    Route::post('/update_password', 'UsersController@update_password')->name('update_password');
    Route::get('/change_password', 'UsersController@change_password')->name('change_password');
    Route::get('/profile', 'UsersController@profile')->name('profile');
    Route::post('/profile_update', 'UsersController@profile_update')->name('profile_update');

    // checkout
    Route::get('/check-out', 'CheckOutController@check_out')->name('check_out');
    Route::post('/proceed-check-out', 'CheckOutController@proceed_check_out')->name('proceed_check_out');
    Route::get('/order-success', 'CustomerOrderController@order_success')->name('order_success');
    Route::get('/distance-detail', 'CheckOutController@distance_detail')->name('distance-detail');

    //Order
    Route::get('/customer-orders', 'CustomerOrderController@customer_orders')->name('customer_orders');
    Route::get('/customer-orders-detail', 'CustomerOrderController@customer_orders_detail')->name('customer_orders_detail');
    Route::get('/change_main_order_status_by_customer', 'CustomerOrderController@change_main_order_status_by_customer')->name('change_main_order_status_by_customer');

    // Paypal
    Route::get('/credits', 'CreditController@index')->name('credits');
    Route::get('/credits_success', 'CreditController@credits_success')->name('credits_success');
    Route::any('/paypal_return', 'CreditController@paypal_return')->name('paypal_return');

    // Wish List
    Route::get('/wish_list', 'UsersController@wish_list')->name('wish_list');
    Route::post('/add_to_cart_remove_wish', 'UsersController@add_to_cart_remove_wish')->name('add_to_cart_remove_wish');
   // Route::get('/remove_wish_list', 'UsersController@remove_wish_list')->name('remove_wish_list');
});


Route::group(['middleware' => ['IsVendor']], function () {
    Route::get('/vendor_dashboard', 'vendor\VendorUserController@vendor_dashboard')->name('vendor_dashboard');
    // Products
    Route::get('/add-product', 'vendor\ProductsController@addProduct')->name('add-product');
    Route::post('/product-submitted', 'vendor\ProductsController@productSubmitted')->name('productSubmitted');
    Route::post('/edit-product-submitted', 'vendor\ProductsController@editProductSubmitted')->name('editProductSubmitted');
    Route::get('/my-products', 'vendor\ProductsController@MyProducts')->name('my-products');
    Route::get('/edit_product', 'vendor\ProductsController@edit_product')->name('edit_product');
    Route::get('/change_offer_status', 'vendor\ProductsController@change_offer_status')->name('change_offer_status');
    Route::get('/child_category', 'vendor\ProductsController@child_category')->name('child_category');
	Route::get('/vendor-import-product', 'vendor\ProductsController@vendorImportProduct')->name('vendor-import-product');
	Route::post('/vendor-import-product-submitted', 'vendor\ProductsController@vendorImportProductSubmitted')->name('vendor-import-product-submitted');

    // orders
    Route::get('/my-orders', 'vendor\MyOrdersController@my_orders')->name('my_orders');
    Route::get('/vendor-order-detail', 'vendor\MyOrdersController@vendor_order_detail')->name('vendor_order_detail');
    Route::get('/change_order_status', 'vendor\MyOrdersController@change_order_status')->name('change_order_status');
    Route::get('/change_main_order_status', 'vendor\MyOrdersController@change_main_order_status')->name('change_main_order_status');
    Route::get('/vendor_change_password', 'UsersController@vendor_change_password')->name('vendor_change_password');

    // Driver
   // Route::get('/driver_dashboard', 'vendor\DriversController@driver_dashboard')->name('driver_dashboard');
    Route::get('/add_driver', 'vendor\DriversController@add_driver')->name('add_driver');
    Route::post('/driver_submitted', 'vendor\DriversController@driver_submitted')->name('driver_submitted');
    Route::get('/edit_driver', 'vendor\DriversController@edit_driver')->name('edit_driver');
    Route::post('/edit_driver_submitted', 'vendor\DriversController@edit_driver_submitted')->name('edit_driver_submitted');
    Route::get('/driver_listing', 'vendor\DriversController@driver_listing')->name('driver_listing');
    Route::get('/change_driver_status', 'vendor\DriversController@change_driver_status')->name('change_driver_status');
    Route::get('/driver_orders', 'vendor\MyOrdersController@driver_orders')->name('driver_orders');
    Route::get('/driver_order_detail', 'vendor\MyOrdersController@driver_order_detail')->name('driver_order_detail');

});


Route::get('/admin', 'admin\AdminController@admin')->name('admin');
Route::post('/admin_submitted', 'admin\AdminController@admin_submitted')->name('admin_submitted');
Route::get('/admin_logout', 'admin\AdminController@admin_logout')->name('admin_logout');
Route::group(['middleware' => ['IsAdmin']], function () {
    Route::get('/admin-dashboard', 'admin\DashboardController@admin_dashboard')->name('admin-dashboard');

    // Category
    Route::get('/category-listing', 'admin\CategoryController@categoryListing')->name('categoryListing');
    Route::get('/add-category', 'admin\CategoryController@add_category')->name('add-category');
    Route::get('/edit_category', 'admin\CategoryController@edit_category')->name('edit_category');
    Route::post('/categorySubmitted', 'admin\CategoryController@categorySubmitted')->name('categorySubmitted');
    Route::post('/editCategorySubmitted', 'admin\CategoryController@editCategorySubmitted')->name('editCategorySubmitted');
    Route::get('/change_category_status', 'admin\CategoryController@change_category_status')->name('change_category_status');

    //Admin product

    // Products
    Route::get('/admin-add-product', 'admin\ProductsController@adminAddProduct')->name('admin-add-product');
    Route::post('/admin-product-submitted', 'admin\ProductsController@adminProductSubmitted')->name('adminProductSubmitted');
    Route::post('/admin-edit-product-submitted', 'admin\ProductsController@adminEditProductSubmitted')->name('adminEditProductSubmitted');
    Route::get('/products_listing', 'admin\ProductsController@products_listing')->name('products_listing');
    Route::get('/admin_edit_product', 'admin\ProductsController@admin_edit_product')->name('admin_edit_product');
    Route::get('/admin_change_product_status', 'admin\ProductsController@admin_change_product_status')->name('admin_change_product_status');

    // Manage customer
    Route::get('/add-customer', 'admin\AdminController@add_customer')->name('add-customer');
    Route::post('/add-customer-submitted', 'admin\AdminController@add_customer_submitted')->name('add-customer-submitted');
    Route::post('/admin-edit-product-submitted', 'admin\AdminController@adminEditProductSubmitted')->name('adminEditProductSubmitted');
    Route::get('/customer-listing', 'admin\AdminController@customer_listing')->name('customer-listing');
    Route::get('/edit-customer', 'admin\AdminController@edit_customer')->name('edit-customer');
    Route::get('/change-customer-status', 'admin\AdminController@change_customer_status')->name('change-customer-status');

    // Vendor listing

    Route::get('/vendor-listing', 'admin\AdminController@vendor_listing')->name('vendor-listing');

    // Orders for vendor

    Route::get('/order-listing', 'admin\OrdersController@order_listing')->name('order-listing');
    Route::get('/order-detail', 'admin\OrdersController@order_detail')->name('order-detail');

    // Home page slider
    Route::get('/slide-listing', 'admin\AdminController@slide_listing')->name('slide-listing');
    Route::get('/add-slide', 'admin\AdminController@add_listing')->name('add-slide');
    Route::get('/edit-slide', 'admin\AdminController@edit_slide')->name('edit-slide');
    Route::post('/add-slide-submitted', 'admin\AdminController@add_slide_submitted')->name('add-slide-submitted');
    Route::post('/edit-slide-submitted', 'admin\AdminController@editSlideSubmitted')->name('edit-slide-submitted');
    Route::get('/change-slide-status', 'admin\AdminController@change_slide_status')->name('change-slide-status');

    // Pages
    Route::get('/pages-listing', 'admin\PagesController@pages_listing')->name('pages-listing');
    Route::get('/add-page', 'admin\PagesController@add_page')->name('add-page');
    Route::get('/edit-page', 'admin\PagesController@edit_page')->name('edit-page');
    Route::post('/add-page-submitted', 'admin\PagesController@add_page_submitted')->name('add-page-submitted');
    Route::post('/edit-page-submitted', 'admin\PagesController@editPageSubmitted')->name('edit-page-submitted');
    Route::get('/change-page-status', 'admin\PagesController@change_page_status')->name('change-page-status');

    // Faq
    Route::get('/faq-listing', 'admin\PagesController@faq_listing')->name('faq-listing');
    Route::get('/add-faq', 'admin\PagesController@add_faq')->name('add-faq');
    Route::get('/edit-faq', 'admin\PagesController@edit_faq')->name('edit-faq');
    Route::post('/add-faq-submitted', 'admin\PagesController@add_faq_submitted')->name('add-faq-submitted');
    Route::post('/edit-faq-submitted', 'admin\PagesController@editFaqSubmitted')->name('edit-faq-submitted');
    Route::get('/change-faq-status', 'admin\PagesController@change_faq_status')->name('change-faq-status');
    // minimum_order
    Route::get('/minimum-order-listing', 'admin\OrdersController@minimum_order_listing')->name('minimum-order-listing');
    Route::get('/add-minimum-order', 'admin\OrdersController@add_minimum_order')->name('add-minimum-order');
    Route::get('/edit-minimum-order', 'admin\OrdersController@edit_minimum_order')->name('edit-minimum-order');
    Route::post('/add-minimum-order-submitted', 'admin\OrdersController@add_minimum_order_submitted')->name('add-minimum-order-submitted');
    Route::post('/edit-minimum-order-submitted', 'admin\OrdersController@edit_minimum_order_submitted')->name('edit-minimum-order-submitted');
    Route::get('/change-minimum-order-status', 'admin\OrdersController@change_minimum_order_status')->name('change-minimum-order-status');

    // Promotions
    Route::get('/promotions-listing', 'admin\PromotionsController@promotions_listing')->name('promotions-listing');
    Route::get('/add-promotions', 'admin\PromotionsController@add_promotions')->name('add-promotions');
    Route::get('/edit-promotions', 'admin\PromotionsController@edit_promotions')->name('edit-promotions');
    Route::post('/add-promotions-submitted', 'admin\PromotionsController@add_promotions_submitted')->name('add-promotions-submitted');
    Route::post('/edit-promotions-submitted', 'admin\PromotionsController@edit_promotions_submitted')->name('edit-promotions-submitted');
    Route::get('/change-promotions-status', 'admin\PromotionsController@change_promotions_status')->name('change-promotions-status');


});


Route::post('/verify_paypal_ipn', 'CreditController@verify_paypal_ipn')->name('verify_paypal_ipn');
